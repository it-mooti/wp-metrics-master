<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '1acd33fcc01b5e3905eb20c776c15baf415d7bfc',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '1acd33fcc01b5e3905eb20c776c15baf415d7bfc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ontraport/sdk-php' => array(
            'pretty_version' => 'v1.2.2',
            'version' => '1.2.2.0',
            'reference' => 'ded60fee67c29f4cfd6a9f654678ce763197731a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ontraport/sdk-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
