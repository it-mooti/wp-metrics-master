"use strict";
(self["webpackChunkRecord_Component"] = self["webpackChunkRecord_Component"] || []).push([["main"],{

/***/ 158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 3184);



const routes = [];
class AppRoutingModule {
}
AppRoutingModule.ɵfac = function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); };
AppRoutingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule.forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule] }); })();


/***/ }),

/***/ 5041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ 8507);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ 6312);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _services_app_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./services/app.service */ 6475);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ 318);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var ng2_tooltip_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-tooltip-directive */ 7762);
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-mask */ 7038);
/* harmony import */ var ngx_autosize_input__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-autosize-input */ 4793);













const _c0 = ["app"];
const _c1 = ["settingContainerAdmin"];
const _c2 = ["settingContainerPublic"];
const _c3 = ["metricsLoop"];
function AppComponent_span_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "span", 15);
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", ctx_r0.loginMessage, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);
} }
function AppComponent_div_1_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_1_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r4);
} }
const _c4 = function (a0) { return { "active": a0 }; };
function AppComponent_div_2_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 28)(1, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_2_div_6_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r33); const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r32.toggleGoals(0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Actuals ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_2_div_6_Template_button_click_3_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r33); const ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r34.toggleGoals(1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Goals ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](4, _c4, !ctx_r27.goalScreenActive));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](6, _c4, ctx_r27.goalScreenActive));
} }
function AppComponent_div_2_div_10_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r37.metrics[ctx_r37.getMetricIndex(metric_r35)].title);
} }
function AppComponent_div_2_div_10_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r44 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_2_div_10_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r44); const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return (ctx_r43.metricValues["m" + metric_r35 + "_title"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("placeholder", ctx_r38.metrics[ctx_r38.getMetricIndex(metric_r35)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r38.isAuthenticated)("ngModel", ctx_r38.metricValues["m" + metric_r35 + "_title"]);
} }
function AppComponent_div_2_div_10_span_5_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_2_div_10_span_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_2_div_10_span_5_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r16);
} }
function AppComponent_div_2_div_10_div_6_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "(Gaol)");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_div_2_div_10_div_6_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "em", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r49 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("(", ctx_r49.metrics[ctx_r49.getMetricIndex(metric_r35)].goalTitle, ") ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r49.metrics[ctx_r49.getMetricIndex(metric_r35)].goalDescription);
} }
function AppComponent_div_2_div_10_div_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_2_div_10_div_6_span_1_Template, 2, 0, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_2_div_10_div_6_span_2_Template, 4, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r40.metrics[ctx_r40.getMetricIndex(metric_r35)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r40.metrics[ctx_r40.getMetricIndex(metric_r35)].hasCustomGoal);
} }
function AppComponent_div_2_div_10_div_8_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 57)(1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r60 = ctx.$implicit;
    const i_r61 = ctx.index;
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r52 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("metric-icon ", metricType_r60.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r52.goalScreenActive && ctx_r52.metrics[ctx_r52.getMetricIndex(metric_r35)].hasCustomGoal ? ctx_r52.metrics[ctx_r52.getMetricIndex(metric_r35)].goalTitle : ctx_r52.metrics[ctx_r52.getMetricIndex(metric_r35)].editable ? ctx_r52.metricValues["m" + metric_r35 + "_title"] : ctx_r52.metrics[ctx_r52.getMetricIndex(metric_r35)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", (!ctx_r52.goalScreenActive || !ctx_r52.metrics[ctx_r52.getMetricIndex(metric_r35)].hasCustomGoal) && ctx_r52.metrics[ctx_r52.getMetricIndex(metric_r35)].type != i_r61 || ctx_r52.goalScreenActive && ctx_r52.metrics[ctx_r52.getMetricIndex(metric_r35)].hasCustomGoal && ctx_r52.metrics[ctx_r52.getMetricIndex(metric_r35)].goalType != i_r61);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r60.symbol);
} }
function AppComponent_div_2_div_10_div_8_input_3_Template(rf, ctx) { if (rf & 1) {
    const _r64 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_2_div_10_div_8_input_3_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r64); const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return (ctx_r63.metricValues["m" + metric_r35 + "_t" + ctx_r63.terms[ctx_r63.selectedTermIndex].period + "_p" + ctx_r63.period.dateString + (ctx_r63.goalScreenActive ? "_goal" : "_actual")] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r53.metrics[ctx_r53.getMetricIndex(metric_r35)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r53.isPublic || !ctx_r53.isAuthenticated)("disabled", ctx_r53.period.date > ctx_r53.today && !ctx_r53.goalScreenActive)("ngModel", ctx_r53.metricValues["m" + metric_r35 + "_t" + ctx_r53.terms[ctx_r53.selectedTermIndex].period + "_p" + ctx_r53.period.dateString + (ctx_r53.goalScreenActive ? "_goal" : "_actual")]);
} }
function AppComponent_div_2_div_10_div_8_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r68 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_2_div_10_div_8_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r68); const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r67 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return (ctx_r67.metricValues["m" + metric_r35 + "_t" + ctx_r67.terms[ctx_r67.selectedTermIndex].period + "_p" + ctx_r67.period.dateString + (ctx_r67.goalScreenActive ? "_goal" : "_actual")] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r54.metrics[ctx_r54.getMetricIndex(metric_r35)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r54.isPublic || !ctx_r54.isAuthenticated)("disabled", ctx_r54.period.date > ctx_r54.today && !ctx_r54.goalScreenActive)("ngModel", ctx_r54.metricValues["m" + metric_r35 + "_t" + ctx_r54.terms[ctx_r54.selectedTermIndex].period + "_p" + ctx_r54.period.dateString + (ctx_r54.goalScreenActive ? "_goal" : "_actual")]);
} }
function AppComponent_div_2_div_10_div_8_input_5_Template(rf, ctx) { if (rf & 1) {
    const _r72 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_2_div_10_div_8_input_5_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r72); const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r71 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return (ctx_r71.metricValues["m" + metric_r35 + "_t" + ctx_r71.terms[ctx_r71.selectedTermIndex].period + "_p" + ctx_r71.period.dateString + (ctx_r71.goalScreenActive ? "_goal" : "_actual")] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r55 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r55.isPublic || !ctx_r55.isAuthenticated)("disabled", ctx_r55.period.date > ctx_r55.today && !ctx_r55.goalScreenActive)("ngModel", ctx_r55.metricValues["m" + metric_r35 + "_t" + ctx_r55.terms[ctx_r55.selectedTermIndex].period + "_p" + ctx_r55.period.dateString + (ctx_r55.goalScreenActive ? "_goal" : "_actual")]);
} }
function AppComponent_div_2_div_10_div_8_input_6_Template(rf, ctx) { if (rf & 1) {
    const _r76 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_2_div_10_div_8_input_6_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r76); const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r75 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return (ctx_r75.metricValues["m" + metric_r35 + "_t" + ctx_r75.terms[ctx_r75.selectedTermIndex].period + "_p" + ctx_r75.period.dateString + (ctx_r75.goalScreenActive ? "_goal" : "_actual")] = $event); })("click", function AppComponent_div_2_div_10_div_8_input_6_Template_input_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r76); const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r78 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r78.toggleProgressBar("m" + metric_r35 + "_t" + ctx_r78.terms[ctx_r78.selectedTermIndex].period + "_p" + ctx_r78.period.dateString + (ctx_r78.goalScreenActive ? "_goal" : "_actual")); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r56 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r56.period.date > ctx_r56.today && !ctx_r56.goalScreenActive)("ngModel", ctx_r56.metricValues["m" + metric_r35 + "_t" + ctx_r56.terms[ctx_r56.selectedTermIndex].period + "_p" + ctx_r56.period.dateString + (ctx_r56.goalScreenActive ? "_goal" : "_actual")]);
} }
function AppComponent_div_2_div_10_div_8_select_7_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const opt_r82 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](opt_r82);
} }
function AppComponent_div_2_div_10_div_8_select_7_Template(rf, ctx) { if (rf & 1) {
    const _r84 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_2_div_10_div_8_select_7_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r84); const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r83 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return (ctx_r83.metricValues["m" + metric_r35 + "_t" + ctx_r83.terms[ctx_r83.selectedTermIndex].period + "_p" + ctx_r83.period.dateString + (ctx_r83.goalScreenActive ? "_goal" : "_actual")] = $event); })("click", function AppComponent_div_2_div_10_div_8_select_7_Template_select_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r84); const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r86 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r86.toggleProgressBar("m" + metric_r35 + "_t" + ctx_r86.terms[ctx_r86.selectedTermIndex].period + "_p" + ctx_r86.period.dateString + (ctx_r86.goalScreenActive ? "_goal" : "_actual")); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_2_div_10_div_8_select_7_option_1_Template, 2, 1, "option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r57.period.date > ctx_r57.today && !ctx_r57.goalScreenActive)("ngModel", ctx_r57.metricValues["m" + metric_r35 + "_t" + ctx_r57.terms[ctx_r57.selectedTermIndex].period + "_p" + ctx_r57.period.dateString + (ctx_r57.goalScreenActive ? "_goal" : "_actual")]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r57.convertToOptions(ctx_r57.metrics[ctx_r57.getMetricIndex(metric_r35)].options));
} }
function AppComponent_div_2_div_10_div_8_span_14_Template(rf, ctx) { if (rf & 1) {
    const _r91 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_2_div_10_div_8_span_14_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r91); const pr_r89 = restoredCtx.$implicit; const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r90 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return (ctx_r90.metricValues["m" + metric_r35 + "_t" + ctx_r90.terms[ctx_r90.selectedTermIndex].period + "_p" + ctx_r90.period.dateString + (ctx_r90.goalScreenActive ? "_goal" : "_actual")] = pr_r89); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_div_2_div_10_div_8_span_16_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_2_div_10_div_8_span_16_Template(rf, ctx) { if (rf & 1) {
    const _r96 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_2_div_10_div_8_span_16_Template_span_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r96); const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r94 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r94.setShowCopyPopup(metric_r35, ctx_r94.currentPeriodIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_2_div_10_div_8_span_16_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    const _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r18);
} }
const _c5 = function (a0) { return { "disabled": a0 }; };
const _c6 = function () { return [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]; };
function AppComponent_div_2_div_10_div_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 42)(1, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_2_div_10_div_8_div_2_Template, 3, 6, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_2_div_10_div_8_input_3_Template, 1, 4, "input", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_2_div_10_div_8_input_4_Template, 1, 4, "input", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_2_div_10_div_8_input_5_Template, 1, 3, "input", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_2_div_10_div_8_input_6_Template, 1, 2, "input", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AppComponent_div_2_div_10_div_8_select_7_Template, 2, 3, "select", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 50)(9, "div", 51)(10, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 52)(13, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, AppComponent_div_2_div_10_div_8_span_14_Template, 2, 0, "span", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "div", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, AppComponent_div_2_div_10_div_8_span_16_Template, 2, 1, "span", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](15, _c5, ctx_r41.periods[ctx_r41.currentPeriodIndex].date > ctx_r41.today && !ctx_r41.goalScreenActive));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r41.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (!ctx_r41.goalScreenActive || !ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal) && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].type < 2 || ctx_r41.goalScreenActive && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].goalType < 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (!ctx_r41.goalScreenActive || !ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal) && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].type == 2 || ctx_r41.goalScreenActive && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].goalType == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (!ctx_r41.goalScreenActive || !ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal) && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].type == 3 || ctx_r41.goalScreenActive && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].goalType == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (!ctx_r41.goalScreenActive || !ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal) && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].type == 4 || ctx_r41.goalScreenActive && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].goalType == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (!ctx_r41.goalScreenActive || !ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal) && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].type == 5 || ctx_r41.goalScreenActive && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].goalType == 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", (!ctx_r41.goalScreenActive || !ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal) && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].type != 4 || ctx_r41.goalScreenActive && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].hasCustomGoal && ctx_r41.metrics[ctx_r41.getMetricIndex(metric_r35)].goalType != 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r41.period.date > ctx_r41.today && !ctx_r41.goalScreenActive || !ctx_r41.isAuthenticated || !ctx_r41.isPublic || !ctx_r41.progressBarSelected["m" + metric_r35 + "_t" + ctx_r41.terms[ctx_r41.selectedTermIndex].period + "_p" + ctx_r41.period.dateString + (ctx_r41.goalScreenActive ? "_goal" : "_actual")]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Set Progress: ", ctx_r41.metricValues["m" + metric_r35 + "_t" + ctx_r41.terms[ctx_r41.selectedTermIndex].period + "_p" + ctx_r41.period.dateString + (ctx_r41.goalScreenActive ? "_goal" : "_actual")] ? ctx_r41.metricValues["m" + metric_r35 + "_t" + ctx_r41.terms[ctx_r41.selectedTermIndex].period + "_p" + ctx_r41.period.dateString + (ctx_r41.goalScreenActive ? "_goal" : "_actual")] : 0, "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](17, _c6));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", ctx_r41.metricValues["m" + metric_r35 + "_t" + ctx_r41.terms[ctx_r41.selectedTermIndex].period + "_p" + ctx_r41.period.dateString + (ctx_r41.goalScreenActive ? "_goal" : "_actual")], "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r41.isAuthenticated && ctx_r41.isPublic && ctx_r41.goalScreenActive);
} }
function AppComponent_div_2_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 31)(1, "div", 32)(2, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_2_div_10_span_3_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_2_div_10_input_4_Template, 1, 3, "input", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_2_div_10_span_5_Template, 2, 1, "span", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_2_div_10_div_6_Template, 3, 2, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AppComponent_div_2_div_10_div_8_Template, 17, 18, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r35 = ctx.$implicit;
    const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r28.compactMetrics.length > 0 && ctx_r28.compactMetrics.indexOf(metric_r35) === -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r28.metrics[ctx_r28.getMetricIndex(metric_r35)].editable);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r28.metrics[ctx_r28.getMetricIndex(metric_r35)].editable);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r28.metrics[ctx_r28.getMetricIndex(metric_r35)].editable);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r28.goalScreenActive);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r28.goalScreenActive || ctx_r28.metrics[ctx_r28.getMetricIndex(metric_r35)].hasGoal);
} }
function AppComponent_div_2_ng_container_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_2_ng_container_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_2_div_16_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
const _c7 = function (a0) { return { "processing": a0 }; };
function AppComponent_div_2_div_16_Template(rf, ctx) { if (rf & 1) {
    const _r100 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 65)(1, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_2_div_16_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r100); const ctx_r99 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r99.saveValues(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_2_div_16_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Save");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](3, _c7, ctx_r31.processing));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r4);
} }
function AppComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r102 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 17)(1, "div", 18)(2, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](4, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](5, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_2_div_6_Template, 5, 8, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 20)(8, "div", 21)(9, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, AppComponent_div_2_div_10_Template, 9, 6, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 24)(12, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_2_Template_div_click_12_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r102); const ctx_r101 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r101.setPreviousPeriodIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, AppComponent_div_2_ng_container_13_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_2_Template_div_click_14_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r102); const ctx_r103 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r103.setNextPeriodIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, AppComponent_div_2_ng_container_15_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, AppComponent_div_2_div_16_Template, 5, 5, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    const _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](23);
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r2.terms[ctx_r2.selectedTermIndex].period === 1 ? "Week " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](4, 10, ctx_r2.periods[ctx_r2.currentPeriodIndex].date, "W (MMM)") : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](5, 13, ctx_r2.periods[ctx_r2.currentPeriodIndex].date, "MMM yyyy"));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r2.hideGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r2.terms[ctx_r2.selectedTermIndex].metrics);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](16, _c5, !ctx_r2.hasPreviousPeriod()));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](18, _c5, !ctx_r2.hasNextPeriod()));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.isAuthenticated);
} }
function AppComponent_div_3_div_2_ng_container_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_2_input_14_Template(rf, ctx) { if (rf & 1) {
    const _r119 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 97);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_2_input_14_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r119); const ctx_r118 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r118.metricValues["m" + ctx_r118.showCopyPopup + "_t" + ctx_r118.terms[ctx_r118.selectedTermIndex].period + "_p" + ctx_r118.periods[ctx_r118.showCopyOffset].dateString + "_goal"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r116 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r116.metricValues["m" + ctx_r116.showCopyPopup + "_t" + ctx_r116.terms[ctx_r116.selectedTermIndex].period + "_p" + ctx_r116.periods[ctx_r116.showCopyOffset].dateString + "_goal"])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_2_input_15_Template(rf, ctx) { if (rf & 1) {
    const _r121 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 98);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_2_input_15_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r121); const ctx_r120 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r120.metricValues["m" + ctx_r120.showCopyPopup + "_t" + ctx_r120.terms[ctx_r120.selectedTermIndex].period + "_p" + ctx_r120.periods[ctx_r120.showCopyOffset].dateString + "_goal"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r117 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r117.metricValues["m" + ctx_r117.showCopyPopup + "_t" + ctx_r117.terms[ctx_r117.selectedTermIndex].period + "_p" + ctx_r117.periods[ctx_r117.showCopyOffset].dateString + "_goal"])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r123 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "div", 81);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 82)(3, "div", 83)(4, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "Apply Formula to future data");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "span", 84)(7, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_2_Template_a_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r123); const ctx_r122 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r122.setShowCopyPopup("", 0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AppComponent_div_3_div_2_ng_container_8_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 85)(10, "div", 86)(11, "label")(12, "input", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_2_Template_input_ngModelChange_12_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r123); const ctx_r124 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r124.copyOption = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, AppComponent_div_3_div_2_input_14_Template, 1, 2, "input", 88);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, AppComponent_div_3_div_2_input_15_Template, 1, 2, "input", 89);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "div", 86)(17, "label")(18, "input", 90);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_2_Template_input_ngModelChange_18_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r123); const ctx_r125 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r125.copyOption = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "input", 91);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_2_Template_input_ngModelChange_20_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r123); const ctx_r126 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r126.copyOptionAmount = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "div", 86)(22, "label")(23, "input", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_2_Template_input_ngModelChange_23_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r123); const ctx_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r127.copyOption = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "input", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_2_Template_input_ngModelChange_25_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r123); const ctx_r128 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r128.copyOptionPercent = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "div", 94)(27, "button", 95);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_2_Template_button_click_27_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r123); const ctx_r129 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r129.applyFormula(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28, "Apply");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "button", 96);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_2_Template_button_click_29_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r123); const ctx_r130 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r130.setShowCopyPopup("", 0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
} if (rf & 2) {
    const ctx_r105 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("top", ctx_r105.currentScrollPosition + 82, "px")("height", 300, "px");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r105.copyOption);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Apply fixed amount to each ", ctx_r105.terms[ctx_r105.selectedTermIndex].period ? "week" : "month", ":");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r105.metrics[ctx_r105.getMetricIndex(ctx_r105.showCopyPopup)].type == 2 || ctx_r105.metrics[ctx_r105.getMetricIndex(ctx_r105.showCopyPopup)].type == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r105.metrics[ctx_r105.getMetricIndex(ctx_r105.showCopyPopup)].type != 2 && ctx_r105.metrics[ctx_r105.getMetricIndex(ctx_r105.showCopyPopup)].type != 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r105.copyOption);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Adjust by amount each ", ctx_r105.terms[ctx_r105.selectedTermIndex].period ? "week" : "month", ":");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r105.copyOptionAmount);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r105.copyOption);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Adjust by percentage each ", ctx_r105.terms[ctx_r105.selectedTermIndex].period ? "week" : "month", ":");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r105.copyOptionPercent);
} }
function AppComponent_div_3_div_6_div_1_input_8_Template(rf, ctx) { if (rf & 1) {
    const _r137 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_6_div_1_input_8_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r137); const meta_r132 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return meta_r132.value = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const meta_r132 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r133 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", meta_r132.value)("readOnly", !ctx_r133.isAuthenticated);
} }
function AppComponent_div_3_div_6_div_1_select_9_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const option_r140 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](option_r140);
} }
function AppComponent_div_3_div_6_div_1_select_9_Template(rf, ctx) { if (rf & 1) {
    const _r143 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 106);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_6_div_1_select_9_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r143); const meta_r132 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return meta_r132.value = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_6_div_1_select_9_option_1_Template, 2, 1, "option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const meta_r132 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r134 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", meta_r132.value)("disabled", !ctx_r134.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", meta_r132.options);
} }
function AppComponent_div_3_div_6_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 101)(1, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 42)(4, "div", 43)(5, "div", 102)(6, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AppComponent_div_3_div_6_div_1_input_8_Template, 1, 2, "input", 103);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, AppComponent_div_3_div_6_div_1_select_9_Template, 2, 3, "select", 104);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} if (rf & 2) {
    const meta_r132 = ctx.$implicit;
    const ctx_r131 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", meta_r132.viewOnly || ctx_r131.userSettingOnMainPage.indexOf(meta_r132.id) === -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](meta_r132.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](meta_r132.type == 0 ? "T" : meta_r132.type == 1 ? "\u2630" : "$");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", meta_r132.type == 0 || meta_r132.type == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", meta_r132.type == 1);
} }
function AppComponent_div_3_div_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 99);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_6_div_1_Template, 10, 5, "div", 100);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r106 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r106.usermeta);
} }
function AppComponent_div_3_div_7_div_12_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_7_div_12_Template(rf, ctx) { if (rf & 1) {
    const _r150 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 117)(1, "div", 118)(2, "div", 119);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_7_div_12_Template_div_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r150); const ctx_r149 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r149.deleteTerm(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_7_div_12_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r8);
} }
function AppComponent_div_3_div_7_div_13_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_7_div_13_Template(rf, ctx) { if (rf & 1) {
    const _r153 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 65)(1, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_7_div_13_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r153); const ctx_r152 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r152.saveSettings(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_7_div_13_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Update");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r147 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](3, _c7, ctx_r147.processing));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r4);
} }
const _c8 = function () { return { standalone: true }; };
function AppComponent_div_3_div_7_Template(rf, ctx) { if (rf & 1) {
    const _r155 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 107, 108)(2, "div", 109)(3, "input", 110);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function AppComponent_div_3_div_7_Template_input_change_3_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r155); const ctx_r154 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); ctx_r154.terms[ctx_r154.selectedTermIndex].startDateUTC = $event.target.valueAsDate; return ctx_r154.setPeriods(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 111)(5, "input", 112);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function AppComponent_div_3_div_7_Template_input_change_5_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r155); const ctx_r156 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); ctx_r156.terms[ctx_r156.selectedTermIndex].endDateUTC = $event.target.valueAsDate; return ctx_r156.setPeriods(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 113)(7, "select", 114);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_7_Template_select_ngModelChange_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r155); const ctx_r157 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r157.terms[ctx_r157.selectedTermIndex].period = $event; })("change", function AppComponent_div_3_div_7_Template_select_change_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r155); const ctx_r158 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r158.setPeriods(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "option", 115);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "Monthly");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "option", 115);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Weekly");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, AppComponent_div_3_div_7_div_12_Template, 4, 1, "div", 116);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, AppComponent_div_3_div_7_div_13_Template, 5, 5, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r107 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("top", ctx_r107.currentScrollPosition, "px");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readOnly", !ctx_r107.isAuthenticated)("valueAsDate", ctx_r107.terms[ctx_r107.selectedTermIndex].startDateUTC);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readOnly", !ctx_r107.isAuthenticated)("valueAsDate", ctx_r107.terms[ctx_r107.selectedTermIndex].endDateUTC);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r107.isAuthenticated)("ngModel", ctx_r107.terms[ctx_r107.selectedTermIndex].period)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](13, _c8));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngValue", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngValue", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r107.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r107.isAuthenticated);
} }
function AppComponent_div_3_div_8_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r166 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 122)(1, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_8_div_2_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r166); const ctx_r165 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r165.toggleGoals(0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Actuals ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_8_div_2_Template_button_click_3_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r166); const ctx_r167 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r167.toggleGoals(1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Goals ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r160 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](4, _c4, !ctx_r160.goalScreenActive));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](6, _c4, ctx_r160.goalScreenActive));
} }
function AppComponent_div_3_div_8_div_3_div_1_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_8_div_3_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r171 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 124)(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_8_div_3_div_1_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r171); const ctx_r170 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r170.ssoLogin(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " View your Scorecard ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_8_div_3_div_1_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r168 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    const _r20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](21);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("ssoprocessing", ctx_r168.ssoProcessing);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r20);
} }
function AppComponent_div_3_div_8_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 122);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_8_div_3_div_1_Template, 4, 3, "div", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r161 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r161.isAuthenticated && ctx_r161.kfSso && !ctx_r161.hideVitalStats && (ctx_r161.theme == "boardroom" || ctx_r161.theme == "outcomes"));
} }
function AppComponent_div_3_div_8_div_4_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_8_div_4_Template(rf, ctx) { if (rf & 1) {
    const _r174 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 117)(1, "div", 125)(2, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_8_div_4_Template_a_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r174); const ctx_r173 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r173.ssoLogin(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_8_div_4_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} if (rf & 2) {
    const ctx_r162 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r20 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](21);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("ssoprocessing", ctx_r162.ssoProcessing);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r20);
} }
function AppComponent_div_3_div_8_div_5_span_3_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_8_div_5_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_8_div_5_span_3_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r10);
} }
function AppComponent_div_3_div_8_div_5_span_4_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_8_div_5_span_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_8_div_5_span_4_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r6);
} }
function AppComponent_div_3_div_8_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r180 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 117)(1, "div", 118)(2, "div", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_8_div_5_Template_div_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r180); const ctx_r179 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r179.toggleSettings(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_8_div_5_span_3_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_8_div_5_span_4_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} if (rf & 2) {
    const ctx_r163 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", !ctx_r163.settingsScreenActive ? "Settings" : "Close");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r163.settingsScreenActive);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r163.settingsScreenActive);
} }
function AppComponent_div_3_div_8_div_6_button_1_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_8_div_6_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r185 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_8_div_6_button_1_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r185); const ctx_r184 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r184.saveValues(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_8_div_6_button_1_ng_container_2_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, " Save");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r181 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](3, _c7, ctx_r181.processing));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r4);
} }
function AppComponent_div_3_div_8_div_6_button_2_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_8_div_6_button_2_Template(rf, ctx) { if (rf & 1) {
    const _r188 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_8_div_6_button_2_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r188); const ctx_r187 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r187.saveUserSettings(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_8_div_6_button_2_ng_container_2_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, " Save Settings");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r182 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](3, _c7, ctx_r182.processing));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r4);
} }
function AppComponent_div_3_div_8_div_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_8_div_6_button_1_Template, 4, 5, "button", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_8_div_6_button_2_Template, 4, 5, "button", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r164 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r164.settingsScreenActive && ctx_r164.theme === "default");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r164.settingsScreenActive);
} }
function AppComponent_div_3_div_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 107, 120);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_8_div_2_Template, 5, 8, "div", 121);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_8_div_3_Template, 2, 1, "div", 121);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_8_div_4_Template, 4, 3, "div", 116);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_8_div_5_Template, 5, 3, "div", 116);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_8_div_6_Template, 3, 2, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r108 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("top", ctx_r108.currentScrollPosition, "px");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r108.settingsScreenActive && !ctx_r108.hideGoal && ctx_r108.theme == "default");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r108.theme == "boardroom" || ctx_r108.theme == "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r108.isAuthenticated && ctx_r108.kfSso && !ctx_r108.hideVitalStats && ctx_r108.theme == "default");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r108.isAuthenticated && !ctx_r108.hideSettings);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r108.isAuthenticated);
} }
function AppComponent_div_3_div_11_li_2_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const term_r191 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](term_r191.title);
} }
function AppComponent_div_3_div_11_li_2_span_4_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_11_li_2_span_4_Template(rf, ctx) { if (rf & 1) {
    const _r198 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 136)(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_11_li_2_span_4_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r198); const ctx_r197 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r197.cloneTerm(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_11_li_2_span_4_ng_container_2_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    const _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r14);
} }
function AppComponent_div_3_div_11_li_2_Template(rf, ctx) { if (rf & 1) {
    const _r200 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li", 132)(1, "a", 133);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_11_li_2_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r200); const term_index_r192 = restoredCtx.index; const ctx_r199 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r199.updateSelectedIndex(term_index_r192); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_11_li_2_span_2_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "input", 134);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_11_li_2_Template_input_ngModelChange_3_listener($event) { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r200); const term_index_r192 = restoredCtx.index; const ctx_r201 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r201.terms[term_index_r192].title = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_11_li_2_span_4_Template, 3, 1, "span", 135);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const term_index_r192 = ctx.index;
    const ctx_r189 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](4, _c4, term_index_r192 == ctx_r189.selectedTermIndex));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r189.isPublic);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r189.terms[term_index_r192].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", term_index_r192 == ctx_r189.selectedTermIndex);
} }
function AppComponent_div_3_div_11_li_3_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_11_li_3_Template(rf, ctx) { if (rf & 1) {
    const _r204 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li", 137)(1, "a", 138);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_11_li_3_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r204); const ctx_r203 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r203.addTerm(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_11_li_3_ng_container_2_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r12);
} }
function AppComponent_div_3_div_11_Template(rf, ctx) { if (rf & 1) {
    const _r206 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 128)(1, "ul", 129);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("cdkDropListDropped", function AppComponent_div_3_div_11_Template_ul_cdkDropListDropped_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r206); const ctx_r205 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r205.dropTerms($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_11_li_2_Template, 5, 6, "li", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_11_li_3_Template, 3, 1, "li", 131);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r109 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r109.terms);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r109.isAuthenticated);
} }
function AppComponent_div_3_div_12_li_2_Template(rf, ctx) { if (rf & 1) {
    const _r211 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li", 140)(1, "a", 133);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_12_li_2_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r211); const term_index_r209 = restoredCtx.index; const ctx_r210 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r210.updateSelectedIndex(term_index_r209); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} if (rf & 2) {
    const term_r208 = ctx.$implicit;
    const term_index_r209 = ctx.index;
    const ctx_r207 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](2, _c4, term_index_r209 == ctx_r207.selectedTermIndex));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](term_r208.title);
} }
function AppComponent_div_3_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 128)(1, "ul");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_12_li_2_Template, 4, 4, "li", 139);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r110 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r110.terms);
} }
function AppComponent_div_3_div_13_div_2_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r240 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 65)(1, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_2_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r240); const ctx_r239 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r239.saveValues(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_2_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Save");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r212 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](3, _c7, ctx_r212.processing));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r4);
} }
function AppComponent_div_3_div_13_h1_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Actuals");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_div_3_div_13_div_4_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_4_ng_container_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_4_Template(rf, ctx) { if (rf & 1) {
    const _r244 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 164)(1, "div", 165)(2, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_4_Template_div_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r244); const ctx_r243 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r243.setPreviousPeriodIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_4_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_4_Template_div_click_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r244); const ctx_r245 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r245.setNextPeriodIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_13_div_4_ng_container_5_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "span", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](8, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](9, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](10, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](11, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](12, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} if (rf & 2) {
    const ctx_r214 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](23);
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](22, _c5, !ctx_r214.hasPreviousPeriod()));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](24, _c5, !ctx_r214.hasNextPeriod()));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r214.terms[ctx_r214.selectedTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](8, 7, ctx_r214.periods[ctx_r214.currentPeriodIndex].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](9, 10, ctx_r214.periods[ctx_r214.currentPeriodIndex].date, "dd") + (ctx_r214.periods[ctx_r214.currentPeriodIndex].date.getMonth() != ctx_r214.periods[ctx_r214.currentPeriodIndex].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](10, 13, ctx_r214.periods[ctx_r214.currentPeriodIndex].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](11, 16, ctx_r214.periods[ctx_r214.currentPeriodIndex].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](12, 19, ctx_r214.periods[ctx_r214.currentPeriodIndex].date, "MMM yyyy"));
} }
function AppComponent_div_3_div_13_span_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](2, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](3, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](4, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](5, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](6, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r215 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](": ", ctx_r215.terms[ctx_r215.selectedTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](2, 1, ctx_r215.periods[ctx_r215.currentPeriodIndex].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](3, 4, ctx_r215.periods[ctx_r215.currentPeriodIndex].date, "dd") + (ctx_r215.periods[ctx_r215.currentPeriodIndex].date.getMonth() != ctx_r215.periods[ctx_r215.currentPeriodIndex].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](4, 7, ctx_r215.periods[ctx_r215.currentPeriodIndex].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](5, 10, ctx_r215.periods[ctx_r215.currentPeriodIndex].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](6, 13, ctx_r215.periods[ctx_r215.currentPeriodIndex].date, "MMM yyyy"), "");
} }
function AppComponent_div_3_div_13_div_13_div_2_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r246 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r250 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r250.metrics[ctx_r250.getMetricIndex(metric_r246)].title);
} }
function AppComponent_div_3_div_13_div_13_div_2_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r246 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r251 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r251.metricValues["m" + metric_r246 + "_title"]);
} }
function AppComponent_div_3_div_13_div_13_div_2_span_3_em_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "em", 173);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r246 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;
    const ctx_r257 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r257.metrics[ctx_r257.getMetricIndex(metric_r246)].goalDescription);
} }
function AppComponent_div_3_div_13_div_13_div_2_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span", 171);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_13_div_2_span_3_em_3_Template, 2, 1, "em", 172);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r246 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r252 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx_r252.metrics[ctx_r252.getMetricIndex(metric_r246)].goalTitle, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r252.metrics[ctx_r252.getMetricIndex(metric_r246)].goalDescription);
} }
function AppComponent_div_3_div_13_div_13_div_2_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r261 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_13_div_2_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r261); const metric_r246 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r260 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r260.metricValues["m" + metric_r246 + "_title"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r246 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r253 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("placeholder", ctx_r253.metrics[ctx_r253.getMetricIndex(metric_r246)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r253.isAuthenticated)("ngModel", ctx_r253.metricValues["m" + metric_r246 + "_title"]);
} }
function AppComponent_div_3_div_13_div_13_div_2_span_5_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_13_div_2_span_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_13_div_2_span_5_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    const _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r16);
} }
const _c9 = function (a0) { return { "greyed": a0 }; };
function AppComponent_div_3_div_13_div_13_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_13_div_2_span_1_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_13_div_2_span_2_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_13_div_2_span_3_Template, 4, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_13_div_13_div_2_input_4_Template, 1, 3, "input", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_13_div_13_div_2_span_5_Template, 2, 1, "span", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r246 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r249 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](9, _c9, ctx_r249.goalScreenActive && !ctx_r249.metrics[ctx_r249.getMetricIndex(metric_r246)].hasGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r249.metrics[ctx_r249.getMetricIndex(metric_r246)].description);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("display", ctx_r249.metrics[ctx_r249.getMetricIndex(metric_r246)].description != "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r249.metrics[ctx_r249.getMetricIndex(metric_r246)].editable && (!ctx_r249.goalScreenActive || !ctx_r249.metrics[ctx_r249.getMetricIndex(metric_r246)].hasCustomGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r249.metrics[ctx_r249.getMetricIndex(metric_r246)].editable && ctx_r249.goalScreenActive && !ctx_r249.metrics[ctx_r249.getMetricIndex(metric_r246)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r249.goalScreenActive && ctx_r249.metrics[ctx_r249.getMetricIndex(metric_r246)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r249.metrics[ctx_r249.getMetricIndex(metric_r246)].editable && !ctx_r249.goalScreenActive);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r249.metrics[ctx_r249.getMetricIndex(metric_r246)].editable && !ctx_r249.goalScreenActive);
} }
function AppComponent_div_3_div_13_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 167, 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_13_div_2_Template, 6, 11, "div", 169);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r246 = ctx.$implicit;
    const ctx_r216 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("cdkDragDisabled", ctx_r216.isPublic);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r216.isPublic && (ctx_r216.theme != "outcomes" || ctx_r216.metrics[ctx_r216.getMetricIndex(metric_r246)].type != 3));
} }
function AppComponent_div_3_div_13_div_17_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_17_Template(rf, ctx) { if (rf & 1) {
    const _r268 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 65)(1, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_17_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r268); const ctx_r267 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r267.saveValues(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_17_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Save");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r217 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](3, _c7, ctx_r217.processing));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r4);
} }
function AppComponent_div_3_div_13_h1_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Actuals");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_div_3_div_13_div_19_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_19_ng_container_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_19_Template(rf, ctx) { if (rf & 1) {
    const _r272 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 164)(1, "div", 165)(2, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_19_Template_div_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r272); const ctx_r271 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r271.setPreviousPeriodIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_19_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_19_Template_div_click_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r272); const ctx_r273 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r273.setNextPeriodIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_13_div_19_ng_container_5_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "span", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](8, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](9, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](10, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](11, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](12, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} if (rf & 2) {
    const ctx_r219 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](23);
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](22, _c5, !ctx_r219.hasPreviousPeriod()));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](24, _c5, !ctx_r219.hasNextPeriod()));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r219.terms[ctx_r219.selectedTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](8, 7, ctx_r219.periods[ctx_r219.currentPeriodIndex].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](9, 10, ctx_r219.periods[ctx_r219.currentPeriodIndex].date, "dd") + (ctx_r219.periods[ctx_r219.currentPeriodIndex].date.getMonth() != ctx_r219.periods[ctx_r219.currentPeriodIndex].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](10, 13, ctx_r219.periods[ctx_r219.currentPeriodIndex].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](11, 16, ctx_r219.periods[ctx_r219.currentPeriodIndex].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](12, 19, ctx_r219.periods[ctx_r219.currentPeriodIndex].date, "MMM yyyy"));
} }
function AppComponent_div_3_div_13_div_23_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_23_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_23_Template(rf, ctx) { if (rf & 1) {
    const _r277 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 165)(1, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_23_Template_div_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r277); const ctx_r276 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r276.setPreviousPeriodIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_23_ng_container_2_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_23_Template_div_click_3_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r277); const ctx_r278 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r278.setNextPeriodIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_13_div_23_ng_container_4_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "span", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](7, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](8, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](9, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](10, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](11, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r220 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](23);
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](22, _c5, !ctx_r220.hasPreviousPeriod()));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](24, _c5, !ctx_r220.hasNextPeriod()));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r220.terms[ctx_r220.selectedTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](7, 7, ctx_r220.periods[ctx_r220.currentPeriodIndex].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](8, 10, ctx_r220.periods[ctx_r220.currentPeriodIndex].date, "dd") + (ctx_r220.periods[ctx_r220.currentPeriodIndex].date.getMonth() != ctx_r220.periods[ctx_r220.currentPeriodIndex].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](9, 13, ctx_r220.periods[ctx_r220.currentPeriodIndex].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](10, 16, ctx_r220.periods[ctx_r220.currentPeriodIndex].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](11, 19, ctx_r220.periods[ctx_r220.currentPeriodIndex].date, "MMM yyyy"));
} }
function AppComponent_div_3_div_13_div_24_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 174);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Actuals ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_div_3_div_13_div_25_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Actuals ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_div_3_div_13_span_30_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](2, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](3, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](4, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](5, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](6, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r223 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](": ", ctx_r223.terms[ctx_r223.selectedTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](2, 1, ctx_r223.periods[ctx_r223.currentPeriodIndex].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](3, 4, ctx_r223.periods[ctx_r223.currentPeriodIndex].date, "dd") + (ctx_r223.periods[ctx_r223.currentPeriodIndex].date.getMonth() != ctx_r223.periods[ctx_r223.currentPeriodIndex].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](4, 7, ctx_r223.periods[ctx_r223.currentPeriodIndex].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](5, 10, ctx_r223.periods[ctx_r223.currentPeriodIndex].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](6, 13, ctx_r223.periods[ctx_r223.currentPeriodIndex].date, "MMM yyyy"), "");
} }
function AppComponent_div_3_div_13_div_31_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Projected ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_div_3_div_13_div_32_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_32_ng_container_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_32_Template(rf, ctx) { if (rf & 1) {
    const _r282 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 42)(1, "div", 43)(2, "div", 165)(3, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_32_Template_div_click_3_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r282); const ctx_r281 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r281.setPreviousPeriodIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_13_div_32_ng_container_4_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_32_Template_div_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r282); const ctx_r283 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r283.setNextPeriodIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_13_div_32_ng_container_6_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "span", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](9, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](10, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](11, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](12, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](13, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
} if (rf & 2) {
    const ctx_r225 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](23);
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](22, _c5, !ctx_r225.hasPreviousPeriod()));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](24, _c5, !ctx_r225.hasNextPeriod()));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r225.terms[ctx_r225.selectedTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](9, 7, ctx_r225.periods[ctx_r225.currentPeriodIndex].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](10, 10, ctx_r225.periods[ctx_r225.currentPeriodIndex].date, "dd") + (ctx_r225.periods[ctx_r225.currentPeriodIndex].date.getMonth() != ctx_r225.periods[ctx_r225.currentPeriodIndex].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](11, 13, ctx_r225.periods[ctx_r225.currentPeriodIndex].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](12, 16, ctx_r225.periods[ctx_r225.currentPeriodIndex].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](13, 19, ctx_r225.periods[ctx_r225.currentPeriodIndex].date, "MMM yyyy"));
} }
function AppComponent_div_3_div_13_div_33_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Goals ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_div_3_div_13_div_34_div_1_div_1_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;
    const ctx_r291 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r291.metrics[ctx_r291.getMetricIndex(metric_r284)].title);
} }
function AppComponent_div_3_div_13_div_34_div_1_div_1_input_2_Template(rf, ctx) { if (rf & 1) {
    const _r296 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_1_div_1_input_2_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r296); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit; const ctx_r295 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r295.metricValues["m" + metric_r284 + "_title"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;
    const ctx_r292 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("placeholder", ctx_r292.metrics[ctx_r292.getMetricIndex(metric_r284)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r292.isAuthenticated)("ngModel", ctx_r292.metricValues["m" + metric_r284 + "_title"]);
} }
function AppComponent_div_3_div_13_div_34_div_1_div_1_span_3_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_34_div_1_div_1_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_34_div_1_div_1_span_3_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](6);
    const _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r16);
} }
function AppComponent_div_3_div_13_div_34_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_34_div_1_div_1_span_1_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_34_div_1_div_1_input_2_Template, 1, 3, "input", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_34_div_1_div_1_span_3_Template, 2, 1, "span", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r290 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](7, _c9, ctx_r290.goalScreenActive && !ctx_r290.metrics[ctx_r290.getMetricIndex(metric_r284)].hasGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r290.metrics[ctx_r290.getMetricIndex(metric_r284)].description);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("display", ctx_r290.metrics[ctx_r290.getMetricIndex(metric_r284)].description != "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r290.metrics[ctx_r290.getMetricIndex(metric_r284)].editable);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r290.metrics[ctx_r290.getMetricIndex(metric_r284)].editable);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r290.metrics[ctx_r290.getMetricIndex(metric_r284)].editable);
} }
function AppComponent_div_3_div_13_div_34_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 155);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_34_div_1_div_1_Template, 4, 9, "div", 169);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r286 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r286.isPublic);
} }
function AppComponent_div_3_div_13_div_34_div_2_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 57)(1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r308 = ctx.$implicit;
    const i_r309 = ctx.index;
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r301 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("metric-icon ", metricType_r308.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r301.metrics[ctx_r301.getMetricIndex(metric_r284)].editable ? ctx_r301.metricValues["m" + metric_r284 + "_title"] : ctx_r301.metrics[ctx_r301.getMetricIndex(metric_r284)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r301.metrics[ctx_r301.getMetricIndex(metric_r284)].type != i_r309);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r308.symbol);
} }
function AppComponent_div_3_div_13_div_34_div_2_input_3_Template(rf, ctx) { if (rf & 1) {
    const _r312 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 183);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_2_input_3_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r312); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r311 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r311.metricValues["m" + metric_r284 + "_t" + ctx_r311.terms[ctx_r311.selectedTermIndex].period + "_p" + ctx_r311.periods[ctx_r311.currentPeriodIndex].dateString + "_actual"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r302 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r302.metrics[ctx_r302.getMetricIndex(metric_r284)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r302.isAuthenticated)("disabled", ctx_r302.periods[ctx_r302.currentPeriodIndex].date > ctx_r302.today)("ngModel", ctx_r302.metricValues["m" + metric_r284 + "_t" + ctx_r302.terms[ctx_r302.selectedTermIndex].period + "_p" + ctx_r302.periods[ctx_r302.currentPeriodIndex].dateString + "_actual"])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_13_div_34_div_2_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r316 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 184);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_2_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r316); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r315 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r315.metricValues["m" + metric_r284 + "_t" + ctx_r315.terms[ctx_r315.selectedTermIndex].period + "_p" + ctx_r315.periods[ctx_r315.currentPeriodIndex].dateString + "_actual"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r303 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r303.metrics[ctx_r303.getMetricIndex(metric_r284)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r303.isAuthenticated)("disabled", ctx_r303.periods[ctx_r303.currentPeriodIndex].date > ctx_r303.today)("ngModel", ctx_r303.metricValues["m" + metric_r284 + "_t" + ctx_r303.terms[ctx_r303.selectedTermIndex].period + "_p" + ctx_r303.periods[ctx_r303.currentPeriodIndex].dateString + "_actual"])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_13_div_34_div_2_input_5_Template(rf, ctx) { if (rf & 1) {
    const _r320 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_2_input_5_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r320); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r319 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r319.metricValues["m" + metric_r284 + "_t" + ctx_r319.terms[ctx_r319.selectedTermIndex].period + "_p" + ctx_r319.periods[ctx_r319.currentPeriodIndex].dateString + "_actual"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r304 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r304.isAuthenticated)("disabled", ctx_r304.periods[ctx_r304.currentPeriodIndex].date > ctx_r304.today)("ngModel", ctx_r304.metricValues["m" + metric_r284 + "_t" + ctx_r304.terms[ctx_r304.selectedTermIndex].period + "_p" + ctx_r304.periods[ctx_r304.currentPeriodIndex].dateString + "_actual"]);
} }
function AppComponent_div_3_div_13_div_34_div_2_input_6_Template(rf, ctx) { if (rf & 1) {
    const _r324 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_2_input_6_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r324); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r323 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r323.metricValues["m" + metric_r284 + "_t" + ctx_r323.terms[ctx_r323.selectedTermIndex].period + "_p" + ctx_r323.periods[ctx_r323.currentPeriodIndex].dateString + "_actual"] = $event); })("click", function AppComponent_div_3_div_13_div_34_div_2_input_6_Template_input_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r324); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r326 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r326.toggleProgressBar("m" + metric_r284 + "_t" + ctx_r326.terms[ctx_r326.selectedTermIndex].period + "_p" + ctx_r326.periods[ctx_r326.currentPeriodIndex].dateString + "_actual"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r305 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r305.periods[ctx_r305.currentPeriodIndex].date > ctx_r305.today)("ngModel", ctx_r305.metricValues["m" + metric_r284 + "_t" + ctx_r305.terms[ctx_r305.selectedTermIndex].period + "_p" + ctx_r305.periods[ctx_r305.currentPeriodIndex].dateString + "_actual"]);
} }
function AppComponent_div_3_div_13_div_34_div_2_select_7_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const opt_r330 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](opt_r330);
} }
function AppComponent_div_3_div_13_div_34_div_2_select_7_Template(rf, ctx) { if (rf & 1) {
    const _r332 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_2_select_7_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r332); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r331 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r331.metricValues["m" + metric_r284 + "_t" + ctx_r331.terms[ctx_r331.selectedTermIndex].period + "_p" + ctx_r331.periods[ctx_r331.currentPeriodIndex].dateString + "_actual"] = $event); })("click", function AppComponent_div_3_div_13_div_34_div_2_select_7_Template_select_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r332); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r334 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r334.toggleProgressBar("m" + metric_r284 + "_t" + ctx_r334.terms[ctx_r334.selectedTermIndex].period + "_p" + ctx_r334.periods[ctx_r334.currentPeriodIndex].dateString + "_actual"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_34_div_2_select_7_option_1_Template, 2, 1, "option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r306 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r306.periods[ctx_r306.currentPeriodIndex].date > ctx_r306.today)("ngModel", ctx_r306.metricValues["m" + metric_r284 + "_t" + ctx_r306.terms[ctx_r306.selectedTermIndex].period + "_p" + ctx_r306.periods[ctx_r306.currentPeriodIndex].dateString + "_actual"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r306.convertToOptions(ctx_r306.metrics[ctx_r306.getMetricIndex(metric_r284)].options));
} }
function AppComponent_div_3_div_13_div_34_div_2_span_14_Template(rf, ctx) { if (rf & 1) {
    const _r339 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_34_div_2_span_14_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r339); const pr_r337 = restoredCtx.$implicit; const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r338 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r338.metricValues["m" + metric_r284 + "_t" + ctx_r338.terms[ctx_r338.selectedTermIndex].period + "_p" + ctx_r338.periods[ctx_r338.currentPeriodIndex].dateString + "_actual"] = pr_r337); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
const _c10 = function (a0, a1, a2) { return { "missing": a0, "bg-red": a1, "bg-green": a2 }; };
function AppComponent_div_3_div_13_div_34_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 180)(1, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_34_div_2_div_2_Template, 3, 6, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_34_div_2_input_3_Template, 1, 5, "input", 181);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_13_div_34_div_2_input_4_Template, 1, 5, "input", 182);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_13_div_34_div_2_input_5_Template, 1, 3, "input", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_13_div_34_div_2_input_6_Template, 1, 2, "input", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AppComponent_div_3_div_13_div_34_div_2_select_7_Template, 2, 3, "select", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 50)(9, "div", 51)(10, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 52)(13, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, AppComponent_div_3_div_13_div_34_div_2_span_14_Template, 2, 0, "span", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "div", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()()();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r287 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction3"](13, _c10, !ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"], ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type != 5 && ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"] && (!ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].goalAlert && ctx_r287.toNumber(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"]) < ctx_r287.toNumber(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_goal"]) || ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].goalAlert && ctx_r287.toNumber(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"]) > ctx_r287.toNumber(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_goal"])) || ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type == 5 && (!ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].goalAlert && ctx_r287.convertToOptions(ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].options).indexOf(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"]) < ctx_r287.convertToOptions(ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].options).indexOf(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_goal"]) || ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].goalAlert && ctx_r287.convertToOptions(ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].options).indexOf(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"]) > ctx_r287.convertToOptions(ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].options).indexOf(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_goal"])), ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type != 5 && ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"] && (!ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].goalAlert && ctx_r287.toNumber(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"]) >= ctx_r287.toNumber(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_goal"]) || ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].goalAlert && ctx_r287.toNumber(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"]) <= ctx_r287.toNumber(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_goal"])) || ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type == 5 && ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"] && (!ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].goalAlert && ctx_r287.convertToOptions(ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].options).indexOf(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"]) >= ctx_r287.convertToOptions(ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].options).indexOf(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_goal"]) || ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].goalAlert && ctx_r287.convertToOptions(ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].options).indexOf(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"]) <= ctx_r287.convertToOptions(ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].options).indexOf(ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_goal"]))));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r287.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type != 5 && ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type != 4 && ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type != 2 && ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type != 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type == 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r287.metrics[ctx_r287.getMetricIndex(metric_r284)].type != 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r287.periods[ctx_r287.currentPeriodIndex].date > ctx_r287.today || !ctx_r287.isAuthenticated || !ctx_r287.progressBarSelected["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Set Progress: ", ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"] ? ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"] : 0, "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](17, _c6));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", ctx_r287.metricValues["m" + metric_r284 + "_t" + ctx_r287.terms[ctx_r287.selectedTermIndex].period + "_p" + ctx_r287.periods[ctx_r287.currentPeriodIndex].dateString + "_actual"], "%");
} }
function AppComponent_div_3_div_13_div_34_div_3_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 57)(1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r343 = ctx.$implicit;
    const i_r344 = ctx.index;
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r342 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("metric-icon ", metricType_r343.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r342.metrics[ctx_r342.getMetricIndex(metric_r284)].editable ? ctx_r342.metricValues["m" + metric_r284 + "_title"] : ctx_r342.metrics[ctx_r342.getMetricIndex(metric_r284)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r342.metrics[ctx_r342.getMetricIndex(metric_r284)].type != i_r344);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r343.symbol);
} }
function AppComponent_div_3_div_13_div_34_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 185)(1, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_34_div_3_div_2_Template, 3, 6, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "input", 186);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r288 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](4, _c5, ctx_r288.periods[ctx_r288.currentPeriodIndex].date > ctx_r288.today));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r288.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("value", ctx_r288.metricValues["m" + metric_r284 + "_t" + ctx_r288.terms[ctx_r288.selectedTermIndex].period + "_p" + ctx_r288.periods[ctx_r288.currentPeriodIndex].dateString + "_goal"]);
} }
function AppComponent_div_3_div_13_div_34_div_4_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 57)(1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r354 = ctx.$implicit;
    const i_r355 = ctx.index;
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r347 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("metric-icon ", metricType_r354.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r347.metrics[ctx_r347.getMetricIndex(metric_r284)].editable ? ctx_r347.metricValues["m" + metric_r284 + "_title"] : ctx_r347.metrics[ctx_r347.getMetricIndex(metric_r284)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r347.metrics[ctx_r347.getMetricIndex(metric_r284)].type != i_r355);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r354.symbol);
} }
function AppComponent_div_3_div_13_div_34_div_4_input_3_Template(rf, ctx) { if (rf & 1) {
    const _r358 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 193);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_4_input_3_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r358); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r357 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r357.metricValues["m" + metric_r284 + "_t" + ctx_r357.terms[ctx_r357.goalTermIndex].period + "_p" + ctx_r357.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_34_div_4_input_3_Template_input_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r358); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r360 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r360.updateGoals(metric_r284, ctx_r360.terms[ctx_r360.goalTermIndex].period, ctx_r360.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r348 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r348.metrics[ctx_r348.getMetricIndex(metric_r284)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r348.isAuthenticated)("ngModel", ctx_r348.metricValues["m" + metric_r284 + "_t" + ctx_r348.terms[ctx_r348.goalTermIndex].period + "_p" + ctx_r348.goalPeriods[0].dateString + "_goal"])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_13_div_34_div_4_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r364 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 194);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_4_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r364); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r363 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r363.metricValues["m" + metric_r284 + "_t" + ctx_r363.terms[ctx_r363.goalTermIndex].period + "_p" + ctx_r363.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_34_div_4_input_4_Template_input_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r364); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r366 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r366.updateGoals(metric_r284, ctx_r366.terms[ctx_r366.goalTermIndex].period, ctx_r366.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r349 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r349.metrics[ctx_r349.getMetricIndex(metric_r284)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r349.isAuthenticated)("ngModel", ctx_r349.metricValues["m" + metric_r284 + "_t" + ctx_r349.terms[ctx_r349.goalTermIndex].period + "_p" + ctx_r349.goalPeriods[0].dateString + "_goal"])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_13_div_34_div_4_input_5_Template(rf, ctx) { if (rf & 1) {
    const _r370 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 195);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_4_input_5_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r370); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r369 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r369.metricValues["m" + metric_r284 + "_t" + ctx_r369.terms[ctx_r369.goalTermIndex].period + "_p" + ctx_r369.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_34_div_4_input_5_Template_input_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r370); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r372 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r372.updateGoals(metric_r284, ctx_r372.terms[ctx_r372.goalTermIndex].period, ctx_r372.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r350 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r350.isAuthenticated)("ngModel", ctx_r350.metricValues["m" + metric_r284 + "_t" + ctx_r350.terms[ctx_r350.goalTermIndex].period + "_p" + ctx_r350.goalPeriods[0].dateString + "_goal"]);
} }
function AppComponent_div_3_div_13_div_34_div_4_input_6_Template(rf, ctx) { if (rf & 1) {
    const _r376 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 196);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_4_input_6_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r376); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r375 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r375.metricValues["m" + metric_r284 + "_t" + ctx_r375.terms[ctx_r375.goalTermIndex].period + "_p" + ctx_r375.goalPeriods[0].dateString + "_goal"] = $event); })("click", function AppComponent_div_3_div_13_div_34_div_4_input_6_Template_input_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r376); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r378 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r378.toggleProgressBar("m" + metric_r284 + "_t" + ctx_r378.terms[ctx_r378.goalTermIndex].period + "_p" + ctx_r378.goalPeriods[0].dateString + "_goal"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r351 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r351.metricValues["m" + metric_r284 + "_t" + ctx_r351.terms[ctx_r351.goalTermIndex].period + "_p" + ctx_r351.goalPeriods[0].dateString + "_goal"]);
} }
function AppComponent_div_3_div_13_div_34_div_4_select_7_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const opt_r382 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](opt_r382);
} }
function AppComponent_div_3_div_13_div_34_div_4_select_7_Template(rf, ctx) { if (rf & 1) {
    const _r384 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 197);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_34_div_4_select_7_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r384); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r383 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r383.metricValues["m" + metric_r284 + "_t" + ctx_r383.terms[ctx_r383.goalTermIndex].period + "_p" + ctx_r383.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_34_div_4_select_7_Template_select_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r384); const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r386 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r386.updateGoals(metric_r284, ctx_r386.terms[ctx_r386.goalTermIndex].period, ctx_r386.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_34_div_4_select_7_option_1_Template, 2, 1, "option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r352 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r352.metricValues["m" + metric_r284 + "_t" + ctx_r352.terms[ctx_r352.goalTermIndex].period + "_p" + ctx_r352.goalPeriods[0].dateString + "_goal"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r352.convertToOptions(ctx_r352.metrics[ctx_r352.getMetricIndex(metric_r284)].options));
} }
function AppComponent_div_3_div_13_div_34_div_4_span_14_Template(rf, ctx) { if (rf & 1) {
    const _r391 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_34_div_4_span_14_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r391); const pr_r389 = restoredCtx.$implicit; const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r390 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); ctx_r390.metricValues["m" + metric_r284 + "_t" + ctx_r390.terms[ctx_r390.goalTermIndex].period + "_p" + ctx_r390.goalPeriods[0].dateString + "_goal"] = pr_r389; return ctx_r390.updateGoals(metric_r284, ctx_r390.terms[ctx_r390.goalTermIndex].period, ctx_r390.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_div_3_div_13_div_34_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 187)(1, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_34_div_4_div_2_Template, 3, 6, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_34_div_4_input_3_Template, 1, 4, "input", 188);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_13_div_34_div_4_input_4_Template, 1, 4, "input", 189);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_13_div_34_div_4_input_5_Template, 1, 2, "input", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_13_div_34_div_4_input_6_Template, 1, 1, "input", 191);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AppComponent_div_3_div_13_div_34_div_4_select_7_Template, 2, 2, "select", 192);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 50)(9, "div", 51)(10, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 52)(13, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, AppComponent_div_3_div_13_div_34_div_4_span_14_Template, 2, 0, "span", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "div", 55)(16, "div", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()()();
} if (rf & 2) {
    const metric_r284 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r289 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r289.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r289.metrics[ctx_r289.getMetricIndex(metric_r284)].type <= 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r289.metrics[ctx_r289.getMetricIndex(metric_r284)].type == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r289.metrics[ctx_r289.getMetricIndex(metric_r284)].type == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r289.metrics[ctx_r289.getMetricIndex(metric_r284)].type == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r289.metrics[ctx_r289.getMetricIndex(metric_r284)].type == 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r289.metrics[ctx_r289.getMetricIndex(metric_r284)].type != 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r289.isAuthenticated || !ctx_r289.progressBarSelected["m" + metric_r284 + "_t" + ctx_r289.terms[ctx_r289.goalTermIndex].period + "_p" + ctx_r289.goalPeriods[0].dateString + "_goal"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Set Progress: ", ctx_r289.metricValues["m" + metric_r284 + "_t" + ctx_r289.terms[ctx_r289.goalTermIndex].period + "_p" + ctx_r289.goalPeriods[0].dateString + "_goal"] ? ctx_r289.metricValues["m" + metric_r284 + "_t" + ctx_r289.terms[ctx_r289.goalTermIndex].period + "_p" + ctx_r289.goalPeriods[0].dateString + "_goal"] : 0, "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](14, _c6));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", ctx_r289.metricValues["m" + metric_r284 + "_t" + ctx_r289.terms[ctx_r289.goalTermIndex].period + "_p" + ctx_r289.goalPeriods[0].dateString + "_goal"], "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", ctx_r289.metricValues["m" + metric_r284 + "_t" + ctx_r289.terms[ctx_r289.goalTermIndex].period + "_p" + ctx_r289.goalPeriods[0].dateString + "_goal"], "%");
} }
function AppComponent_div_3_div_13_div_34_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_34_div_1_Template, 2, 1, "div", 176);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_34_div_2_Template, 16, 18, "div", 177);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_34_div_3_Template, 4, 6, "div", 178);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_13_div_34_div_4_Template, 17, 15, "div", 179);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r284 = ctx.$implicit;
    const ctx_r227 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](8, _c9, ctx_r227.goalScreenActive && !ctx_r227.metrics[ctx_r227.getMetricIndex(metric_r284)].hasGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", 90 / (ctx_r227.screenWidth > 768 ? ctx_r227.terms[ctx_r227.selectedTermIndex].metrics.length : 1), "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r227.theme != "outcomes" || ctx_r227.metrics[ctx_r227.getMetricIndex(metric_r284)].type != 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r227.theme != "outcomes" || ctx_r227.metrics[ctx_r227.getMetricIndex(metric_r284)].type != 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r227.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r227.theme == "outcomes" && ctx_r227.metrics[ctx_r227.getMetricIndex(metric_r284)].type != 3);
} }
function AppComponent_div_3_div_13_div_36_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 198);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](2, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](3, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](4, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](5, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](6, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](7, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](8, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](9, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](10, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](11, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "Goals ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r228 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"](" ", ctx_r228.terms[ctx_r228.selectedTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](2, 2, ctx_r228.periods[0].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](3, 5, ctx_r228.periods[0].date, "dd") + (ctx_r228.periods[0].date.getMonth() != ctx_r228.periods[0].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](4, 8, ctx_r228.periods[0].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](5, 11, ctx_r228.periods[0].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](6, 14, ctx_r228.periods[0].date, "MMM yyyy"), " - ", ctx_r228.terms[ctx_r228.selectedTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](7, 17, ctx_r228.periods[ctx_r228.periods.length - 1].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](8, 20, ctx_r228.periods[ctx_r228.periods.length - 1].date, "dd") + (ctx_r228.periods[ctx_r228.periods.length - 1].date.getMonth() != ctx_r228.periods[ctx_r228.periods.length - 1].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](9, 23, ctx_r228.periods[ctx_r228.periods.length - 1].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](10, 26, ctx_r228.periods[ctx_r228.periods.length - 1].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](11, 29, ctx_r228.periods[ctx_r228.periods.length - 1].date, "MMM yyyy"), "");
} }
function AppComponent_div_3_div_13_div_37_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 198);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Projected");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_div_3_div_13_div_38_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 57)(1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r399 = ctx.$implicit;
    const i_r400 = ctx.index;
    const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r398 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("metric-icon ", metricType_r399.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r398.metrics[ctx_r398.getMetricIndex(metric_r394)].editable ? ctx_r398.metricValues["m" + metric_r394 + "_title"] : ctx_r398.metrics[ctx_r398.getMetricIndex(metric_r394)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r398.metrics[ctx_r398.getMetricIndex(metric_r394)].type != i_r400);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r399.symbol);
} }
function AppComponent_div_3_div_13_div_38_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 200)(1, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_38_div_1_div_2_Template, 3, 6, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "input", 186);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r396 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r396.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("value", ctx_r396.metricValues["m" + metric_r394 + "_t" + ctx_r396.terms[ctx_r396.selectedTermIndex].period + "_p" + ctx_r396.periods[ctx_r396.currentPeriodIndex].dateString + "_goal"]);
} }
function AppComponent_div_3_div_13_div_38_div_2_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 57)(1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r410 = ctx.$implicit;
    const i_r411 = ctx.index;
    const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r403 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("metric-icon ", metricType_r410.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r403.metrics[ctx_r403.getMetricIndex(metric_r394)].editable ? ctx_r403.metricValues["m" + metric_r394 + "_title"] : ctx_r403.metrics[ctx_r403.getMetricIndex(metric_r394)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r403.metrics[ctx_r403.getMetricIndex(metric_r394)].type != i_r411);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r410.symbol);
} }
function AppComponent_div_3_div_13_div_38_div_2_input_3_Template(rf, ctx) { if (rf & 1) {
    const _r414 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 193);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_38_div_2_input_3_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r414); const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r413 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r413.metricValues["m" + metric_r394 + "_t" + ctx_r413.terms[ctx_r413.goalTermIndex].period + "_p" + ctx_r413.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_38_div_2_input_3_Template_input_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r414); const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r416 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r416.updateGoals(metric_r394, ctx_r416.terms[ctx_r416.goalTermIndex].period, ctx_r416.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r404 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r404.metrics[ctx_r404.getMetricIndex(metric_r394)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r404.isAuthenticated)("ngModel", ctx_r404.metricValues["m" + metric_r394 + "_t" + ctx_r404.terms[ctx_r404.goalTermIndex].period + "_p" + ctx_r404.goalPeriods[0].dateString + "_goal"])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_13_div_38_div_2_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r420 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 194);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_38_div_2_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r420); const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r419 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r419.metricValues["m" + metric_r394 + "_t" + ctx_r419.terms[ctx_r419.goalTermIndex].period + "_p" + ctx_r419.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_38_div_2_input_4_Template_input_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r420); const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r422 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r422.updateGoals(metric_r394, ctx_r422.terms[ctx_r422.goalTermIndex].period, ctx_r422.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r405 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r405.metrics[ctx_r405.getMetricIndex(metric_r394)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r405.isAuthenticated)("ngModel", ctx_r405.metricValues["m" + metric_r394 + "_t" + ctx_r405.terms[ctx_r405.goalTermIndex].period + "_p" + ctx_r405.goalPeriods[0].dateString + "_goal"])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_13_div_38_div_2_input_5_Template(rf, ctx) { if (rf & 1) {
    const _r426 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 195);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_38_div_2_input_5_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r426); const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r425 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r425.metricValues["m" + metric_r394 + "_t" + ctx_r425.terms[ctx_r425.goalTermIndex].period + "_p" + ctx_r425.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_38_div_2_input_5_Template_input_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r426); const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r428 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r428.updateGoals(metric_r394, ctx_r428.terms[ctx_r428.goalTermIndex].period, ctx_r428.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r406 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r406.isAuthenticated)("ngModel", ctx_r406.metricValues["m" + metric_r394 + "_t" + ctx_r406.terms[ctx_r406.goalTermIndex].period + "_p" + ctx_r406.goalPeriods[0].dateString + "_goal"]);
} }
function AppComponent_div_3_div_13_div_38_div_2_input_6_Template(rf, ctx) { if (rf & 1) {
    const _r432 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 196);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_38_div_2_input_6_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r432); const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r431 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r431.metricValues["m" + metric_r394 + "_t" + ctx_r431.terms[ctx_r431.goalTermIndex].period + "_p" + ctx_r431.goalPeriods[0].dateString + "_goal"] = $event); })("click", function AppComponent_div_3_div_13_div_38_div_2_input_6_Template_input_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r432); const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r434 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r434.toggleProgressBar("m" + metric_r394 + "_t" + ctx_r434.terms[ctx_r434.goalTermIndex].period + "_p" + ctx_r434.goalPeriods[0].dateString + "_goal"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r407 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r407.metricValues["m" + metric_r394 + "_t" + ctx_r407.terms[ctx_r407.goalTermIndex].period + "_p" + ctx_r407.goalPeriods[0].dateString + "_goal"]);
} }
function AppComponent_div_3_div_13_div_38_div_2_select_7_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const opt_r438 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](opt_r438);
} }
function AppComponent_div_3_div_13_div_38_div_2_select_7_Template(rf, ctx) { if (rf & 1) {
    const _r440 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 197);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_38_div_2_select_7_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r440); const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r439 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return (ctx_r439.metricValues["m" + metric_r394 + "_t" + ctx_r439.terms[ctx_r439.goalTermIndex].period + "_p" + ctx_r439.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_38_div_2_select_7_Template_select_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r440); const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r442 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r442.updateGoals(metric_r394, ctx_r442.terms[ctx_r442.goalTermIndex].period, ctx_r442.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_38_div_2_select_7_option_1_Template, 2, 1, "option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r408 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r408.metricValues["m" + metric_r394 + "_t" + ctx_r408.terms[ctx_r408.goalTermIndex].period + "_p" + ctx_r408.goalPeriods[0].dateString + "_goal"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r408.convertToOptions(ctx_r408.metrics[ctx_r408.getMetricIndex(metric_r394)].options));
} }
function AppComponent_div_3_div_13_div_38_div_2_span_14_Template(rf, ctx) { if (rf & 1) {
    const _r447 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_38_div_2_span_14_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r447); const pr_r445 = restoredCtx.$implicit; const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r446 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); ctx_r446.metricValues["m" + metric_r394 + "_t" + ctx_r446.terms[ctx_r446.goalTermIndex].period + "_p" + ctx_r446.goalPeriods[0].dateString + "_goal"] = pr_r445; return ctx_r446.updateGoals(metric_r394, ctx_r446.terms[ctx_r446.goalTermIndex].period, ctx_r446.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_div_3_div_13_div_38_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 42)(1, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_38_div_2_div_2_Template, 3, 6, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_38_div_2_input_3_Template, 1, 4, "input", 188);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_13_div_38_div_2_input_4_Template, 1, 4, "input", 189);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_13_div_38_div_2_input_5_Template, 1, 2, "input", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_13_div_38_div_2_input_6_Template, 1, 1, "input", 191);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AppComponent_div_3_div_13_div_38_div_2_select_7_Template, 2, 2, "select", 192);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 50)(9, "div", 51)(10, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 52)(13, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, AppComponent_div_3_div_13_div_38_div_2_span_14_Template, 2, 0, "span", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "div", 55)(16, "div", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()()();
} if (rf & 2) {
    const metric_r394 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r397 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r397.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r397.metrics[ctx_r397.getMetricIndex(metric_r394)].type <= 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r397.metrics[ctx_r397.getMetricIndex(metric_r394)].type == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r397.metrics[ctx_r397.getMetricIndex(metric_r394)].type == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r397.metrics[ctx_r397.getMetricIndex(metric_r394)].type == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r397.metrics[ctx_r397.getMetricIndex(metric_r394)].type == 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r397.metrics[ctx_r397.getMetricIndex(metric_r394)].type != 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r397.isAuthenticated || !ctx_r397.progressBarSelected["m" + metric_r394 + "_t" + ctx_r397.terms[ctx_r397.goalTermIndex].period + "_p" + ctx_r397.goalPeriods[0].dateString + "_goal"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Set Progress: ", ctx_r397.metricValues["m" + metric_r394 + "_t" + ctx_r397.terms[ctx_r397.goalTermIndex].period + "_p" + ctx_r397.goalPeriods[0].dateString + "_goal"] ? ctx_r397.metricValues["m" + metric_r394 + "_t" + ctx_r397.terms[ctx_r397.goalTermIndex].period + "_p" + ctx_r397.goalPeriods[0].dateString + "_goal"] : 0, "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](14, _c6));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", ctx_r397.metricValues["m" + metric_r394 + "_t" + ctx_r397.terms[ctx_r397.goalTermIndex].period + "_p" + ctx_r397.goalPeriods[0].dateString + "_goal"], "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", ctx_r397.metricValues["m" + metric_r394 + "_t" + ctx_r397.terms[ctx_r397.goalTermIndex].period + "_p" + ctx_r397.goalPeriods[0].dateString + "_goal"], "%");
} }
function AppComponent_div_3_div_13_div_38_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_38_div_1_Template, 4, 2, "div", 199);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_38_div_2_Template, 17, 15, "div", 156);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r394 = ctx.$implicit;
    const ctx_r230 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](6, _c9, !ctx_r230.metrics[ctx_r230.getMetricIndex(metric_r394)].hasGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", 90 / (ctx_r230.screenWidth > 768 ? ctx_r230.terms[ctx_r230.goalTermIndex].metrics.length : 1), "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r230.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r230.theme == "outcomes" && ctx_r230.metrics[ctx_r230.getMetricIndex(metric_r394)].type != 3);
} }
function AppComponent_div_3_div_13_div_39_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r455 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div")(1, "label")(2, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "textarea", 203);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_39_div_1_div_1_Template_textarea_ngModelChange_5_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r455); const metric_r451 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r454 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r454.metricValues["m" + metric_r451 + "_t" + ctx_r454.terms[ctx_r454.selectedTermIndex].period + "_p" + ctx_r454.periods[ctx_r454.currentPeriodIndex].dateString + "_actual"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r451 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r453 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r453.metrics[ctx_r453.getMetricIndex(metric_r451)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](": ", ctx_r453.metrics[ctx_r453.getMetricIndex(metric_r451)].description, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r453.metricValues["m" + metric_r451 + "_t" + ctx_r453.terms[ctx_r453.selectedTermIndex].period + "_p" + ctx_r453.periods[ctx_r453.currentPeriodIndex].dateString + "_actual"]);
} }
function AppComponent_div_3_div_13_div_39_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 202);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_39_div_1_div_1_Template, 6, 3, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r451 = ctx.$implicit;
    const ctx_r450 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", 90 / (ctx_r450.screenWidth > 768 ? ctx_r450.terms[ctx_r450.goalTermIndex].metrics.length : 1), "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r450.metrics[ctx_r450.getMetricIndex(metric_r451)].type == 3);
} }
function AppComponent_div_3_div_13_div_39_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_39_div_1_Template, 2, 3, "div", 201);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r231 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r231.terms[ctx_r231.selectedTermIndex].metrics);
} }
function AppComponent_div_3_div_13_div_40_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 204)(1, "strong")(2, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} if (rf & 2) {
    const ctx_r232 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("* Attention: ", ctx_r232.missingValues(), " Metrics Missing");
} }
function AppComponent_div_3_div_13_hr_41_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "hr", 205);
} }
function AppComponent_div_3_div_13_div_42_div_1_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_42_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r461 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 65)(1, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_42_div_1_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r461); const ctx_r460 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r460.saveValues(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_42_div_1_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Save");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r458 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](3, _c7, ctx_r458.processing));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r4);
} }
function AppComponent_div_3_div_13_div_42_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 142);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_42_div_1_Template, 5, 5, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Goals");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r234 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r234.isAuthenticated);
} }
function AppComponent_div_3_div_13_div_43_div_7_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r465 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r468 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r468.metrics[ctx_r468.getMetricIndex(metric_r465)].title);
} }
function AppComponent_div_3_div_13_div_43_div_7_span_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r465 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r469 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r469.metricValues["m" + metric_r465 + "_title"]);
} }
function AppComponent_div_3_div_13_div_43_div_7_span_5_em_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "em", 173);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r465 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r475 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r475.metrics[ctx_r475.getMetricIndex(metric_r465)].goalDescription);
} }
function AppComponent_div_3_div_13_div_43_div_7_span_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span", 171);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_43_div_7_span_5_em_3_Template, 2, 1, "em", 172);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r465 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r470 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx_r470.metrics[ctx_r470.getMetricIndex(metric_r465)].goalTitle, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r470.metrics[ctx_r470.getMetricIndex(metric_r465)].goalDescription);
} }
function AppComponent_div_3_div_13_div_43_div_7_input_6_Template(rf, ctx) { if (rf & 1) {
    const _r479 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_43_div_7_input_6_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r479); const metric_r465 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r478 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r478.metricValues["m" + metric_r465 + "_title"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r465 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r471 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("placeholder", ctx_r471.metrics[ctx_r471.getMetricIndex(metric_r465)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r471.isAuthenticated)("ngModel", ctx_r471.metricValues["m" + metric_r465 + "_title"]);
} }
function AppComponent_div_3_div_13_div_43_div_7_span_7_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_43_div_7_span_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_43_div_7_span_7_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    const _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r16);
} }
function AppComponent_div_3_div_13_div_43_div_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 167, 168)(2, "div", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_43_div_7_span_3_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_13_div_43_div_7_span_4_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_13_div_43_div_7_span_5_Template, 4, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_13_div_43_div_7_input_6_Template, 1, 3, "input", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AppComponent_div_3_div_13_div_43_div_7_span_7_Template, 2, 1, "span", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r465 = ctx.$implicit;
    const ctx_r462 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("cdkDragDisabled", ctx_r462.isPublic);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](10, _c9, ctx_r462.goalScreenActive && !ctx_r462.metrics[ctx_r462.getMetricIndex(metric_r465)].hasGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r462.metrics[ctx_r462.getMetricIndex(metric_r465)].description);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("display", ctx_r462.metrics[ctx_r462.getMetricIndex(metric_r465)].description != "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r462.metrics[ctx_r462.getMetricIndex(metric_r465)].editable && (!ctx_r462.goalScreenActive || !ctx_r462.metrics[ctx_r462.getMetricIndex(metric_r465)].hasCustomGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r462.metrics[ctx_r462.getMetricIndex(metric_r465)].editable && ctx_r462.goalScreenActive && !ctx_r462.metrics[ctx_r462.getMetricIndex(metric_r465)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r462.goalScreenActive && ctx_r462.metrics[ctx_r462.getMetricIndex(metric_r465)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r462.metrics[ctx_r462.getMetricIndex(metric_r465)].editable && !ctx_r462.goalScreenActive);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r462.metrics[ctx_r462.getMetricIndex(metric_r465)].editable && !ctx_r462.goalScreenActive);
} }
function AppComponent_div_3_div_13_div_43_div_11_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_43_div_11_Template(rf, ctx) { if (rf & 1) {
    const _r485 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 65)(1, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_43_div_11_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r485); const ctx_r484 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r484.saveValues(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_43_div_11_ng_container_3_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, " Save");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r463 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](3, _c7, ctx_r463.processing));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r4);
} }
function AppComponent_div_3_div_13_div_43_div_47_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r488 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r488.metrics[ctx_r488.getMetricIndex(metric_r486)].title);
} }
function AppComponent_div_3_div_13_div_43_div_47_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r500 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_43_div_47_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r500); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r499 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r499.metricValues["m" + metric_r486 + "_title"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r489 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("placeholder", ctx_r489.metrics[ctx_r489.getMetricIndex(metric_r486)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r489.isAuthenticated)("ngModel", ctx_r489.metricValues["m" + metric_r486 + "_title"]);
} }
function AppComponent_div_3_div_13_div_43_div_47_span_5_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_13_div_43_div_47_span_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_43_div_47_span_5_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    const _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r16);
} }
function AppComponent_div_3_div_13_div_43_div_47_div_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 57)(1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r504 = ctx.$implicit;
    const i_r505 = ctx.index;
    const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r491 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("metric-icon ", metricType_r504.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r491.metrics[ctx_r491.getMetricIndex(metric_r486)].editable ? ctx_r491.metricValues["m" + metric_r486 + "_title"] : ctx_r491.metrics[ctx_r491.getMetricIndex(metric_r486)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r491.metrics[ctx_r491.getMetricIndex(metric_r486)].type != i_r505);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r504.symbol);
} }
function AppComponent_div_3_div_13_div_43_div_47_input_9_Template(rf, ctx) { if (rf & 1) {
    const _r508 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 193);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_43_div_47_input_9_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r508); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r507 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r507.metricValues["m" + metric_r486 + "_t" + ctx_r507.terms[ctx_r507.goalTermIndex].period + "_p" + ctx_r507.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_43_div_47_input_9_Template_input_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r508); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r510 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r510.updateGoals(metric_r486, ctx_r510.terms[ctx_r510.goalTermIndex].period, ctx_r510.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r492 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r492.metrics[ctx_r492.getMetricIndex(metric_r486)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r492.isAuthenticated)("ngModel", ctx_r492.metricValues["m" + metric_r486 + "_t" + ctx_r492.terms[ctx_r492.goalTermIndex].period + "_p" + ctx_r492.goalPeriods[0].dateString + "_goal"])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_13_div_43_div_47_input_10_Template(rf, ctx) { if (rf & 1) {
    const _r514 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 194);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_43_div_47_input_10_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r514); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r513 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r513.metricValues["m" + metric_r486 + "_t" + ctx_r513.terms[ctx_r513.goalTermIndex].period + "_p" + ctx_r513.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_43_div_47_input_10_Template_input_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r514); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r516 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r516.updateGoals(metric_r486, ctx_r516.terms[ctx_r516.goalTermIndex].period, ctx_r516.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r493 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r493.metrics[ctx_r493.getMetricIndex(metric_r486)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r493.isAuthenticated)("ngModel", ctx_r493.metricValues["m" + metric_r486 + "_t" + ctx_r493.terms[ctx_r493.goalTermIndex].period + "_p" + ctx_r493.goalPeriods[0].dateString + "_goal"])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_13_div_43_div_47_input_11_Template(rf, ctx) { if (rf & 1) {
    const _r520 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 195);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_43_div_47_input_11_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r520); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r519 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r519.metricValues["m" + metric_r486 + "_t" + ctx_r519.terms[ctx_r519.goalTermIndex].period + "_p" + ctx_r519.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_43_div_47_input_11_Template_input_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r520); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r522 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r522.updateGoals(metric_r486, ctx_r522.terms[ctx_r522.goalTermIndex].period, ctx_r522.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r494 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r494.isAuthenticated)("ngModel", ctx_r494.metricValues["m" + metric_r486 + "_t" + ctx_r494.terms[ctx_r494.goalTermIndex].period + "_p" + ctx_r494.goalPeriods[0].dateString + "_goal"]);
} }
function AppComponent_div_3_div_13_div_43_div_47_input_12_Template(rf, ctx) { if (rf & 1) {
    const _r526 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 196);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_43_div_47_input_12_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r526); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r525 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r525.metricValues["m" + metric_r486 + "_t" + ctx_r525.terms[ctx_r525.goalTermIndex].period + "_p" + ctx_r525.goalPeriods[0].dateString + "_goal"] = $event); })("click", function AppComponent_div_3_div_13_div_43_div_47_input_12_Template_input_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r526); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r528 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r528.toggleProgressBar("m" + metric_r486 + "_t" + ctx_r528.terms[ctx_r528.goalTermIndex].period + "_p" + ctx_r528.goalPeriods[0].dateString + "_goal"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r495 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r495.metricValues["m" + metric_r486 + "_t" + ctx_r495.terms[ctx_r495.goalTermIndex].period + "_p" + ctx_r495.goalPeriods[0].dateString + "_goal"]);
} }
function AppComponent_div_3_div_13_div_43_div_47_select_13_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const opt_r532 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](opt_r532);
} }
function AppComponent_div_3_div_13_div_43_div_47_select_13_Template(rf, ctx) { if (rf & 1) {
    const _r534 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 197);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_13_div_43_div_47_select_13_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r534); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r533 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r533.metricValues["m" + metric_r486 + "_t" + ctx_r533.terms[ctx_r533.goalTermIndex].period + "_p" + ctx_r533.goalPeriods[0].dateString + "_goal"] = $event); })("change", function AppComponent_div_3_div_13_div_43_div_47_select_13_Template_select_change_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r534); const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r536 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r536.updateGoals(metric_r486, ctx_r536.terms[ctx_r536.goalTermIndex].period, ctx_r536.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_13_div_43_div_47_select_13_option_1_Template, 2, 1, "option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r496 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r496.metricValues["m" + metric_r486 + "_t" + ctx_r496.terms[ctx_r496.goalTermIndex].period + "_p" + ctx_r496.goalPeriods[0].dateString + "_goal"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r496.convertToOptions(ctx_r496.metrics[ctx_r496.getMetricIndex(metric_r486)].options));
} }
function AppComponent_div_3_div_13_div_43_div_47_span_20_Template(rf, ctx) { if (rf & 1) {
    const _r541 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_13_div_43_div_47_span_20_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r541); const pr_r539 = restoredCtx.$implicit; const metric_r486 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r540 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); ctx_r540.metricValues["m" + metric_r486 + "_t" + ctx_r540.terms[ctx_r540.goalTermIndex].period + "_p" + ctx_r540.goalPeriods[0].dateString + "_goal"] = pr_r539; return ctx_r540.updateGoals(metric_r486, ctx_r540.terms[ctx_r540.goalTermIndex].period, ctx_r540.goalPeriods[0].dateString); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_div_3_div_13_div_43_div_47_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 32)(1, "div", 155)(2, "div", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_div_43_div_47_span_3_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_13_div_43_div_47_input_4_Template, 1, 3, "input", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_13_div_43_div_47_span_5_Template, 2, 1, "span", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 42)(7, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AppComponent_div_3_div_13_div_43_div_47_div_8_Template, 3, 6, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, AppComponent_div_3_div_13_div_43_div_47_input_9_Template, 1, 4, "input", 188);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, AppComponent_div_3_div_13_div_43_div_47_input_10_Template, 1, 4, "input", 189);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, AppComponent_div_3_div_13_div_43_div_47_input_11_Template, 1, 2, "input", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, AppComponent_div_3_div_13_div_43_div_47_input_12_Template, 1, 1, "input", 191);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, AppComponent_div_3_div_13_div_43_div_47_select_13_Template, 2, 2, "select", 192);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 50)(15, "div", 51)(16, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "div", 52)(19, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](20, AppComponent_div_3_div_13_div_43_div_47_span_20_Template, 2, 0, "span", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](21, "div", 55)(22, "div", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()()()();
} if (rf & 2) {
    const metric_r486 = ctx.$implicit;
    const ctx_r464 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](25, _c9, ctx_r464.goalScreenActive && !ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].hasGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", 90 / (ctx_r464.screenWidth > 768 ? ctx_r464.terms[ctx_r464.selectedTermIndex].metrics.length : 1), "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](27, _c9, ctx_r464.goalScreenActive && !ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].hasGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].description);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("display", ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].description != "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].editable);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].editable);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].editable);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r464.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].type <= 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].type == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].type == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].type == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].type == 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r464.metrics[ctx_r464.getMetricIndex(metric_r486)].type != 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r464.isAuthenticated || !ctx_r464.progressBarSelected["m" + metric_r486 + "_t" + ctx_r464.terms[ctx_r464.goalTermIndex].period + "_p" + ctx_r464.goalPeriods[0].dateString + "_goal"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Set Progress: ", ctx_r464.metricValues["m" + metric_r486 + "_t" + ctx_r464.terms[ctx_r464.goalTermIndex].period + "_p" + ctx_r464.goalPeriods[0].dateString + "_goal"] ? ctx_r464.metricValues["m" + metric_r486 + "_t" + ctx_r464.terms[ctx_r464.goalTermIndex].period + "_p" + ctx_r464.goalPeriods[0].dateString + "_goal"] : 0, "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](29, _c6));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", ctx_r464.metricValues["m" + metric_r486 + "_t" + ctx_r464.terms[ctx_r464.goalTermIndex].period + "_p" + ctx_r464.goalPeriods[0].dateString + "_goal"], "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", ctx_r464.metricValues["m" + metric_r486 + "_t" + ctx_r464.terms[ctx_r464.goalTermIndex].period + "_p" + ctx_r464.goalPeriods[0].dateString + "_goal"], "%");
} }
function AppComponent_div_3_div_13_div_43_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 21)(1, "div", 22)(2, "div", 144)(3, "div", 145)(4, "div", 33)(5, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Month");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AppComponent_div_3_div_13_div_43_div_7_Template, 8, 12, "div", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 147)(9, "div", 148)(10, "div", 149);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, AppComponent_div_3_div_13_div_43_div_11_Template, 5, 5, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "Goals");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 206);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](16, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](17, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](18, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](19, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](20, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](21, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](22, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](23, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](24, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](25, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](26, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](27, "Goals ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "div", 154)(29, "div", 155)(30, "div", 33)(31, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](32, "Period");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "div", 207);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](35, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](36, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](37, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](38, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](39, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](40, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](41, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](42, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](43, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](44, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](45, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](46, "Goals ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](47, AppComponent_div_3_div_13_div_43_div_47_Template, 23, 30, "div", 157);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} if (rf & 2) {
    const ctx_r235 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r235.terms[ctx_r235.selectedTermIndex].metrics);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r235.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"](" ", ctx_r235.terms[ctx_r235.goalTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](16, 7, ctx_r235.goalPeriods[0].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](17, 10, ctx_r235.goalPeriods[0].date, "dd") + (ctx_r235.goalPeriods[0].date.getMonth() != ctx_r235.goalPeriods[0].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](18, 13, ctx_r235.goalPeriods[0].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](19, 16, ctx_r235.goalPeriods[0].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](20, 19, ctx_r235.goalPeriods[0].date, "MMM yyyy"), " - ", ctx_r235.terms[ctx_r235.goalTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](21, 22, ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](22, 25, ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].date, "dd") + (ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].date.getMonth() != ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](23, 28, ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](24, 31, ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](25, 34, ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].date, "MMM yyyy"), "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"](" ", ctx_r235.terms[ctx_r235.goalTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](35, 37, ctx_r235.goalPeriods[0].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](36, 40, ctx_r235.goalPeriods[0].date, "dd") + (ctx_r235.goalPeriods[0].date.getMonth() != ctx_r235.goalPeriods[0].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](37, 43, ctx_r235.goalPeriods[0].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](38, 46, ctx_r235.goalPeriods[0].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](39, 49, ctx_r235.goalPeriods[0].date, "MMM yyyy"), " - ", ctx_r235.terms[ctx_r235.goalTermIndex].period ? "Week" + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](40, 52, ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].date, "W") + (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](41, 55, ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].date, "dd") + (ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].date.getMonth() != ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].dateEnd.getMonth() ? _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](42, 58, ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].date, "MMM") : "") + (" - " + _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](43, 61, ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].dateEnd, "dd MMM"))) : _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](44, 64, ctx_r235.goalPeriods[ctx_r235.goalPeriods.length - 1].date, "MMM yyyy"), "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r235.terms[ctx_r235.selectedTermIndex].metrics);
} }
function AppComponent_div_3_div_13_hr_44_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "hr", 205);
} }
function AppComponent_div_3_div_13_div_45_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 208)(1, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r237 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Goals should be entered as a monthly average for the ", ctx_r237.periods.length + " " + (ctx_r237.terms[ctx_r237.selectedTermIndex].period ? "Weeks" : "Months"), " time period.");
} }
function AppComponent_div_3_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 141)(1, "div", 142);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_13_div_2_Template, 5, 5, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_13_h1_3_Template, 2, 0, "h1", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_13_div_4_Template, 13, 26, "div", 143);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 21)(6, "div", 22)(7, "div", 144)(8, "div", 145)(9, "div", 33)(10, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Month");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, AppComponent_div_3_div_13_span_12_Template, 7, 16, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, AppComponent_div_3_div_13_div_13_Template, 3, 2, "div", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 147)(15, "div", 148)(16, "div", 149);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](17, AppComponent_div_3_div_13_div_17_Template, 5, 5, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](18, AppComponent_div_3_div_13_h1_18_Template, 2, 0, "h1", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, AppComponent_div_3_div_13_div_19_Template, 13, 26, "div", 143);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "div", 150)(21, "div", 42)(22, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](23, AppComponent_div_3_div_13_div_23_Template, 12, 26, "div", 151);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](24, AppComponent_div_3_div_13_div_24_Template, 2, 0, "div", 152);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](25, AppComponent_div_3_div_13_div_25_Template, 2, 0, "div", 153);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "div", 154)(27, "div", 155)(28, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](29, "Month");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](30, AppComponent_div_3_div_13_span_30_Template, 7, 16, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](31, AppComponent_div_3_div_13_div_31_Template, 2, 0, "div", 153);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](32, AppComponent_div_3_div_13_div_32_Template, 14, 26, "div", 156);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](33, AppComponent_div_3_div_13_div_33_Template, 2, 0, "div", 153);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](34, AppComponent_div_3_div_13_div_34_Template, 5, 10, "div", 157);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "div", 158);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](36, AppComponent_div_3_div_13_div_36_Template, 14, 32, "div", 159);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](37, AppComponent_div_3_div_13_div_37_Template, 2, 0, "div", 159);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](38, AppComponent_div_3_div_13_div_38_Template, 3, 8, "div", 157);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](39, AppComponent_div_3_div_13_div_39_Template, 2, 1, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](40, AppComponent_div_3_div_13_div_40_Template, 4, 1, "div", 160);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](41, AppComponent_div_3_div_13_hr_41_Template, 1, 0, "hr", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](42, AppComponent_div_3_div_13_div_42_Template, 4, 1, "div", 162);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](43, AppComponent_div_3_div_13_div_43_Template, 48, 67, "div", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](44, AppComponent_div_3_div_13_hr_44_Template, 1, 0, "hr", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](45, AppComponent_div_3_div_13_div_45_Template, 3, 1, "div", 78);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r111 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme == "boardroom");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme == "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme == "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r111.terms[ctx_r111.selectedTermIndex].metrics);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme == "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme == "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme == "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme == "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r111.terms[ctx_r111.selectedTermIndex].metrics);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme == "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r111.terms[ctx_r111.selectedTermIndex].metrics);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme == "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.missingValues() && ctx_r111.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme != "outcomes");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r111.theme != "outcomes");
} }
function AppComponent_div_3_div_14_div_1_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_14_div_1_li_5_Template(rf, ctx) { if (rf & 1) {
    const _r550 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li")(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_1_li_5_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r550); const i_r548 = restoredCtx.index; const ctx_r549 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r549.addMetric(i_r548); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r547 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metric_r547.title);
} }
function AppComponent_div_3_div_14_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r552 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 211)(1, "a", 212);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_1_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r552); const ctx_r551 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r551.showDropdown = !ctx_r551.showDropdown; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_1_ng_container_2_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 213)(4, "ul");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_14_div_1_li_5_Template, 3, 1, "li", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "li")(7, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_1_Template_a_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r552); const ctx_r553 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r553.addMetric(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, "New Metric");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()();
} if (rf & 2) {
    const ctx_r543 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r543.showDropdown);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r543.metrics);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_br_25_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "br");
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_span_26_Template(rf, ctx) { if (rf & 1) {
    const _r574 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "label")(2, "div", 218)(3, "input", 219);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_5_div_2_span_26_Template_input_ngModelChange_3_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r574); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r573 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r573.metrics[ctx_r573.getMetricIndex(metric_r556)].hasCustomGoal = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Custom Goal ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "em", 234);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r562 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r562.isAuthenticated)("ngModel", ctx_r562.metrics[ctx_r562.getMetricIndex(metric_r556)].hasCustomGoal);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_div_27_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 236)(1, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r579 = ctx.$implicit;
    const i_r580 = ctx.index;
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;
    const ctx_r577 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("tag ", metricType_r579.name, " active");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r577.metrics[ctx_r577.getMetricIndex(metric_r556)].goalType != i_r580);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r579.symbol);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_div_27_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r586 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 140)(1, "a", 237);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_5_div_2_div_27_div_6_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r586); const j_r583 = restoredCtx.index; const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit; const ctx_r584 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r584.updateGoalType(ctx_r584.getMetricIndex(metric_r556), j_r583); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r582 = ctx.$implicit;
    const j_r583 = ctx.index;
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;
    const ctx_r578 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("tag ", metricType_r582.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](6, _c4, ctx_r578.metrics[ctx_r578.getMetricIndex(metric_r556)].goalType == j_r583));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", metricType_r582.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r582.symbol);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_div_27_Template(rf, ctx) { if (rf & 1) {
    const _r590 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 235);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_5_div_2_div_27_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r590); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r588 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r588.updateShowGoalTypeSelector(ctx_r588.getMetricIndex(metric_r556)); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 227);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_2_div_5_div_2_div_27_div_2_Template, 3, 5, "div", 228);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "\u25BC");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 229);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_14_div_2_div_5_div_2_div_27_div_6_Template, 3, 8, "div", 230);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r563 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](5, _c4, ctx_r563.showGoalTypeSelector == ctx_r563.getMetricIndex(metric_r556)));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r563.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r563.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r563.metricTypes);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_br_28_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "br");
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_span_29_Template(rf, ctx) { if (rf & 1) {
    const _r593 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "input", 238);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_5_div_2_span_29_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r593); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r592 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r592.metrics[ctx_r592.getMetricIndex(metric_r556)].goalTitle = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "em", 239);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r565 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r565.isAuthenticated)("ngModel", ctx_r565.metrics[ctx_r565.getMetricIndex(metric_r556)].goalTitle);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_br_30_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "br");
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_span_31_Template(rf, ctx) { if (rf & 1) {
    const _r597 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "input", 240);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_5_div_2_span_31_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r597); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r596 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r596.metrics[ctx_r596.getMetricIndex(metric_r556)].goalDescription = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "em", 241);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r567 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r567.isAuthenticated)("ngModel", ctx_r567.metrics[ctx_r567.getMetricIndex(metric_r556)].goalDescription);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_div_43_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 236)(1, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r600 = ctx.$implicit;
    const i_r601 = ctx.index;
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r568 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("tag ", metricType_r600.name, " active");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r568.metrics[ctx_r568.getMetricIndex(metric_r556)].type != i_r601);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r600.symbol);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_div_47_Template(rf, ctx) { if (rf & 1) {
    const _r607 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 140)(1, "a", 237);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_5_div_2_div_47_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r607); const j_r604 = restoredCtx.index; const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r605 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r605.updateType(ctx_r605.getMetricIndex(metric_r556), j_r604); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r603 = ctx.$implicit;
    const j_r604 = ctx.index;
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r569 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("tag ", metricType_r603.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](6, _c4, ctx_r569.metrics[ctx_r569.getMetricIndex(metric_r556)].type == j_r604));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", metricType_r603.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r603.symbol);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_div_48_Template(rf, ctx) { if (rf & 1) {
    const _r611 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 242);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_5_div_2_div_48_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r611); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r609 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r609.updateShowDecimalDigitsSelector(ctx_r609.getMetricIndex(metric_r556)); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 227)(2, "div", 243)(3, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "em");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "\u25BC");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 229)(8, "div", 244)(9, "a", 245);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_5_div_2_div_48_Template_a_click_9_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r611); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r612 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r612.updateDecimalDigits(ctx_r612.getMetricIndex(metric_r556), 0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, "###");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 244)(12, "a", 246);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_5_div_2_div_48_Template_a_click_12_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r611); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r614 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r614.updateDecimalDigits(ctx_r614.getMetricIndex(metric_r556), 1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "###.0");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 244)(15, "a", 247);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_5_div_2_div_48_Template_a_click_15_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r611); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r616 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r616.updateDecimalDigits(ctx_r616.getMetricIndex(metric_r556), 2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "###.00");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r570 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](7, _c4, ctx_r570.showDecimalDigitsSelector == ctx_r570.getMetricIndex(metric_r556)));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r570.metrics[ctx_r570.getMetricIndex(metric_r556)].decimalDigits === 0 ? "###" : ctx_r570.metrics[ctx_r570.getMetricIndex(metric_r556)].decimalDigits === 1 ? "###.0" : "###.00");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r570.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](9, _c4, ctx_r570.metrics[ctx_r570.getMetricIndex(metric_r556)].decimalDigits == 0));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](11, _c4, ctx_r570.metrics[ctx_r570.getMetricIndex(metric_r556)].decimalDigits == 1));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](13, _c4, ctx_r570.metrics[ctx_r570.getMetricIndex(metric_r556)].decimalDigits == 2));
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_div_49_Template(rf, ctx) { if (rf & 1) {
    const _r621 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 248)(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_5_div_2_div_49_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r621); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r619 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r619.updateShowOptionBox(ctx_r619.getMetricIndex(metric_r556)); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "\u2630 Options");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 229)(4, "div", 249)(5, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Enter options each in new Line");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "textarea", 203);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_5_div_2_div_49_Template_textarea_ngModelChange_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r621); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r622 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r622.metrics[ctx_r622.getMetricIndex(metric_r556)].options = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r571 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](4, _c4, ctx_r571.showOptionBox == ctx_r571.getMetricIndex(metric_r556)));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r571.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r571.metrics[ctx_r571.getMetricIndex(metric_r556)].options);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_div_50_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_div_50_Template(rf, ctx) { if (rf & 1) {
    const _r628 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 250)(1, "a", 251);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_5_div_2_div_50_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r628); const i_r557 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).index; const ctx_r626 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r626.deleteMetric(i_r557); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_2_div_5_div_2_div_50_ng_container_2_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](6);
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r8);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r630 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 33)(1, "textarea", 217);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_5_div_2_Template_textarea_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r630); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r629 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r629.metrics[ctx_r629.getMetricIndex(metric_r556)].title = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 171)(3, "span")(4, "label")(5, "div", 218)(6, "input", 219);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_5_div_2_Template_input_ngModelChange_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r630); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r632 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r632.metrics[ctx_r632.getMetricIndex(metric_r556)].override = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, "Override Settings for This Widget ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "em", 220);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](10, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "span")(12, "label")(13, "div", 218)(14, "input", 219);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_5_div_2_Template_input_ngModelChange_14_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r630); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r634 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r634.metrics[ctx_r634.getMetricIndex(metric_r556)].hasGoal = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, "Goal ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "em", 221);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "span")(19, "label")(20, "div", 218)(21, "input", 219);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_5_div_2_Template_input_ngModelChange_21_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r630); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r636 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r636.metrics[ctx_r636.getMetricIndex(metric_r556)].goalAlert = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22, "Reverse Goal Alert ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "em", 222);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](25, AppComponent_div_3_div_14_div_2_div_5_div_2_br_25_Template, 1, 0, "br", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](26, AppComponent_div_3_div_14_div_2_div_5_div_2_span_26_Template, 7, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](27, AppComponent_div_3_div_14_div_2_div_5_div_2_div_27_Template, 7, 7, "div", 223);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](28, AppComponent_div_3_div_14_div_2_div_5_div_2_br_28_Template, 1, 0, "br", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](29, AppComponent_div_3_div_14_div_2_div_5_div_2_span_29_Template, 4, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](30, AppComponent_div_3_div_14_div_2_div_5_div_2_br_30_Template, 1, 0, "br", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](31, AppComponent_div_3_div_14_div_2_div_5_div_2_span_31_Template, 4, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](32, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "span")(34, "label")(35, "div", 218)(36, "input", 219);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_5_div_2_Template_input_ngModelChange_36_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r630); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r638 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r638.metrics[ctx_r638.getMetricIndex(metric_r556)].editable = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](37, "User Defined Title ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "em", 224);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](39, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "div", 225)(41, "div", 226);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_5_div_2_Template_div_click_41_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r630); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r640 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r640.updateShowTypeSelector(ctx_r640.getMetricIndex(metric_r556)); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "button", 227);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](43, AppComponent_div_3_div_14_div_2_div_5_div_2_div_43_Template, 3, 5, "div", 228);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](44, "em");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](45, "\u25BC");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "div", 229);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](47, AppComponent_div_3_div_14_div_2_div_5_div_2_div_47_Template, 3, 8, "div", 230);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](48, AppComponent_div_3_div_14_div_2_div_5_div_2_div_48_Template, 17, 15, "div", 231);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](49, AppComponent_div_3_div_14_div_2_div_5_div_2_div_49_Template, 8, 6, "div", 232);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](50, AppComponent_div_3_div_14_div_2_div_5_div_2_div_50_Template, 3, 1, "div", 233);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r559 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readOnly", !ctx_r559.isAuthenticated)("ngModel", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r559.isAuthenticated)("ngModel", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].override);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r559.isAuthenticated)("ngModel", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].hasGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r559.isAuthenticated)("ngModel", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].goalAlert);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].hasGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].hasGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r559.isAuthenticated)("ngModel", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].editable);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](25, _c4, ctx_r559.showTypeSelector == ctx_r559.getMetricIndex(metric_r556)));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r559.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r559.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r559.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].type <= 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r559.metrics[ctx_r559.getMetricIndex(metric_r556)].type == 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r559.isAuthenticated);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_3_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r643 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r643.metrics[ctx_r643.getMetricIndex(metric_r556)].title);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_3_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r644 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r644.metricValues["m" + metric_r556 + "_title"]);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_3_span_3_em_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "em", 173);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;
    const ctx_r650 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r650.metrics[ctx_r650.getMetricIndex(metric_r556)].goalDescription);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_3_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span", 171);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_14_div_2_div_5_div_3_span_3_em_3_Template, 2, 1, "em", 172);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r645 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx_r645.metrics[ctx_r645.getMetricIndex(metric_r556)].goalTitle, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r645.metrics[ctx_r645.getMetricIndex(metric_r556)].goalDescription);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_3_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r654 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_5_div_3_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r654); const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r653 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r653.metricValues["m" + metric_r556 + "_title"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r646 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("placeholder", ctx_r646.metrics[ctx_r646.getMetricIndex(metric_r556)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r646.isAuthenticated)("ngModel", ctx_r646.metricValues["m" + metric_r556 + "_title"]);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_3_span_5_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_3_span_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_14_div_2_div_5_div_3_span_5_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](6);
    const _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r16);
} }
function AppComponent_div_3_div_14_div_2_div_5_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_14_div_2_div_5_div_3_span_1_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_2_div_5_div_3_span_2_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_14_div_2_div_5_div_3_span_3_Template, 4, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_14_div_2_div_5_div_3_input_4_Template, 1, 3, "input", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_14_div_2_div_5_div_3_span_5_Template, 2, 1, "span", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r560 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](9, _c9, ctx_r560.goalScreenActive && !ctx_r560.metrics[ctx_r560.getMetricIndex(metric_r556)].hasGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r560.metrics[ctx_r560.getMetricIndex(metric_r556)].description);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("display", ctx_r560.metrics[ctx_r560.getMetricIndex(metric_r556)].description != "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r560.metrics[ctx_r560.getMetricIndex(metric_r556)].editable && (!ctx_r560.goalScreenActive || !ctx_r560.metrics[ctx_r560.getMetricIndex(metric_r556)].hasCustomGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r560.metrics[ctx_r560.getMetricIndex(metric_r556)].editable && ctx_r560.goalScreenActive && !ctx_r560.metrics[ctx_r560.getMetricIndex(metric_r556)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r560.goalScreenActive && ctx_r560.metrics[ctx_r560.getMetricIndex(metric_r556)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r560.metrics[ctx_r560.getMetricIndex(metric_r556)].editable && !ctx_r560.goalScreenActive);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r560.metrics[ctx_r560.getMetricIndex(metric_r556)].editable && !ctx_r560.goalScreenActive);
} }
function AppComponent_div_3_div_14_div_2_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 167, 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_2_div_5_div_2_Template, 51, 27, "div", 216);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_14_div_2_div_5_div_3_Template, 6, 11, "div", 169);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r554 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("cdkDragDisabled", ctx_r554.isPublic);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r554.isPublic);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r554.isPublic);
} }
function AppComponent_div_3_div_14_div_2_div_6_span_3_span_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](2, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](2, 1, period_r659.date, "MMM"));
} }
function AppComponent_div_3_div_14_div_2_div_6_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](2, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "small")(5, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](7, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AppComponent_div_3_div_14_div_2_div_6_span_3_span_8_Template, 3, 4, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](11, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} if (rf & 2) {
    const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" Week ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](2, 4, period_r659.date, "W"), "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](7, 7, period_r659.date, "dd"), " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", period_r659.date.getMonth() != period_r659.dateEnd.getMonth());
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" - ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](11, 10, period_r659.dateEnd, "dd MMM"), "");
} }
function AppComponent_div_3_div_14_div_2_div_6_span_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](2, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind2"](2, 1, period_r659.date, "MMM yyyy"));
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_br_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "br");
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_span_11_Template(rf, ctx) { if (rf & 1) {
    const _r686 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "label")(2, "div", 218)(3, "input", 219);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_span_11_Template_input_ngModelChange_3_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r686); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r685 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r685.metrics[ctx_r685.getMetricIndex(metric_r668)].hasCustomGoal = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Custom Goal ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "em", 234);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r674 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r674.isAuthenticated)("ngModel", ctx_r674.metrics[ctx_r674.getMetricIndex(metric_r668)].hasCustomGoal);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_12_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 236)(1, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r691 = ctx.$implicit;
    const i_r692 = ctx.index;
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;
    const ctx_r689 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("tag ", metricType_r691.name, " active");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r689.metrics[ctx_r689.getMetricIndex(metric_r668)].goalType != i_r692);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r691.symbol);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_12_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r698 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 140)(1, "a", 237);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_12_div_6_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r698); const j_r695 = restoredCtx.index; const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit; const ctx_r696 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r696.updateGoalType(ctx_r696.getMetricIndex(metric_r668), j_r695); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r694 = ctx.$implicit;
    const j_r695 = ctx.index;
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;
    const ctx_r690 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("tag ", metricType_r694.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](6, _c4, ctx_r690.metrics[ctx_r690.getMetricIndex(metric_r668)].goalType == j_r695));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", metricType_r694.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r694.symbol);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_12_Template(rf, ctx) { if (rf & 1) {
    const _r702 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 235);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_12_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r702); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r700 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r700.updateShowGoalTypeSelector(ctx_r700.getMetricIndex(metric_r668)); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 227);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_12_div_2_Template, 3, 5, "div", 228);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "\u25BC");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 229);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_12_div_6_Template, 3, 8, "div", 230);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r675 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](5, _c4, ctx_r675.showGoalTypeSelector == ctx_r675.getMetricIndex(metric_r668)));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r675.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r675.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r675.metricTypes);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_br_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "br");
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_span_14_Template(rf, ctx) { if (rf & 1) {
    const _r705 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "input", 238);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_span_14_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r705); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r704 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r704.metrics[ctx_r704.getMetricIndex(metric_r668)].goalTitle = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "em", 239);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r677 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r677.isAuthenticated)("ngModel", ctx_r677.metrics[ctx_r677.getMetricIndex(metric_r668)].goalTitle);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_br_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "br");
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_span_16_Template(rf, ctx) { if (rf & 1) {
    const _r709 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "input", 240);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_span_16_Template_input_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r709); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r708 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r708.metrics[ctx_r708.getMetricIndex(metric_r668)].goalDescription = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "em", 241);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r679 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r679.isAuthenticated)("ngModel", ctx_r679.metrics[ctx_r679.getMetricIndex(metric_r668)].goalDescription);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_28_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 236)(1, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r712 = ctx.$implicit;
    const i_r713 = ctx.index;
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r680 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("tag ", metricType_r712.name, " active");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", ctx_r680.metrics[ctx_r680.getMetricIndex(metric_r668)].type != i_r713);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r712.symbol);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_32_Template(rf, ctx) { if (rf & 1) {
    const _r719 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 140)(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_32_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r719); const j_r716 = restoredCtx.index; const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r717 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r717.updateType(ctx_r717.getMetricIndex(metric_r668), j_r716); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r715 = ctx.$implicit;
    const j_r716 = ctx.index;
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r681 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("tag ", metricType_r715.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](5, _c4, ctx_r681.metrics[ctx_r681.getMetricIndex(metric_r668)].type == j_r716));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r715.symbol);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_33_Template(rf, ctx) { if (rf & 1) {
    const _r723 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 242);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_33_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r723); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r721 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r721.updateShowDecimalDigitsSelector(ctx_r721.getMetricIndex(metric_r668)); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 227)(2, "div", 243)(3, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "em");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "\u25BC");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 229)(8, "div", 244)(9, "a", 245);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_33_Template_a_click_9_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r723); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r724 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r724.updateDecimalDigits(ctx_r724.getMetricIndex(metric_r668), 0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, "###");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 244)(12, "a", 246);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_33_Template_a_click_12_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r723); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r726 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r726.updateDecimalDigits(ctx_r726.getMetricIndex(metric_r668), 1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "###.0");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 244)(15, "a", 247);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_33_Template_a_click_15_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r723); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r728 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r728.updateDecimalDigits(ctx_r728.getMetricIndex(metric_r668), 2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "###.00");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r682 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](7, _c4, ctx_r682.showDecimalDigitsSelector == ctx_r682.getMetricIndex(metric_r668)));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r682.metrics[ctx_r682.getMetricIndex(metric_r668)].decimalDigits === 0 ? "###" : ctx_r682.metrics[ctx_r682.getMetricIndex(metric_r668)].decimalDigits === 1 ? "###.0" : "###.00");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r682.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](9, _c4, ctx_r682.metrics[ctx_r682.getMetricIndex(metric_r668)].decimalDigits == 0));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](11, _c4, ctx_r682.metrics[ctx_r682.getMetricIndex(metric_r668)].decimalDigits == 1));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](13, _c4, ctx_r682.metrics[ctx_r682.getMetricIndex(metric_r668)].decimalDigits == 2));
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_34_Template(rf, ctx) { if (rf & 1) {
    const _r733 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 248)(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_34_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r733); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r731 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r731.updateShowOptionBox(ctx_r731.getMetricIndex(metric_r668)); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "\u2630 Options");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 229)(4, "div", 249)(5, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Enter options each in new Line");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "textarea", 203);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_34_Template_textarea_ngModelChange_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r733); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r734 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r734.metrics[ctx_r734.getMetricIndex(metric_r668)].options = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r683 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](4, _c4, ctx_r683.showOptionBox == ctx_r683.getMetricIndex(metric_r668)));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r683.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r683.metrics[ctx_r683.getMetricIndex(metric_r668)].options);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_35_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_35_Template(rf, ctx) { if (rf & 1) {
    const _r739 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 250)(1, "a", 251);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_35_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r739); const ctx_r738 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](7); return ctx_r738.deleteMetric(ctx_r738.i); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_35_ng_container_2_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](7);
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r8);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r741 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 33)(1, "textarea", 253);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_Template_textarea_ngModelChange_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r741); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r740 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r740.metrics[ctx_r740.getMetricIndex(metric_r668)].title = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 171)(3, "span")(4, "label")(5, "div", 218)(6, "input", 219);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_Template_input_ngModelChange_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r741); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r743 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r743.metrics[ctx_r743.getMetricIndex(metric_r668)].hasGoal = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, "Goal ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "em", 221);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_br_10_Template, 1, 0, "br", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_span_11_Template, 7, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_12_Template, 7, 7, "div", 223);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_br_13_Template, 1, 0, "br", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_span_14_Template, 4, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_br_15_Template, 1, 0, "br", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_span_16_Template, 4, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](17, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "span")(19, "label")(20, "div", 218)(21, "input", 219);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_Template_input_ngModelChange_21_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r741); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r745 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r745.metrics[ctx_r745.getMetricIndex(metric_r668)].editable = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22, "User Defined Title ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "em", 224);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24, "?");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "div", 225)(26, "div", 254);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_Template_div_click_26_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r741); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r747 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return ctx_r747.updateShowTypeSelector(ctx_r747.getMetricIndex(metric_r668)); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "button", 227);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](28, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_28_Template, 3, 5, "div", 228);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "em");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30, "\u25BC");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "div", 229);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](32, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_32_Template, 3, 7, "div", 230);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](33, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_33_Template, 17, 15, "div", 231);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](34, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_34_Template, 8, 6, "div", 232);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](35, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_div_35_Template, 3, 1, "div", 233);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r670 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r670.isAuthenticated)("ngModel", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r670.isAuthenticated)("ngModel", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].hasGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].hasGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].hasGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", !ctx_r670.isAuthenticated)("ngModel", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].editable);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](21, _c4, ctx_r670.showTypeSelector == ctx_r670.getMetricIndex(metric_r668)));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r670.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !ctx_r670.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r670.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].type <= 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r670.metrics[ctx_r670.getMetricIndex(metric_r668)].type == 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r670.isAuthenticated);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r750 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r750.metrics[ctx_r750.getMetricIndex(metric_r668)].title);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r751 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r751.metricValues["m" + metric_r668 + "_title"]);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_3_em_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "em", 173);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "i");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3).$implicit;
    const ctx_r757 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r757.metrics[ctx_r757.getMetricIndex(metric_r668)].goalDescription);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span", 171);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_3_em_3_Template, 2, 1, "em", 172);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r752 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx_r752.metrics[ctx_r752.getMetricIndex(metric_r668)].goalTitle, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r752.metrics[ctx_r752.getMetricIndex(metric_r668)].goalDescription);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r761 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r761); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const ctx_r760 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5); return (ctx_r760.metricValues["m" + metric_r668 + "_title"] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r753 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("placeholder", ctx_r753.metrics[ctx_r753.getMetricIndex(metric_r668)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r753.isAuthenticated)("ngModel", ctx_r753.metricValues["m" + metric_r668 + "_title"]);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_5_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_5_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](7);
    const _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r16);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_1_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_2_Template, 2, 1, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_3_Template, 4, 2, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_input_4_Template, 1, 3, "input", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_span_5_Template, 2, 1, "span", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r671 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](9, _c9, ctx_r671.goalScreenActive && !ctx_r671.metrics[ctx_r671.getMetricIndex(metric_r668)].hasGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r671.metrics[ctx_r671.getMetricIndex(metric_r668)].description);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("display", ctx_r671.metrics[ctx_r671.getMetricIndex(metric_r668)].description != "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r671.metrics[ctx_r671.getMetricIndex(metric_r668)].editable && (!ctx_r671.goalScreenActive || !ctx_r671.metrics[ctx_r671.getMetricIndex(metric_r668)].hasCustomGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r671.metrics[ctx_r671.getMetricIndex(metric_r668)].editable && ctx_r671.goalScreenActive && !ctx_r671.metrics[ctx_r671.getMetricIndex(metric_r668)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r671.goalScreenActive && ctx_r671.metrics[ctx_r671.getMetricIndex(metric_r668)].hasCustomGoal);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r671.metrics[ctx_r671.getMetricIndex(metric_r668)].editable && !ctx_r671.goalScreenActive);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r671.metrics[ctx_r671.getMetricIndex(metric_r668)].editable && !ctx_r671.goalScreenActive);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 57)(1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metricType_r774 = ctx.$implicit;
    const i_r775 = ctx.index;
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const ctx_r766 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("metric-icon ", metricType_r774.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("tooltip", ctx_r766.goalScreenActive && ctx_r766.metrics[ctx_r766.getMetricIndex(metric_r668)].hasCustomGoal ? ctx_r766.metrics[ctx_r766.getMetricIndex(metric_r668)].goalTitle : ctx_r766.metrics[ctx_r766.getMetricIndex(metric_r668)].editable ? ctx_r766.metricValues["m" + metric_r668 + "_title"] : ctx_r766.metrics[ctx_r766.getMetricIndex(metric_r668)].title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", (!ctx_r766.goalScreenActive || !ctx_r766.metrics[ctx_r766.getMetricIndex(metric_r668)].hasCustomGoal) && ctx_r766.metrics[ctx_r766.getMetricIndex(metric_r668)].type != i_r775 || ctx_r766.goalScreenActive && ctx_r766.metrics[ctx_r766.getMetricIndex(metric_r668)].hasCustomGoal && ctx_r766.metrics[ctx_r766.getMetricIndex(metric_r668)].goalType != i_r775);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](metricType_r774.symbol);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_3_Template(rf, ctx) { if (rf & 1) {
    const _r778 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 183);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_3_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r778); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r777 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r777.metricValues["m" + metric_r668 + "_t" + ctx_r777.terms[ctx_r777.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r777.goalScreenActive ? "_goal" : "_actual")] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r767 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r767.metrics[ctx_r767.getMetricIndex(metric_r668)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r767.isPublic || !ctx_r767.isAuthenticated)("disabled", period_r659.date > ctx_r767.today && !ctx_r767.goalScreenActive)("ngModel", ctx_r767.metricValues["m" + metric_r668 + "_t" + ctx_r767.terms[ctx_r767.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r767.goalScreenActive ? "_goal" : "_actual")])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r784 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 184);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r784); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r783 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r783.metricValues["m" + metric_r668 + "_t" + ctx_r783.terms[ctx_r783.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r783.goalScreenActive ? "_goal" : "_actual")] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r768 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("mask", "separator.", ctx_r768.metrics[ctx_r768.getMetricIndex(metric_r668)].decimalDigits, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r768.isPublic || !ctx_r768.isAuthenticated)("disabled", period_r659.date > ctx_r768.today && !ctx_r768.goalScreenActive)("ngModel", ctx_r768.metricValues["m" + metric_r668 + "_t" + ctx_r768.terms[ctx_r768.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r768.goalScreenActive ? "_goal" : "_actual")])("allowNegativeNumbers", true);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_5_Template(rf, ctx) { if (rf & 1) {
    const _r790 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_5_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r790); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r789 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r789.metricValues["m" + metric_r668 + "_t" + ctx_r789.terms[ctx_r789.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r789.goalScreenActive ? "_goal" : "_actual")] = $event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r769 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("readonly", !ctx_r769.isPublic || !ctx_r769.isAuthenticated)("disabled", period_r659.date > ctx_r769.today && !ctx_r769.goalScreenActive)("ngModel", ctx_r769.metricValues["m" + metric_r668 + "_t" + ctx_r769.terms[ctx_r769.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r769.goalScreenActive ? "_goal" : "_actual")]);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_6_Template(rf, ctx) { if (rf & 1) {
    const _r796 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_6_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r796); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r795 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r795.metricValues["m" + metric_r668 + "_t" + ctx_r795.terms[ctx_r795.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r795.goalScreenActive ? "_goal" : "_actual")] = $event); })("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_6_Template_input_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r796); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r799 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r799.toggleProgressBar("m" + metric_r668 + "_t" + ctx_r799.terms[ctx_r799.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r799.goalScreenActive ? "_goal" : "_actual")); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r770 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", period_r659.date > ctx_r770.today && !ctx_r770.goalScreenActive)("ngModel", ctx_r770.metricValues["m" + metric_r668 + "_t" + ctx_r770.terms[ctx_r770.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r770.goalScreenActive ? "_goal" : "_actual")]);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_select_7_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const opt_r805 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](opt_r805);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_select_7_Template(rf, ctx) { if (rf & 1) {
    const _r807 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_select_7_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r807); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r806 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r806.metricValues["m" + metric_r668 + "_t" + ctx_r806.terms[ctx_r806.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r806.goalScreenActive ? "_goal" : "_actual")] = $event); })("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_select_7_Template_select_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r807); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r810 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r810.toggleProgressBar("m" + metric_r668 + "_t" + ctx_r810.terms[ctx_r810.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r810.goalScreenActive ? "_goal" : "_actual")); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_select_7_option_1_Template, 2, 1, "option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r771 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", period_r659.date > ctx_r771.today && !ctx_r771.goalScreenActive)("ngModel", ctx_r771.metricValues["m" + metric_r668 + "_t" + ctx_r771.terms[ctx_r771.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r771.goalScreenActive ? "_goal" : "_actual")]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r771.convertToOptions(ctx_r771.metrics[ctx_r771.getMetricIndex(metric_r668)].options));
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_span_14_Template(rf, ctx) { if (rf & 1) {
    const _r817 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span")(1, "a", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_span_14_Template_a_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r817); const pr_r815 = restoredCtx.$implicit; const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r816 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return (ctx_r816.metricValues["m" + metric_r668 + "_t" + ctx_r816.terms[ctx_r816.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r816.goalScreenActive ? "_goal" : "_actual")] = pr_r815); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_span_16_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_span_16_Template(rf, ctx) { if (rf & 1) {
    const _r823 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_span_16_Template_span_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r823); const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; const period_index_r660 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().index; const ctx_r821 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4); return ctx_r821.setShowCopyPopup(metric_r668, period_index_r660); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_span_16_ng_container_1_Template, 1, 0, "ng-container", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](7);
    const _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", _r18);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 42)(1, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_div_2_Template, 3, 6, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_3_Template, 1, 5, "input", 181);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_4_Template, 1, 5, "input", 182);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_5_Template, 1, 3, "input", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_input_6_Template, 1, 2, "input", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_select_7_Template, 2, 3, "select", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 50)(9, "div", 51)(10, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 52)(13, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_span_14_Template, 2, 0, "span", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "div", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_span_16_Template, 2, 1, "span", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const metric_r668 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const period_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r672 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](15, _c5, period_r659.date > ctx_r672.today && !ctx_r672.goalScreenActive));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r672.metricTypes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (!ctx_r672.goalScreenActive || !ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal) && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].type < 2 || ctx_r672.goalScreenActive && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].goalType < 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (!ctx_r672.goalScreenActive || !ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal) && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].type == 2 || ctx_r672.goalScreenActive && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].goalType == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (!ctx_r672.goalScreenActive || !ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal) && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].type == 3 || ctx_r672.goalScreenActive && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].goalType == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (!ctx_r672.goalScreenActive || !ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal) && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].type == 4 || ctx_r672.goalScreenActive && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].goalType == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (!ctx_r672.goalScreenActive || !ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal) && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].type == 5 || ctx_r672.goalScreenActive && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].goalType == 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", (!ctx_r672.goalScreenActive || !ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal) && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].type != 4 || ctx_r672.goalScreenActive && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].hasCustomGoal && ctx_r672.metrics[ctx_r672.getMetricIndex(metric_r668)].goalType != 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", period_r659.date > ctx_r672.today && !ctx_r672.goalScreenActive || !ctx_r672.isAuthenticated || !ctx_r672.isPublic || !ctx_r672.progressBarSelected["m" + metric_r668 + "_t" + ctx_r672.terms[ctx_r672.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r672.goalScreenActive ? "_goal" : "_actual")]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Set Progress: ", ctx_r672.metricValues["m" + metric_r668 + "_t" + ctx_r672.terms[ctx_r672.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r672.goalScreenActive ? "_goal" : "_actual")] ? ctx_r672.metricValues["m" + metric_r668 + "_t" + ctx_r672.terms[ctx_r672.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r672.goalScreenActive ? "_goal" : "_actual")] : 0, "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](17, _c6));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", ctx_r672.metricValues["m" + metric_r668 + "_t" + ctx_r672.terms[ctx_r672.selectedTermIndex].period + "_p" + period_r659.dateString + (ctx_r672.goalScreenActive ? "_goal" : "_actual")], "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r672.isAuthenticated && ctx_r672.isPublic && ctx_r672.goalScreenActive);
} }
function AppComponent_div_3_div_14_div_2_div_6_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 32)(1, "div", 155);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_2_div_6_div_5_div_2_Template, 36, 23, "div", 216);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_14_div_2_div_6_div_5_div_3_Template, 6, 11, "div", 169);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_14_div_2_div_6_div_5_div_4_Template, 17, 18, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const metric_r668 = ctx.$implicit;
    const ctx_r663 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](7, _c9, ctx_r663.goalScreenActive && !ctx_r663.metrics[ctx_r663.getMetricIndex(metric_r668)].hasGoal));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("width", 90 / (ctx_r663.screenWidth > 768 ? ctx_r663.terms[ctx_r663.selectedTermIndex].metrics.length : 1), "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r663.isPublic);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r663.isPublic);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r663.goalScreenActive || ctx_r663.metrics[ctx_r663.getMetricIndex(metric_r668)].hasGoal);
} }
function AppComponent_div_3_div_14_div_2_div_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 147)(1, "div", 252)(2, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_div_14_div_2_div_6_span_3_Template, 12, 13, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_div_3_div_14_div_2_div_6_span_4_Template, 3, 4, "span", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_14_div_2_div_6_div_5_Template, 5, 9, "div", 157);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r555 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r555.terms[ctx_r555.selectedTermIndex].period === 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r555.terms[ctx_r555.selectedTermIndex].period !== 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r555.terms[ctx_r555.selectedTermIndex].metrics);
} }
function AppComponent_div_3_div_14_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r828 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 21)(1, "div", 22)(2, "div", 214);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("cdkDropListDropped", function AppComponent_div_3_div_14_div_2_Template_div_cdkDropListDropped_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r828); const ctx_r827 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r827.dropMetrics($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 145);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "\u00A0");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, AppComponent_div_3_div_14_div_2_div_5_Template, 4, 3, "div", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_14_div_2_div_6_Template, 6, 3, "div", 215);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const ctx_r544 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r544.terms[ctx_r544.selectedTermIndex].metrics);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r544.periods);
} }
function AppComponent_div_3_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 209);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_14_div_1_Template, 9, 3, "div", 210);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_14_div_2_Template, 7, 2, "div", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r112 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r112.isPublic && ctx_r112.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r112.selectedTermIndex > -1 && !ctx_r112.settingsScreenActive);
} }
function AppComponent_div_3_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 208)(1, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Add a term by clicking plus(+) icon from above menu.");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_div_3_div_16_div_8_input_7_Template(rf, ctx) { if (rf & 1) {
    const _r836 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_16_div_8_input_7_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r836); const meta_r831 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return meta_r831.value = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const meta_r831 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r832 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", meta_r831.value)("readOnly", !ctx_r832.isAuthenticated);
} }
function AppComponent_div_3_div_16_div_8_select_8_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const option_r839 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](option_r839);
} }
function AppComponent_div_3_div_16_div_8_select_8_Template(rf, ctx) { if (rf & 1) {
    const _r842 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "select", 106);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function AppComponent_div_3_div_16_div_8_select_8_Template_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r842); const meta_r831 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return meta_r831.value = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_3_div_16_div_8_select_8_option_1_Template, 2, 1, "option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const meta_r831 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r833 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", meta_r831.value)("disabled", !ctx_r833.isAuthenticated);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", meta_r831.options);
} }
function AppComponent_div_3_div_16_div_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 257)(1, "div", 252)(2, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 32)(5, "div", 42)(6, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AppComponent_div_3_div_16_div_8_input_7_Template, 1, 2, "input", 103);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AppComponent_div_3_div_16_div_8_select_8_Template, 2, 3, "select", 104);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
} if (rf & 2) {
    const meta_r831 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", meta_r831.viewOnly);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](meta_r831.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", meta_r831.type == 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", meta_r831.type != 0);
} }
function AppComponent_div_3_div_16_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 257)(1, "div", 252)(2, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} if (rf & 2) {
    const meta_r844 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("hidden", !meta_r844.viewOnly);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](meta_r844.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", meta_r844.value, " ");
} }
function AppComponent_div_3_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 255)(1, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "User Settings");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 22)(4, "div", 147)(5, "div", 32)(6, "div", 21)(7, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AppComponent_div_3_div_16_div_8_Template, 9, 4, "div", 256);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 32)(10, "div", 21)(11, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, AppComponent_div_3_div_16_div_12_Template, 6, 3, "div", 256);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()()();
} if (rf & 2) {
    const ctx_r114 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r114.usermeta);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r114.usermeta);
} }
const _c11 = function (a0) { return { "public": a0 }; };
const _c12 = function (a0) { return { "boardroomContainer": a0 }; };
const _c13 = function (a0, a1, a2) { return { "active": a0, "success": a1, "error": a2 }; };
function AppComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r846 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 66, 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function AppComponent_div_3_Template_div_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r846); const ctx_r845 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r845.hideProgressBars(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_3_div_2_Template, 31, 15, "div", 68);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 69)(4, "div", 70)(5, "div", 71);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_div_3_div_6_Template, 2, 1, "div", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, AppComponent_div_3_div_7_Template, 14, 14, "div", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AppComponent_div_3_div_8_Template, 7, 7, "div", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, AppComponent_div_3_div_11_Template, 4, 2, "div", 75);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, AppComponent_div_3_div_12_Template, 3, 1, "div", 75);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, AppComponent_div_3_div_13_Template, 46, 25, "div", 76);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, AppComponent_div_3_div_14_Template, 3, 2, "div", 77);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, AppComponent_div_3_div_15_Template, 3, 0, "div", 78);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, AppComponent_div_3_div_16_Template, 13, 2, "div", 79);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](15, _c11, ctx_r3.isPublic));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.showCopyPopup);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](17, _c12, ctx_r3.theme === "boardroom" || ctx_r3.theme === "outcomes"));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.userSettingOnMainPage.length > 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r3.isPublic && ctx_r3.selectedTermIndex > -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.isPublic && ctx_r3.selectedTermIndex > -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction3"](19, _c13, ctx_r3.notification.text, ctx_r3.notification.type == 1, ctx_r3.notification.type == 0));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r3.notification.text);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r3.settingsScreenActive && !ctx_r3.isPublic);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r3.settingsScreenActive && ctx_r3.isPublic && ctx_r3.theme == "default");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", (ctx_r3.theme == "boardroom" || ctx_r3.theme == "outcomes") && !ctx_r3.settingsScreenActive);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.theme == "default");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r3.isPublic && ctx_r3.selectedTermIndex == -1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.settingsScreenActive);
} }
function AppComponent_ng_template_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 258)(1, "rect", 259);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "animate", 260)(3, "animate", 261);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "rect", 262);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "animate", 263)(6, "animate", 264);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "rect", 265);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](8, "animate", 266)(9, "animate", 267);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_ng_template_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 268);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "polygon", 269);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_ng_template_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 270)(1, "style", 271);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " .st0{fill:#FFFFFF;}\n");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "g");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "path", 272)(5, "path", 273)(6, "path", 274)(7, "path", 275);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_ng_template_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 276);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "path", 277);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_ng_template_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 278)(1, "g");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "path", 279);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_ng_template_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 280)(1, "style", 271);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " .st0{fill:#FFBEB6;}\n");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "path", 281)(4, "path", 282);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_ng_template_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 283)(1, "g");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "path", 284);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_ng_template_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 285);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "rect", 286)(2, "polygon", 287);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function AppComponent_ng_template_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 288)(1, "defs")(2, "style");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, ".cls-1{fill:#07bdff;}.cls-2{fill:#ffd014;}.cls-3{fill:#ff5456;}");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "g", 289)(5, "g", 290);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "path", 291)(7, "rect", 292)(8, "rect", 293);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
} }
function AppComponent_ng_template_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 294)(1, "g");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "path", 295);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
function AppComponent_ng_template_24_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "svg", 294)(1, "g");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "path", 296);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
} }
class AppComponent {
    constructor(appService, route, host, sanitizer) {
        this.appService = appService;
        this.route = route;
        this.host = host;
        this.sanitizer = sanitizer;
        this.customPatterns = {
            'V': { pattern: new RegExp('[-?0-9]') },
            '0': { pattern: new RegExp('[0-9]') }
        };
        this.title = 'Record-Component';
        this.appLoaded = false;
        this.currentScrollPosition = 0;
        this.hideGoal = false;
        this.hideVitalStats = false;
        this.hideSettings = false;
        this.showDropdown = false;
        this.terms = [];
        this.usermeta = [];
        this.periods = [];
        this.metricValues = {};
        this.progressBarSelected = {};
        this.selectedTermIndex = 0;
        this.goalTermIndex = 0;
        this.goalPeriods = [];
        this.today = new Date();
        this.processing = false;
        this.notification = {
            type: 0,
            text: ''
        };
        this.goalScreenActive = false;
        this.settingsScreenActive = false;
        this.ssoRetried = 0;
        this.ssoProcessing = false;
        this.metricTypes = [
            {
                name: 'currency',
                symbol: '$'
            },
            {
                name: 'number',
                symbol: '#'
            },
            {
                name: 'percentage',
                symbol: '%'
            },
            {
                name: 'text',
                symbol: 'T'
            },
            {
                name: 'progress',
                symbol: '✓'
            },
            {
                name: 'dropdown-list',
                symbol: '☰'
            }
        ];
        this.showToolTip = '';
        this.showTypeSelector = -1;
        this.showDecimalDigitsSelector = -1;
        this.showOptionBox = -1;
        this.showGoalTypeSelector = -1;
        this.iframeId = '';
        this.showCopyPopup = '';
        this.showCopyOffset = 0;
        this.copyOption = '0';
        this.copyOptionAmount = 0;
        this.copyOptionPercent = 0;
        this.isAuthenticated = false;
        this.isLoggedIn = true;
        this.loginMessage = '';
        this.currentPeriodIndex = -1;
        this.todayPeriodIndex = -1;
        this.todayTermIndex = 0;
        this.lastMonth = 1;
        this.goalsRequired = false;
        this.actualsRequired = false;
        this.hasPeriodGoal = false;
        this.allowCurrentPeriod = false;
        this.userSettingOnMainPage = [];
        window.addEventListener('message', data => {
            if (data.data.windowHeight) {
                this.currentScrollPosition = data.data.scrollTop - data.data.iframePosition;
                if (this.currentScrollPosition < 0) {
                    this.currentScrollPosition = 0;
                }
            }
        }, false);
        this.route.queryParams.subscribe(params => {
            if (params.url) {
                appService.setApiUrl(params.url);
            }
            else {
                return;
            }
            this.appService.checkLogin().subscribe(response => {
                if (response.status) {
                    this.isPublic = params.public ? true : false;
                    this.isAuthenticated = params.is_authenticated ? true : false;
                    this.theme = params.theme ? params.theme : 'default';
                    this.kfSso = response.kfSso ? response.kfSso : '';
                    this.kfCompany = response.kfCompany ? response.kfCompany : '';
                    this.ssoRedirect = response.ssoRedirect ? response.ssoRedirect : '';
                    if (params.primary_color) {
                        document.documentElement.style.setProperty('--primary-color', '#' + params.primary_color);
                        document.documentElement.style.setProperty('--primary-color-transparent', 'rgba(' + parseInt(params.primary_color.substring(0, 2), 16) + ',' + parseInt(params.primary_color.substring(2, 4), 16) + ',' + parseInt(params.primary_color.substring(4, 6), 16) + ',0.2)');
                    }
                    if (params.transparent) {
                        document.documentElement.style.setProperty('--background-color', 'transparent');
                    }
                    this.iframeId = 'v' + params.v;
                    this.userId = params.user_id ? params.user_id : 0;
                    if (params.hideGoal) {
                        this.hideGoal = true;
                    }
                    if (params.hasPeriodGoal) {
                        this.hasPeriodGoal = true;
                    }
                    if (params.hideVitalStats) {
                        this.hideVitalStats = true;
                    }
                    if (params.hideSettings) {
                        this.hideSettings = true;
                    }
                    if (params.compactMetrics) {
                        this.compactMetrics = JSON.parse(params.compactMetrics);
                    }
                    else {
                        this.compactMetrics = [];
                    }
                    if (params.allowCurrentPeriod) {
                        this.allowCurrentPeriod = true;
                    }
                    else {
                        this.allowCurrentPeriod = false;
                    }
                    if (params.userSettingOnMainPage) {
                        this.userSettingOnMainPage = params.userSettingOnMainPage.split(',');
                        this.userSettingOnMainPage = this.userSettingOnMainPage.map((str) => {
                            return parseInt(str);
                        });
                    }
                    else {
                        this.userSettingOnMainPage = [];
                    }
                    if (params.post) {
                        this.post = params.post;
                        this.appService.getSettings(this.post, this.userId).subscribe(response => {
                            this.goalsRequired = response.goalsRequired;
                            this.actualsRequired = response.actualsRequired;
                            if (response.terms.length > 0) {
                                this.terms = response.terms.map(term => {
                                    term.startDateUTC = new Date(Date.UTC(term.startDate.y, term.startDate.m, term.startDate.d));
                                    term.startDate = new Date(term.startDate.y, term.startDate.m, term.startDate.d, 0, 0, 0, 0);
                                    term.endDateUTC = new Date(Date.UTC(term.endDate.y, term.endDate.m, term.endDate.d));
                                    term.endDate = new Date(term.endDate.y, term.endDate.m, term.endDate.d, 0, 0, 0, 0);
                                    return term;
                                });
                            }
                            else {
                                this.terms.push(this.appService.getDefaultTerm());
                            }
                            this.usermeta = response.usermeta;
                            this.getMetrics();
                            this.setPeriods();
                            if (params.compact || this.theme === 'boardroom' || this.theme === 'outcomes') {
                                if (params.compact) {
                                    this.isCompact = true;
                                }
                                let today = new Date();
                                let i = 0;
                                let abort = false;
                                for (const term of this.terms) {
                                    if (abort) {
                                        break;
                                    }
                                    today = new Date();
                                    if (!term.period) {
                                        today = new Date(today.getFullYear(), today.getMonth(), 0, 0, 0, 0);
                                    }
                                    const periods = this.appService.getPeriods(term);
                                    periods.reverse();
                                    let j = periods.length - 1;
                                    for (const period of periods) {
                                        if (abort) {
                                            break;
                                        }
                                        if (period.date < today) {
                                            this.updateSelectedIndex(i);
                                            this.currentPeriodIndex = j;
                                            this.todayTermIndex = i;
                                            this.todayPeriodIndex = j;
                                            if (this.hasPeriodGoal) {
                                                this.goalTermIndex = i;
                                                this.goalPeriods = [period];
                                            }
                                            else {
                                                if (j === periods.length - 1 && i > 0) {
                                                    this.goalTermIndex = i - 1;
                                                }
                                                else {
                                                    this.goalTermIndex = i;
                                                }
                                                this.goalPeriods = this.appService.getPeriods(this.terms[this.goalTermIndex]);
                                            }
                                            abort = true;
                                            break;
                                        }
                                        j--;
                                    }
                                    i++;
                                    /*if (term.startDate > today || term.endDate > today){
                                      this.updateSelectedIndex(i);
                                      this.todayTermIndex = i;
                                      this.goalTermIndex = this.selectedTermIndex;
                                      let j = 0;
                                      for (const period of this.periods){
                                        if (period.date >= this.today){
                                          this.currentPeriodIndex = j - 1;
                                          this.todayPeriodIndex = j - 1;
                                          if (this.currentPeriodIndex === -1){
                                            if (i > 0){
                                              this.updateSelectedIndex(i - 1);
                                              this.currentPeriodIndex = this.periods.length - 1;
                                              this.todayPeriodIndex = this.periods.length - 1;
                                              this.todayTermIndex = i - 1;
                                              if(this.goalTermIndex > 0){
                                                this.goalTermIndex = this.goalTermIndex - 1;
                                              }
                                            }
                                          }
                                          break;
                                        }
                                        j++;
                                      }
                                    }
                                    break;*/
                                }
                                if (this.currentPeriodIndex === -1) {
                                    this.updateSelectedIndex(0);
                                    this.currentPeriodIndex = this.periods.length - 1;
                                    this.todayPeriodIndex = this.periods.length - 1;
                                    this.goalTermIndex = 0;
                                }
                            }
                        });
                        if (this.isPublic) {
                            this.appService.getValues(this.post, this.userId).subscribe(metricValues => {
                                this.metricValues = metricValues;
                            });
                        }
                    }
                    else {
                        this.terms.push(this.appService.getDefaultTerm());
                        this.getMetrics();
                        this.setPeriods();
                    }
                }
                else {
                    this.isLoggedIn = false;
                    this.loginMessage = this.sanitizer.bypassSecurityTrustHtml(response.message);
                }
            });
        });
        this.resizeObservable$ = (0,rxjs__WEBPACK_IMPORTED_MODULE_2__.fromEvent)(window, 'resize');
        this.resizeSubscription$ = this.resizeObservable$.subscribe(evt => {
            this.resizeIframe();
        });
    }
    resizeIframe() {
        if (window !== top) {
            window.top.postMessage({ iframeId: this.iframeId, iframeHeight: this.appContainer.nativeElement.offsetHeight }, location.origin);
        }
    }
    dropTerms(event) {
        (0,_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__.moveItemInArray)(this.terms, event.previousIndex, event.currentIndex);
        this.selectedTermIndex = event.currentIndex;
    }
    dropMetrics(event) {
        (0,_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__.moveItemInArray)(this.terms[this.selectedTermIndex].metrics, event.previousIndex, event.currentIndex);
    }
    updateShowToolTip(str) {
        if (this.showToolTip === str) {
            this.showToolTip = '';
        }
        else {
            this.showToolTip = str;
        }
    }
    updateShowTypeSelector(i) {
        if (this.showTypeSelector === i) {
            this.showTypeSelector = -1;
        }
        else {
            this.showTypeSelector = i;
        }
    }
    updateShowDecimalDigitsSelector(i) {
        if (this.showDecimalDigitsSelector === i) {
            this.showDecimalDigitsSelector = -1;
        }
        else {
            this.showDecimalDigitsSelector = i;
        }
    }
    updateShowOptionBox(i) {
        if (this.showOptionBox === i) {
            this.showOptionBox = -1;
        }
        else {
            this.showOptionBox = i;
        }
    }
    updateShowGoalTypeSelector(i) {
        if (this.showGoalTypeSelector === i) {
            this.showGoalTypeSelector = -1;
        }
        else {
            this.showGoalTypeSelector = i;
        }
    }
    ngOnInit() {
        this.screenWidth = window.innerWidth;
    }
    onResize(event) {
        this.screenWidth = window.innerWidth;
    }
    getMetrics() {
        /*this.metrics = [];
        this.appLoaded = true;
        return;*/
        this.appService.getMetrics(this.post).subscribe(metrics => {
            this.metrics = metrics;
            this.metrics.map((item) => {
                if (typeof item.decimalDigits === 'undefined') {
                    item.decimalDigits = 2;
                }
                return item;
            });
            this.appLoaded = true;
            setTimeout(() => {
                this.resizeIframe();
            }, 3000);
        });
    }
    addTerm() {
        this.terms.push(this.appService.getDefaultTerm());
        this.selectedTermIndex = this.terms.length - 1;
        this.setPeriods();
    }
    cloneTerm() {
        const t = JSON.parse(JSON.stringify(this.terms[this.selectedTermIndex]));
        t.id = Math.floor(Math.random() * 100000);
        this.terms.push(t);
        this.selectedTermIndex = this.selectedTermIndex + 1;
        this.setPeriods();
    }
    moveTerm(index, direction) {
        [this.terms[index], this.terms[index + direction]] = [this.terms[index + direction], this.terms[index]];
    }
    updateSelectedIndex(i) {
        this.selectedTermIndex = i;
        this.settingsScreenActive = false;
        this.setPeriods();
    }
    deleteTerm() {
        if (this.terms.length < 1) {
            return;
        }
        this.terms.splice(this.selectedTermIndex, 1);
        if (this.terms.length > this.selectedTermIndex) {
            this.selectedTermIndex++;
        }
        else {
            this.selectedTermIndex--;
        }
        this.setPeriods();
    }
    updateType(index, type) {
        this.metrics[index].type = type;
    }
    updateDecimalDigits(index, decimalDigits) {
        this.metrics[index].decimalDigits = decimalDigits;
    }
    updateGoalType(index, type) {
        this.metrics[index].goalType = type;
    }
    saveSettings() {
        if (!this.processing) {
            this.processing = true;
            this.appService.saveSettings(this.post ? this.post : 0, this.terms, this.metrics).subscribe(response => {
                this.processing = false;
                if (response.status) {
                    this.notification.type = 1;
                    this.notification.text = 'Successfully Saved!';
                    setTimeout(() => {
                        this.notification.text = '';
                    }, 3000);
                    if (!this.post) {
                        window.top.location.href = this.appService.config.url + 'wp-admin/post.php?post=' + response.post + '&action=edit';
                    }
                }
            });
        }
    }
    saveValues() {
        if (this.goalsRequired || this.actualsRequired) {
            for (const period of this.periods) {
                for (const metric of this.terms[this.selectedTermIndex].metrics) {
                    if (this.goalsRequired) {
                        if (!this.metricValues['m' + metric + '_t' + this.terms[this.selectedTermIndex].period + '_p' + period.dateString + '_goal']) {
                            this.notification.type = 1;
                            this.notification.text = 'Goals are required before actuals can be entered.';
                            this.processing = false;
                            setTimeout(() => {
                                this.notification.text = '';
                            }, 3000);
                            return;
                        }
                    }
                    if (this.actualsRequired) {
                        if (period.date < this.today && !this.metricValues['m' + metric + '_t' + this.terms[this.selectedTermIndex].period + '_p' + period.dateString + '_actual']) {
                            this.notification.type = 1;
                            this.notification.text = 'Actuals are required.';
                            this.processing = false;
                            setTimeout(() => {
                                this.notification.text = '';
                            }, 3000);
                            return;
                        }
                    }
                }
            }
        }
        if (!this.processing) {
            if (this.userSettingOnMainPage.length > 0) {
                this.saveUserSettings();
            }
            this.processing = true;
            this.appService.saveValues(this.post, this.userId, this.metricValues).subscribe(response => {
                if (response.status) {
                    this.notification.type = 1;
                    this.notification.text = 'Successfully Saved!';
                    this.processing = false;
                    setTimeout(() => {
                        this.notification.text = '';
                    }, 3000);
                }
            });
        }
    }
    setPeriods() {
        this.periods = this.appService.getPeriods(this.terms[this.selectedTermIndex]);
        this.today = new Date();
        if (!this.terms[this.selectedTermIndex].period) {
            this.today = new Date(this.today.getFullYear(), this.today.getMonth(), 0, 0, 0, 0);
        }
        if (this.allowCurrentPeriod) {
            this.today = new Date(this.today.getFullYear(), this.today.getMonth() + 1, 1, 0, 0, 0);
        }
    }
    addMetric(index = -1) {
        if (index === -1) {
            /*const id = Math.floor(Math.random() * 100000);
            this.metrics.push({
              id: id,
              title: 'Test Title',
              type: 0,
              hasGoal: false,
              editable: false
            });
            this.terms[this.selectedTermIndex].metrics.push( id );
            this.showDropdown = false;
            return;*/
            this.appService.newMetric().subscribe(response => {
                if (response.status) {
                    this.metrics.push(response.metric);
                    this.terms[this.selectedTermIndex].metrics.push(response.metric.id);
                }
            });
        }
        else {
            this.terms[this.selectedTermIndex].metrics.push(this.metrics[index].id);
        }
        this.showDropdown = false;
    }
    deleteMetric(index) {
        if (confirm('Are you sure you want to delete this metric?')) {
            this.terms[this.selectedTermIndex].metrics.splice(index, 1);
        }
    }
    getMetricIndex(id) {
        return this.metrics.findIndex(function (metric) {
            return metric.id === id;
        });
    }
    toggleGoals(value) {
        this.goalScreenActive = value;
    }
    toggleSettings() {
        this.settingsScreenActive = !this.settingsScreenActive;
    }
    saveUserSettings() {
        if (!this.processing) {
            this.processing = true;
            this.appService.saveUserSettings(this.usermeta, this.userId).subscribe(response => {
                if (response.status) {
                    this.notification.type = 1;
                    this.notification.text = 'Successfully Saved!';
                    this.processing = false;
                    setTimeout(() => {
                        this.notification.text = '';
                    }, 3000);
                }
            });
        }
    }
    hideProgressBars() {
        const target = event.target;
        if (!target.classList.contains('progress-input-field') && !target.classList.contains('progress-input') && !target.closest('.progress-input')) {
            this.progressBarSelected = {};
        }
        if (target.classList.contains('tooltip') && !target.closest('.tooltip')) {
            this.showToolTip = '';
        }
    }
    setShowCopyPopup(metric, offset) {
        this.showCopyPopup = metric;
        this.showCopyOffset = offset;
    }
    applyFormula() {
        let i = 0;
        for (const period of this.periods) {
            i++;
            if (i <= this.showCopyOffset + 1) {
                continue;
            }
            let val = 0;
            let out = '';
            switch (this.copyOption) {
                case '0':
                    val = this.metricValues['m' + this.showCopyPopup + '_t' +
                        this.terms[this.selectedTermIndex].period + '_p' + this.periods[this.showCopyOffset].dateString + '_goal'];
                    out = val + '';
                    break;
                case '1':
                    val = Number(this.metricValues['m' + this.showCopyPopup + '_t' + this.terms[this.selectedTermIndex].period + '_p' + this.periods[i - 2].dateString + '_goal']) + Number(this.copyOptionAmount);
                    out = Number(val.toFixed(2)).toString();
                    break;
                case '2':
                    val = Number(this.metricValues['m' + this.showCopyPopup + '_t' + this.terms[this.selectedTermIndex].period + '_p' + this.periods[i - 2].dateString + '_goal']);
                    val = val + val * Number(this.copyOptionPercent) / 100;
                    out = Number(val.toFixed(2)).toString();
                    break;
            }
            this.metricValues['m' + this.showCopyPopup + '_t' + this.terms[this.selectedTermIndex].period + '_p' + period.dateString + '_goal'] = out;
        }
        this.showCopyPopup = '';
    }
    toggleProgressBar(key) {
        const temp = this.progressBarSelected[key];
        this.progressBarSelected = {};
        this.progressBarSelected[key] = !temp;
    }
    ssoLogin() {
        this.ssoProcessing = true;
        this.appService.ssoLogin(this.kfSso, this.kfCompany).subscribe(response => {
            this.ssoProcessing = false;
            window.open(this.ssoRedirect, '_blank');
        }, error => {
            this.appService.checkLogin().subscribe(response => {
                if (response.status) {
                    this.kfSso = response.kfSso ? response.kfSso : '';
                    this.kfCompany = response.kfCompany ? response.kfCompany : '';
                    this.ssoRedirect = response.ssoRedirect ? response.ssoRedirect : '';
                    if (this.ssoRetried < 4) {
                        this.ssoRetried++;
                        this.ssoLogin();
                    }
                }
                else {
                    this.isLoggedIn = false;
                    this.loginMessage = this.sanitizer.bypassSecurityTrustHtml(response.message);
                }
            });
        });
    }
    hasPreviousPeriod() {
        return (this.theme != 'boardroom' || this.lastMonth < 5) && (this.currentPeriodIndex > 0 || this.selectedTermIndex < this.terms.length - 1);
    }
    hasNextPeriod() {
        return this.periods.length - 1 > this.currentPeriodIndex &&
            this.periods[this.currentPeriodIndex + 1].date < this.today ||
            (this.periods.length - 1) === this.currentPeriodIndex &&
                this.selectedTermIndex > 0 &&
                this.today > this.appService.getPeriods(this.terms[this.selectedTermIndex - 1])[0].date;
    }
    setPreviousPeriodIndex() {
        if (this.hasPreviousPeriod()) {
            this.lastMonth++;
            if (this.currentPeriodIndex > 0) {
                this.currentPeriodIndex--;
                if (this.hasPeriodGoal) {
                    this.goalPeriods = [this.periods[this.currentPeriodIndex]];
                }
            }
            else {
                this.updateSelectedIndex(this.selectedTermIndex + 1);
                this.currentPeriodIndex = this.periods.length - 1;
                if (this.hasPeriodGoal) {
                    this.goalTermIndex = this.selectedTermIndex;
                    this.goalPeriods = [this.periods[this.currentPeriodIndex]];
                }
            }
        }
    }
    setNextPeriodIndex() {
        if (this.hasNextPeriod()) {
            this.lastMonth--;
            if (this.periods.length - 1 > this.currentPeriodIndex) {
                this.currentPeriodIndex++;
                if (this.hasPeriodGoal) {
                    this.goalPeriods = [this.periods[this.currentPeriodIndex]];
                }
            }
            else {
                this.updateSelectedIndex(this.selectedTermIndex - 1);
                this.currentPeriodIndex = 0;
                if (this.hasPeriodGoal) {
                    this.goalTermIndex = this.selectedTermIndex;
                    this.goalPeriods = [this.periods[this.currentPeriodIndex]];
                }
            }
        }
    }
    missingValues() {
        let emptyPeriods = [];
        let currentPeriodIndex = this.todayPeriodIndex;
        let selectedTermIndex = this.todayTermIndex;
        let count = 0;
        while (currentPeriodIndex > 0 || selectedTermIndex < this.terms.length - 1) {
            if (currentPeriodIndex > 0) {
                currentPeriodIndex--;
            }
            else {
                selectedTermIndex++;
                currentPeriodIndex = this.periods.length - 1;
            }
            const periods = this.appService.getPeriods(this.terms[selectedTermIndex]);
            periods.reverse();
            for (const period of periods) {
                if (period.date > this.today) {
                    continue;
                }
                count++;
                if (count > 5) {
                    continue;
                }
                for (const metric of this.terms[selectedTermIndex].metrics) {
                    if (!this.metricValues['m' + metric + '_t' + this.terms[selectedTermIndex].period + '_p' + period.dateString + '_actual']) {
                        const emptyPeriod = this.terms[selectedTermIndex].period ?
                            ('Week' + (0,_angular_common__WEBPACK_IMPORTED_MODULE_4__.formatDate)(period.date, 'W', 'en-US') +
                                ((0,_angular_common__WEBPACK_IMPORTED_MODULE_4__.formatDate)(period.date, 'dd', 'en-US') +
                                    (period.date.getMonth() !== period.dateEnd.getMonth() ?
                                        (0,_angular_common__WEBPACK_IMPORTED_MODULE_4__.formatDate)(period.date, 'MMM', 'en-US') :
                                        '') +
                                    (' - ' +
                                        (0,_angular_common__WEBPACK_IMPORTED_MODULE_4__.formatDate)(period.dateEnd, 'dd MMM', 'en-US')))) :
                            (0,_angular_common__WEBPACK_IMPORTED_MODULE_4__.formatDate)(period.date, 'MMM yyyy', 'en-US');
                        if (emptyPeriods.indexOf(emptyPeriod) === -1) {
                            emptyPeriods.push(emptyPeriod);
                        }
                        break;
                    }
                }
            }
        }
        return emptyPeriods.join(', ');
    }
    convertToOptions(text) {
        return text ? text.split('\n').map(s => s.trim()) : [];
    }
    updateGoals(metric, periodType, dateString) {
        console.log(periodType);
        if (!this.hasPeriodGoal) {
            for (const period of this.goalPeriods) {
                if (period.dateString !== dateString) {
                    this.metricValues['m' + metric + '_t' + periodType + '_p' + period.dateString + '_goal'] =
                        this.metricValues['m' + metric + '_t' + periodType + '_p' + dateString + '_goal'];
                }
            }
        }
    }
    toNumber(a) {
        return Number(a);
    }
    ngAfterViewInit() {
        this.metricsLoop.changes.subscribe(t => {
            this.resizeIframe();
        });
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_services_app_service__WEBPACK_IMPORTED_MODULE_0__.AppService), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__.DomSanitizer)); };
AppComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], viewQuery: function AppComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c0, 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c1, 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c2, 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c3, 5);
    } if (rf & 2) {
        let _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.appContainer = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.settingContainerAdmin = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.settingContainerPublic = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.metricsLoop = _t);
    } }, hostBindings: function AppComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("resize", function AppComponent_resize_HostBindingHandler($event) { return ctx.onResize($event); }, false, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵresolveWindow"])("click", function AppComponent_click_HostBindingHandler($event) { return ctx.hideProgressBars($event); });
    } }, decls: 26, vars: 4, consts: [["class", "notification error active", 3, "innerHTML", 4, "ngIf"], [4, "ngIf"], ["class", "widget public", 4, "ngIf"], ["class", "form-container", 3, "ngClass", "click", 4, "ngIf"], ["loadingIcon", ""], ["closeIcon", ""], ["trashIcon", ""], ["settingsIcon", ""], ["plusIcon", ""], ["cloneIcon", ""], ["editIcon", ""], ["copyIcon", ""], ["vitalstatsIcon", ""], ["angleLeftIcon", ""], ["angleRightIcon", ""], [1, "notification", "error", "active", 3, "innerHTML"], [4, "ngTemplateOutlet"], [1, "widget", "public"], [1, "header", "setting", "clearfix"], ["class", "update", 4, "ngIf"], [1, "content"], [1, "divTable"], [1, "divTableBody"], ["class", "divTableRow clearfix", 3, "hidden", 4, "ngFor", "ngForOf"], [1, "footer", "setting", "clearfix"], [1, "left-btn", 3, "click"], [1, "right-btn", 3, "click"], ["class", "update primary-btn", 4, "ngIf"], [1, "update"], ["type", "button", 3, "click"], ["type", "button", 2, "margin-left", "10px !important", 3, "click"], [1, "divTableRow", "clearfix", 3, "hidden"], [1, "divTableCell"], [1, "heading"], ["type", "text", 3, "readonly", "ngModel", "placeholder", "ngModelChange", 4, "ngIf"], ["class", "editIcon", 4, "ngIf"], ["class", "metric-settings goal-title", 4, "ngIf"], ["class", "textmetric", 3, "class", 4, "ngIf"], ["type", "text", 3, "readonly", "ngModel", "placeholder", "ngModelChange"], [1, "editIcon"], [1, "metric-settings", "goal-title"], [1, "tooltip", 3, "tooltip"], [1, "textmetric"], [1, "inn-metric"], [3, "tooltip", "class", "hidden", 4, "ngFor", "ngForOf"], ["type", "text", "thousandSeparator", ",", "pattern", "[0-9]*", "inputmode", "numeric", 3, "readonly", "disabled", "ngModel", "mask", "ngModelChange", 4, "ngIf"], ["type", "text", "suffix", "%", 3, "readonly", "disabled", "ngModel", "mask", "ngModelChange", 4, "ngIf"], ["type", "text", 3, "readonly", "disabled", "ngModel", "ngModelChange", 4, "ngIf"], ["type", "text", "readonly", "", "mask", "percent.1", "suffix", "%", "class", "progress-input-field", "pattern", "[0-9]*", "inputmode", "numeric", 3, "disabled", "ngModel", "ngModelChange", "click", 4, "ngIf"], ["mask", "percent.1", "suffix", "%", "class", "progress-input-field", 3, "disabled", "ngModel", "ngModelChange", "click", 4, "ngIf"], [1, "progress-input", 3, "hidden"], [1, "progress-bar", 3, "hidden"], [1, "progress-bar-bg"], [1, "progress-bar-selector"], [4, "ngFor", "ngForOf"], [1, "progress-bar-filling"], ["class", "copyToAll", "tooltip", "Apply formula to future dates", 3, "click", 4, "ngIf"], [3, "tooltip", "hidden"], ["type", "text", "thousandSeparator", ",", "pattern", "[0-9]*", "inputmode", "numeric", 3, "readonly", "disabled", "ngModel", "mask", "ngModelChange"], ["type", "text", "suffix", "%", 3, "readonly", "disabled", "ngModel", "mask", "ngModelChange"], ["type", "text", 3, "readonly", "disabled", "ngModel", "ngModelChange"], ["type", "text", "readonly", "", "mask", "percent.1", "suffix", "%", "pattern", "[0-9]*", "inputmode", "numeric", 1, "progress-input-field", 3, "disabled", "ngModel", "ngModelChange", "click"], ["mask", "percent.1", "suffix", "%", 1, "progress-input-field", 3, "disabled", "ngModel", "ngModelChange", "click"], [3, "click"], ["tooltip", "Apply formula to future dates", 1, "copyToAll", 3, "click"], [1, "update", "primary-btn"], [1, "form-container", 3, "ngClass", "click"], ["app", ""], ["class", "copyPopup", 3, "top", "height", 4, "ngIf"], [1, "main-container", 3, "ngClass"], [1, "container-fluid"], [1, "inner-container"], ["class", "divTableRow mainpage-user-settings", 4, "ngIf"], ["class", "setting clearfix", 3, "top", 4, "ngIf"], [1, "notification"], ["class", "tabs-nav clearfix", 4, "ngIf"], ["class", "clearfix boardroom", 4, "ngIf"], ["class", "clearfix", 4, "ngIf"], ["class", "alert", 4, "ngIf"], ["class", "divTable settingsTable", 4, "ngIf"], [1, "copyPopup"], [1, "bg"], [1, "copyPopupContent"], [1, "copyPopupHeader"], [1, "close"], [1, "copyPopupBody"], [1, "copyPopupOptions", "clearfix"], ["type", "radio", "value", "0", 3, "ngModel", "ngModelChange"], ["type", "text", "mask", "percent.1", "suffix", "%", 3, "ngModel", "allowNegativeNumbers", "ngModelChange", 4, "ngIf"], ["type", "text", "mask", "separator.2", "thousandSeparator", ",", 3, "ngModel", "allowNegativeNumbers", "ngModelChange", 4, "ngIf"], ["type", "radio", "value", "1", 3, "ngModel", "ngModelChange"], ["type", "text", 3, "ngModel", "ngModelChange"], ["type", "radio", "value", "2", 3, "ngModel", "ngModelChange"], ["type", "text", "mask", "percent.1", "suffix", "%", 3, "ngModel", "ngModelChange"], [1, "copyPopupFooter"], [1, "copyPopupSave", 3, "click"], [1, "copyPopupCancel", 3, "click"], ["type", "text", "mask", "percent.1", "suffix", "%", 3, "ngModel", "allowNegativeNumbers", "ngModelChange"], ["type", "text", "mask", "separator.2", "thousandSeparator", ",", 3, "ngModel", "allowNegativeNumbers", "ngModelChange"], [1, "divTableRow", "mainpage-user-settings"], ["class", "divTableCell", 3, "hidden", 4, "ngFor", "ngForOf"], [1, "divTableCell", 3, "hidden"], [1, "metric-icon", "text"], ["type", "text", 3, "ngModel", "readOnly", "ngModelChange", 4, "ngIf"], [3, "ngModel", "disabled", "ngModelChange", 4, "ngIf"], ["type", "text", 3, "ngModel", "readOnly", "ngModelChange"], [3, "ngModel", "disabled", "ngModelChange"], [1, "setting", "clearfix"], ["settingContainerAdmin", ""], [1, "date-box", "start-date"], ["type", "date", "placeholder", "Start Date", 3, "readOnly", "valueAsDate", "change"], [1, "date-box", "end-date"], ["type", "date", "placeholder", "End Date", 3, "readOnly", "valueAsDate", "change"], [1, "date-box", "select-box"], [3, "disabled", "ngModel", "ngModelOptions", "ngModelChange", "change"], [3, "ngValue"], ["class", "update delete", 4, "ngIf"], [1, "update", "delete"], [1, "delteTerm", 2, "text-align", "right"], ["tooltip", "Delete Reporting Period", 1, "btn-dlt", 3, "click"], ["settingContainerPublic", ""], ["class", "update left", 4, "ngIf"], [1, "update", "left"], ["class", "delteTerm dashboard-link dashboard-link-with-text", "style", "text-align: right;", "tooltip", "Dashboard", 4, "ngIf"], ["tooltip", "Dashboard", 1, "delteTerm", "dashboard-link", "dashboard-link-with-text", 2, "text-align", "right"], ["tooltip", "Dashboard", 1, "delteTerm", "dashboard-link", 2, "text-align", "right"], [1, "btn-dlt", "btn-settings", 3, "tooltip", "click"], ["type", "button", 3, "class", "click", 4, "ngIf"], [1, "tabs-nav", "clearfix"], ["cdkDropList", "", "cdkDropListOrientation", "horizontal", 3, "cdkDropListDropped"], ["cdkDrag", "", 3, "ngClass", 4, "ngFor", "ngForOf"], ["class", "plus", 4, "ngIf"], ["cdkDrag", "", 3, "ngClass"], [1, "input-sizer", 3, "click"], ["autoSizeInput", "", "type", "text", 3, "ngModel", "ngModelChange"], ["class", "clone", 4, "ngIf"], [1, "clone"], [1, "plus"], ["tooltip", "Add New Term", 3, "click"], [3, "ngClass", 4, "ngFor", "ngForOf"], [3, "ngClass"], [1, "clearfix", "boardroom"], [1, "clearfix", "hide-sm"], ["class", "period-selector-top", 4, "ngIf"], [1, "divTableRow", "hiddenrow"], [1, "divTableCell", "firstcell"], ["cdkDrag", "", "class", "divTableCell", 3, "cdkDragDisabled", 4, "ngFor", "ngForOf"], [1, "divTableRow"], [1, "firstcell", "divTableCell", "show-sm"], [1, "clearfix", "heading-sm"], [1, "firstcell", "divTableCell", "hide-sm"], ["class", "period-selector", 4, "ngIf"], ["class", "goal-header", 4, "ngIf"], ["class", "show-sm goal-header", 4, "ngIf"], [1, "divTableCell", "show-sm"], [1, "metric-hidden"], ["class", "textmetric", 4, "ngIf"], ["class", "divTableCell", 3, "width", "class", 4, "ngFor", "ngForOf"], [1, "divTableRow", "hide-sm"], ["class", "firstcell divTableCell", "style", "text-align: center", 4, "ngIf"], ["class", "alert alert-danger", 4, "ngIf"], ["class", "separator", 4, "ngIf"], ["class", "clearfix hide-sm", 4, "ngIf"], ["class", "divTable", 4, "ngIf"], [1, "period-selector-top"], [1, "period-selector"], [1, "selected-period"], ["cdkDrag", "", 1, "divTableCell", 3, "cdkDragDisabled"], ["metricsLoop", ""], ["class", "heading", 3, "tooltip", "display", "class", 4, "ngIf"], [1, "heading", 3, "tooltip", "display"], [1, "metric-settings"], ["class", "tooltip", "style", "margin-top: 0", 3, "tooltip", 4, "ngIf"], [1, "tooltip", 2, "margin-top", "0", 3, "tooltip"], [1, "goal-header"], [1, "show-sm", "goal-header"], ["class", "metric-hidden", 4, "ngIf"], ["class", "textmetric", 3, "ngClass", 4, "ngIf"], ["class", "textmetric show-sm goalView", 3, "class", 4, "ngIf"], ["class", "textmetric show-sm", 4, "ngIf"], [1, "textmetric", 3, "ngClass"], ["type", "text", "thousandSeparator", ",", "pattern", "[0-9]*", "inputmode", "numeric", 3, "readonly", "disabled", "ngModel", "mask", "allowNegativeNumbers", "ngModelChange", 4, "ngIf"], ["type", "text", "suffix", "%", 3, "readonly", "disabled", "ngModel", "mask", "allowNegativeNumbers", "ngModelChange", 4, "ngIf"], ["type", "text", "thousandSeparator", ",", "pattern", "[0-9]*", "inputmode", "numeric", 3, "readonly", "disabled", "ngModel", "mask", "allowNegativeNumbers", "ngModelChange"], ["type", "text", "suffix", "%", 3, "readonly", "disabled", "ngModel", "mask", "allowNegativeNumbers", "ngModelChange"], [1, "textmetric", "show-sm", "goalView"], ["readonly", "", 3, "value"], [1, "textmetric", "show-sm"], ["type", "text", "thousandSeparator", ",", "pattern", "[0-9]*", "inputmode", "numeric", 3, "readonly", "ngModel", "mask", "allowNegativeNumbers", "ngModelChange", "change", 4, "ngIf"], ["type", "text", "suffix", "%", "inputmode", "numeric", 3, "readonly", "ngModel", "mask", "allowNegativeNumbers", "ngModelChange", "change", 4, "ngIf"], ["type", "text", 3, "readonly", "ngModel", "ngModelChange", "change", 4, "ngIf"], ["type", "text", "readonly", "", "mask", "percent.1", "suffix", "%", "class", "progress-input-field", "pattern", "[0-9]*", "inputmode", "numeric", 3, "ngModel", "ngModelChange", "click", 4, "ngIf"], [3, "ngModel", "ngModelChange", "change", 4, "ngIf"], ["type", "text", "thousandSeparator", ",", "pattern", "[0-9]*", "inputmode", "numeric", 3, "readonly", "ngModel", "mask", "allowNegativeNumbers", "ngModelChange", "change"], ["type", "text", "suffix", "%", "inputmode", "numeric", 3, "readonly", "ngModel", "mask", "allowNegativeNumbers", "ngModelChange", "change"], ["type", "text", 3, "readonly", "ngModel", "ngModelChange", "change"], ["type", "text", "readonly", "", "mask", "percent.1", "suffix", "%", "pattern", "[0-9]*", "inputmode", "numeric", 1, "progress-input-field", 3, "ngModel", "ngModelChange", "click"], [3, "ngModel", "ngModelChange", "change"], [1, "firstcell", "divTableCell", 2, "text-align", "center"], ["class", "textmetric goalView", 4, "ngIf"], [1, "textmetric", "goalView"], ["class", "textarea", 3, "width", 4, "ngFor", "ngForOf"], [1, "textarea"], [3, "ngModel", "ngModelChange"], [1, "alert", "alert-danger"], [1, "separator"], [1, "firstcell", "divTableCell", "hide-sm", 2, "text-align", "center"], [2, "text-align", "center"], [1, "alert"], [1, "clearfix"], ["class", "add-button add-icon-container", 4, "ngIf"], [1, "add-button", "add-icon-container"], ["tooltip", "Add New Metric", 1, "add-icon", 3, "click"], [1, "dropdown", 3, "hidden"], ["cdkDropList", "", "cdkDropListOrientation", "horizontal", 1, "divTableRow", "hiddenrow", 3, "cdkDropListDropped"], ["class", "divTableRow", 4, "ngFor", "ngForOf"], ["class", "heading", 4, "ngIf"], ["placeholder", "Metric Title", 3, "readOnly", "ngModel", "ngModelChange"], [1, "checkbox-style"], ["type", "checkbox", 3, "disabled", "ngModel", "ngModelChange"], ["tooltip", "Check this box if the setting of this metric should not be saved globally.", 1, "tooltip"], ["tooltip", "Check this box if this metric has a goal as well as an actual that will be reported.", 1, "tooltip"], ["tooltip", "Check this box if this metric goal is reverse.", 1, "tooltip"], ["class", "dropdown-nav", "tooltip", "Custom Goal Type", 3, "class", "click", 4, "ngIf"], ["tooltip", "Check this box if this metric will have a name that the user submitting the data will define. Eg. Leads may be called 'Phone Calls' in their scenario.", 1, "tooltip"], [1, "button-delete"], ["tooltip", "Metric Type", 1, "dropdown-nav", 3, "click"], [1, "dropbtn"], [3, "class", "hidden", 4, "ngFor", "ngForOf"], [1, "dropdown-content", 3, "hidden"], [3, "class", "ngClass", 4, "ngFor", "ngForOf"], ["class", "dropdown-nav dropdown-dd", "tooltip", "Decimal Places", 3, "class", "click", 4, "ngIf"], ["class", "options-button", 3, "class", 4, "ngIf"], ["class", "delete", 4, "ngIf"], ["tooltip", "Check this box if this metric has a custom goal.", 1, "tooltip"], ["tooltip", "Custom Goal Type", 1, "dropdown-nav", 3, "click"], [3, "hidden"], ["placement", "right", 3, "tooltip", "click"], ["type", "text", "placeholder", "Goal Title", 1, "normal-text", 3, "disabled", "ngModel", "ngModelChange"], ["tooltip", "Enter custom Title of the goal here.", 1, "tooltip"], ["type", "text", "placeholder", "Goal Description", 1, "normal-text", 3, "disabled", "ngModel", "ngModelChange"], ["tooltip", "Enter description of custom goal here.", 1, "tooltip"], ["tooltip", "Decimal Places", 1, "dropdown-nav", "dropdown-dd", 3, "click"], [1, "tag", "active"], [1, "tag", 3, "ngClass"], ["tooltip", "No Decimal", "placement", "right", 3, "click"], ["tooltip", "One Decimal Digit", "placement", "right", 3, "click"], ["tooltip", "Two Decimal Digits", "placement", "right", 3, "click"], [1, "options-button"], [1, "options-input"], [1, "delete"], ["tooltip", "Delete", 3, "click"], [1, "firstcell", "divTableCell"], ["placeholder", "Metric Title", 3, "readonly", "ngModel", "ngModelChange"], [1, "dropdown-nav", 3, "click"], [1, "divTable", "settingsTable"], ["class", "divTableRow", 3, "hidden", 4, "ngFor", "ngForOf"], [1, "divTableRow", 3, "hidden"], ["xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "width", "128px", "height", "128px", "viewBox", "0 0 100 100", "preserveAspectRatio", "xMidYMid", 2, "margin", "auto", "background", "none", "display", "block", "shape-rendering", "auto"], ["x", "14.5", "y", "27", "width", "21", "height", "46", "fill", "#ff5455"], ["attributeName", "y", "repeatCount", "indefinite", "dur", "1s", "calcMode", "spline", "keyTimes", "0;0.5;1", "values", "8.600000000000001;27;27", "keySplines", "0 0.5 0.5 1;0 0.5 0.5 1", "begin", "-0.2s"], ["attributeName", "height", "repeatCount", "indefinite", "dur", "1s", "calcMode", "spline", "keyTimes", "0;0.5;1", "values", "82.8;46;46", "keySplines", "0 0.5 0.5 1;0 0.5 0.5 1", "begin", "-0.2s"], ["x", "39.5", "y", "27", "width", "21", "height", "46", "fill", "#ffd012"], ["attributeName", "y", "repeatCount", "indefinite", "dur", "1s", "calcMode", "spline", "keyTimes", "0;0.5;1", "values", "13.199999999999996;27;27", "keySplines", "0 0.5 0.5 1;0 0.5 0.5 1", "begin", "-0.1s"], ["attributeName", "height", "repeatCount", "indefinite", "dur", "1s", "calcMode", "spline", "keyTimes", "0;0.5;1", "values", "73.60000000000001;46;46", "keySplines", "0 0.5 0.5 1;0 0.5 0.5 1", "begin", "-0.1s"], ["x", "64.5", "y", "27", "width", "21", "height", "46", "fill", "#00c6fc"], ["attributeName", "y", "repeatCount", "indefinite", "dur", "1s", "calcMode", "spline", "keyTimes", "0;0.5;1", "values", "13.199999999999996;27;27", "keySplines", "0 0.5 0.5 1;0 0.5 0.5 1"], ["attributeName", "height", "repeatCount", "indefinite", "dur", "1s", "calcMode", "spline", "keyTimes", "0;0.5;1", "values", "73.60000000000001;46;46", "keySplines", "0 0.5 0.5 1;0 0.5 0.5 1"], ["version", "1.1", "id", "Capa_1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 490 490", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 490 490"], ["points", "386.813,0 245,141.812 103.188,0 0,103.188 141.813,245 0,386.812 103.187,489.999 245,348.187 386.813,490\n\t490,386.812 348.187,244.999 490,103.187 ", 1, "st0"], ["version", "1.1", "id", "Layer_1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 512 512", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 512 512"], ["type", "text/css"], ["d", "M424,64h-88V48c0-26.5-21.5-48-48-48h-64c-26.5,0-48,21.5-48,48v16H88c-22.1,0-40,17.9-40,40v56\n\t\tc0,8.8,7.2,16,16,16h8.7l13.8,290.3c1.2,25.6,22.3,45.7,47.9,45.7h243c25.7,0,46.7-20.1,47.9-45.7L439.3,176h8.7\n\t\tc8.8,0,16-7.2,16-16v-56C464,81.9,446.1,64,424,64L424,64z M208,48c0-8.8,7.2-16,16-16h64c8.8,0,16,7.2,16,16v16h-96V48z M80,104\n\t\tc0-4.4,3.6-8,8-8h336c4.4,0,8,3.6,8,8v40c-4.9,0-331.6,0-352,0V104z M393.5,464.8c-0.4,8.5-7.4,15.2-16,15.2h-243\n\t\tc-8.6,0-15.6-6.7-16-15.2L104.8,176h302.4L393.5,464.8z", 1, "st0"], ["d", "M256,448c8.8,0,16-7.2,16-16V224c0-8.8-7.2-16-16-16s-16,7.2-16,16v208C240,440.8,247.2,448,256,448z", 1, "st0"], ["d", "M336,448c8.8,0,16-7.2,16-16V224c0-8.8-7.2-16-16-16s-16,7.2-16,16v208C320,440.8,327.2,448,336,448z", 1, "st0"], ["d", "M176,448c8.8,0,16-7.2,16-16V224c0-8.8-7.2-16-16-16s-16,7.2-16,16v208C160,440.8,167.2,448,176,448z", 1, "st0"], ["version", "1.1", "id", "Capa_1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 294 294", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 294 294"], ["d", "M279.333,117h-18.178C258.423,107,254.3,96.814,249,87.747l13.383-13.361c5.857-5.857,5.858-15.344,0-21.202l-20.737-20.733\n\tc-2.813-2.813-6.628-4.391-10.606-4.391c-3.978,0-7.794,1.581-10.606,4.395l-13.516,13.254c-9.308-5.519-19.417-9.796-30.417-12.591\n\tV15c0-8.284-6.191-15-14.476-15h-29.327C124.413,0,117.5,6.716,117.5,15v18.118c-10,2.684-19.944,6.729-28.944,11.933L76.168,32.613\n\tc-5.857-5.857-15.331-5.857-21.188,0L34.254,53.351c-5.858,5.857-5.852,15.355,0.006,21.213l12.117,12.071\n\tC40.759,96.008,36.411,106,33.567,117H15.833C7.549,117,0.5,123.802,0.5,132.086v29.328C0.5,169.698,7.549,176,15.833,176h17.613\n\tc2.794,11,7.094,21.211,12.652,30.584l-11.989,12.092c-5.858,5.857-5.858,15.407,0,21.265l20.737,20.763\n\tc2.929,2.929,6.768,4.406,10.606,4.406s7.678-1.458,10.606-4.387l12.02-12.115c9.133,5.338,18.421,9.487,29.421,12.218V278.5\n\tc0,8.284,6.913,15.5,15.197,15.5h29.327c8.284,0,14.476-7.216,14.476-15.5v-17.673c11-2.843,21.456-7.217,30.894-12.874\n\tl13.065,12.934c2.929,2.929,6.833,4.394,10.672,4.394s7.711-1.465,10.639-4.394l20.754-20.737\n\tc5.858-5.857,5.867-15.355,0.008-21.213l-13.254-13.465c5.242-9.065,9.314-18.471,11.997-29.471h18.058\n\tc8.284,0,14.167-6.302,14.167-14.586v-29.328C293.5,123.802,287.617,117,279.333,117z M191.016,146.973\n\tc0,24.071-19.583,43.654-43.654,43.654s-43.654-19.583-43.654-43.654s19.583-43.654,43.654-43.654S191.016,122.901,191.016,146.973z\n\t"], ["version", "1.1", "id", "Capa_1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "width", "349.03px", "height", "349.031px", "viewBox", "0 0 349.03 349.031", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 349.03 349.031"], ["d", "M349.03,141.226v66.579c0,5.012-4.061,9.079-9.079,9.079H216.884v123.067c0,5.019-4.067,9.079-9.079,9.079h-66.579\n\t\tc-5.009,0-9.079-4.061-9.079-9.079V216.884H9.079c-5.016,0-9.079-4.067-9.079-9.079v-66.579c0-5.013,4.063-9.079,9.079-9.079\n\t\th123.068V9.079c0-5.018,4.069-9.079,9.079-9.079h66.579c5.012,0,9.079,4.061,9.079,9.079v123.068h123.067\n\t\tC344.97,132.147,349.03,136.213,349.03,141.226z"], ["version", "1.1", "id", "Layer_1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 6.2 6.2", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 6.2 6.2"], ["d", "M3.5,4.6H1.1C0.5,4.6,0,4.1,0,3.5c0,0,0,0,0,0V1.1C0,0.5,0.5,0,1,0c0,0,0,0,0,0h2.5c0.6,0,1.1,0.5,1.1,1\n\tc0,0,0,0,0,0v2.5C4.6,4.1,4.1,4.6,3.5,4.6C3.5,4.6,3.5,4.6,3.5,4.6z M1.1,0.7c-0.2,0-0.4,0.2-0.4,0.4v2.5c0,0.2,0.2,0.4,0.4,0.4h0\n\th2.5c0.2,0,0.4-0.2,0.4-0.4v0V1.1c0-0.2-0.2-0.4-0.4-0.4l0,0H1.1z", 1, "st0"], ["d", "M5.1,6.2H2.6c-0.6,0-1-0.5-1-1.1V2.6c0-0.6,0.4-1,1-1c0,0,0,0,0.1,0h2.4c0.6,0,1,0.4,1.1,1v2.5\n\tC6.2,5.7,5.7,6.2,5.1,6.2z M2.6,2.3c-0.2,0-0.3,0.2-0.3,0.3v2.5c0,0.2,0.2,0.3,0.3,0.3h2.5c0.2,0,0.3-0.2,0.3-0.3V2.6\n\tc0-0.2-0.2-0.3-0.3-0.3L2.6,2.3z", 1, "st0"], ["version", "1.1", "id", "Capa_1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "width", "528.899px", "height", "528.899px", "viewBox", "0 0 528.899 528.899", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 528.899 528.899"], ["d", "M328.883,89.125l107.59,107.589l-272.34,272.34L56.604,361.465L328.883,89.125z M518.113,63.177l-47.981-47.981\n\t\tc-18.543-18.543-48.653-18.543-67.259,0l-45.961,45.961l107.59,107.59l53.611-53.611\n\t\tC532.495,100.753,532.495,77.559,518.113,63.177z M0.3,512.69c-1.958,8.812,5.998,16.708,14.811,14.565l119.891-29.069\n\t\tL27.473,390.597L0.3,512.69z"], ["version", "1.1", "id", "Layer_1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 512.016 512.016", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 512.016 512.016"], ["x", "225.937", "y", "0.235", "transform", "matrix(1 -0.0087 0.0087 1 -2.038 2.2179)", "width", "55.966", "height", "470.269", 2, "fill", "#999999"], ["points", "256.008,512.016 64.291,328.686 102.97,288.248 256.008,434.586 409.046,288.248\n\t447.725,328.686 ", 2, "fill", "#1A1718"], ["xmlns", "http://www.w3.org/2000/svg", "viewBox", "0 0 412.44 381.36"], ["id", "Layer_2", "data-name", "Layer 2"], ["id", "Layer_1-2", "data-name", "Layer 1"], ["d", "M285.32,381.36h0A127.12,127.12,0,0,0,412.44,254.24V0H285.32Z", 1, "cls-1"], ["x", "142.66", "y", "127.12", "width", "127.12", "height", "254.24", "transform", "translate(412.44 508.49) rotate(-180)", 1, "cls-2"], ["y", "254.24", "width", "127.12", "height", "127.12", "transform", "translate(127.12 635.61) rotate(-180)", 1, "cls-3"], ["version", "1.1", "id", "Capa_1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 123.969 123.97", 0, "xml", "space", "preserve", 2, "enable-background", "new 0 0 123.969 123.97"], ["d", "M96.059,24.603c5.799-5.801,5.699-15.301-0.5-20.9c-5.801-5.3-14.801-4.8-20.301,0.8l-47.4,47.3\n\t\tc-2.8,2.801-4.2,6.5-4.2,10.2s1.4,7.4,4.2,10.2l47.3,47.3c5.5,5.5,14.6,6.101,20.3,0.8c6.101-5.6,6.3-15.1,0.5-20.899l-30.2-30.3\n\t\tc-3.9-3.9-3.9-10.2,0-14.101L96.059,24.603z"], ["d", "M27.961,99.367c-5.8,5.8-5.7,15.3,0.5,20.899c5.8,5.301,14.8,4.801,20.3-0.8l47.3-47.3c2.8-2.8,4.2-6.5,4.2-10.2\n\t\ts-1.4-7.399-4.2-10.2l-47.2-47.3c-5.5-5.5-14.6-6.1-20.3-0.8c-6.1,5.6-6.3,15.1-0.5,20.9l30.2,30.399c3.9,3.9,3.9,10.2,0,14.101\n\t\tL27.961,99.367z"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, AppComponent_span_0_Template, 1, 1, "span", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, AppComponent_div_1_Template, 2, 1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_div_2_Template, 17, 20, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, AppComponent_div_3_Template, 17, 23, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_ng_template_4_Template, 10, 0, "ng-template", null, 4, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, AppComponent_ng_template_6_Template, 2, 0, "ng-template", null, 5, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, AppComponent_ng_template_8_Template, 8, 0, "ng-template", null, 6, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, AppComponent_ng_template_10_Template, 2, 0, "ng-template", null, 7, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, AppComponent_ng_template_12_Template, 3, 0, "ng-template", null, 8, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, AppComponent_ng_template_14_Template, 5, 0, "ng-template", null, 9, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, AppComponent_ng_template_16_Template, 3, 0, "ng-template", null, 10, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](18, AppComponent_ng_template_18_Template, 3, 0, "ng-template", null, 11, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](20, AppComponent_ng_template_20_Template, 9, 0, "ng-template", null, 12, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](22, AppComponent_ng_template_22_Template, 3, 0, "ng-template", null, 13, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](24, AppComponent_ng_template_24_Template, 3, 0, "ng-template", null, 14, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx.isLoggedIn);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx.appLoaded && ctx.isLoggedIn);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.appLoaded && ctx.isLoggedIn && ctx.isCompact);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.appLoaded && ctx.isLoggedIn && !ctx.isCompact);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgTemplateOutlet, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgForOf, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.NgModel, ng2_tooltip_directive__WEBPACK_IMPORTED_MODULE_8__.TooltipDirective, ngx_mask__WEBPACK_IMPORTED_MODULE_9__.MaskDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.PatternValidator, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.SelectControlValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.NgSelectOption, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ɵNgSelectMultipleOption"], _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgClass, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.RadioControlValueAccessor, _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__.CdkDropList, _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__.CdkDrag, ngx_autosize_input__WEBPACK_IMPORTED_MODULE_10__.AutoSizeInputDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.CheckboxControlValueAccessor], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.DatePipe], styles: ["@import url(\"https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap\");\ninput[_ngcontent-%COMP%]::-webkit-outer-spin-button, input[_ngcontent-%COMP%]::-webkit-inner-spin-button {\n  -webkit-appearance: none;\n  margin: 0;\n}\n\ninput[type=number][_ngcontent-%COMP%] {\n  -moz-appearance: textmetric;\n}\n.clearfix[_ngcontent-%COMP%]:after {\n  content: \"\";\n  display: block;\n  clear: both;\n}\nbody[_ngcontent-%COMP%] {\n  margin: 0 !important;\n  font-family: Montserrat;\n}\nh1[_ngcontent-%COMP%], h2[_ngcontent-%COMP%], h3[_ngcontent-%COMP%], h4[_ngcontent-%COMP%], h5[_ngcontent-%COMP%], h6[_ngcontent-%COMP%], p[_ngcontent-%COMP%] {\n  margin: 0px;\n}\n.btn-dlt[_ngcontent-%COMP%] {\n  background: var(--primary-color);\n  border-radius: 6px;\n  padding: 10px 15px;\n  cursor: pointer;\n  width: auto;\n  font-size: 13px;\n  text-align: center;\n}\n.btn-dlt[_ngcontent-%COMP%]:hover {\n  opacity: 0.85;\n}\n@media only screen and (max-width: 767px) {\n  .btn-dlt[_ngcontent-%COMP%] {\n    margin-right: 0px;\n  }\n}\nli.delete[_ngcontent-%COMP%] {\n  background: none !important;\n  padding: 0px !important;\n}\n.btn-dlt[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 18px;\n}\n.v-none[_ngcontent-%COMP%] {\n  visibility: hidden;\n}\n.preview-img[_ngcontent-%COMP%] {\n  border-radius: 6px;\n  width: 60px;\n  height: 60px;\n  object-fit: cover;\n}\na[_ngcontent-%COMP%] {\n  text-decoration: none;\n  cursor: pointer;\n}\n.form-container[_ngcontent-%COMP%] {\n  padding: 0px 0;\n}\n.main-container[_ngcontent-%COMP%] {\n  margin: 0 auto;\n  width: 100%;\n}\n.main-container[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n  width: 100%;\n}\n@media only screen and (max-width: 767px) {\n  .main-container[_ngcontent-%COMP%]   .container[_ngcontent-%COMP%] {\n    width: 100%;\n    max-width: 425px;\n    margin: 0 auto 20px;\n    display: block;\n    padding: 0px 20px;\n  }\n}\n.main-container[_ngcontent-%COMP%]   .inner-container[_ngcontent-%COMP%] {\n  \n  border-radius: 6px;\n  padding-top: 82px;\n  padding-bottom: 40px;\n}\n@media only screen and (max-width: 767px) {\n  .main-container[_ngcontent-%COMP%]   .inner-container[_ngcontent-%COMP%] {\n    margin: 0 auto;\n  }\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%] {\n  width: 30%;\n  display: inline-block;\n  vertical-align: top;\n  padding: 0px 15px 0px 0px;\n}\n@media only screen and (max-width: 767px) {\n  .main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%] {\n    width: 100%;\n    max-width: 425px;\n    margin: 0 auto;\n    display: block;\n    padding: 0px 20px;\n  }\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%] {\n  \n  border-radius: 6px;\n  padding: 20px;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .upload-box[_ngcontent-%COMP%] {\n  position: relative;\n  cursor: pointer;\n  min-width: 60px;\n  width: 20%;\n  height: 60px;\n  padding-right: 0px;\n  float: left;\n  background: #C4C4C4;\n  border-radius: 6px;\n  z-index: 2;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .upload-box[_ngcontent-%COMP%]   .upload-img[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  left: 50%;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .update-box[_ngcontent-%COMP%] {\n  border-bottom: 1px solid #F3F4F4;\n  padding: 0px 0px 26px;\n  margin-bottom: 26px;\n  position: relative;\n  z-index: 2;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .update-box[_ngcontent-%COMP%]:last-child {\n  border: none !important;\n  padding: 0px !important;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .metric-box[_ngcontent-%COMP%] {\n  float: right;\n  vertical-align: top;\n  padding: 10px 0px 10px 0px;\n  width: 70%;\n  z-index: 1;\n}\n@media only screen and (max-width: 992px) {\n  .main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .metric-box[_ngcontent-%COMP%] {\n    width: 65%;\n  }\n}\n@media only screen and (max-width: 767px) {\n  .main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .metric-box[_ngcontent-%COMP%] {\n    width: 73%;\n  }\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .metric-box[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  border: 1px solid #F3F4F4;\n  box-sizing: border-box;\n  border-radius: 6px;\n  padding: 6px 6px;\n  width: 100%;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .metric-box[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%]:focus {\n  outline: none;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .edit-icon[_ngcontent-%COMP%] {\n  width: 10px;\n  float: right;\n  vertical-align: top;\n  padding: 20px 0px;\n  position: relative;\n  z-index: 2;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .edit-icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 4px;\n  float: right;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .add-metric[_ngcontent-%COMP%] {\n  position: relative;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .add-metric[_ngcontent-%COMP%]   span.add-icon[_ngcontent-%COMP%] {\n  background: #F3F4F4;\n  box-shadow: 3px 3px 5px rgba(0, 0, 0, 0.11);\n  border-radius: 100%;\n  width: 33px;\n  height: 33px;\n  display: block;\n  text-align: center;\n  line-height: 27px;\n  font-size: 20px;\n  font-weight: 700;\n  margin: 0 auto;\n  position: relative;\n  z-index: 2;\n}\n.main-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%]   .inner-sidebar[_ngcontent-%COMP%]   .add-metric[_ngcontent-%COMP%]:before {\n  content: \"\";\n  height: 1px;\n  width: 100%;\n  background: #f8f8f8;\n  display: block;\n  position: absolute;\n  bottom: 16px;\n  z-index: 1;\n}\n.public[_ngcontent-%COMP%]   .tabs-nav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]::after {\n  display: none !important;\n}\n.tabs-nav[_ngcontent-%COMP%] {\n  margin-bottom: 20px;\n  overflow: auto;\n  white-space: nowrap;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\n  margin: 0px;\n  padding: 0px;\n  list-style: none;\n  float: left;\n  display: grid;\n  grid-auto-flow: column;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  float: left;\n  background: #F3F4F4;\n  border-radius: 6px;\n  padding: 5px 20px;\n  margin-right: 12px;\n  position: relative;\n}\n@media only screen and (max-width: 768px) {\n  .tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n    margin-bottom: 10px;\n  }\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  font-family: Montserrat;\n  font-style: normal;\n  font-weight: 600;\n  line-height: 30px;\n  font-size: 14px;\n  color: #000000;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   span.clone[_ngcontent-%COMP%] {\n  width: 20px;\n  height: 20px;\n  display: block;\n  position: absolute;\n  right: 13px;\n  top: 11px;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   span.clone[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: block;\n  line-height: 0px;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   .move[_ngcontent-%COMP%] {\n  position: absolute;\n  left: 100%;\n  top: 0;\n  height: 44px;\n  width: 30px;\n  display: none;\n  background-color: #eee;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   .move[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: block;\n  line-height: 38px;\n  text-align: center;\n  font-size: 2em;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   .move.left[_ngcontent-%COMP%] {\n  left: auto;\n  right: calc(100% - 4px);\n  border-radius: 5px 0 0 5px;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   .move.right[_ngcontent-%COMP%] {\n  left: calc(100% - 4px);\n  border-radius: 0 5px 5px 0;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]:hover:last-child {\n  margin-left: 0px;\n  margin-right: 0px;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]:hover {\n  box-shadow: 0 0 50px var(--primary-color-transparent) inset;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li.active[_ngcontent-%COMP%] {\n  background: var(--primary-color);\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li.active[_ngcontent-%COMP%]   a.input-sizer[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  color: #fff;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li.active[_ngcontent-%COMP%]   a.input-sizer[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:focus {\n  outline: none;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li.active[_ngcontent-%COMP%]   a.input-sizer[_ngcontent-%COMP%]   [_ngcontent-%COMP%]::placeholder {\n  color: #fff;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li.active[_ngcontent-%COMP%]   a.input-sizer[_ngcontent-%COMP%] {\n  margin-right: 20px;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%] {\n  display: inline-grid;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]::after, .tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  grid-area: 2/1;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]::after, .tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  width: auto;\n  min-width: 1em;\n  grid-area: 1/2;\n  font: inherit;\n  margin: 0;\n  resize: none;\n  background: none;\n  -webkit-appearance: none;\n          appearance: none;\n  border: none;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:focus {\n  outline: solid 1px #000;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]   [_ngcontent-%COMP%]::placeholder {\n  color: #000;\n}\n.tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]::after {\n  content: attr(data-value) \" \";\n  visibility: hidden;\n  white-space: pre-wrap;\n}\n.tabs-nav[_ngcontent-%COMP%]   .plus[_ngcontent-%COMP%] {\n  border: 1px solid #F3F4F4;\n  box-sizing: border-box;\n  border-radius: 6px;\n  padding: 0px !important;\n}\n.tabs-nav[_ngcontent-%COMP%]   .plus[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  font-size: 16px;\n  display: block;\n  padding: 0px 16px;\n  font-weight: 700;\n  height: 40px;\n}\n.tabs-nav[_ngcontent-%COMP%]   .plus[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 10px;\n  height: 10px;\n  padding: 15px 0px;\n}\nli.cdk-drag[_ngcontent-%COMP%] {\n  background: var(--primary-color);\n  border-radius: 6px;\n  padding: 5px 20px;\n  margin-right: 12px;\n  position: relative;\n  list-style: none;\n}\nli.cdk-drag[_ngcontent-%COMP%]   a.input-sizer[_ngcontent-%COMP%] {\n  margin-right: 20px;\n  font-family: Montserrat;\n  font-style: normal;\n  font-weight: 600;\n  line-height: 30px;\n  font-size: 14px;\n  color: #000000;\n}\nli.cdk-drag[_ngcontent-%COMP%]   span.clone[_ngcontent-%COMP%] {\n  width: 20px;\n  height: 20px;\n  display: block;\n  position: absolute;\n  right: 13px;\n  top: 11px;\n}\nli.cdk-drag[_ngcontent-%COMP%]   span.clone[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: block;\n  line-height: 0px;\n}\nli.cdk-drag[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%] {\n  display: inline-grid;\n  color: #fff;\n}\nli.cdk-drag[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]::after, li.cdk-drag[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  grid-area: 2/1;\n}\nli.cdk-drag[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]::after, li.cdk-drag[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  width: auto;\n  min-width: 1em;\n  grid-area: 1/2;\n  font: inherit;\n  margin: 0;\n  resize: none;\n  background: none;\n  -webkit-appearance: none;\n          appearance: none;\n  border: none;\n}\nli.cdk-drag[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:focus {\n  outline: solid 1px #000;\n}\nli.cdk-drag[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]   [_ngcontent-%COMP%]::placeholder {\n  color: #000;\n}\nli.cdk-drag[_ngcontent-%COMP%]   .input-sizer[_ngcontent-%COMP%]::after {\n  content: attr(data-value) \" \";\n  visibility: hidden;\n  white-space: pre-wrap;\n}\n@media only screen and (max-width: 768px) {\n  li.cdk-drag[_ngcontent-%COMP%] {\n    margin-bottom: 10px;\n  }\n}\n.divTableCell[_ngcontent-%COMP%] {\n  width: auto !important;\n}\n.divTableCell[_ngcontent-%COMP%]   .normal-text[_ngcontent-%COMP%] {\n  border: solid 1px #ccc;\n  padding: 0 5px;\n  width: 75%;\n  line-height: 25px;\n  text-align: left;\n  border-radius: 5px;\n}\n.cdk-drag-preview[_ngcontent-%COMP%] {\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2), 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12);\n}\n.cdk-drag-placeholder[_ngcontent-%COMP%] {\n  opacity: 0;\n}\n.cdk-drag-animating[_ngcontent-%COMP%] {\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n}\n.example-box[_ngcontent-%COMP%]:last-child {\n  border: none;\n}\n.tabs-nav[_ngcontent-%COMP%]   li.cdk-drop-list-dragging[_ngcontent-%COMP%]   .tabs-nav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]:not(.cdk-drag-placeholder) {\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n}\n.public[_ngcontent-%COMP%]   .tabs-nav[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li.active[_ngcontent-%COMP%]   a.input-sizer[_ngcontent-%COMP%] {\n  margin-right: 0px;\n  color: #fff;\n}\n.divTableRow.mainpage-user-settings[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n}\n.divTableRow.mainpage-user-settings[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  font-weight: bold;\n  text-align: center;\n  margin-bottom: 5px;\n}\na.ssoprocessing[_ngcontent-%COMP%] {\n  opacity: 0.5;\n  cursor: wait;\n}\n.metric-settings[_ngcontent-%COMP%] {\n  font-family: Montserrat;\n  font-size: 10px;\n  font-weight: 600;\n  margin-bottom: 10px;\n  text-align: left;\n}\n.metric-settings[_ngcontent-%COMP%]   .options[_ngcontent-%COMP%] {\n  border: solid 1px #ccc;\n  background: #f1f1f1;\n  border-radius: 4px;\n  padding: 4px 6px;\n  font-size: 12px;\n  display: block;\n  min-width: 102px;\n  margin: 0 auto 6px;\n}\n.metric-settings[_ngcontent-%COMP%]   .options[_ngcontent-%COMP%]   em[_ngcontent-%COMP%] {\n  font-size: 9px;\n  display: block;\n  float: right;\n  line-height: 14px;\n  margin-left: 5px;\n}\n.metric-settings[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  display: inline-block;\n}\n.metric-settings[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  float: left;\n  margin: 0px 5px 0px 0px;\n  width: auto;\n}\n.metric-settings[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]   .checkbox-style[_ngcontent-%COMP%] {\n  margin-top: 5px;\n  display: inline-block;\n  background: #f1f1f1;\n  padding: 5px;\n  border-radius: 5px;\n  border: solid 1px #ccc;\n}\n.metric-settings[_ngcontent-%COMP%]   em[_ngcontent-%COMP%] {\n  background: #FFF067;\n  display: inline-block;\n  width: 13px;\n  height: 13px;\n  border-radius: 100%;\n  font-size: 10px;\n  color: #000;\n  line-height: 16px;\n  position: relative;\n  border-bottom: 1px dotted black;\n  margin-left: 5px;\n  cursor: pointer;\n  margin: 10px 5px 0px;\n  text-align: center;\n}\n.metric-settings[_ngcontent-%COMP%]   em[_ngcontent-%COMP%]   .tooltiptext[_ngcontent-%COMP%] {\n  visibility: hidden;\n  background-color: rgba(0, 0, 0, 0.9);\n  color: #fff;\n  text-align: center;\n  padding: 5px;\n  border-radius: 6px;\n  position: absolute;\n  z-index: 1;\n  width: 200px;\n  top: 100%;\n  left: 50%;\n  margin-top: 10px;\n  margin-left: -100px;\n  font-style: normal;\n}\n@media (max-width: 768px) {\n  .metric-settings[_ngcontent-%COMP%]   em[_ngcontent-%COMP%]   .tooltiptext[_ngcontent-%COMP%] {\n    left: 0;\n    margin-left: 10px;\n  }\n}\n.metric-settings[_ngcontent-%COMP%]   em.active[_ngcontent-%COMP%]   .tooltiptext[_ngcontent-%COMP%] {\n  visibility: visible;\n}\n.metric-settings.goal-title[_ngcontent-%COMP%] {\n  text-align: center;\n  font-size: 12px;\n}\n@media (max-width: 768px) {\n  .metric-settings.goal-title[_ngcontent-%COMP%] {\n    text-align: left;\n  }\n}\n@media (min-width: 769px) {\n  .divTableCell[_ngcontent-%COMP%]:last-child   em[_ngcontent-%COMP%]   .tooltiptext[_ngcontent-%COMP%] {\n    margin-left: 0;\n    left: auto;\n    right: 0;\n  }\n}\n.divTableCell[_ngcontent-%COMP%]:nth-child(2)   em[_ngcontent-%COMP%]   .tooltiptext[_ngcontent-%COMP%] {\n  margin-left: 0px;\n}\n.dropdown-nav[_ngcontent-%COMP%] {\n  position: relative;\n  display: inline-block;\n}\n.dropdown-nav[_ngcontent-%COMP%]   .dropbtn[_ngcontent-%COMP%] {\n  border: solid 1px #ccc;\n  background: #f1f1f1;\n  border-radius: 4px;\n  padding: 4px;\n  font-size: 12px;\n  display: block;\n  margin: 0 auto 6px;\n}\n.dropdown-nav[_ngcontent-%COMP%]   .dropbtn[_ngcontent-%COMP%]   em[_ngcontent-%COMP%] {\n  font-size: 8px;\n  display: block;\n  float: right;\n  line-height: 16px;\n}\n.dropdown-nav[_ngcontent-%COMP%]   .dropbtn[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%] {\n  float: left;\n  margin: 0px 6px 0px 0px !important;\n  width: 16px !important;\n  height: 16px !important;\n  line-height: 17px !important;\n}\n.dropdown-nav.dropdown-dd[_ngcontent-%COMP%] {\n  margin-left: 5px;\n}\n.dropdown-nav.dropdown-dd[_ngcontent-%COMP%]   .dropbtn[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%] {\n  width: auto !important;\n}\n.dropdown-nav.dropdown-dd[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%] {\n  display: block;\n  width: auto;\n  text-align: left;\n}\n.dropdown-nav.dropdown-dd[_ngcontent-%COMP%]:hover   .dropbtn[_ngcontent-%COMP%] {\n  background-color: #f1f1f1;\n  color: inherit;\n}\n.dropdown-nav[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%] {\n  display: none;\n  position: absolute;\n  background-color: #fafafa;\n  padding: 8px 4px 0px 4px;\n  border-radius: 5px;\n  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n  z-index: 1;\n}\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  background-color: #ddd;\n}\n.dropdown-nav.active[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%] {\n  display: block;\n}\n.dropdown-nav[_ngcontent-%COMP%]:hover   .dropbtn[_ngcontent-%COMP%] {\n  background-color: var(--primary-color);\n  color: #fff;\n}\n.setting[_ngcontent-%COMP%] {\n  border-bottom: 1px solid #EFEFEF;\n  border-top: 1px solid #EFEFEF;\n  padding: 10px 0px;\n  font-family: Montserrat;\n  margin-bottom: 16px;\n  position: fixed;\n  top: 0;\n  width: 100%;\n  background: var(--background-color);\n  z-index: 999;\n}\n.setting[_ngcontent-%COMP%]   .date-box.select-box[_ngcontent-%COMP%] {\n  background: #fff;\n}\n@media only screen and (max-width: 767px) {\n  .setting[_ngcontent-%COMP%]   .date-box.select-box[_ngcontent-%COMP%] {\n    margin-bottom: 10px;\n  }\n}\n@media only screen and (max-width: 767px) {\n  .setting[_ngcontent-%COMP%]   .date-box[_ngcontent-%COMP%] {\n    width: 100%;\n  }\n}\n.setting[_ngcontent-%COMP%]   .date-box[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .setting[_ngcontent-%COMP%]   .date-box[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n  font-family: Montserrat;\n}\n@media only screen and (max-width: 767px) {\n  .setting[_ngcontent-%COMP%]   .date-box[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .setting[_ngcontent-%COMP%]   .date-box[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n    width: 100% !important;\n    margin-bottom: 8px;\n  }\n}\n.setting[_ngcontent-%COMP%]   .date-box[_ngcontent-%COMP%] {\n  float: left;\n}\n.setting[_ngcontent-%COMP%]   .update[_ngcontent-%COMP%] {\n  float: right;\n}\n.setting[_ngcontent-%COMP%]   .update.left[_ngcontent-%COMP%] {\n  float: left;\n}\n.setting[_ngcontent-%COMP%]   .update.delete[_ngcontent-%COMP%] {\n  margin-left: 10px;\n}\n.setting[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  border: 1px solid #EFEFEF;\n  box-sizing: border-box;\n  border-radius: 6px;\n  padding: 0px 12px;\n  margin-right: 15px;\n  width: 150px;\n  height: 42px;\n  line-height: 42px;\n  font-weight: 600;\n  font-size: 12px;\n  font-family: Montserrat;\n}\n@media only screen and (max-width: 768px) {\n  .setting[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n    width: 100px;\n  }\n}\n.setting[_ngcontent-%COMP%]   input[type=date][_ngcontent-%COMP%]:focus {\n  outline: none;\n}\n.setting[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n  outline: none;\n  font-weight: 600;\n}\n.setting[_ngcontent-%COMP%]   .update.primary-btn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #ccc;\n}\n.setting[_ngcontent-%COMP%]   .update[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  margin: 0px !important;\n  background: #F3F4F4;\n  border-radius: 6px;\n  padding: 0px 18px;\n  border: none !important;\n  font-weight: 600;\n  position: relative;\n  line-height: 40px;\n  height: 40px;\n  font-family: Montserrat;\n}\n.setting[_ngcontent-%COMP%]   .update[_ngcontent-%COMP%]   button.processing[_ngcontent-%COMP%] {\n  padding-left: 40px;\n}\n.setting[_ngcontent-%COMP%]   .update[_ngcontent-%COMP%]   button.processing[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  display: block;\n  position: absolute;\n  left: 0;\n  top: 0;\n}\n.setting[_ngcontent-%COMP%]   .update[_ngcontent-%COMP%]   button.processing[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 40px;\n  height: 40px;\n}\n.setting[_ngcontent-%COMP%]   .update[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  display: none;\n}\n.setting[_ngcontent-%COMP%]   .update[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover {\n  box-shadow: 0 0 50px var(--primary-color-transparent) inset;\n}\n.setting[_ngcontent-%COMP%]   .update[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:active, .setting[_ngcontent-%COMP%]   .update[_ngcontent-%COMP%]   button.active[_ngcontent-%COMP%] {\n  background-color: var(--primary-color);\n  color: #fff;\n}\n.setting[_ngcontent-%COMP%]   .select-box[_ngcontent-%COMP%] {\n  border: 1px solid #EFEFEF;\n  box-sizing: border-box;\n  border-radius: 6px;\n  padding: 9px 10px;\n  height: 42px;\n}\n.setting[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n  border: none;\n  font-size: 12px;\n}\n.divTable[_ngcontent-%COMP%] {\n  position: relative;\n  box-sizing: border-box;\n  overflow-x: scroll;\n  overflow-y: visible;\n  z-index: 9;\n  padding-top: 20px;\n}\n@media only screen and (max-width: 767px) {\n  .divTable[_ngcontent-%COMP%] {\n    width: 100%;\n  }\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%] {\n  display: table-row-group;\n}\n@media (max-width: 767px) {\n  .divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%] {\n    display: block;\n  }\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableRow[_ngcontent-%COMP%] {\n  display: table-row;\n  margin-bottom: 12px;\n}\n@media only screen and (max-width: 767px) {\n  .divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableRow[_ngcontent-%COMP%] {\n    display: block;\n    border-radius: 6px;\n    \n  }\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .hiddenrow[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%] {\n  vertical-align: top;\n}\n@media only screen and (max-width: 767px) {\n  .divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .hiddenrow[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n    display: none;\n  }\n  .divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .hiddenrow[_ngcontent-%COMP%]    > div.add-icon-container[_ngcontent-%COMP%] {\n    display: block;\n    width: 100%;\n  }\n  .divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .hiddenrow[_ngcontent-%COMP%]    > div.add-icon-container[_ngcontent-%COMP%]   .add-icon[_ngcontent-%COMP%] {\n    float: right;\n  }\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableHeading[_ngcontent-%COMP%] {\n  background-color: #EEE;\n  display: table-header-group;\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .firstcell[_ngcontent-%COMP%] {\n  position: sticky !important;\n  min-width: 90px !important;\n  left: 0px;\n  background: var(--background-color);\n  width: 90px !important;\n  padding: 4px 0px;\n  z-index: 10 !important;\n}\n@media only screen and (max-width: 767px) {\n  .divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .firstcell[_ngcontent-%COMP%] {\n    width: auto !important;\n  }\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .firstcell.divTableCell[_ngcontent-%COMP%] {\n  width: 10%;\n}\n@media only screen and (max-width: 767px) {\n  .divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .firstcell.divTableCell[_ngcontent-%COMP%] {\n    width: auto;\n    display: block;\n    border-bottom: solid 1px #f8f8f8;\n    padding: 15px 12px 15px;\n  }\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .firstcell.divTableCell[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  font-family: Montserrat;\n  font-style: normal;\n  font-weight: 700;\n  font-size: 12px;\n  line-height: 15px;\n  margin: 0px;\n  text-transform: uppercase;\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%]:after {\n  content: \"\";\n  height: 12px;\n  width: 100%;\n  display: block;\n}\n@media only screen and (max-width: 767px) {\n  .divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%]:after {\n    display: none;\n  }\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableHead[_ngcontent-%COMP%] {\n  border: 1px solid #999999;\n  display: table-cell;\n  padding: 3px 10px;\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableHeading[_ngcontent-%COMP%] {\n  background-color: #EEE;\n  display: table-header-group;\n  font-weight: 600;\n}\n.divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableFoot[_ngcontent-%COMP%] {\n  background-color: #EEE;\n  display: table-footer-group;\n  font-weight: bold;\n}\n.divTableCell[_ngcontent-%COMP%] {\n  display: table-cell;\n  padding-right: 6px;\n  padding-left: 6px;\n  position: relative;\n  box-sizing: border-box;\n}\n.divTableCell.greyed[_ngcontent-%COMP%] {\n  background-color: #ddd;\n  border-left: solid 6px #fff;\n  border-right: solid 6px #fff;\n}\n.divTableCell[_ngcontent-%COMP%]   .add-icon[_ngcontent-%COMP%] {\n  background-color: #F3F4F4;\n  border-radius: 6px;\n  height: 42px;\n  width: 44px;\n  display: inline-block;\n  text-align: center;\n  padding: 0 16px;\n}\n.divTableCell[_ngcontent-%COMP%]   .add-icon[_ngcontent-%COMP%]:hover {\n  background-color: #07BDFF;\n}\n.divTableCell[_ngcontent-%COMP%]   .add-icon[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 10px;\n  height: 10px;\n  padding: 15px 0px;\n}\n.divTableCell.add-icon-container[_ngcontent-%COMP%] {\n  width: 44px;\n}\n.divTableCell[_ngcontent-%COMP%]:last-child {\n  padding-right: 0px;\n}\n@media only screen and (max-width: 767px) {\n  .divTableCell[_ngcontent-%COMP%]:last-child {\n    padding: 0px 12px;\n  }\n}\n@media only screen and (max-width: 767px) {\n  .divTableCell[_ngcontent-%COMP%] {\n    display: flex;\n    align-items: center;\n    border-bottom: solid 1px #f8f8f8;\n    \n    padding: 0px 12px;\n    width: 100% !important;\n  }\n}\n.divTableCell[_ngcontent-%COMP%]   .metric-hidden[_ngcontent-%COMP%] {\n  display: none;\n}\n.divTableCell[_ngcontent-%COMP%]   .metric-hidden[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  text-align: left;\n}\n@media only screen and (max-width: 767px) {\n  .divTableCell[_ngcontent-%COMP%]   .metric-hidden[_ngcontent-%COMP%] {\n    width: 50%;\n    display: block;\n    padding: 10px 0px;\n    border-right: solid 1px #f8f8f8;\n    padding-right: 12px;\n  }\n}\n.divTableCell[_ngcontent-%COMP%]   .metric-hidden[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%] {\n  text-align: left;\n  font-size: 12px;\n  font-weight: 600;\n}\n.divTableCell[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  padding: 0;\n  border: 0;\n  font-weight: 600;\n  font-size: 12px;\n  font-family: \"Montserrat\";\n  width: 100%;\n  text-align: center;\n  background: var(--background-color);\n}\n.divTableCell[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:active, .divTableCell[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:focus {\n  outline: none;\n}\n@media (max-width: 767px) {\n  .divTableCell[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n    text-align: left;\n  }\n}\n.divTableCell[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  width: 100%;\n  padding: 0px;\n  border: navajowhite;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 15px;\n  text-align: center;\n  margin-bottom: 7px;\n  font-family: Montserrat;\n  resize: none;\n}\n.divTableCell[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%]:focus, .divTableCell[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%]:active {\n  outline: none;\n}\n@media only screen and (max-width: 767px) {\n  .divTableCell[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n    margin-bottom: 0px;\n    line-height: 12px;\n  }\n}\n.divTableCell[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%] {\n  text-align: center;\n  margin-bottom: 4px;\n}\n.divTableCell[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  padding-right: 20px;\n  box-sizing: border-box;\n}\n.divTableCell[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%]   .button-delete[_ngcontent-%COMP%] {\n  display: block;\n  text-align: left;\n}\n.divTableCell[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%]   .button-delete[_ngcontent-%COMP%]   .delete[_ngcontent-%COMP%] {\n  background: var(--primary-color);\n  \n  box-sizing: border-box;\n  border-radius: 5px;\n  float: right;\n  margin-left: 5px;\n  position: absolute;\n  top: 0;\n  right: 5px;\n}\n.divTableCell[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%]   .button-delete[_ngcontent-%COMP%]   .delete[_ngcontent-%COMP%]:hover {\n  opacity: 0.85;\n}\n.divTableCell[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%]   .button-delete[_ngcontent-%COMP%]   .delete[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 10px;\n  height: 10px;\n  overflow: hidden;\n}\n.divTableCell[_ngcontent-%COMP%]   .currency.active[_ngcontent-%COMP%], .divTableCell[_ngcontent-%COMP%]   .currency[_ngcontent-%COMP%]:hover {\n  background: #FFF067 !important;\n}\n.divTableCell[_ngcontent-%COMP%]   .number.active[_ngcontent-%COMP%], .divTableCell[_ngcontent-%COMP%]   .number[_ngcontent-%COMP%]:hover {\n  background: #07BDFF !important;\n}\n.divTableCell[_ngcontent-%COMP%]   .percentage.active[_ngcontent-%COMP%], .divTableCell[_ngcontent-%COMP%]   .percentage[_ngcontent-%COMP%]:hover {\n  background: #ff5455 !important;\n}\n.divTableCell[_ngcontent-%COMP%]   .text.active[_ngcontent-%COMP%], .divTableCell[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%]:hover {\n  background: #A379C9 !important;\n}\n.divTableCell[_ngcontent-%COMP%]   .progress.active[_ngcontent-%COMP%], .divTableCell[_ngcontent-%COMP%]   .progress[_ngcontent-%COMP%]:hover {\n  background: #35CE8D !important;\n}\n.divTableCell[_ngcontent-%COMP%]   .dropdown-list.active[_ngcontent-%COMP%], .divTableCell[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]:hover {\n  background: #FDA90D !important;\n}\n.divTableCell[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%] {\n  background: #F3F4F4;\n  border-radius: 3px;\n  display: inline-block;\n  font-size: 10px;\n  width: 20px;\n  height: 20px;\n  text-align: center;\n  line-height: 20px;\n  margin: 0px 2px 4px;\n  font-family: Montserrat;\n  font-weight: 700;\n  overflow: hidden;\n}\n.divTableCell[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #000;\n  display: block;\n}\n@media only screen and (max-width: 767px) {\n  .divTableCell[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%] {\n    width: 18px;\n    height: 18px;\n    line-height: 18px;\n    margin: 0 1px;\n  }\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%] {\n  background: #fff;\n  border: 1px solid #EFEFEF;\n  box-sizing: border-box;\n  border-radius: 8px;\n  padding: 0px;\n  width: 100%;\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric.disabled[_ngcontent-%COMP%] {\n  background: #eee;\n}\n@media only screen and (max-width: 767px) {\n  .divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%] {\n    width: 61%;\n    margin: 0 0 0 4%;\n  }\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%] {\n  position: relative;\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   .metric-icon[_ngcontent-%COMP%] {\n  background: #FFF067;\n  border-radius: 6px 0px 0px 6px;\n  display: inline-block;\n  height: 33px;\n  width: 33px;\n  padding: 0px 8px;\n  text-align: center;\n  line-height: 33px;\n  font-family: Montserrat;\n  font-size: 12px;\n  font-weight: 600;\n  position: absolute;\n  top: 0px;\n  left: 0px;\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   .metric-icon.number[_ngcontent-%COMP%] {\n  background: #07BDFF;\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   .metric-icon.percentage[_ngcontent-%COMP%] {\n  background: #ff5455;\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   .metric-icon.text[_ngcontent-%COMP%] {\n  background: #A379C9;\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   .metric-icon.progress[_ngcontent-%COMP%] {\n  background: #35CE8D;\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   .metric-icon.dropdown-list[_ngcontent-%COMP%] {\n  background: #FDA90D;\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n  border: none;\n  width: 100%;\n  padding: 4px 10px 4px 40px;\n  box-sizing: border-box;\n  height: 33px;\n  font-family: \"Montserrat\";\n  font-weight: 600;\n  font-size: 12px;\n  border-radius: 8px;\n  border: solid 1px red;\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:disabled, .divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]:disabled {\n  background: #eee;\n}\n.divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:focus {\n  outline: none;\n}\n.settingsTable[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%] {\n  width: 100% !important;\n  margin: 0 !important;\n}\n.settingsTable[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%]:last-child {\n  padding: 12px !important;\n}\n.settingsTable[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .settingsTable[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n  padding-left: 10px !important;\n}\n.options-button[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  float: right;\n}\n.options-button[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  border: solid 1px #ccc;\n  border-radius: 5px;\n  background: #f1f1f1;\n  padding: 5px;\n  display: block;\n  font-size: 11px;\n  line-height: 14px;\n}\n.options-button[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%] {\n  width: 180px;\n  position: absolute;\n  right: 0;\n  background-color: #fff;\n  border: solid 1px #ccc;\n  z-index: 999;\n  padding: 10px;\n  border-radius: 5px;\n  top: 30px;\n  display: none;\n}\n.options-button.active[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%] {\n  display: block;\n}\n.options-button[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\n  border: solid 1px #ccc !important;\n  text-align: left;\n  margin-bottom: 0 !important;\n  margin-top: 10px;\n  height: 100px;\n  padding: 5px;\n}\n.textmetric.disabled[_ngcontent-%COMP%]   .metric-icon[_ngcontent-%COMP%] {\n  background: #ddd !important;\n  color: #fff !important;\n}\n.textmetric.disabled[_ngcontent-%COMP%]   .progress-bar-value[_ngcontent-%COMP%] {\n  color: #fff;\n}\n.public[_ngcontent-%COMP%]   .metric-icon[_ngcontent-%COMP%] {\n  background-color: #ccc !important;\n  width: 24px;\n  box-sizing: border-box;\n}\n.cdk-drag[_ngcontent-%COMP%] {\n  cursor: move;\n}\n.cdk-drag[_ngcontent-%COMP%]    > *[_ngcontent-%COMP%] {\n  cursor: auto;\n}\n.add-upload-box[_ngcontent-%COMP%] {\n  position: relative;\n}\n.add-upload-box[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  display: block;\n  text-align: center;\n  font-size: 20px;\n  font-weight: 600;\n  margin: 0 auto;\n  position: relative;\n  z-index: 2;\n}\n.add-upload-box[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: block;\n  width: 33px;\n  height: 33px;\n  background: #F3F4F4;\n  line-height: 28px;\n  margin: 0 auto;\n  box-shadow: 3px 3px 5px rgba(0, 0, 0, 0.11);\n  border-radius: 100%;\n}\n.add-upload-box[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 8px;\n  margin: 0 auto;\n}\n.add-upload-box[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  background-color: var(--primary-color);\n}\n.add-upload-box[_ngcontent-%COMP%]:before {\n  content: \"\";\n  height: 1px;\n  width: 100%;\n  background: #f8f8f8;\n  position: absolute;\n  top: 17px;\n  z-index: 1;\n}\n.img-popup[_ngcontent-%COMP%] {\n  box-shadow: 1px 2px 7px 1px grey;\n  background: white;\n  padding: 20px;\n  border-radius: 6px;\n  height: 10rem;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  top: 0;\n  right: 0;\n  margin: auto;\n  width: 400px;\n}\n.img-popup[_ngcontent-%COMP%]   .form[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  border: 1px solid #F3F4F4;\n  box-sizing: border-box;\n  border-radius: 6px;\n  padding: 6px 6px;\n  width: 100%;\n}\n.btn-add[_ngcontent-%COMP%] {\n  padding: 10px;\n  background: lightgray;\n  width: 50px;\n  margin-top: 4px;\n  border-radius: 6px;\n  border: none;\n}\n.m-0[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.popup-head[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n}\n.close-popup[_ngcontent-%COMP%] {\n  text-align: right;\n  cursor: pointer;\n}\n.d-none[_ngcontent-%COMP%] {\n  display: none;\n}\n.progress-bar-value[_ngcontent-%COMP%] {\n  text-align: left;\n  padding-left: 30px;\n  font-family: \"Montserrat\";\n  font-weight: 600;\n}\n.progress-bar[_ngcontent-%COMP%] {\n  position: absolute;\n  max-width: 220px;\n  width: 100%;\n  border: solid 1px #EFEFEF;\n  padding: 10px;\n  border-radius: 5px;\n  background-color: #fefefe;\n  bottom: 6px;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  box-sizing: border-box;\n  z-index: 99;\n}\n.progress-bar[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  line-height: 20px;\n  font-weight: 600;\n  display: block;\n  font-size: 11px;\n}\n.progress-bar-bg[_ngcontent-%COMP%] {\n  border: solid 1px #EFEFEF;\n  background: #EFEFEF;\n  border-radius: 5px;\n  height: 10px;\n}\n.progress-bar-selector[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  display: block;\n  float: left;\n  width: 9%;\n  cursor: pointer;\n  height: 10px;\n  position: relative;\n  z-index: 9;\n}\n.progress-bar-selector[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:first-child, .progress-bar-selector[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:nth-last-child(2) {\n  width: 9%;\n}\n.progress-bar-selector[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  height: 100%;\n}\n.progress-bar-filling[_ngcontent-%COMP%] {\n  background: #89d3da;\n  height: 10px;\n  position: absolute;\n}\n.progress-bar-selector[_ngcontent-%COMP%] {\n  height: 10px;\n  position: relative;\n}\n.dropdown[_ngcontent-%COMP%] {\n  position: absolute;\n  background: #efefef;\n  border-radius: 5px;\n  right: 0;\n  width: 200px;\n  overflow: hidden;\n  z-index: 999;\n  margin-top: 5px;\n  text-align: left;\n}\n.divTableCell.add-icon-container[_ngcontent-%COMP%] {\n  position: relative;\n  text-align: right;\n}\n.add-icon-container[_ngcontent-%COMP%] {\n  position: sticky !important;\n  right: 0px;\n  z-index: 10 !important;\n  margin-bottom: 15px;\n}\n.dropdown[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\n  list-style: none;\n  padding: 0;\n  margin: 0;\n}\n.dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: block;\n  padding: 10px;\n  border-bottom: solid 1px #ddd;\n  font-size: 12px;\n  font-weight: 600;\n}\n.dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  background: #fa5d48;\n  color: #fff;\n}\n.delete[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  font-size: 11px;\n  color: var(--primary-color);\n  padding: 4px 6px;\n  display: block;\n}\n[hidden][_ngcontent-%COMP%] {\n  display: none !important;\n}\nspan.notification[_ngcontent-%COMP%] {\n  margin-right: 10px;\n  font-size: 12px;\n  font-weight: 600;\n  opacity: 0;\n  text-align: center;\n}\nspan.notification.active[_ngcontent-%COMP%] {\n  transition: opacity 1s ease-in-out;\n  opacity: 1;\n}\nspan.notification.success[_ngcontent-%COMP%] {\n  color: dodgerblue;\n}\nspan.notification.error[_ngcontent-%COMP%], span.notification.error[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: var(--primary-color) !important;\n  font-size: 12px;\n  font-weight: 600;\n}\n.progress-input[_ngcontent-%COMP%] {\n  line-height: 32px;\n  font-size: 12px;\n}\n.add-button[_ngcontent-%COMP%] {\n  position: relative;\n  float: right;\n  z-index: 10;\n}\n.add-button[_ngcontent-%COMP%]   .add-icon[_ngcontent-%COMP%] {\n  background-color: #F3F4F4;\n  border-radius: 6px;\n  height: 42px;\n  width: 44px;\n  box-sizing: border-box;\n  display: inline-block;\n  text-align: center;\n  padding: 0 16px;\n}\n.add-button[_ngcontent-%COMP%]   .add-icon[_ngcontent-%COMP%]:hover {\n  box-shadow: 0 0 50px rgba(255, 84, 85, 0.1) inset;\n}\n.add-button[_ngcontent-%COMP%]   .add-icon[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 10px;\n  height: 10px;\n  padding: 15px 0px;\n}\n.heading[_ngcontent-%COMP%] {\n  font-size: 12px;\n  font-weight: 600;\n}\n.divTable.settingsTable[_ngcontent-%COMP%]    > .divTableBody[_ngcontent-%COMP%]    > .divTableRow[_ngcontent-%COMP%]    > .divTableCell[_ngcontent-%COMP%] {\n  vertical-align: top !important;\n  padding: 15px 2.5%;\n}\n.divTable.settingsTable[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.divTable.settingsTable[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%] {\n  text-align: center;\n  text-transform: uppercase;\n  font-size: 14px;\n}\n.divTable.settingsTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%] {\n  display: table;\n  width: 100%;\n}\n.divTable.settingsTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableRow[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%] {\n  width: 45%;\n  vertical-align: middle;\n  font-family: Montserrat;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n}\n.divTable.settingsTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableRow[_ngcontent-%COMP%]   .divTableCell.firstcell[_ngcontent-%COMP%] {\n  width: 33% !important;\n  position: relative !important;\n}\n@media (max-width: 767px) {\n  .divTable.settingsTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .divTableRow[_ngcontent-%COMP%]   .divTableCell.firstcell[_ngcontent-%COMP%] {\n    width: 100% !important;\n  }\n}\n.btn-dlt.btn-settings[_ngcontent-%COMP%] {\n  background: #F3F4F4;\n}\n.btn-dlt.btn-settings[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  display: block;\n}\n.btn-dlt.btn-settings[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 20px;\n  height: 20px;\n  display: block;\n}\n.btn-dlt.btn-settings[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   *[_ngcontent-%COMP%] {\n  fill: #000;\n}\n.btn-dlt.btn-settings[_ngcontent-%COMP%]:hover {\n  box-shadow: 0 0 50px var(--primary-color-transparent) inset;\n}\n.notification[_ngcontent-%COMP%] {\n  padding: 20px;\n  border-radius: 5px;\n  font-size: 12px;\n  font-weight: 600;\n  transition: all 1s ease;\n  opacity: 0;\n  z-index: 99999;\n  display: none;\n  margin: 10px auto;\n  box-sizing: border-box;\n  color: #fff;\n}\n.notification.active[_ngcontent-%COMP%] {\n  opacity: 1;\n  display: block;\n}\n.notification.success[_ngcontent-%COMP%] {\n  background-color: rgba(5, 129, 14, 0.8);\n}\nspan.copyToAll[_ngcontent-%COMP%] {\n  position: absolute;\n  width: 20px;\n  height: 20px;\n  right: 5px;\n  cursor: pointer;\n  top: 5px;\n  display: none;\n}\n.inn-metric[_ngcontent-%COMP%]:hover   span.copyToAll[_ngcontent-%COMP%], .inn-metric[_ngcontent-%COMP%]:focus   span.copyToAll[_ngcontent-%COMP%] {\n  display: block;\n}\n.copyPopup[_ngcontent-%COMP%] {\n  position: absolute;\n  width: 100%;\n  height: 500px;\n  overflow: auto;\n  z-index: 99;\n  \n}\n.copyPopup[_ngcontent-%COMP%]   .bg[_ngcontent-%COMP%] {\n  background-color: rgba(0, 0, 0, 0.1);\n  width: 100%;\n  height: 100%;\n  position: fixed;\n  left: 0;\n  top: 0;\n}\n.copyPopupContent[_ngcontent-%COMP%] {\n  width: 90%;\n  max-width: 480px;\n  min-width: 360px;\n  background-color: #fff;\n  line-height: 40px;\n  border-radius: 8px;\n  box-shadow: 0 0 10px #ccc;\n  margin: 0 auto;\n  position: relative;\n}\n.copyPopupHeader[_ngcontent-%COMP%] {\n  border-bottom: solid 1px #eee;\n  padding-right: 40px;\n  padding-left: 15px;\n  font-size: 14px;\n  font-weight: 600;\n}\n.copyPopupHeader[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%] {\n  display: block;\n  position: absolute;\n  width: 40px;\n  height: 40px;\n  border-left: solid 1px #eee;\n  right: 0;\n  top: 0;\n  background: #F3F4F4;\n  border-radius: 0 8px 0 0;\n}\n.copyPopupHeader[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 18px;\n  height: 18px;\n  margin-top: 11px;\n  margin-left: 11px;\n}\n.copyPopupHeader[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   .st0[_ngcontent-%COMP%] {\n  fill: #000;\n}\n.copyPopupHeader[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: block;\n  width: 40px;\n  height: 40px;\n}\n.copyPopupHeader[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%]:hover {\n  background: var(--primary-color);\n}\n.copyPopupHeader[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%]:hover   svg[_ngcontent-%COMP%]   .st0[_ngcontent-%COMP%] {\n  fill: #fff;\n}\n.copyPopupHeader[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover   svg[_ngcontent-%COMP%] {\n  fill: #fff;\n}\n.copyPopupBody[_ngcontent-%COMP%] {\n  padding: 15px;\n  font-size: 12px;\n  font-weight: 600;\n}\n.copyPopupOptions[_ngcontent-%COMP%]   input[type=text][_ngcontent-%COMP%] {\n  width: 20%;\n  float: right;\n  height: 40px;\n  box-sizing: border-box;\n  padding: 0 5px;\n  border: solid 1px #eee;\n}\n.copyPopupOptions[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  float: left;\n}\n.copyPopupOptions[_ngcontent-%COMP%] {\n  margin-bottom: 15px;\n}\n.copyPopupFooter[_ngcontent-%COMP%] {\n  border-top: solid 1px #eee;\n  padding: 5px 15px;\n  text-align: right;\n}\n.copyPopupFooter[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  height: 40px;\n  margin-left: 10px;\n  width: 120px;\n  font-weight: 600;\n  border: 0;\n  border-radius: 8px;\n  background: #F3F4F4;\n}\n.copyPopupFooter[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover {\n  box-shadow: 0 0 50px var(--primary-color-transparent) inset;\n}\n.copyPopupFooter[_ngcontent-%COMP%]   button.copyPopupSave[_ngcontent-%COMP%] {\n  background: var(--primary-color);\n  color: #fff;\n}\n.copyPopupFooter[_ngcontent-%COMP%]   button.copyPopupSave[_ngcontent-%COMP%]:hover {\n  opacity: 0.85;\n}\nbutton[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\nspan.editIcon[_ngcontent-%COMP%] {\n  width: 12px;\n  display: block;\n  position: absolute;\n  top: 0;\n  right: 0;\n}\nspan.editIcon[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 12px;\n  height: 12px;\n}\n.heading[_ngcontent-%COMP%] {\n  position: relative;\n}\n.tooltip[_ngcontent-%COMP%]   *[_ngcontent-%COMP%] {\n  font-size: 10px;\n  font-weight: 600;\n}\n.delteTerm[_ngcontent-%COMP%]:hover   img[_ngcontent-%COMP%] {\n  opacity: 0.85;\n}\n@media (max-width: 767px) {\n  .divTableRow[_ngcontent-%COMP%] {\n    border-radius: 10px;\n  }\n\n  .firstcell.divTableCell[_ngcontent-%COMP%] {\n    background-color: #f3f4f4 !important;\n    border-radius: 10px 10px 0 0;\n    padding-bottom: 10px;\n  }\n\n  .divTableCell[_ngcontent-%COMP%] {\n    border-left: solid 1px #f3f4f4 !important;\n    border-right: solid 1px #f3f4f4 !important;\n  }\n\n  .divTableCell[_ngcontent-%COMP%]:last-child {\n    border-radius: 0 0 10px 10px;\n  }\n}\n.dashboard-link[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 28px;\n  margin-top: 2px;\n}\n@media (max-width: 380px) {\n  .setting[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n    padding: 0 10px !important;\n  }\n}\n.clearfix[_ngcontent-%COMP%]:after {\n  content: \"\";\n  display: block;\n  clear: both;\n}\n.widget[_ngcontent-%COMP%]   .setting[_ngcontent-%COMP%] {\n  position: relative;\n}\n.widget[_ngcontent-%COMP%] {\n  max-width: 480px;\n  margin: 0 auto;\n  border: solid 1px #eee;\n  border-radius: 5px;\n}\n.widget[_ngcontent-%COMP%]   .header[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  float: left;\n  line-height: 40px;\n}\n.widget[_ngcontent-%COMP%]   .header[_ngcontent-%COMP%] {\n  padding: 5px 10px 5px 15px;\n  margin: 0;\n}\n.widget[_ngcontent-%COMP%]   *[_ngcontent-%COMP%] {\n  box-sizing: border-box;\n}\n.widget[_ngcontent-%COMP%]   .divTable[_ngcontent-%COMP%] {\n  padding-top: 0;\n}\n.widget[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%], .widget[_ngcontent-%COMP%]   .divTableRow[_ngcontent-%COMP%], .widget[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%] {\n  display: block !important;\n  margin: 0 !important;\n}\n.widget[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%] {\n  width: 50% !important;\n  float: left;\n  padding: 5px;\n}\n.widget[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%] {\n  width: 100% !important;\n  margin: 0 !important;\n}\n.widget[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%]:after {\n  display: none !important;\n}\n.widget[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%]:first-child {\n  border-right: solid 1px #eee;\n}\n.widget[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%] {\n  text-align: left !important;\n  padding: 10px;\n  margin: 0 !important;\n}\n.widget[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%]   .heading[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  text-align: left !important;\n}\n.widget[_ngcontent-%COMP%]   .divTableRow[_ngcontent-%COMP%] {\n  border-bottom: solid 1px #eee;\n}\n.widget[_ngcontent-%COMP%]   .footer[_ngcontent-%COMP%] {\n  border: 0 !important;\n  padding: 5px 30px !important;\n  margin: 0 !important;\n  text-align: center;\n}\n.footer.setting[_ngcontent-%COMP%]   .update.primary-btn[_ngcontent-%COMP%] {\n  float: none;\n}\n.widget[_ngcontent-%COMP%]   .footer[_ngcontent-%COMP%]   .left-btn[_ngcontent-%COMP%], .widget[_ngcontent-%COMP%]   .footer[_ngcontent-%COMP%]   .right-btn[_ngcontent-%COMP%] {\n  position: absolute;\n  left: 0;\n  top: 0;\n  width: 30px;\n  height: 100%;\n  cursor: pointer;\n}\n.widget[_ngcontent-%COMP%]   .footer[_ngcontent-%COMP%]   .left-btn[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%], .widget[_ngcontent-%COMP%]   .footer[_ngcontent-%COMP%]   .right-btn[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 20px;\n  height: 20px;\n  margin-top: 15px;\n}\n.widget[_ngcontent-%COMP%]   .footer[_ngcontent-%COMP%]   .right-btn[_ngcontent-%COMP%] {\n  left: auto;\n  right: 0;\n}\n.widget[_ngcontent-%COMP%]   .footer[_ngcontent-%COMP%]   .left-btn.disabled[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%], .widget[_ngcontent-%COMP%]   .footer[_ngcontent-%COMP%]   .right-btn.disabled[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  fill: #eee;\n}\n.widget[_ngcontent-%COMP%]   span.editIcon[_ngcontent-%COMP%] {\n  top: 50%;\n  transform: translateY(-50%);\n}\n.widget[_ngcontent-%COMP%]   .metric-settings.goal-title[_ngcontent-%COMP%] {\n  text-align: left;\n}\n.widget[_ngcontent-%COMP%]   [hidden][_ngcontent-%COMP%] {\n  display: none !important;\n}\n.widget[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .widget[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n  text-align: left;\n}\n\n.heading.greyed[_ngcontent-%COMP%], .divTableCell.greyed[_ngcontent-%COMP%] {\n  opacity: 0;\n}\n@media (max-width: 768px) {\n  .heading.greyed[_ngcontent-%COMP%], .divTableCell.greyed[_ngcontent-%COMP%] {\n    display: none;\n  }\n}\n.dashboard-link-with-text[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: block;\n  background-color: #ccc;\n  border-radius: 5px;\n  padding: 0px 50px 0px 10px;\n  line-height: 40px;\n  position: relative;\n  font-weight: bold;\n}\n.dashboard-link-with-text[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  position: absolute;\n  right: 11px;\n  top: 8px;\n  width: 20px !important;\n  height: 20px;\n}\n.notification.active[_ngcontent-%COMP%] {\n  position: absolute;\n  width: 100%;\n}\n.boardroomContainer[_ngcontent-%COMP%]   .setting[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0 !important;\n}\n.boardroom[_ngcontent-%COMP%]   .divTable[_ngcontent-%COMP%] {\n  padding-top: 10px;\n  padding-bottom: 30px;\n}\n.boardroom[_ngcontent-%COMP%]   .alert[_ngcontent-%COMP%] {\n  padding: 0 10px 30px;\n}\n.boardroom[_ngcontent-%COMP%]   .alert.alert-danger[_ngcontent-%COMP%] {\n  color: red;\n}\n.boardroom[_ngcontent-%COMP%]   hr.separator[_ngcontent-%COMP%] {\n  margin: 30px 0;\n}\n.boardroom[_ngcontent-%COMP%]   .textmetric.goalView[_ngcontent-%COMP%] {\n  border: 0 !important;\n}\n.boardroom[_ngcontent-%COMP%]   .textmetric.goalView[_ngcontent-%COMP%]   select[_ngcontent-%COMP%], .boardroom[_ngcontent-%COMP%]   .textmetric.goalView[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  border: 0 !important;\n  border-bottom: solid 1px #eee !important;\n  border-radius: 0 !important;\n}\n.boardroom[_ngcontent-%COMP%]   .textmetric.goalView[_ngcontent-%COMP%]   .inn-metric[_ngcontent-%COMP%]   .metric-icon[_ngcontent-%COMP%] {\n  background: #fff !important;\n}\n.boardroom[_ngcontent-%COMP%]   .textmetric.goalView[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]:disabled {\n  background: #fff !important;\n  opacity: 1 !important;\n}\n.boardroom[_ngcontent-%COMP%]   .update.primary-btn[_ngcontent-%COMP%] {\n  float: right;\n}\n.boardroom[_ngcontent-%COMP%]   .update.primary-btn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  margin: 0px !important;\n  background: #ccc;\n  border-radius: 6px;\n  padding: 0px 18px;\n  border: none !important;\n  font-weight: 600;\n  position: relative;\n  line-height: 40px;\n  height: 40px;\n  font-family: Montserrat;\n}\n.boardroom[_ngcontent-%COMP%]   .update.primary-btn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  display: none;\n}\n.boardroom[_ngcontent-%COMP%]   .update.primary-btn[_ngcontent-%COMP%]   button.processing[_ngcontent-%COMP%] {\n  padding-left: 40px;\n}\n.boardroom[_ngcontent-%COMP%]   .update.primary-btn[_ngcontent-%COMP%]   button.processing[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  display: block;\n  position: absolute;\n  left: 0;\n  top: 0;\n}\n.boardroom[_ngcontent-%COMP%]   .update.primary-btn[_ngcontent-%COMP%]   button.processing[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 40px;\n  height: 40px;\n}\n.boardroom[_ngcontent-%COMP%]   .heading-sm[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  line-height: 40px;\n}\n.boardroom[_ngcontent-%COMP%]   .textmetric.bg-red[_ngcontent-%COMP%], .boardroom[_ngcontent-%COMP%]   .textmetric.bg-red[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .boardroom[_ngcontent-%COMP%]   .textmetric.bg-red[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n  background: #f6b2b2 !important;\n}\n.boardroom[_ngcontent-%COMP%]   .textmetric.bg-green[_ngcontent-%COMP%], .boardroom[_ngcontent-%COMP%]   .textmetric.bg-green[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .boardroom[_ngcontent-%COMP%]   .textmetric.bg-green[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n  background: #b2ddc6 !important;\n}\n.boardroom[_ngcontent-%COMP%]   .period-selector[_ngcontent-%COMP%] {\n  position: relative;\n  padding: 0 20px;\n  text-align: center;\n  height: 33px;\n  line-height: 33px;\n  min-width: 75px;\n}\n.boardroom[_ngcontent-%COMP%]   .period-selector[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  width: 10px;\n  position: absolute;\n  top: 12px;\n  left: 5px;\n}\n.boardroom[_ngcontent-%COMP%]   .period-selector[_ngcontent-%COMP%]   .left-btn[_ngcontent-%COMP%], .boardroom[_ngcontent-%COMP%]   .period-selector[_ngcontent-%COMP%]   .right-btn[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0;\n  width: 20px;\n  height: 33px;\n  left: 0;\n  cursor: pointer;\n}\n.boardroom[_ngcontent-%COMP%]   .period-selector[_ngcontent-%COMP%]   .right-btn[_ngcontent-%COMP%] {\n  left: auto;\n  right: 0;\n}\n.boardroom[_ngcontent-%COMP%]   .period-selector[_ngcontent-%COMP%]   .disabled[_ngcontent-%COMP%] {\n  display: none;\n}\n.boardroom[_ngcontent-%COMP%]   .divTableCell[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  flex-basis: 0;\n  flex-grow: 1;\n  border: 0 !important;\n}\n.boardroom[_ngcontent-%COMP%]   .goal-header[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.boardroom[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .boardroom[_ngcontent-%COMP%]   .textmetric[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n  border: solid 1px #ccc !important;\n}\n.boardroom[_ngcontent-%COMP%]   .textmetric.missing[_ngcontent-%COMP%]   input[_ngcontent-%COMP%], .boardroom[_ngcontent-%COMP%]   .textmetric.missing[_ngcontent-%COMP%]   select[_ngcontent-%COMP%] {\n  border: solid 1px red !important;\n}\n.boardroom[_ngcontent-%COMP%]   .divTable[_ngcontent-%COMP%]   .divTableBody[_ngcontent-%COMP%]   .firstcell[_ngcontent-%COMP%] {\n  min-width: 140px !important;\n}\n@media only screen and (min-width: 768px) {\n  .boardroom[_ngcontent-%COMP%]   .show-sm[_ngcontent-%COMP%] {\n    display: none;\n  }\n}\n@media only screen and (max-width: 767px) {\n  .boardroom[_ngcontent-%COMP%]   .hide-sm[_ngcontent-%COMP%] {\n    display: none !important;\n  }\n}\n.period-selector-top[_ngcontent-%COMP%] {\n  float: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBUSxxTUFBQTtBQUNSOztFQUVFLHdCQUFBO0VBQ0EsU0FBQTtBQUNGO0FBRUEsWUFBQTtBQUNBO0VBQ0UsMkJBQUE7QUFDRjtBQUNBO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0FBRUY7QUFBQTtFQUNFLG9CQUFBO0VBQ0EsdUJBQUE7QUFHRjtBQURBO0VBQ0UsV0FBQTtBQUlGO0FBRkE7RUFDSSxnQ0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQUtKO0FBSkk7RUFDRSxhQUFBO0FBTU47QUFKSTtFQVhKO0lBWU0saUJBQUE7RUFPSjtBQUNGO0FBTEE7RUFDSSwyQkFBQTtFQUNBLHVCQUFBO0FBUUo7QUFOQTtFQUNJLFdBQUE7QUFTSjtBQVBBO0VBQ0ksa0JBQUE7QUFVSjtBQVJBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBV0o7QUFUQTtFQUFHLHFCQUFBO0VBQXVCLGVBQUE7QUFjMUI7QUFiQTtFQUFpQixjQUFBO0FBaUJqQjtBQWhCQTtFQUNJLGNBQUE7RUFDQSxXQUFBO0FBbUJKO0FBbEJJO0VBQ0ksV0FBQTtBQW9CUjtBQW5CUTtFQUZKO0lBR1EsV0FBQTtJQUNBLGdCQUFBO0lBQ0EsbUJBQUE7SUFDQSxjQUFBO0lBQ0EsaUJBQUE7RUFzQlY7QUFDRjtBQXBCSTtFQUNJLDBCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FBc0JSO0FBckJRO0VBTEo7SUFNTSxjQUFBO0VBd0JSO0FBQ0Y7QUFyQkk7RUFDSSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FBdUJSO0FBdEJRO0VBTEo7SUFNUSxXQUFBO0lBQ0EsZ0JBQUE7SUFDQSxjQUFBO0lBQ0EsY0FBQTtJQUNBLGlCQUFBO0VBeUJWO0FBQ0Y7QUF4QlE7RUFDSSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtBQTBCWjtBQXpCWTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBMkJoQjtBQXhCb0I7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxnQ0FBQTtFQUNBLFNBQUE7QUEwQnhCO0FBdEJZO0VBQ0ksZ0NBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBd0JoQjtBQXRCWTtFQUNJLHVCQUFBO0VBQ0EsdUJBQUE7QUF3QmhCO0FBdEJZO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0VBQ0EsMEJBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtBQXdCaEI7QUF2QmdCO0VBTko7SUFPUSxVQUFBO0VBMEJsQjtBQUNGO0FBdkJnQjtFQVhKO0lBWVEsVUFBQTtFQTBCbEI7QUFDRjtBQXpCZ0I7RUFDSSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUEyQnBCO0FBekJnQjtFQUNJLGFBQUE7QUEyQnBCO0FBeEJZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBMEJoQjtBQXZCZ0I7RUFDSSxVQUFBO0VBQ0EsWUFBQTtBQXlCcEI7QUF0Qlk7RUFDSSxrQkFBQTtBQXdCaEI7QUF2QmdCO0VBQ0ksbUJBQUE7RUFDQSwyQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUF5QnBCO0FBdEJZO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtBQXdCaEI7QUFoQkk7RUFDRSx3QkFBQTtBQW1CTjtBQWZBO0VBQ1EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUFrQlI7QUFqQkk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNGLGFBQUE7RUFDQSxzQkFBQTtBQW1CTjtBQWxCUTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FBb0JaO0FBbkJZO0VBUEo7SUFRUSxtQkFBQTtFQXNCZDtBQUNGO0FBckJZO0VBQ0ksdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQXVCaEI7QUFyQlk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FBdUJoQjtBQXRCZ0I7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7QUF3QnBCO0FBckJVO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsTUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0FBdUJaO0FBdEJZO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBd0JkO0FBdEJZO0VBQ0UsVUFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7QUF3QmQ7QUF0Qlk7RUFDRSxzQkFBQTtFQUNBLDBCQUFBO0FBd0JkO0FBcEJZO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtBQXNCZDtBQWxCUTtFQUNFLDJEQUFBO0FBb0JWO0FBbEJRO0VBQ0ksZ0NBQUE7QUFvQlo7QUFsQmdCO0VBQU8sV0FBQTtBQXFCdkI7QUFwQmdCO0VBQ0ksYUFBQTtBQXNCcEI7QUFwQmdCO0VBQWUsV0FBQTtBQXVCL0I7QUFyQlk7RUFDSSxrQkFBQTtBQXVCaEI7QUFwQlE7RUFDSSxvQkFBQTtBQXNCWjtBQXJCYzs7RUFFRSxjQUFBO0FBdUJoQjtBQXJCWTs7RUFFRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxhQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLHdCQUFBO1VBQUEsZ0JBQUE7RUFDQSxZQUFBO0FBdUJkO0FBckJZO0VBQ0ksdUJBQUE7QUF1QmhCO0FBckJZO0VBQWUsV0FBQTtBQXdCM0I7QUF2Qlk7RUFDRSw2QkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QUF5QmQ7QUFwQkk7RUFDSSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtBQXNCUjtBQWpCSTtFQUNJLGVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUFtQlI7QUFsQlE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBb0JaO0FBZkE7RUFDRSxnQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFrQkY7QUFqQkU7RUFDRSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFtQko7QUFoQkU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FBa0JKO0FBaEJJO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0FBa0JOO0FBZkU7RUFDRSxvQkFBQTtFQUNBLFdBQUE7QUFpQko7QUFoQkk7O0VBRUUsY0FBQTtBQWtCTjtBQWhCSTs7RUFFRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxhQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLHdCQUFBO1VBQUEsZ0JBQUE7RUFDQSxZQUFBO0FBa0JOO0FBaEJJO0VBQ0UsdUJBQUE7QUFrQk47QUFoQkk7RUFBZSxXQUFBO0FBbUJuQjtBQWxCSTtFQUNFLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBQW9CTjtBQWpCRTtFQTNERjtJQTRESSxtQkFBQTtFQW9CRjtBQUNGO0FBbEJBO0VBQ0Usc0JBQUE7QUFxQkY7QUFwQkU7RUFDRSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBc0JKO0FBbkJBO0VBQ0Usc0JBQUE7RUFDQSxrQkFBQTtFQUNBLHFIQUFBO0FBc0JGO0FBakJBO0VBQ0UsVUFBQTtBQW9CRjtBQWpCQTtFQUNFLHNEQUFBO0FBb0JGO0FBakJBO0VBQ0UsWUFBQTtBQW9CRjtBQWpCQTtFQUNFLHNEQUFBO0FBb0JGO0FBVFE7RUFDRSxpQkFBQTtFQUNBLFdBQUE7QUFZVjtBQU5BO0VBQ0UsbUJBQUE7QUFTRjtBQU5BO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FBU0Y7QUFQQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0FBVUY7QUFSQTtFQUNFLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQVdGO0FBVkU7RUFDRSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQVlKO0FBWEk7RUFDRSxjQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FBYU47QUFWRTtFQUNFLHFCQUFBO0FBWUo7QUFYSTtFQUNFLFdBQUE7RUFDRix1QkFBQTtFQUNFLFdBQUE7QUFhTjtBQVhJO0VBQ0UsZUFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtBQWFOO0FBVEU7RUFDRSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLCtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtBQVdKO0FBVkk7RUFDRSxrQkFBQTtFQUNBLG9DQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBWU47QUFYTTtFQWZGO0lBZ0JJLE9BQUE7SUFDQSxpQkFBQTtFQWNOO0FBQ0Y7QUFaSTtFQUNFLG1CQUFBO0FBY047QUFWQTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtBQWFGO0FBWkU7RUFIRjtJQUlJLGdCQUFBO0VBZUY7QUFDRjtBQWJBO0VBR007SUFDRSxjQUFBO0lBQ0EsVUFBQTtJQUNBLFFBQUE7RUFjTjtBQUNGO0FBUkk7RUFDRSxnQkFBQTtBQVVOO0FBTkE7RUFDRSxrQkFBQTtFQUNBLHFCQUFBO0FBU0Y7QUFSRTtFQUNFLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FBVUo7QUFSSTtFQUNFLGNBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBVU47QUFSSTtFQUNFLFdBQUE7RUFDQSxrQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSw0QkFBQTtBQVVOO0FBUEU7RUFDRSxnQkFBQTtBQVNKO0FBUE07RUFDRSxzQkFBQTtBQVNSO0FBTkk7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBUU47QUFMTTtFQUNFLHlCQUFBO0VBQ0EsY0FBQTtBQU9SO0FBSEU7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSwrQ0FBQTtFQUNBLFVBQUE7QUFLSjtBQUtBO0VBQTJCLHNCQUFBO0FBRDNCO0FBR0E7RUFBd0MsY0FBQTtBQUN4QztBQUNBO0VBQThCLHNDQUFBO0VBQXdDLFdBQUE7QUFJdEU7QUFIQTtFQUNJLGdDQUFBO0VBQ0EsNkJBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFDQSxtQ0FBQTtFQUNBLFlBQUE7QUFNSjtBQUxJO0VBQ0ksZ0JBQUE7QUFPUjtBQU5RO0VBRko7SUFHTSxtQkFBQTtFQVNSO0FBQ0Y7QUFOUTtFQURKO0lBRVksV0FBQTtFQVNkO0FBQ0Y7QUFOSTtFQUNFLHVCQUFBO0FBUU47QUFQUTtFQUZKO0lBSVEsc0JBQUE7SUFDQSxrQkFBQTtFQVNWO0FBQ0Y7QUFOSTtFQUNJLFdBQUE7QUFRUjtBQU5JO0VBQ0ksWUFBQTtBQVFSO0FBUE07RUFDRSxXQUFBO0FBU1I7QUFQTTtFQUNFLGlCQUFBO0FBU1I7QUFMSTtFQUNJLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7QUFPUjtBQU5RO0VBWko7SUFhUSxZQUFBO0VBU1Y7QUFDRjtBQU5JO0VBQ0ksYUFBQTtBQVFSO0FBTkk7RUFBUSxhQUFBO0VBQWUsZ0JBQUE7QUFVM0I7QUFSSTtFQUNFLHNCQUFBO0FBVU47QUFQSTtFQUNJLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7QUFTUjtBQVBRO0VBQ0Usa0JBQUE7QUFTVjtBQVBVO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7QUFTWjtBQVBZO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFTZDtBQUxNO0VBQ0UsYUFBQTtBQU9SO0FBTE07RUFDRSwyREFBQTtBQU9SO0FBTE07RUFDRSxzQ0FBQTtFQUNBLFdBQUE7QUFPUjtBQUhJO0VBQ0kseUJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FBS1I7QUFISTtFQUNJLFlBQUE7RUFDQSxlQUFBO0FBS1I7QUFGQTtFQUNJLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0FBS0o7QUFKSTtFQVBKO0lBUU0sV0FBQTtFQU9KO0FBQ0Y7QUFOSTtFQUNJLHdCQUFBO0FBUVI7QUFQUTtFQUZKO0lBR00sY0FBQTtFQVVSO0FBQ0Y7QUFSUTtFQUNJLGtCQUFBO0VBQ0EsbUJBQUE7QUFVWjtBQVJZO0VBSko7SUFLUSxjQUFBO0lBQ0Esa0JBQUE7SUFDQSxvQkFBQTtFQVdkO0FBQ0Y7QUFQVTtFQUNFLG1CQUFBO0FBU1o7QUFQWTtFQUNJO0lBQ0UsYUFBQTtFQVNoQjtFQVJnQjtJQUNFLGNBQUE7SUFDQSxXQUFBO0VBVWxCO0VBVGtCO0lBQ0UsWUFBQTtFQVdwQjtBQUNGO0FBTlE7RUFDSSxzQkFBQTtFQUNBLDJCQUFBO0FBUVo7QUFOUTtFQUNFLDJCQUFBO0VBQ0EsMEJBQUE7RUFDQSxTQUFBO0VBQ0EsbUNBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7QUFRVjtBQVBVO0VBUkY7SUFTSSxzQkFBQTtFQVVWO0FBQ0Y7QUFSUTtFQUNJLFVBQUE7QUFVWjtBQVRZO0VBRko7SUFHUSxXQUFBO0lBQ0EsY0FBQTtJQUNBLGdDQUFBO0lBQ0EsdUJBQUE7RUFZZDtBQUNGO0FBWFM7RUFDRyx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7QUFhWjtBQVRRO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQVdaO0FBVlk7RUFMSjtJQU1RLGFBQUE7RUFhZDtBQUNGO0FBVlE7RUFDSSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUFZWjtBQVZRO0VBQ0ksc0JBQUE7RUFDQSwyQkFBQTtFQUNBLGdCQUFBO0FBWVo7QUFWUTtFQUNJLHNCQUFBO0VBQ0EsMkJBQUE7RUFDQSxpQkFBQTtBQVlaO0FBUEE7RUFDRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0FBVUY7QUFURTtFQUNFLHNCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtBQVdKO0FBVEU7RUFDRSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQVdKO0FBVkk7RUFDRSx5QkFBQTtBQVlOO0FBVkk7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBWU47QUFURTtFQUNFLFdBQUE7QUFXSjtBQVRFO0VBQ0Usa0JBQUE7QUFXSjtBQVZJO0VBRkY7SUFHSSxpQkFBQTtFQWFKO0FBQ0Y7QUFYRTtFQXJDRjtJQXNDSSxhQUFBO0lBQ0EsbUJBQUE7SUFDQSxnQ0FBQTtJQUNBLG9CQUFBO0lBQ0EsaUJBQUE7SUFDQSxzQkFBQTtFQWNGO0FBQ0Y7QUFiRTtFQUNFLGFBQUE7QUFlSjtBQWRJO0VBQ0UsZ0JBQUE7QUFnQk47QUFkSTtFQUxGO0lBTUksVUFBQTtJQUNBLGNBQUE7SUFDQSxpQkFBQTtJQUNBLCtCQUFBO0lBQ0EsbUJBQUE7RUFpQko7QUFDRjtBQWhCSTtFQUNFLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBa0JOO0FBZkU7RUFDRSxVQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUNBQUE7QUFpQko7QUFoQkk7RUFDRSxhQUFBO0FBa0JOO0FBaEJJO0VBWkY7SUFhSSxnQkFBQTtFQW1CSjtBQUNGO0FBakJFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0FBbUJKO0FBbEJJO0VBQ0UsYUFBQTtBQW9CTjtBQWhCSTtFQURGO0lBRUksa0JBQUE7SUFDQSxpQkFBQTtFQW1CSjtBQUNGO0FBakJFO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQW1CSjtBQWxCSTtFQUNFLG1CQUFBO0VBQ0Esc0JBQUE7QUFvQk47QUFsQkk7RUFBZ0IsY0FBQTtFQUFnQixnQkFBQTtBQXNCcEM7QUFyQk07RUFDRSxnQ0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFVBQUE7QUF1QlI7QUF0QlE7RUFDRSxhQUFBO0FBd0JWO0FBdEJRO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQXdCVjtBQW5CRTtFQUFrQyw4QkFBQTtBQXNCcEM7QUFyQkU7RUFBOEIsOEJBQUE7QUF3QmhDO0FBdkJFO0VBQXNDLDhCQUFBO0FBMEJ4QztBQXpCRTtFQUEwQiw4QkFBQTtBQTRCNUI7QUEzQkU7RUFBa0MsOEJBQUE7QUE4QnBDO0FBN0JFO0VBQTRDLDhCQUFBO0FBZ0M5QztBQS9CRTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQWlDSjtBQWhDSTtFQUFHLFdBQUE7RUFBWSxjQUFBO0FBb0NuQjtBQW5DSTtFQWRGO0lBZUksV0FBQTtJQUNBLFlBQUE7SUFDQSxpQkFBQTtJQUNBLGFBQUE7RUFzQ0o7QUFDRjtBQXBDRTtFQUNFLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFzQ0o7QUFyQ0k7RUFDRSxnQkFBQTtBQXVDTjtBQXJDSTtFQVZGO0lBV0ksVUFBQTtJQUNBLGdCQUFBO0VBd0NKO0FBQ0Y7QUF2Q0k7RUFDRSxrQkFBQTtBQXlDTjtBQXZDTTtFQUNFLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FBeUNSO0FBeENRO0VBQ0UsbUJBQUE7QUEwQ1Y7QUF4Q1E7RUFDRSxtQkFBQTtBQTBDVjtBQXhDUTtFQUNFLG1CQUFBO0FBMENWO0FBeENRO0VBQ0UsbUJBQUE7QUEwQ1Y7QUF4Q1E7RUFDRSxtQkFBQTtBQTBDVjtBQXZDTTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QUF5Q1I7QUF4Q1E7RUFDRSxnQkFBQTtBQTBDVjtBQXRDTTtFQUNFLGFBQUE7QUF3Q1I7QUFsQ0E7RUFDRSxzQkFBQTtFQUNBLG9CQUFBO0FBcUNGO0FBbkNBO0VBQ0Usd0JBQUE7QUFzQ0Y7QUFwQ0E7RUFDRSw2QkFBQTtBQXVDRjtBQXJDQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUF3Q0Y7QUFyQ0E7RUFDRSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQXdDRjtBQXRDQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxhQUFBO0FBeUNGO0FBdkNBO0VBQ0UsY0FBQTtBQTBDRjtBQXZDQTtFQUNFLGlDQUFBO0VBQ0EsZ0JBQUE7RUFDQSwyQkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUEwQ0Y7QUF2Q0U7RUFDRSwyQkFBQTtFQUNBLHNCQUFBO0FBMENKO0FBeENFO0VBQ0UsV0FBQTtBQTBDSjtBQXZDQTtFQUNFLGlDQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0FBMENGO0FBeENBO0VBQ0UsWUFBQTtBQTJDRjtBQXhDQTtFQUNFLFlBQUE7QUEyQ0Y7QUF4Q0E7RUFDSSxrQkFBQTtBQTJDSjtBQTFDSTtFQUVJLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUEyQ1I7QUExQ1E7RUFDSSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLDJDQUFBO0VBQ0EsbUJBQUE7QUE0Q1o7QUEzQ1k7RUFDSSxVQUFBO0VBQ0EsY0FBQTtBQTZDaEI7QUExQ1E7RUFDSSxzQ0FBQTtBQTRDWjtBQXZDQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtBQTBDSjtBQXRDQTtFQUNJLGdDQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsTUFBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQXlDSjtBQXZDUTtFQUNJLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQXlDWjtBQXJDQTtFQUNJLGFBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FBd0NKO0FBdENBO0VBQ0ksU0FBQTtBQXlDSjtBQXZDQTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtBQTBDSjtBQXhDQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQTJDSjtBQXpDQTtFQUNJLGFBQUE7QUE0Q0o7QUExQ0E7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtBQTZDRjtBQTFDQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7QUE2Q0Y7QUEzQ0E7RUFDRSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUE4Q0Y7QUF6Q0E7RUFDRSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FBNENGO0FBekNBO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUE0Q0Y7QUExQ0E7RUFDRSxTQUFBO0FBNkNGO0FBM0NBO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBOENGO0FBM0NBO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUE4Q0Y7QUE1Q0E7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7QUErQ0Y7QUE3Q0E7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQWdERjtBQTVDQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7QUErQ0Y7QUE1Q0E7RUFDRSwyQkFBQTtFQUNBLFVBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0FBK0NGO0FBN0NBO0VBQ0UsZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtBQWdERjtBQTdDQTtFQUNFLGNBQUE7RUFDQSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFnREY7QUE3Q0E7RUFDRSxtQkFBQTtFQUNBLFdBQUE7QUFnREY7QUE5Q0E7RUFDRSxlQUFBO0VBQ0EsMkJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFpREY7QUEvQ0E7RUFBVyx3QkFBQTtBQW1EWDtBQWxEQTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0FBcURGO0FBbkRBO0VBQ0Usa0NBQUE7RUFDQSxVQUFBO0FBc0RGO0FBcERBO0VBQ0UsaUJBQUE7QUF1REY7QUFyREE7RUFDRSxzQ0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQXdERjtBQXREQTtFQUNFLGlCQUFBO0VBQ0EsZUFBQTtBQXlERjtBQXZEQTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUEwREY7QUF6REU7RUFDRSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBMkRKO0FBMURJO0VBQ0UsaURBQUE7QUE0RE47QUExREk7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBNEROO0FBeERBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0FBMkRGO0FBekRBO0VBQ0UsOEJBQUE7RUFDQSxrQkFBQTtBQTRERjtBQTFEQTtFQU1FLFdBQUE7QUF3REY7QUE3REU7RUFDRSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBQStESjtBQTVERTtFQUNFLGNBQUE7RUFDQSxXQUFBO0FBOERKO0FBNURNO0VBQ0UsVUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQThEUjtBQTdEUTtFQUNFLHFCQUFBO0VBQ0EsNkJBQUE7QUErRFY7QUE5RFU7RUFIRjtJQUlJLHNCQUFBO0VBaUVWO0FBQ0Y7QUEzREE7RUFDRSxtQkFBQTtBQThERjtBQTdERTtFQUNFLGNBQUE7QUErREo7QUE3REU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUErREo7QUE5REk7RUFDRSxVQUFBO0FBZ0VOO0FBN0RFO0VBQ0UsMkRBQUE7QUErREo7QUE1REE7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0FBK0RGO0FBN0RBO0VBQ0UsVUFBQTtFQUNBLGNBQUE7QUFnRUY7QUE5REE7RUFDRSx1Q0FBQTtBQWlFRjtBQS9EQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLFFBQUE7RUFDQSxhQUFBO0FBa0VGO0FBaEVBO0VBQ0UsY0FBQTtBQW1FRjtBQWpFQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FBb0VGO0FBakVBO0VBQ0Usb0NBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxPQUFBO0VBQ0EsTUFBQTtBQW9FRjtBQWpFQTtFQUNFLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFvRUY7QUFqRUE7RUFDRSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFvRUY7QUFqRUE7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDJCQUFBO0VBQ0EsUUFBQTtFQUNBLE1BQUE7RUFDQSxtQkFBQTtFQUNBLHdCQUFBO0FBb0VGO0FBakVBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBb0VGO0FBbEVBO0VBQ0UsVUFBQTtBQXFFRjtBQWxFQTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQXFFRjtBQWxFQTtFQUNFLGdDQUFBO0FBcUVGO0FBbkVBO0VBQ0UsVUFBQTtBQXNFRjtBQW5FQTtFQUNFLFVBQUE7QUFzRUY7QUFuRUE7RUFDRSxhQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBc0VGO0FBbkVBO0VBQ0UsVUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7QUFzRUY7QUFuRUE7RUFDRSxXQUFBO0FBc0VGO0FBbkVBO0VBQ0UsbUJBQUE7QUFzRUY7QUFuRUE7RUFDRSwwQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUFzRUY7QUFuRUE7RUFDRSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQXNFRjtBQXBFQTtFQUNFLDJEQUFBO0FBdUVGO0FBcEVBO0VBQ0UsZ0NBQUE7RUFDQSxXQUFBO0FBdUVGO0FBckVBO0VBQ0UsYUFBQTtBQXdFRjtBQXRFQTtFQUNFLGVBQUE7QUF5RUY7QUF2RUE7RUFDRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFFBQUE7QUEwRUY7QUF2RUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQTBFRjtBQXhFQTtFQUNFLGtCQUFBO0FBMkVGO0FBekVBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0FBNEVGO0FBMUVBO0VBQ0UsYUFBQTtBQTZFRjtBQTFFQTtFQUNFO0lBQ0UsbUJBQUE7RUE2RUY7O0VBMUVBO0lBQ0Usb0NBQUE7SUFDQSw0QkFBQTtJQUNBLG9CQUFBO0VBNkVGOztFQTFFQTtJQUNFLHlDQUFBO0lBQ0EsMENBQUE7RUE2RUY7O0VBeEVBO0lBQ0UsNEJBQUE7RUEyRUY7QUFDRjtBQXpFQTtFQUNFLFdBQUE7RUFDQSxlQUFBO0FBMkVGO0FBeEVBO0VBQ0U7SUFDRSwwQkFBQTtFQTJFRjtBQUNGO0FBeEVBO0VBQWlCLFdBQUE7RUFBWSxjQUFBO0VBQWUsV0FBQTtBQTZFNUM7QUEzRUE7RUFDRSxrQkFBQTtBQThFRjtBQTNFQTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUE4RUY7QUEzRUE7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7QUE4RUY7QUEzRUE7RUFDRSwwQkFBQTtFQUNBLFNBQUE7QUE4RUY7QUEzRUE7RUFDRSxzQkFBQTtBQThFRjtBQXpFQTtFQUNFLGNBQUE7QUE0RUY7QUF6RUE7RUFDRSx5QkFBQTtFQUNBLG9CQUFBO0FBNEVGO0FBekVBO0VBQ0UscUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQTRFRjtBQTFFQTtFQUNFLHNCQUFBO0VBQ0Esb0JBQUE7QUE2RUY7QUExRUE7RUFDRSx3QkFBQTtBQTZFRjtBQTFFQTtFQUNFLDRCQUFBO0FBNkVGO0FBMUVBO0VBQ0UsMkJBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7QUE2RUY7QUEzRUE7RUFDRSwyQkFBQTtBQThFRjtBQTVFQTtFQUNFLDZCQUFBO0FBK0VGO0FBN0VBO0VBQ0Usb0JBQUE7RUFDQSw0QkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7QUFnRkY7QUE3RUE7RUFDRSxXQUFBO0FBZ0ZGO0FBN0VBO0VBQ0Usa0JBQUE7RUFDQSxPQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQWdGRjtBQTlFQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFpRkY7QUEvRUE7RUFDRSxVQUFBO0VBQ0EsUUFBQTtBQWtGRjtBQWhGQTtFQUNFLFVBQUE7QUFtRkY7QUFqRkE7RUFDRSxRQUFBO0VBQ0EsMkJBQUE7QUFvRkY7QUFsRkE7RUFDRSxnQkFBQTtBQXFGRjtBQW5GQTtFQUFtQix3QkFBQTtBQXVGbkI7QUF0RkE7RUFDRSxnQkFBQTtBQXlGRjtBQXRGQTs7Ozs7OztHQUFBO0FBUUE7RUFDRSxVQUFBO0FBeUZGO0FBeEZFO0VBRkY7SUFHSSxhQUFBO0VBMkZGO0FBQ0Y7QUF6RkE7RUFDRSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FBNEZGO0FBekZBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtBQTRGRjtBQTFGQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtBQTZGRjtBQTFGRTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7QUE2Rko7QUF6RkU7RUFDRSxpQkFBQTtFQUNBLG9CQUFBO0FBNEZKO0FBMUZFO0VBQ0Usb0JBQUE7QUE0Rko7QUEzRkk7RUFDRSxVQUFBO0FBNkZOO0FBMUZFO0VBQ0UsY0FBQTtBQTRGSjtBQTFGRTtFQUNJLG9CQUFBO0FBNEZOO0FBM0ZNO0VBQ0Usb0JBQUE7RUFDQSx3Q0FBQTtFQUNBLDJCQUFBO0FBNkZSO0FBMUZRO0VBQ0UsMkJBQUE7QUE0RlY7QUF6Rkk7RUFDRSwyQkFBQTtFQUNBLHFCQUFBO0FBMkZOO0FBeEZFO0VBQ0UsWUFBQTtBQTBGSjtBQXpGSTtFQUNFLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7QUEyRk47QUExRk07RUFDRSxhQUFBO0FBNEZSO0FBMUZNO0VBQ0Usa0JBQUE7QUE0RlI7QUEzRlE7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsTUFBQTtBQTZGVjtBQTVGVTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FBOEZaO0FBeEZFO0VBQ0UsaUJBQUE7QUEwRko7QUF4RkU7RUFDRSw4QkFBQTtBQTBGSjtBQXhGRTtFQUNFLDhCQUFBO0FBMEZKO0FBeEZFO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBMEZKO0FBdkZFO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7QUF5Rko7QUF0RkU7RUFDRSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0FBd0ZKO0FBckZFO0VBQ0UsVUFBQTtFQUNBLFFBQUE7QUF1Rko7QUFwRkU7RUFDRSxhQUFBO0FBc0ZKO0FBcEZFO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtBQXNGSjtBQXBGRTtFQUNFLGtCQUFBO0FBc0ZKO0FBcEZFO0VBQ0UsaUNBQUE7QUFzRko7QUFwRkU7RUFDRSxnQ0FBQTtBQXNGSjtBQWxGTTtFQUNFLDJCQUFBO0FBb0ZSO0FBL0VJO0VBREY7SUFFSSxhQUFBO0VBa0ZKO0FBQ0Y7QUEvRUk7RUFERjtJQUVJLHdCQUFBO0VBa0ZKO0FBQ0Y7QUEvRUE7RUFDRSxXQUFBO0FBa0ZGIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PU1vbnRzZXJyYXQ6aXRhbCx3Z2h0QDAsMTAwOzAsMjAwOzAsMzAwOzAsNDAwOzAsNTAwOzAsNjAwOzAsNzAwOzAsODAwOzAsOTAwOzEsMTAwOzEsMjAwOzEsMzAwOzEsNDAwOzEsNTAwOzEsNjAwOzEsNzAwOzEsODAwOzEsOTAwJmRpc3BsYXk9c3dhcCcpO1xuaW5wdXQ6Oi13ZWJraXQtb3V0ZXItc3Bpbi1idXR0b24sXG5pbnB1dDo6LXdlYmtpdC1pbm5lci1zcGluLWJ1dHRvbiB7XG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbiAgbWFyZ2luOiAwO1xufVxuXG4vKiBGaXJlZm94ICovXG5pbnB1dFt0eXBlPW51bWJlcl0ge1xuICAtbW96LWFwcGVhcmFuY2U6IHRleHRtZXRyaWM7XG59XG4uY2xlYXJmaXg6YWZ0ZXJ7XG4gIGNvbnRlbnQ6XCJcIjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNsZWFyOiBib3RoO1xufVxuYm9keSB7XG4gIG1hcmdpbjogMCAhaW1wb3J0YW50O1xuICBmb250LWZhbWlseTogTW9udHNlcnJhdDtcbn1cbmgxLGgyLGgzLGg0LGg1LGg2LHB7XG4gIG1hcmdpbjogMHB4O1xufVxuLmJ0bi1kbHQge1xuICAgIGJhY2tncm91bmQ6IHZhcigtLXByaW1hcnktY29sb3IpO1xuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICBwYWRkaW5nOiAxMHB4IDE1cHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgJjpob3ZlcntcbiAgICAgIG9wYWNpdHk6IDAuODU7XG4gICAgfVxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgIH1cbn1cbmxpLmRlbGV0ZSB7XG4gICAgYmFja2dyb3VuZDogbm9uZSAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuLmJ0bi1kbHQgc3ZnIHtcbiAgICB3aWR0aDogMThweDtcbn1cbi52LW5vbmV7XG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xufVxuLnByZXZpZXctaW1ne1xuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICB3aWR0aDogNjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG59XG5heyB0ZXh0LWRlY29yYXRpb246IG5vbmU7IGN1cnNvcjogcG9pbnRlcn1cbi5mb3JtLWNvbnRhaW5lcnsgcGFkZGluZzogMHB4IDA7fVxuLm1haW4tY29udGFpbmVyIHtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICB3aWR0aDogMTAwJTtcbiAgICAuY29udGFpbmVye1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBtYXgtd2lkdGg6IDQyNXB4O1xuICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG8gMjBweDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgcGFkZGluZzogMHB4IDIwcHg7XG4gICAgICAgIH1cbiAgICB9XG4gICAgLmlubmVyLWNvbnRhaW5lciB7XG4gICAgICAgIC8qYmFja2dyb3VuZC1jb2xvcjogI2ZmZjsqL1xuICAgICAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgICAgIHBhZGRpbmctdG9wOiA4MnB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogNDBweDtcbiAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgICB9XG5cbiAgICB9XG4gICAgLnNpZGViYXJ7XG4gICAgICAgIHdpZHRoOiAzMCU7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgICAgICAgcGFkZGluZzogMHB4IDE1cHggMHB4IDBweDtcbiAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBtYXgtd2lkdGg6IDQyNXB4O1xuICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweCAyMHB4O1xuICAgICAgICB9XG4gICAgICAgIC5pbm5lci1zaWRlYmFyIHtcbiAgICAgICAgICAgIC8qYmFja2dyb3VuZDogI2ZmZjsqL1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNnB4O1xuICAgICAgICAgICAgcGFkZGluZzogMjBweDtcbiAgICAgICAgICAgIC51cGxvYWQtYm94IHtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogNjBweDtcbiAgICAgICAgICAgICAgICB3aWR0aDogMjAlO1xuICAgICAgICAgICAgICAgIGhlaWdodDogNjBweDtcbiAgICAgICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG4gICAgICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI0M0QzRDNDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgICAgICAgICAgICAgei1pbmRleDogMjtcbiAgICAgICAgICAgICAgICAudXBsb2FkLWltZyB7XG5cbiAgICAgICAgICAgICAgICAgICAgaW1nIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OjUwJTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnVwZGF0ZS1ib3gge1xuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjRjNGNEY0O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDBweCAwcHggMjZweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyNnB4O1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgICAgICB6LWluZGV4OiAyO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnVwZGF0ZS1ib3g6bGFzdC1jaGlsZCB7XG4gICAgICAgICAgICAgICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAubWV0cmljLWJveCB7XG4gICAgICAgICAgICAgICAgZmxvYXQ6IHJpZ2h0O1xuICAgICAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMTBweCAwcHggMTBweCAwcHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDcwJTtcbiAgICAgICAgICAgICAgICB6LWluZGV4OiAxO1xuICAgICAgICAgICAgICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpe1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogNjUlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KXtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCl7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiA3MyU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRleHRhcmVhIHtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0YzRjRGNDtcbiAgICAgICAgICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNnB4O1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiA2cHggNnB4O1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGV4dGFyZWE6Zm9jdXMge1xuICAgICAgICAgICAgICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5lZGl0LWljb24ge1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMHB4O1xuICAgICAgICAgICAgICAgIGZsb2F0OiByaWdodDtcbiAgICAgICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDIwcHggMHB4O1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgICAgICB6LWluZGV4OjI7XG4gICAgICAgICAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCl7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGltZyB7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiA0cHg7XG4gICAgICAgICAgICAgICAgICAgIGZsb2F0OiByaWdodDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuYWRkLW1ldHJpY3tcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICAgICAgc3Bhbi5hZGQtaWNvbiB7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNGM0Y0RjQ7XG4gICAgICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDNweCAzcHggNXB4IHJnYigwIDAgMCAvIDExJSk7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzM3B4O1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDMzcHg7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyN3B4O1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICAgICAgICAgIHotaW5kZXg6IDI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmFkZC1tZXRyaWM6YmVmb3JlIHtcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgICAgICAgICAgIGhlaWdodDogMXB4O1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmOGY4Zjg7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgIGJvdHRvbTogMTZweDtcbiAgICAgICAgICAgICAgICB6LWluZGV4OiAxO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICB9XG59XG4ucHVibGljIC50YWJzLW5hdiBsaSB7XG4gIC5pbnB1dC1zaXplciB7XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xuICAgIH1cbiAgfVxufVxuLnRhYnMtbmF2e1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICBvdmVyZmxvdzogYXV0bztcbiAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICB1bCB7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBwYWRkaW5nOiAwcHg7XG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgZGlzcGxheTogZ3JpZDtcbiAgICAgIGdyaWQtYXV0by1mbG93OiBjb2x1bW47XG4gICAgICAgIGxpIHtcbiAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICAgICAgYmFja2dyb3VuZDogI0YzRjRGNDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDVweCAyMHB4O1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBhIHtcbiAgICAgICAgICAgICAgICBmb250LWZhbWlseTogTW9udHNlcnJhdDtcbiAgICAgICAgICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgY29sb3I6ICMwMDAwMDA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzcGFuLmNsb25le1xuICAgICAgICAgICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICAgICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IDEzcHg7XG4gICAgICAgICAgICAgICAgdG9wOiAxMXB4O1xuICAgICAgICAgICAgICAgIGF7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDowcHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIC5tb3Zle1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgbGVmdDogMTAwJTtcbiAgICAgICAgICAgIHRvcDogMDtcbiAgICAgICAgICAgIGhlaWdodDogNDRweDtcbiAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlZWU7XG4gICAgICAgICAgICBhe1xuICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDM4cHg7XG4gICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAyZW07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAmLmxlZnR7XG4gICAgICAgICAgICAgIGxlZnQ6IGF1dG87XG4gICAgICAgICAgICAgIHJpZ2h0OiBjYWxjKDEwMCUgLSA0cHgpO1xuICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHggMCAwIDVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICYucmlnaHR7XG4gICAgICAgICAgICAgIGxlZnQ6IGNhbGMoMTAwJSAtIDRweCk7XG4gICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDAgNXB4IDVweCAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICAmOmhvdmVye1xuICAgICAgICAgICAgJjpsYXN0LWNoaWxke1xuICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgbGk6aG92ZXJ7XG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwIDUwcHggdmFyKC0tcHJpbWFyeS1jb2xvci10cmFuc3BhcmVudCkgaW5zZXQ7XG4gICAgICAgIH1cbiAgICAgICAgbGkuYWN0aXZle1xuICAgICAgICAgICAgYmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeS1jb2xvcik7XG4gICAgICAgICAgICBhLmlucHV0LXNpemVye1xuICAgICAgICAgICAgICAgIGlucHV0eyBjb2xvcjogI2ZmZjsgfVxuICAgICAgICAgICAgICAgIGlucHV0OmZvY3VzIHtcbiAgICAgICAgICAgICAgICAgICAgb3V0bGluZTpub25lO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICA6OnBsYWNlaG9sZGVyeyBjb2xvcjogI2ZmZjt9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBhLmlucHV0LXNpemVye1xuICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDoyMHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5pbnB1dC1zaXplciB7XG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZ3JpZDtcbiAgICAgICAgICAgICAgJjo6YWZ0ZXIsXG4gICAgICAgICAgICAgIGlucHV0IHtcbiAgICAgICAgICAgICAgICBncmlkLWFyZWE6IDIgLyAxO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAmOjphZnRlcixcbiAgICAgICAgICAgIGlucHV0IHtcbiAgICAgICAgICAgICAgd2lkdGg6IGF1dG87XG4gICAgICAgICAgICAgIG1pbi13aWR0aDogMWVtO1xuICAgICAgICAgICAgICBncmlkLWFyZWE6IDEgLyAyO1xuICAgICAgICAgICAgICBmb250OiBpbmhlcml0O1xuICAgICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICAgIHJlc2l6ZTogbm9uZTtcbiAgICAgICAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcbiAgICAgICAgICAgICAgYXBwZWFyYW5jZTogbm9uZTtcbiAgICAgICAgICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaW5wdXQ6Zm9jdXMge1xuICAgICAgICAgICAgICAgIG91dGxpbmU6c29saWQgMXB4ICMwMDA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICA6OnBsYWNlaG9sZGVyeyBjb2xvcjogIzAwMDt9XG4gICAgICAgICAgICAmOjphZnRlciB7XG4gICAgICAgICAgICAgIGNvbnRlbnQ6IGF0dHIoZGF0YS12YWx1ZSkgJyAnO1xuICAgICAgICAgICAgICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgfVxuXG4gICAgLnBsdXMge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjRjNGNEY0O1xuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgICAgIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xuXG5cblxuICAgIH1cbiAgICAucGx1cyBhIHtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgcGFkZGluZzogMHB4IDE2cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgc3ZnIHtcbiAgICAgICAgICAgIHdpZHRoOiAxMHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiAxMHB4O1xuICAgICAgICAgICAgcGFkZGluZzogMTVweCAwcHg7XG4gICAgICAgIH1cbiAgICB9XG5cbn1cbmxpLmNkay1kcmFnIHtcbiAgYmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeS1jb2xvcik7XG4gIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgcGFkZGluZzogNXB4IDIwcHg7XG4gIG1hcmdpbi1yaWdodDogMTJweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsaXN0LXN0eWxlOiBub25lO1xuICBhLmlucHV0LXNpemVye1xuICAgIG1hcmdpbi1yaWdodDoyMHB4O1xuICAgIGZvbnQtZmFtaWx5OiBNb250c2VycmF0O1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBjb2xvcjogIzAwMDAwMDtcbiAgfVxuXG4gIHNwYW4uY2xvbmUge1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIGhlaWdodDogMjBweDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDEzcHg7XG4gICAgdG9wOiAxMXB4O1xuXG4gICAgYSB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIGxpbmUtaGVpZ2h0OiAwcHg7XG4gICAgfVxuICB9XG4gIC5pbnB1dC1zaXplciB7XG4gICAgZGlzcGxheTogaW5saW5lLWdyaWQ7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgJjo6YWZ0ZXIsXG4gICAgaW5wdXQge1xuICAgICAgZ3JpZC1hcmVhOiAyIC8gMTtcbiAgICB9XG4gICAgJjo6YWZ0ZXIsXG4gICAgaW5wdXQge1xuICAgICAgd2lkdGg6IGF1dG87XG4gICAgICBtaW4td2lkdGg6IDFlbTtcbiAgICAgIGdyaWQtYXJlYTogMSAvIDI7XG4gICAgICBmb250OiBpbmhlcml0O1xuICAgICAgbWFyZ2luOiAwO1xuICAgICAgcmVzaXplOiBub25lO1xuICAgICAgYmFja2dyb3VuZDogbm9uZTtcbiAgICAgIGFwcGVhcmFuY2U6IG5vbmU7XG4gICAgICBib3JkZXI6IG5vbmU7XG4gICAgfVxuICAgIGlucHV0OmZvY3VzIHtcbiAgICAgIG91dGxpbmU6c29saWQgMXB4ICMwMDA7XG4gICAgfVxuICAgIDo6cGxhY2Vob2xkZXJ7IGNvbG9yOiAjMDAwO31cbiAgICAmOjphZnRlciB7XG4gICAgICBjb250ZW50OiBhdHRyKGRhdGEtdmFsdWUpICcgJztcbiAgICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICAgIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcbiAgICB9XG4gIH1cbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIH1cbn1cbi5kaXZUYWJsZUNlbGx7XG4gIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XG4gIC5ub3JtYWwtdGV4dHtcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjY2NjO1xuICAgIHBhZGRpbmc6IDAgNXB4O1xuICAgIHdpZHRoOiA3NSU7XG4gICAgbGluZS1oZWlnaHQ6IDI1cHg7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIH1cbn1cbi5jZGstZHJhZy1wcmV2aWV3IHtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3gtc2hhZG93OiAwIDVweCA1cHggLTNweCByZ2JhKDAsIDAsIDAsIDAuMiksXG4gIDAgOHB4IDEwcHggMXB4IHJnYmEoMCwgMCwgMCwgMC4xNCksXG4gIDAgM3B4IDE0cHggMnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG59XG5cbi5jZGstZHJhZy1wbGFjZWhvbGRlciB7XG4gIG9wYWNpdHk6IDA7XG59XG5cbi5jZGstZHJhZy1hbmltYXRpbmcge1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMjUwbXMgY3ViaWMtYmV6aWVyKDAsIDAsIDAuMiwgMSk7XG59XG5cbi5leGFtcGxlLWJveDpsYXN0LWNoaWxkIHtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4udGFicy1uYXYgbGkuY2RrLWRyb3AtbGlzdC1kcmFnZ2luZyAudGFicy1uYXYgbGk6bm90KC5jZGstZHJhZy1wbGFjZWhvbGRlcikge1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMjUwbXMgY3ViaWMtYmV6aWVyKDAsIDAsIDAuMiwgMSk7XG59XG4ucHVibGljIHtcbiAgLnRhYnMtbmF2IHtcbiAgICB1bCB7XG4gICAgICBsaXtcbiAgICAgICAgYS5pbnB1dC1zaXplciBzcGFue1xuXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGxpLmFjdGl2ZSB7XG4gICAgICAgIGEuaW5wdXQtc2l6ZXIge1xuICAgICAgICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4uZGl2VGFibGVSb3cubWFpbnBhZ2UtdXNlci1zZXR0aW5ncyB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5kaXZUYWJsZVJvdy5tYWlucGFnZS11c2VyLXNldHRpbmdzIHAge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5hLnNzb3Byb2Nlc3Npbmcge1xuICBvcGFjaXR5OiAwLjU7XG4gIGN1cnNvcjogd2FpdDtcbn1cbi5tZXRyaWMtc2V0dGluZ3N7XG4gIGZvbnQtZmFtaWx5OiBNb250c2VycmF0O1xuICBmb250LXNpemU6IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIC5vcHRpb25zIHtcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjY2NjO1xuICAgIGJhY2tncm91bmQ6ICNmMWYxZjE7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIHBhZGRpbmc6IDRweCA2cHg7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1pbi13aWR0aDogMTAycHg7XG4gICAgbWFyZ2luOiAwIGF1dG8gNnB4O1xuICAgIGVtIHtcbiAgICAgIGZvbnQtc2l6ZTogOXB4O1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBmbG9hdDogcmlnaHQ7XG4gICAgICBsaW5lLWhlaWdodDogMTRweDtcbiAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gIH1cbn1cbiAgbGFiZWx7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGlucHV0e1xuICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWFyZ2luOiAwcHggNXB4IDBweCAwcHg7XG4gICAgICB3aWR0aDogYXV0bztcbiAgICB9XG4gICAgLmNoZWNrYm94LXN0eWxle1xuICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgYmFja2dyb3VuZDogI2YxZjFmMTtcbiAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAgIGJvcmRlcjogc29saWQgMXB4ICNjY2M7XG4gICAgfVxuICB9XG5cbiAgZW0ge1xuICAgIGJhY2tncm91bmQ6ICNGRkYwNjc7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxM3B4O1xuICAgIGhlaWdodDogMTNweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBjb2xvcjogIzAwMDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IGRvdHRlZCBibGFjaztcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBtYXJnaW46IDEwcHggNXB4IDBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgLnRvb2x0aXB0ZXh0IHtcbiAgICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6cmdiYSgwLDAsMCwwLjkpO1xuICAgICAgY29sb3I6ICNmZmY7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBwYWRkaW5nOiA1cHg7XG4gICAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB6LWluZGV4OiAxO1xuICAgICAgd2lkdGg6IDIwMHB4O1xuICAgICAgdG9wOiAxMDAlO1xuICAgICAgbGVmdDogNTAlO1xuICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgIG1hcmdpbi1sZWZ0OiAtMTAwcHg7XG4gICAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgICBAbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICB9XG4gICAgfVxuICAgICYuYWN0aXZlIC50b29sdGlwdGV4dCB7XG4gICAgICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xuICAgIH1cbiAgfVxufVxuLm1ldHJpYy1zZXR0aW5ncy5nb2FsLXRpdGxlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDEycHg7XG4gIEBtZWRpYSAobWF4LXdpZHRoOiA3NjhweCkge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA3NjlweCkge1xuICAuZGl2VGFibGVDZWxsOmxhc3QtY2hpbGQge1xuICAgIGVtIHtcbiAgICAgIC50b29sdGlwdGV4dCB7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xuICAgICAgICBsZWZ0OiBhdXRvO1xuICAgICAgICByaWdodDogMFxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuLmRpdlRhYmxlQ2VsbDpudGgtY2hpbGQoMil7XG4gIGVte1xuICAgIC50b29sdGlwdGV4dHtcbiAgICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgfVxuICB9XG59XG4uZHJvcGRvd24tbmF2IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIC5kcm9wYnRuIHtcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjY2NjO1xuICAgIGJhY2tncm91bmQ6ICNmMWYxZjE7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIHBhZGRpbmc6IDRweDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWFyZ2luOiAwIGF1dG8gNnB4O1xuXG4gICAgZW0ge1xuICAgICAgZm9udC1zaXplOiA4cHg7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIGZsb2F0OiByaWdodDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIH1cbiAgICAudGFnIHtcbiAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgbWFyZ2luOiAwcHggNnB4IDBweCAwcHggIWltcG9ydGFudDtcbiAgICAgIHdpZHRoOiAxNnB4ICFpbXBvcnRhbnQ7XG4gICAgICBoZWlnaHQ6IDE2cHggIWltcG9ydGFudDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAxN3B4ICFpbXBvcnRhbnQ7XG4gICAgfVxuICB9XG4gICYuZHJvcGRvd24tZGQge1xuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgLmRyb3BidG4ge1xuICAgICAgLnRhZyB7XG4gICAgICAgIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XG4gICAgICB9XG4gICAgfVxuICAgIC50YWd7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHdpZHRoOiBhdXRvO1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB9XG4gICAgJjpob3ZlcntcbiAgICAgIC5kcm9wYnRue1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjFmMWYxO1xuICAgICAgICBjb2xvcjogaW5oZXJpdDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLmRyb3Bkb3duLWNvbnRlbnQge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmYWZhZmE7XG4gICAgcGFkZGluZzo4cHggNHB4IDBweCA0cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xuICAgIHotaW5kZXg6IDE7XG5cbiAgfVxufVxuXG5cblxuXG5cblxuLmRyb3Bkb3duLWNvbnRlbnQgYTpob3ZlciB7YmFja2dyb3VuZC1jb2xvcjogI2RkZDt9XG5cbi5kcm9wZG93bi1uYXYuYWN0aXZlIC5kcm9wZG93bi1jb250ZW50IHtkaXNwbGF5OiBibG9jazt9XG5cbi5kcm9wZG93bi1uYXY6aG92ZXIgLmRyb3BidG4ge2JhY2tncm91bmQtY29sb3I6IHZhcigtLXByaW1hcnktY29sb3IpOyBjb2xvcjogI2ZmZjt9XG4uc2V0dGluZyB7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNFRkVGRUY7XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNFRkVGRUY7XG4gICAgcGFkZGluZzogMTBweCAwcHg7XG4gICAgZm9udC1mYW1pbHk6IE1vbnRzZXJyYXQ7XG4gICAgbWFyZ2luLWJvdHRvbTogMTZweDtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgdG9wOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWJhY2tncm91bmQtY29sb3IpO1xuICAgIHotaW5kZXg6IDk5OTtcbiAgICAuZGF0ZS1ib3guc2VsZWN0LWJveCB7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICB9XG4gICAgfVxuICAgIC5kYXRlLWJveCB7XG4gICAgICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJSA7XG4gICAgICAgIH1cblxuICAgIH1cbiAgICAuZGF0ZS1ib3ggaW5wdXQsIC5kYXRlLWJveCBzZWxlY3Qge1xuICAgICAgZm9udC1mYW1pbHk6IE1vbnRzZXJyYXQ7XG4gICAgICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcblxuICAgICAgICAgICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcbiAgICB9XG4gICAgfVxuXG4gICAgLmRhdGUtYm94IHtcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgfVxuICAgIC51cGRhdGUge1xuICAgICAgICBmbG9hdDogcmlnaHQ7XG4gICAgICAmLmxlZnR7XG4gICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgfVxuICAgICAgJi5kZWxldGV7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgfVxuXG4gICAgfVxuICAgIGlucHV0IHtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI0VGRUZFRjtcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNnB4O1xuICAgICAgICBwYWRkaW5nOjBweCAxMnB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG4gICAgICAgIHdpZHRoOiAxNTBweDtcbiAgICAgICAgaGVpZ2h0OiA0MnB4O1xuICAgICAgICBsaW5lLWhlaWdodDogNDJweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICBmb250LWZhbWlseTogTW9udHNlcnJhdDtcbiAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAgICAgICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgICB9XG5cbiAgICB9XG4gICAgaW5wdXRbdHlwZT1cImRhdGVcIl06Zm9jdXMge1xuICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgIH1cbiAgICBzZWxlY3R7IG91dGxpbmU6IG5vbmU7IGZvbnQtd2VpZ2h0OiA2MDA7fVxuICAudXBkYXRlLnByaW1hcnktYnRue1xuICAgIGJ1dHRvbntcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNjY2M7XG4gICAgfVxuICB9XG4gICAgLnVwZGF0ZSBidXR0b24ge1xuICAgICAgICBtYXJnaW46IDBweCAhaW1wb3J0YW50O1xuICAgICAgICBiYWNrZ3JvdW5kOiAjRjNGNEY0O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgICAgIHBhZGRpbmc6IDBweCAxOHB4O1xuICAgICAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBsaW5lLWhlaWdodDogNDBweDtcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICBmb250LWZhbWlseTogTW9udHNlcnJhdDtcblxuICAgICAgICAmLnByb2Nlc3Npbmcge1xuICAgICAgICAgIHBhZGRpbmctbGVmdDogNDBweDtcblxuICAgICAgICAgIHNwYW4ge1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBsZWZ0OiAwO1xuICAgICAgICAgICAgdG9wOiAwO1xuXG4gICAgICAgICAgICBzdmcge1xuICAgICAgICAgICAgICB3aWR0aDogNDBweDtcbiAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgc3BhbntcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgIH1cbiAgICAgICY6aG92ZXJ7XG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCA1MHB4IHZhcigtLXByaW1hcnktY29sb3ItdHJhbnNwYXJlbnQpIGluc2V0O1xuICAgICAgfVxuICAgICAgJjphY3RpdmUsICYuYWN0aXZle1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1wcmltYXJ5LWNvbG9yKTtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICB9XG5cbiAgICB9XG4gICAgLnNlbGVjdC1ib3gge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjRUZFRkVGO1xuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgICAgIHBhZGRpbmc6IDlweCAxMHB4O1xuICAgICAgICBoZWlnaHQ6IDQycHg7XG4gICAgfVxuICAgIHNlbGVjdCB7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgIH1cbn1cbi5kaXZUYWJsZXtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XG4gICAgb3ZlcmZsb3cteTogdmlzaWJsZTtcbiAgICB6LWluZGV4OiA5O1xuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICB9XG4gICAgLmRpdlRhYmxlQm9keSB7XG4gICAgICAgIGRpc3BsYXk6IHRhYmxlLXJvdy1ncm91cDtcbiAgICAgICAgQG1lZGlhIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIH1cblxuICAgICAgICAuZGl2VGFibGVSb3cge1xuICAgICAgICAgICAgZGlzcGxheTogdGFibGUtcm93O1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTJweDtcblxuICAgICAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICAgICAgICAgICAgICAvKm92ZXJmbG93OiBoaWRkZW47Ki9cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5oaWRkZW5yb3d7XG4gICAgICAgICAgLmRpdlRhYmxlQ2VsbHtcbiAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gICAgICAgICAgfVxuICAgICAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgICAgICAgICAgID4gZGl2e1xuICAgICAgICAgICAgICAgICAgZGlzcGxheTpub25lO1xuICAgICAgICAgICAgICAgICAgJi5hZGQtaWNvbi1jb250YWluZXJ7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICAgICAgLmFkZC1pY29ue1xuICAgICAgICAgICAgICAgICAgICAgIGZsb2F0OiByaWdodDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuZGl2VGFibGVIZWFkaW5nIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNFRUU7XG4gICAgICAgICAgICBkaXNwbGF5OiB0YWJsZS1oZWFkZXItZ3JvdXA7XG4gICAgICAgIH1cbiAgICAgICAgLmZpcnN0Y2VsbHtcbiAgICAgICAgICBwb3NpdGlvbjpzdGlja3kgIWltcG9ydGFudDtcbiAgICAgICAgICBtaW4td2lkdGg6IDkwcHggIWltcG9ydGFudDtcbiAgICAgICAgICBsZWZ0OiAwcHg7XG4gICAgICAgICAgYmFja2dyb3VuZDogdmFyKC0tYmFja2dyb3VuZC1jb2xvcik7XG4gICAgICAgICAgd2lkdGg6IDkwcHggIWltcG9ydGFudDtcbiAgICAgICAgICBwYWRkaW5nOiA0cHggMHB4O1xuICAgICAgICAgIHotaW5kZXg6IDEwICFpbXBvcnRhbnQ7XG4gICAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgICAgICAgd2lkdGg6YXV0byAhaW1wb3J0YW50O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuZmlyc3RjZWxsLmRpdlRhYmxlQ2VsbCB7XG4gICAgICAgICAgICB3aWR0aDogMTAlO1xuICAgICAgICAgICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgICAgICAgICAgIHdpZHRoOiBhdXRvO1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjZjhmOGY4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDE1cHggMTJweCAxNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgcCB7XG4gICAgICAgICAgICBmb250LWZhbWlseTogTW9udHNlcnJhdDtcbiAgICAgICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMTVweDtcbiAgICAgICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5kaXZUYWJsZUNlbGw6YWZ0ZXIge1xuICAgICAgICAgICAgY29udGVudDogXCJcIjtcbiAgICAgICAgICAgIGhlaWdodDogMTJweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5kaXZUYWJsZUhlYWQge1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzk5OTk5OTtcbiAgICAgICAgICAgIGRpc3BsYXk6IHRhYmxlLWNlbGw7XG4gICAgICAgICAgICBwYWRkaW5nOiAzcHggMTBweDtcbiAgICAgICAgfVxuICAgICAgICAuZGl2VGFibGVIZWFkaW5nIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNFRUU7XG4gICAgICAgICAgICBkaXNwbGF5OiB0YWJsZS1oZWFkZXItZ3JvdXA7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICB9XG4gICAgICAgIC5kaXZUYWJsZUZvb3Qge1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI0VFRTtcbiAgICAgICAgICAgIGRpc3BsYXk6IHRhYmxlLWZvb3Rlci1ncm91cDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICB9XG4gICAgfVxuXG59XG4uZGl2VGFibGVDZWxse1xuICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG4gIHBhZGRpbmctbGVmdDogNnB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICYuZ3JleWVke1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XG4gICAgYm9yZGVyLWxlZnQ6IHNvbGlkIDZweCAjZmZmO1xuICAgIGJvcmRlci1yaWdodDogc29saWQgNnB4ICNmZmY7XG4gIH1cbiAgLmFkZC1pY29ue1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGM0Y0RjQ7XG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xuICAgIGhlaWdodDogNDJweDtcbiAgICB3aWR0aDogNDRweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDAgMTZweDtcbiAgICAmOmhvdmVye1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzA3QkRGRjtcbiAgICB9XG4gICAgc3Zne1xuICAgICAgd2lkdGg6IDEwcHg7XG4gICAgICBoZWlnaHQ6IDEwcHg7XG4gICAgICBwYWRkaW5nOiAxNXB4IDBweDtcbiAgICB9XG4gIH1cbiAgJi5hZGQtaWNvbi1jb250YWluZXJ7XG4gICAgd2lkdGg6IDQ0cHg7XG4gIH1cbiAgJjpsYXN0LWNoaWxkIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgcGFkZGluZzogMHB4IDEycHg7XG4gICAgfVxuICB9XG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICNmOGY4Zjg7XG4gICAgLypvdmVyZmxvdzogaGlkZGVuOyovXG4gICAgcGFkZGluZzogMHB4IDEycHg7XG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgfVxuICAubWV0cmljLWhpZGRlbntcbiAgICBkaXNwbGF5OiBub25lO1xuICAgIHRleHRhcmVhe1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB9XG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgd2lkdGg6IDUwJTtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgcGFkZGluZzogMTBweCAwcHg7XG4gICAgICBib3JkZXItcmlnaHQ6IHNvbGlkIDFweCAjZjhmOGY4O1xuICAgICAgcGFkZGluZy1yaWdodDogMTJweDtcbiAgICB9XG4gICAgLmhlYWRpbmd7XG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICB9XG4gIH1cbiAgaW5wdXR7XG4gICAgcGFkZGluZzogMDtcbiAgICBib3JkZXI6IDA7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0JztcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZDogdmFyKC0tYmFja2dyb3VuZC1jb2xvcik7XG4gICAgJjphY3RpdmUsICY6Zm9jdXN7XG4gICAgICBvdXRsaW5lOiBub25lO1xuICAgIH1cbiAgICBAbWVkaWEgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgfVxuICB9XG4gIHRleHRhcmVhIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgYm9yZGVyOiBuYXZham93aGl0ZTtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDE1cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi1ib3R0b206IDdweDtcbiAgICBmb250LWZhbWlseTogTW9udHNlcnJhdDtcbiAgICByZXNpemU6IG5vbmU7XG4gICAgJjpmb2N1cywgJjphY3RpdmV7XG4gICAgICBvdXRsaW5lOiBub25lO1xuICAgIH1cbiAgfVxuICB0ZXh0YXJlYSB7XG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDEycHg7XG4gICAgfVxuICB9XG4gIC5oZWFkaW5nIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLWJvdHRvbTogNHB4O1xuICAgIHRleHRhcmVhe1xuICAgICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgfVxuICAgIC5idXR0b24tZGVsZXRleyBkaXNwbGF5OiBibG9jazsgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgIC5kZWxldGUge1xuICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1wcmltYXJ5LWNvbG9yKTtcbiAgICAgICAgLyogd2lkdGg6IDIwcHg7ICovXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogMDtcbiAgICAgICAgcmlnaHQ6IDVweDtcbiAgICAgICAgJjpob3ZlcntcbiAgICAgICAgICBvcGFjaXR5OiAwLjg1O1xuICAgICAgICB9XG4gICAgICAgIHN2ZyB7XG4gICAgICAgICAgd2lkdGg6IDEwcHg7XG4gICAgICAgICAgaGVpZ2h0OiAxMHB4O1xuICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgLmN1cnJlbmN5LmFjdGl2ZSwuY3VycmVuY3k6aG92ZXJ7IGJhY2tncm91bmQ6ICNGRkYwNjcgIWltcG9ydGFudDt9XG4gIC5udW1iZXIuYWN0aXZlLC5udW1iZXI6aG92ZXJ7IGJhY2tncm91bmQ6ICMwN0JERkYgIWltcG9ydGFudDt9XG4gIC5wZXJjZW50YWdlLmFjdGl2ZSwucGVyY2VudGFnZTpob3ZlcnsgYmFja2dyb3VuZDogI2ZmNTQ1NSAhaW1wb3J0YW50O31cbiAgLnRleHQuYWN0aXZlLC50ZXh0OmhvdmVyeyBiYWNrZ3JvdW5kOiAjQTM3OUM5ICFpbXBvcnRhbnQ7fVxuICAucHJvZ3Jlc3MuYWN0aXZlLC5wcm9ncmVzczpob3ZlcnsgYmFja2dyb3VuZDogIzM1Q0U4RCAhaW1wb3J0YW50O31cbiAgLmRyb3Bkb3duLWxpc3QuYWN0aXZlLC5kcm9wZG93bi1saXN0OmhvdmVyeyBiYWNrZ3JvdW5kOiAjRkRBOTBEICFpbXBvcnRhbnQ7fVxuICAudGFnIHtcbiAgICBiYWNrZ3JvdW5kOiAjRjNGNEY0O1xuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIGhlaWdodDogMjBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgbWFyZ2luOiAwcHggMnB4IDRweDtcbiAgICBmb250LWZhbWlseTogTW9udHNlcnJhdDtcbiAgICBmb250LXdlaWdodDogNzAwO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgYXsgY29sb3I6IzAwMDsgZGlzcGxheTogYmxvY2s7fVxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgICAgIHdpZHRoOiAxOHB4O1xuICAgICAgaGVpZ2h0OiAxOHB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgICBtYXJnaW46IDAgMXB4O1xuICAgIH1cbiAgfVxuICAudGV4dG1ldHJpYyB7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRUZFRkVGO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIHBhZGRpbmc6IDBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAmLmRpc2FibGVke1xuICAgICAgYmFja2dyb3VuZDogI2VlZTtcbiAgICB9XG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgICAgd2lkdGg6IDYxJTtcbiAgICAgIG1hcmdpbjowIDAgMCA0JTtcbiAgICB9XG4gICAgLmlubi1tZXRyaWMge1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAubWV0cmljLWljb24ge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjRkZGMDY3O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA2cHggMHB4IDBweCA2cHg7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgaGVpZ2h0OiAzM3B4O1xuICAgICAgICB3aWR0aDogMzNweDtcbiAgICAgICAgcGFkZGluZzogMHB4IDhweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBsaW5lLWhlaWdodDogMzNweDtcbiAgICAgICAgZm9udC1mYW1pbHk6IE1vbnRzZXJyYXQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDBweDtcbiAgICAgICAgbGVmdDogMHB4O1xuICAgICAgICAmLm51bWJlcntcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAjMDdCREZGO1xuICAgICAgICB9XG4gICAgICAgICYucGVyY2VudGFnZXtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmY1NDU1O1xuICAgICAgICB9XG4gICAgICAgICYudGV4dHtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAjQTM3OUM5O1xuICAgICAgICB9XG4gICAgICAgICYucHJvZ3Jlc3N7XG4gICAgICAgICAgYmFja2dyb3VuZDogIzM1Q0U4RDtcbiAgICAgICAgfVxuICAgICAgICAmLmRyb3Bkb3duLWxpc3R7XG4gICAgICAgICAgYmFja2dyb3VuZDogI0ZEQTkwRDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaW5wdXQsIHNlbGVjdCB7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIHBhZGRpbmc6IDRweCAxMHB4IDRweCA0MHB4O1xuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgICAgICBoZWlnaHQ6IDMzcHg7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCc7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgICBib3JkZXI6IHNvbGlkIDFweCByZWQ7XG4gICAgICAgICY6ZGlzYWJsZWR7XG4gICAgICAgICAgYmFja2dyb3VuZDogI2VlZTtcbiAgICAgICAgfVxuXG4gICAgICB9XG4gICAgICBpbnB1dDpmb2N1cyB7XG4gICAgICAgIG91dGxpbmU6IG5vbmU7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbn1cbi5zZXR0aW5nc1RhYmxlIC50ZXh0bWV0cmljIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG59XG4uc2V0dGluZ3NUYWJsZSAuZGl2VGFibGVDZWxsOmxhc3QtY2hpbGR7XG4gIHBhZGRpbmc6IDEycHggIWltcG9ydGFudDtcbn1cbi5zZXR0aW5nc1RhYmxlIGlucHV0LCAuc2V0dGluZ3NUYWJsZSBzZWxlY3R7XG4gIHBhZGRpbmctbGVmdDogMTBweCAhaW1wb3J0YW50O1xufVxuLm9wdGlvbnMtYnV0dG9uIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4ub3B0aW9ucy1idXR0b24gYSB7XG4gIGJvcmRlcjogc29saWQgMXB4ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYmFja2dyb3VuZDogI2YxZjFmMTtcbiAgcGFkZGluZzogNXB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiAxMXB4O1xuICBsaW5lLWhlaWdodDogMTRweDtcbn1cbi5vcHRpb25zLWJ1dHRvbiAuZHJvcGRvd24tY29udGVudCB7XG4gIHdpZHRoOiAxODBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2NjYztcbiAgei1pbmRleDogOTk5O1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHRvcDogMzBweDtcbiAgZGlzcGxheTogbm9uZTtcbn1cbi5vcHRpb25zLWJ1dHRvbi5hY3RpdmUgLmRyb3Bkb3duLWNvbnRlbnQge1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLm9wdGlvbnMtYnV0dG9uIHRleHRhcmVhIHtcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2NjYyAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIHBhZGRpbmc6IDVweDtcbn1cbi50ZXh0bWV0cmljLmRpc2FibGVke1xuICAubWV0cmljLWljb257XG4gICAgYmFja2dyb3VuZDogI2RkZCAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XG4gIH1cbiAgLnByb2dyZXNzLWJhci12YWx1ZXtcbiAgICBjb2xvcjogI2ZmZjtcbiAgfVxufVxuLnB1YmxpYyAubWV0cmljLWljb24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2NjICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAyNHB4O1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xufVxuLmNkay1kcmFnIHtcbiAgY3Vyc29yOiBtb3ZlO1xufVxuXG4uY2RrLWRyYWcgPiAqIHtcbiAgY3Vyc29yOiBhdXRvO1xufVxuXG4uYWRkLXVwbG9hZC1ib3h7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHNwYW4ge1xuXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgei1pbmRleDogMjtcbiAgICAgICAgYXtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgd2lkdGg6IDMzcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDMzcHg7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjRjNGNEY0O1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDI4cHg7XG4gICAgICAgICAgICBtYXJnaW46IDAgYXV0bztcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDNweCAzcHggNXB4IHJnYigwIDAgMCAvIDExJSk7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgICAgICAgaW1nIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogOHB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGE6aG92ZXJ7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1wcmltYXJ5LWNvbG9yKTtcbiAgICAgICAgfVxuICAgIH1cblxufVxuLmFkZC11cGxvYWQtYm94OmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBoZWlnaHQ6IDFweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiAjZjhmOGY4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDE3cHg7XG4gICAgei1pbmRleDogMTtcbn1cblxuXG4uaW1nLXBvcHVwe1xuICAgIGJveC1zaGFkb3c6MXB4IDJweCA3cHggMXB4IGdyZXk7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgcGFkZGluZzogMjBweDtcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgaGVpZ2h0OiAxMHJlbTtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgbGVmdDogMDtcbiAgICBib3R0b206IDA7XG4gICAgdG9wOiAwO1xuICAgIHJpZ2h0OiAwO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICB3aWR0aDo0MDBweDtcbiAgICAuZm9ybXtcbiAgICAgICAgaW5wdXR7XG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjRjNGNEY0O1xuICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDZweCA2cHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgfVxuICAgIH1cbn1cbi5idG4tYWRke1xuICAgIHBhZGRpbmc6MTBweDtcbiAgICBiYWNrZ3JvdW5kOmxpZ2h0Z3JheTtcbiAgICB3aWR0aDogNTBweDtcbiAgICBtYXJnaW4tdG9wOiA0cHg7XG4gICAgYm9yZGVyLXJhZGl1czo2cHg7XG4gICAgYm9yZGVyOm5vbmU7XG59XG4ubS0we1xuICAgIG1hcmdpbjogMDtcbn1cbi5wb3B1cC1oZWFke1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmNsb3NlLXBvcHVwe1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5kLW5vbmV7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cbi5wcm9ncmVzcy1iYXItdmFsdWUge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBwYWRkaW5nLWxlZnQ6IDMwcHg7XG4gIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCc7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5wcm9ncmVzcy1iYXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1heC13aWR0aDogMjIwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXI6IHNvbGlkIDFweCAjRUZFRkVGO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XG4gIGJvdHRvbTogNnB4O1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICB6LWluZGV4OiA5OTtcbn1cbi5wcm9ncmVzcy1iYXIgc3BhbiB7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiAxMXB4O1xufVxuXG4ucHJvZ3Jlc3MtaW5wdXQge31cblxuLnByb2dyZXNzLWJhci1iZyB7XG4gIGJvcmRlcjogc29saWQgMXB4ICNFRkVGRUY7XG4gIGJhY2tncm91bmQ6ICNFRkVGRUY7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgaGVpZ2h0OiAxMHB4O1xufVxuXG4ucHJvZ3Jlc3MtYmFyLXNlbGVjdG9yIHNwYW4ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiA5JTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBoZWlnaHQ6IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgei1pbmRleDogOTtcbn1cbi5wcm9ncmVzcy1iYXItc2VsZWN0b3Igc3BhbjpmaXJzdC1jaGlsZCwgLnByb2dyZXNzLWJhci1zZWxlY3RvciBzcGFuOm50aC1sYXN0LWNoaWxkKDIpe1xuICB3aWR0aDogOSU7XG59XG4ucHJvZ3Jlc3MtYmFyLXNlbGVjdG9yIHNwYW4gYXtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5wcm9ncmVzcy1iYXItZmlsbGluZyB7XG4gIGJhY2tncm91bmQ6ICM4OWQzZGE7XG4gIGhlaWdodDogMTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLnByb2dyZXNzLWJhci1zZWxlY3RvcntcbiAgaGVpZ2h0OiAxMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZHJvcGRvd24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6ICNlZmVmZWY7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcmlnaHQ6IDA7XG4gIHdpZHRoOiAyMDBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgei1pbmRleDogOTk5O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG5cbn1cblxuLmRpdlRhYmxlQ2VsbC5hZGQtaWNvbi1jb250YWluZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4uYWRkLWljb24tY29udGFpbmVyIHtcbiAgcG9zaXRpb246IHN0aWNreSAhaW1wb3J0YW50O1xuICByaWdodDogMHB4O1xuICB6LWluZGV4OiAxMCAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuLmRyb3Bkb3duIHVsIHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgcGFkZGluZzogMDtcbiAgbWFyZ2luOiAwO1xufVxuXG4uZHJvcGRvd24gbGkgYSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggI2RkZDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4uZHJvcGRvd24gbGkgYTpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICNmYTVkNDg7XG4gIGNvbG9yOiAjZmZmO1xufVxuLmRlbGV0ZSBhIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBjb2xvcjogdmFyKC0tcHJpbWFyeS1jb2xvcik7XG4gIHBhZGRpbmc6IDRweCA2cHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuW2hpZGRlbl0geyBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7fVxuc3Bhbi5ub3RpZmljYXRpb24ge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgb3BhY2l0eTogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuc3Bhbi5ub3RpZmljYXRpb24uYWN0aXZle1xuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDFzIGVhc2UtaW4tb3V0O1xuICBvcGFjaXR5OiAxO1xufVxuc3Bhbi5ub3RpZmljYXRpb24uc3VjY2Vzc3tcbiAgY29sb3I6IGRvZGdlcmJsdWVcbn1cbnNwYW4ubm90aWZpY2F0aW9uLmVycm9yLCBzcGFuLm5vdGlmaWNhdGlvbi5lcnJvciBhe1xuICBjb2xvcjogdmFyKC0tcHJpbWFyeS1jb2xvcikgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLnByb2dyZXNzLWlucHV0IHtcbiAgbGluZS1oZWlnaHQ6IDMycHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5hZGQtYnV0dG9ue1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZsb2F0OiByaWdodDtcbiAgei1pbmRleDogMTA7XG4gIC5hZGQtaWNvbntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjNGNEY0O1xuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICBoZWlnaHQ6IDQycHg7XG4gICAgd2lkdGg6IDQ0cHg7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDAgMTZweDtcbiAgICAmOmhvdmVye1xuICAgICAgYm94LXNoYWRvdzogMCAwIDUwcHggcmdiKDI1NSA4NCA4NSAvIDEwJSkgaW5zZXQ7XG4gICAgfVxuICAgIHN2Z3tcbiAgICAgIHdpZHRoOiAxMHB4O1xuICAgICAgaGVpZ2h0OiAxMHB4O1xuICAgICAgcGFkZGluZzogMTVweCAwcHg7XG4gICAgfVxuICB9XG59XG4uaGVhZGluZyB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5kaXZUYWJsZS5zZXR0aW5nc1RhYmxlID4gLmRpdlRhYmxlQm9keSA+IC5kaXZUYWJsZVJvdyA+IC5kaXZUYWJsZUNlbGx7XG4gIHZlcnRpY2FsLWFsaWduOiB0b3AgIWltcG9ydGFudDtcbiAgcGFkZGluZzogMTVweCAyLjUlO1xufVxuLmRpdlRhYmxlLnNldHRpbmdzVGFibGV7XG4gIC5oZWFkaW5ne1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxuICB3aWR0aDogMTAwJTtcbiAgLmRpdlRhYmxlQm9keXtcbiAgICBkaXNwbGF5OiB0YWJsZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAuZGl2VGFibGVSb3d7XG4gICAgICAuZGl2VGFibGVDZWxse1xuICAgICAgICB3aWR0aDogNDUlO1xuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgICAgICBmb250LWZhbWlseTogTW9udHNlcnJhdDtcbiAgICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICYuZmlyc3RjZWxse1xuICAgICAgICAgIHdpZHRoOiAzMyUgIWltcG9ydGFudDtcbiAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmUgIWltcG9ydGFudDtcbiAgICAgICAgICBAbWVkaWEgKG1heC13aWR0aDogNzY3cHgpe1xuICAgICAgICAgICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbi5idG4tZGx0LmJ0bi1zZXR0aW5ncyB7XG4gIGJhY2tncm91bmQ6ICNGM0Y0RjQ7XG4gIHNwYW57XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cbiAgc3Zne1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIGhlaWdodDogMjBweDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICAqe1xuICAgICAgZmlsbDogIzAwMDtcbiAgICB9XG4gIH1cbiAgJjpob3ZlcntcbiAgICBib3gtc2hhZG93OiAwIDAgNTBweCB2YXIoLS1wcmltYXJ5LWNvbG9yLXRyYW5zcGFyZW50KSBpbnNldDtcbiAgfVxufVxuLm5vdGlmaWNhdGlvbiB7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICB0cmFuc2l0aW9uOiBhbGwgMXMgZWFzZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogOTk5OTk7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIG1hcmdpbjogMTBweCBhdXRvO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBjb2xvcjogI2ZmZjtcbn1cbi5ub3RpZmljYXRpb24uYWN0aXZle1xuICBvcGFjaXR5OiAxO1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5ub3RpZmljYXRpb24uc3VjY2Vzc3tcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDUgMTI5IDE0IC8gODAlKTtcbn1cbnNwYW4uY29weVRvQWxsIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMjBweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICByaWdodDogNXB4O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHRvcDogNXB4O1xuICBkaXNwbGF5OiBub25lO1xufVxuLmlubi1tZXRyaWM6aG92ZXIgc3Bhbi5jb3B5VG9BbGwsIC5pbm4tbWV0cmljOmZvY3VzIHNwYW4uY29weVRvQWxse1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5jb3B5UG9wdXAge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDUwMHB4O1xuICBvdmVyZmxvdzogYXV0bztcbiAgei1pbmRleDogOTk7XG4gIC8qIGRpc3BsYXk6IG5vbmU7ICovXG59XG5cbi5jb3B5UG9wdXAgLmJnIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjEpO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbn1cblxuLmNvcHlQb3B1cENvbnRlbnQge1xuICB3aWR0aDogOTAlO1xuICBtYXgtd2lkdGg6IDQ4MHB4O1xuICBtaW4td2lkdGg6IDM2MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBib3gtc2hhZG93OiAwIDAgMTBweCAjY2NjO1xuICBtYXJnaW46IDAgYXV0bztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uY29weVBvcHVwSGVhZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICNlZWU7XG4gIHBhZGRpbmctcmlnaHQ6IDQwcHg7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4uY29weVBvcHVwSGVhZGVyIC5jbG9zZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIGJvcmRlci1sZWZ0OiBzb2xpZCAxcHggI2VlZTtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogMDtcbiAgYmFja2dyb3VuZDogI0YzRjRGNDtcbiAgYm9yZGVyLXJhZGl1czogMCA4cHggMCAwO1xufVxuXG4uY29weVBvcHVwSGVhZGVyIC5jbG9zZSBzdmcge1xuICB3aWR0aDogMThweDtcbiAgaGVpZ2h0OiAxOHB4O1xuICBtYXJnaW4tdG9wOiAxMXB4O1xuICBtYXJnaW4tbGVmdDogMTFweDtcbn1cbi5jb3B5UG9wdXBIZWFkZXIgLmNsb3NlIHN2ZyAuc3Qwe1xuICBmaWxsOiAjMDAwO1xufVxuXG4uY29weVBvcHVwSGVhZGVyIC5jbG9zZSBhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG59XG5cbi5jb3B5UG9wdXBIZWFkZXIgLmNsb3NlOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeS1jb2xvcik7XG59XG4uY29weVBvcHVwSGVhZGVyIC5jbG9zZTpob3ZlciBzdmcgLnN0MHtcbiAgZmlsbDogI2ZmZjtcbn1cblxuLmNvcHlQb3B1cEhlYWRlciAuY2xvc2UgYTpob3ZlciBzdmcge1xuICBmaWxsOiAjZmZmO1xufVxuXG4uY29weVBvcHVwQm9keSB7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLmNvcHlQb3B1cE9wdGlvbnMgaW5wdXRbdHlwZT10ZXh0XSB7XG4gIHdpZHRoOiAyMCU7XG4gIGZsb2F0OiByaWdodDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBwYWRkaW5nOiAwIDVweDtcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2VlZTtcbn1cblxuLmNvcHlQb3B1cE9wdGlvbnMgbGFiZWwge1xuICBmbG9hdDogbGVmdDtcbn1cblxuLmNvcHlQb3B1cE9wdGlvbnMge1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4uY29weVBvcHVwRm9vdGVyIHtcbiAgYm9yZGVyLXRvcDogc29saWQgMXB4ICNlZWU7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbn1cblxuLmNvcHlQb3B1cEZvb3RlciBidXR0b24ge1xuICBoZWlnaHQ6IDQwcHg7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICB3aWR0aDogMTIwcHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBiYWNrZ3JvdW5kOiAjRjNGNEY0O1xufVxuLmNvcHlQb3B1cEZvb3RlciBidXR0b246aG92ZXJ7XG4gIGJveC1zaGFkb3c6IDAgMCA1MHB4IHZhcigtLXByaW1hcnktY29sb3ItdHJhbnNwYXJlbnQpIGluc2V0O1xufVxuXG4uY29weVBvcHVwRm9vdGVyIGJ1dHRvbi5jb3B5UG9wdXBTYXZlIHtcbiAgYmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeS1jb2xvcik7XG4gIGNvbG9yOiAjZmZmO1xufVxuLmNvcHlQb3B1cEZvb3RlciBidXR0b24uY29weVBvcHVwU2F2ZTpob3ZlciB7XG4gIG9wYWNpdHk6IDAuODU7XG59XG5idXR0b257XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbnNwYW4uZWRpdEljb24ge1xuICB3aWR0aDogMTJweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbn1cblxuc3Bhbi5lZGl0SWNvbiBzdmcge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuLmhlYWRpbmd7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi50b29sdGlwICp7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5kZWx0ZVRlcm06aG92ZXIgaW1nIHtcbiAgb3BhY2l0eTogMC44NTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDc2N3B4KXtcbiAgLmRpdlRhYmxlUm93IHtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICB9XG5cbiAgLmZpcnN0Y2VsbC5kaXZUYWJsZUNlbGwge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmM2Y0ZjQgIWltcG9ydGFudDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMCAwO1xuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICB9XG5cbiAgLmRpdlRhYmxlQ2VsbCB7XG4gICAgYm9yZGVyLWxlZnQ6IHNvbGlkIDFweCAjZjNmNGY0ICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXJpZ2h0OiBzb2xpZCAxcHggI2YzZjRmNCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmRpdlRhYmxlQ2VsbC5ncmV5ZWQge31cblxuICAuZGl2VGFibGVDZWxsOmxhc3QtY2hpbGQge1xuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAxMHB4IDEwcHg7XG4gIH1cbn1cbi5kYXNoYm9hcmQtbGluayBzdmcge1xuICB3aWR0aDogMjhweDtcbiAgbWFyZ2luLXRvcDogMnB4O1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogMzgwcHgpe1xuICAuc2V0dGluZyBidXR0b24ge1xuICAgIHBhZGRpbmc6IDAgMTBweCAhaW1wb3J0YW50O1xuICB9XG59XG5cbi5jbGVhcmZpeDphZnRlciB7Y29udGVudDogXCJcIjtkaXNwbGF5OiBibG9jaztjbGVhcjogYm90aDt9XG5cbi53aWRnZXQgLnNldHRpbmcge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi53aWRnZXQge1xuICBtYXgtd2lkdGg6IDQ4MHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2VlZTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4ud2lkZ2V0IC5oZWFkZXIgaDMge1xuICBmbG9hdDogbGVmdDtcbiAgbGluZS1oZWlnaHQ6IDQwcHg7XG59XG5cbi53aWRnZXQgLmhlYWRlciB7XG4gIHBhZGRpbmc6IDVweCAxMHB4IDVweCAxNXB4O1xuICBtYXJnaW46IDA7XG59XG5cbi53aWRnZXQgKiB7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG5cbi53aWRnZXQgLmhlYWRlciAubGVmdCB7fVxuXG4ud2lkZ2V0IC5kaXZUYWJsZSB7XG4gIHBhZGRpbmctdG9wOiAwO1xufVxuXG4ud2lkZ2V0IC5kaXZUYWJsZUJvZHksIC53aWRnZXQgLmRpdlRhYmxlUm93LCAud2lkZ2V0IC5kaXZUYWJsZUNlbGwge1xuICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xuICBtYXJnaW46IDAgIWltcG9ydGFudDtcbn1cblxuLndpZGdldCAuZGl2VGFibGVDZWxsIHtcbiAgd2lkdGg6IDUwJSAhaW1wb3J0YW50O1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZzogNXB4O1xufVxuLndpZGdldCAuZGl2VGFibGVDZWxsIC50ZXh0bWV0cmlje1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXJnaW46IDAgIWltcG9ydGFudDtcbn1cblxuLndpZGdldCAuZGl2VGFibGVDZWxsOmFmdGVyIHtcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4ud2lkZ2V0IC5kaXZUYWJsZUNlbGw6Zmlyc3QtY2hpbGQge1xuICBib3JkZXItcmlnaHQ6IHNvbGlkIDFweCAjZWVlO1xufVxuXG4ud2lkZ2V0IC5kaXZUYWJsZUNlbGwgLmhlYWRpbmcge1xuICB0ZXh0LWFsaWduOiBsZWZ0ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIG1hcmdpbjogMCAhaW1wb3J0YW50O1xufVxuLndpZGdldCAuZGl2VGFibGVDZWxsIC5oZWFkaW5nIGlucHV0e1xuICB0ZXh0LWFsaWduOiBsZWZ0ICFpbXBvcnRhbnQ7XG59XG4ud2lkZ2V0IC5kaXZUYWJsZVJvd3tcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICNlZWU7XG59XG4ud2lkZ2V0IC5mb290ZXIge1xuICBib3JkZXI6IDAgIWltcG9ydGFudDtcbiAgcGFkZGluZzogNXB4IDMwcHggIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZvb3Rlci5zZXR0aW5nIC51cGRhdGUucHJpbWFyeS1idG4ge1xuICBmbG9hdDogbm9uZTtcbn1cblxuLndpZGdldCAuZm9vdGVyIC5sZWZ0LWJ0biwgLndpZGdldCAuZm9vdGVyIC5yaWdodC1idG4ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDMwcHg7XG4gIGhlaWdodDogMTAwJTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLndpZGdldCAuZm9vdGVyIC5sZWZ0LWJ0biBzdmcsIC53aWRnZXQgLmZvb3RlciAucmlnaHQtYnRuIHN2Z3tcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMjBweDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi53aWRnZXQgLmZvb3RlciAucmlnaHQtYnRuIHtcbiAgbGVmdDogYXV0bztcbiAgcmlnaHQ6IDA7XG59XG4ud2lkZ2V0IC5mb290ZXIgLmxlZnQtYnRuLmRpc2FibGVkIHN2ZywgLndpZGdldCAuZm9vdGVyIC5yaWdodC1idG4uZGlzYWJsZWQgc3Zne1xuICBmaWxsOiAjZWVlO1xufVxuLndpZGdldCBzcGFuLmVkaXRJY29ue1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuLndpZGdldCAubWV0cmljLXNldHRpbmdzLmdvYWwtdGl0bGV7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG4ud2lkZ2V0IFtoaWRkZW5dIHsgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O31cbi53aWRnZXQgaW5wdXQsIC53aWRnZXQgc2VsZWN0e1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG4vKiAud2lkZ2V0IC5wcmltYXJ5LWJ0biBidXR0b257XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXByaW1hcnktY29sb3IpICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZmZmO1xufVxuLndpZGdldCAucHJpbWFyeS1idG4gYnV0dG9uLnByb2Nlc3Npbmcge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2NjICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjMDAwICFpbXBvcnRhbnQ7XG59ICovXG4uaGVhZGluZy5ncmV5ZWQsIC5kaXZUYWJsZUNlbGwuZ3JleWVkIHtcbiAgb3BhY2l0eTogMDtcbiAgQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxufVxuLmRhc2hib2FyZC1saW5rLXdpdGgtdGV4dCBhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcGFkZGluZzogMHB4IDUwcHggMHB4IDEwcHg7XG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uZGFzaGJvYXJkLWxpbmstd2l0aC10ZXh0IGEgc3ZnIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTFweDtcbiAgdG9wOiA4cHg7XG4gIHdpZHRoOiAyMHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMjBweDtcbn1cbi5ub3RpZmljYXRpb24uYWN0aXZle1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmJvYXJkcm9vbUNvbnRhaW5lcntcbiAgLnNldHRpbmd7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMCAhaW1wb3J0YW50O1xuICB9XG59XG4uYm9hcmRyb29te1xuICAuZGl2VGFibGV7XG4gICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDMwcHg7XG4gIH1cbiAgLmFsZXJ0e1xuICAgIHBhZGRpbmc6IDAgMTBweCAzMHB4O1xuICAgICYuYWxlcnQtZGFuZ2Vye1xuICAgICAgY29sb3I6IHJlZDtcbiAgICB9XG4gIH1cbiAgaHIuc2VwYXJhdG9yIHtcbiAgICBtYXJnaW46IDMwcHggMDtcbiAgfVxuICAudGV4dG1ldHJpYy5nb2FsVmlld3tcbiAgICAgIGJvcmRlcjogMCAhaW1wb3J0YW50O1xuICAgICAgc2VsZWN0LGlucHV0e1xuICAgICAgICBib3JkZXI6IDAgIWltcG9ydGFudDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICNlZWUgIWltcG9ydGFudDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMCAhaW1wb3J0YW50O1xuICAgICAgfVxuICAgICAgLmlubi1tZXRyaWN7XG4gICAgICAgIC5tZXRyaWMtaWNvbntcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICBzZWxlY3Q6ZGlzYWJsZWQge1xuICAgICAgYmFja2dyb3VuZDogI2ZmZiAhaW1wb3J0YW50O1xuICAgICAgb3BhY2l0eTogMSAhaW1wb3J0YW50O1xuICAgIH1cbiAgICB9XG4gIC51cGRhdGUucHJpbWFyeS1idG57XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIGJ1dHRvbntcbiAgICAgIG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XG4gICAgICBiYWNrZ3JvdW5kOiAjY2NjO1xuICAgICAgYm9yZGVyLXJhZGl1czogNnB4O1xuICAgICAgcGFkZGluZzogMHB4IDE4cHg7XG4gICAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBsaW5lLWhlaWdodDogNDBweDtcbiAgICAgIGhlaWdodDogNDBweDtcbiAgICAgIGZvbnQtZmFtaWx5OiBNb250c2VycmF0O1xuICAgICAgc3BhbntcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgIH1cbiAgICAgICYucHJvY2Vzc2luZ3tcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA0MHB4O1xuICAgICAgICBzcGFue1xuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICBsZWZ0OjA7XG4gICAgICAgICAgdG9wOiAwO1xuICAgICAgICAgIHN2Z3tcbiAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuICAuaGVhZGluZy1zbSBoMiB7XG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gIH1cbiAgLnRleHRtZXRyaWMuYmctcmVkLCAudGV4dG1ldHJpYy5iZy1yZWQgaW5wdXQsIC50ZXh0bWV0cmljLmJnLXJlZCBzZWxlY3Qge1xuICAgIGJhY2tncm91bmQ6ICNmNmIyYjIgIWltcG9ydGFudDtcbiAgfVxuICAudGV4dG1ldHJpYy5iZy1ncmVlbiwgLnRleHRtZXRyaWMuYmctZ3JlZW4gaW5wdXQsIC50ZXh0bWV0cmljLmJnLWdyZWVuIHNlbGVjdCB7XG4gICAgYmFja2dyb3VuZDogI2IyZGRjNiAhaW1wb3J0YW50O1xuICB9XG4gIC5wZXJpb2Qtc2VsZWN0b3Ige1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBwYWRkaW5nOiAwIDIwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGhlaWdodDogMzNweDtcbiAgICBsaW5lLWhlaWdodDogMzNweDtcbiAgICBtaW4td2lkdGg6IDc1cHg7XG4gIH1cblxuICAucGVyaW9kLXNlbGVjdG9yIHN2ZyB7XG4gICAgd2lkdGg6IDEwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMTJweDtcbiAgICBsZWZ0OiA1cHg7XG4gIH1cblxuICAucGVyaW9kLXNlbGVjdG9yIC5sZWZ0LWJ0biwgLnBlcmlvZC1zZWxlY3RvciAucmlnaHQtYnRuIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIGhlaWdodDogMzNweDtcbiAgICBsZWZ0OiAwO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgfVxuXG4gIC5wZXJpb2Qtc2VsZWN0b3IgLnJpZ2h0LWJ0biB7XG4gICAgbGVmdDogYXV0bztcbiAgICByaWdodDogMDtcbiAgfVxuXG4gIC5wZXJpb2Qtc2VsZWN0b3IgLmRpc2FibGVkIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG4gIC5kaXZUYWJsZUNlbGwgPiBkaXYge1xuICAgIGZsZXgtYmFzaXM6IDA7XG4gICAgZmxleC1ncm93OiAxO1xuICAgIGJvcmRlcjogMCAhaW1wb3J0YW50O1xuICB9XG4gIC5nb2FsLWhlYWRlcntcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLnRleHRtZXRyaWMgaW5wdXQsIC50ZXh0bWV0cmljIHNlbGVjdHtcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjY2NjICFpbXBvcnRhbnQ7XG4gIH1cbiAgLnRleHRtZXRyaWMubWlzc2luZyBpbnB1dCwgLnRleHRtZXRyaWMubWlzc2luZyBzZWxlY3Qge1xuICAgIGJvcmRlcjogc29saWQgMXB4IHJlZCAhaW1wb3J0YW50O1xuICB9XG4gIC5kaXZUYWJsZXtcbiAgICAuZGl2VGFibGVCb2R5e1xuICAgICAgLmZpcnN0Y2VsbHtcbiAgICAgICAgbWluLXdpZHRoOiAxNDBweCAhaW1wb3J0YW50O1xuICAgICAgfVxuICAgIH1cbiAgfVxuICAuc2hvdy1zbXtcbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgfVxuICAuaGlkZS1zbXtcbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gICAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG4gICAgfVxuICB9XG59XG4ucGVyaW9kLXNlbGVjdG9yLXRvcHtcbiAgZmxvYXQ6IGxlZnQ7XG59XG4iXX0= */"] });


/***/ }),

/***/ 6747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule),
/* harmony export */   "options": () => (/* binding */ options)
/* harmony export */ });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ 318);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app-routing.module */ 158);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component */ 5041);
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/drag-drop */ 8507);
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-mask */ 7038);
/* harmony import */ var ngx_autosize_input__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-autosize-input */ 4793);
/* harmony import */ var ng2_tooltip_directive__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-tooltip-directive */ 7762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);











const options = null;
class AppModule {
}
AppModule.ɵfac = function AppModule_Factory(t) { return new (t || AppModule)(); };
AppModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_1__.AppComponent] });
AppModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ providers: [], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.BrowserModule,
            _app_routing_module__WEBPACK_IMPORTED_MODULE_0__.AppRoutingModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClientModule,
            _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_6__.DragDropModule,
            ngx_mask__WEBPACK_IMPORTED_MODULE_7__.NgxMaskModule.forRoot(),
            ngx_autosize_input__WEBPACK_IMPORTED_MODULE_8__.AutoSizeInputModule,
            ng2_tooltip_directive__WEBPACK_IMPORTED_MODULE_9__.TooltipModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_1__.AppComponent], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.BrowserModule,
        _app_routing_module__WEBPACK_IMPORTED_MODULE_0__.AppRoutingModule,
        _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
        _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClientModule,
        _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_6__.DragDropModule, ngx_mask__WEBPACK_IMPORTED_MODULE_7__.NgxMaskModule, ngx_autosize_input__WEBPACK_IMPORTED_MODULE_8__.AutoSizeInputModule,
        ng2_tooltip_directive__WEBPACK_IMPORTED_MODULE_9__.TooltipModule] }); })();


/***/ }),

/***/ 6475:
/*!*****************************************!*\
  !*** ./src/app/services/app.service.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppService": () => (/* binding */ AppService)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);




class AppService {
    constructor(http) {
        this.http = http;
        this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'];
        // url: 'https://ontraninja.com/'
        this.config = {
            url: 'http://localhost:4200/wp/',
            ssoUrl: 'https://my.vitalstats.live/users/sso_auth'
        };
    }
    setApiUrl(url) {
        this.config.url = url;
    }
    convertUTCDateToLocalDate(date) {
        const newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);
        //var offset = date.getTimezoneOffset() / 60;
        //var hours = date.getHours();
        //newDate.setHours(hours - offset);
        return newDate;
    }
    getPeriods(term) {
        term.startDate = this.convertUTCDateToLocalDate(term.startDateUTC);
        term.endDate = this.convertUTCDateToLocalDate(term.endDateUTC);
        //console.log(term.startDate);
        //console.log(term.endDate);
        const d = new Date(term.startDate.getTime());
        switch (term.period) {
            case 0:
                const result = [];
                if (term.startDate < term.endDate) {
                    let month = term.startDate.getMonth();
                    for (; month < term.endDate.getMonth() +
                        1 + ((term.endDate.getFullYear() -
                        term.startDate.getFullYear()) * 12); month++) {
                        result.push({
                            label: this.monthNames[month % 12].substring(0, 3),
                            date: new Date(d.getTime()),
                            dateEnd: new Date(d.getTime()),
                            dateString: (0,_angular_common__WEBPACK_IMPORTED_MODULE_0__.formatDate)(new Date(d.getTime()), 'yMMdd', 'en-US')
                        });
                        d.setMonth(d.getMonth() + 1);
                    }
                }
                //console.log(result);
                return result;
                break;
            case 1:
                let diff = (term.endDate.getTime() - term.startDate.getTime()) / 1000;
                diff /= (60 * 60 * 24 * 7);
                return [...Array(Math.abs(Math.round(diff))).keys()].map(i => {
                    if (i > 0) {
                        d.setDate(d.getDate() + 7);
                    }
                    const dEnd = new Date(d.getTime());
                    dEnd.setDate(dEnd.getDate() + 6);
                    return {
                        label: 'Week ' + (i + 1),
                        date: new Date(d.getTime()),
                        dateEnd: new Date(dEnd.getTime()),
                        dateString: (0,_angular_common__WEBPACK_IMPORTED_MODULE_0__.formatDate)(new Date(d.getTime()), 'yMMdd', 'en-US')
                    };
                });
                break;
        }
    }
    getDefaultTerm() {
        let today;
        today = new Date();
        let startMonth;
        let endMonth;
        switch (today.getMonth()) {
            case 0:
            case 1:
            case 2:
                startMonth = 0;
                endMonth = 2;
                break;
            case 3:
            case 4:
            case 5:
                startMonth = 3;
                endMonth = 5;
                break;
            case 6:
            case 7:
            case 8:
                startMonth = 6;
                endMonth = 8;
                break;
            default:
                startMonth = 9;
                endMonth = 11;
                break;
        }
        return {
            id: Math.floor(Math.random() * 100000),
            title: 'Reporting Period',
            startDate: new Date(today.getFullYear(), startMonth, 1, 0, 0, 0),
            startDateUTC: new Date(Date.UTC(today.getFullYear(), startMonth, 1, 0, 0, 0)),
            endDate: new Date(today.getFullYear(), endMonth + 1, 0, 0, 0, 0),
            endDateUTC: new Date(Date.UTC(today.getFullYear(), endMonth + 1, 0, 0, 0, 0)),
            period: 0,
            metrics: []
        };
    }
    checkLogin() {
        return this.http.post(this.config.url + '?wpmm_ajax=1&action=checkLogin', {});
    }
    getSettings(post, userId) {
        return this.http.post(this.config.url + '?wpmm_ajax=1&action=getSettings', {
            post,
            userId
        });
    }
    getMetrics(post) {
        return this.http.post(this.config.url + '?wpmm_ajax=1&action=getMetrics', {
            post
        });
    }
    newMetric() {
        return this.http.post(this.config.url + '?wpmm_ajax=1&action=newMetric', {});
    }
    saveSettings(post, terms, metrics) {
        return this.http.post(this.config.url + '?wpmm_ajax=1&action=saveSettings', {
            post,
            terms,
            metrics
        });
    }
    getValues(post, userId) {
        return this.http.post(this.config.url + '?wpmm_ajax=1&action=getValues', {
            post,
            userId
        });
    }
    saveValues(post, userId, metricValues) {
        return this.http.post(this.config.url + '?wpmm_ajax=1&action=saveValues', {
            post,
            userId,
            metricValues
        });
    }
    saveUserSettings(usermeta, userId) {
        return this.http.post(this.config.url + '?wpmm_ajax=1&action=saveUserSettings', {
            usermeta,
            userId
        });
    }
    ssoLogin(kfSso, kfCompany) {
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders({
            'Content-Type': 'application/json',
            'KF-SSO': kfSso,
            'KF-Company': kfCompany,
        });
        const options = { headers, withCredentials: true, dataType: 'json' };
        return this.http.post(this.config.ssoUrl, null, options);
    }
}
AppService.ɵfac = function AppService_Factory(t) { return new (t || AppService)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpClient)); };
AppService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: AppService, factory: AppService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 2340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 4431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ 318);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 6747);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 2340);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.platformBrowser().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch(err => console.error(err));


/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(4431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.e28817726e34a7d0.js.map