<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              dev@itmooti.com
 * @since             1.0.2
 * @package           WPMetricsMaster
 *
 * @wordpress-plugin
 * Plugin Name:       WP Metrics Master
 * Plugin URI:        https://wpmetricsmaster.com/
 * Version:           1.4.9
 * Author:            ITMOOTI
 * Author URI:        dev@itmooti.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-metrics-master
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
require_once('inc/class.wp_metrics_master_updater.php');
require_once('vendor/autoload.php');
use OntraportAPI\Ontraport;
use OntraportAPI\ObjectType;
use OntraportAPI\Models\FieldEditor\ObjectField;
use OntraportAPI\Models\FieldEditor\ObjectSection;

class WPMetricsMaster{
    private $url;
    private $plugin_links;
    private $License=array(
        "authenticated" => false,
        "message" => ""
    );
    private $field_types = ['Currency', 'Number', 'Percentage', 'Text', 'Progress', 'Dropdown'];
    public function __construct()
    {
        $this->plugin_links=(object)array("support_link"=>get_option("wp_metrics_master_link_support_link", ""), "license_link"=>get_option("wp_metrics_master_link_license_link", ""));
        register_activation_hook(__FILE__, array($this, 'plugin_activation'));
        add_action('plugin_scheduled_event', array($this, 'plugin_authentication'));
        register_deactivation_hook(__FILE__, array($this, 'plugin_deactivation'));
        add_action( 'admin_notices', array( $this, 'show_license_info' ) );
        add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array($this, 'plugin_action_link'));
        add_filter( 'plugin_row_meta', array($this, 'plugin_meta_link'), 10, 2);
        if ( is_admin() ) {
            new WPMetricsMasterUpdater( __FILE__, 'it-mooti', "wp-metrics-master" );
            if(isset($_GET["fix_metrics"])){
                global $wpdb;
                //$user_id = absint($_GET["fix_metrics"]);
                $records = $wpdb->get_results("select * from $wpdb->usermeta where meta_key like '_metric_m%' and meta_key like '%_p%' and meta_key not like '%01_goal%' and meta_key not like '%01_actual%' limit 0,1000");
                foreach($records as $record){
                    $meta_key = explode("_p", $record->meta_key);
                    $meta_key = explode("_", $meta_key[1]);
                    $ts = strtotime($meta_key[0]);
                    if(date("j", $ts) != 1){
                        if(date("j", $ts) > 10){
                            $date = date("Ym01", strtotime("+1 month", $ts));
                        }
                        else{
                            $date = date("Ym01", $ts);
                        }
                        $meta_key = str_replace(date("Ymd", $ts), $date, $record->meta_key);
                        //$r = $wpdb->get_results("select * from $wpdb->usermeta where user_id = '".$record->user_id."' and meta_key = '".$meta_key."'");
                        echo $record->meta_key." = ".$meta_key;

                        if(count($r) > 0){
                            echo "delete".PHP_EOL;
                            //echo $record->meta_value." = ".$r[0]->meta_value.PHP_EOL;
                            //$wpdb->query("delete from $wpdb->usermeta where umeta_id='".$record->umeta_id."'");
                        }
                        else{
                            echo "update".PHP_EOL;
                            //$wpdb->query("update $wpdb->usermeta set meta_key = '".$meta_key."' where umeta_id='".$record->umeta_id."'");
                        }
                    }
                }
                die;
            }
        }
        add_action('init', [$this, 'init']);
    }
    public function is_authenticated(){
        if(get_option("wp_metrics_master_authenticated", "no")=="yes")
            return true;
        else
            return false;
    }
    public function plugin_activation(){
        wp_schedule_event(time(), 'twicedaily', 'plugin_scheduled_event');
    }
    public function plugin_deactivation(){
        wp_clear_scheduled_hook('plugin_scheduled_event');
    }
    public function plugin_authentication(){
        $isSecure = false;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $isSecure = true;
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
            $isSecure = true;
        }
        $this->url=($isSecure ? 'http' : 'http')."://app.itmooti.com/wp-plugins/oap-utm/api.php";
        $request= "plugin_links";
        $body = [
                'body' => [
                    "plugin" => "wp-metrics-master",
                "request" => urlencode($request)
            ]
        ];
        $response = wp_remote_post($this->url, $body);
        $response = json_decode($response["body"]);
        if(isset($response->status) && $response->status=="success"){
            update_option("wp_metrics_master_link_support_link", $response->message->support_link);
            update_option("wp_metrics_master_link_license_link", $response->message->license_link);
        }
        $license_key=get_option('wp_metrics_master_license_key', "");
        if(!empty($license_key)){
            $body = [
                'body' => [
                    "plugin" => "wp-metrics-master",
                    "request" => "verify",
                    "domain" => urlencode($_SERVER['HTTP_HOST']),
                    "license_key" => urlencode($license_key)
                ]
            ];
            $response = wp_remote_post($this->url, $body);
            $response = json_decode($response["body"]);
            if(isset($response->status) && $response->status=="success"){
                update_option("wp_metrics_master_authenticated", "yes");
                if(isset($response->message))
                    update_option("wp_metrics_master_message", $response->message);
            }
            else if(isset($response->status) && $response->status=="error"){
                update_option("wp_metrics_master_authenticated", "no");
                if(isset($response->message))
                    update_option("wp_metrics_master_message", $response->message);
            }
        }
        else{
            update_option("wp_metrics_master_authenticated", "no");
            update_option("wp_metrics_master_message", "Please enter valid license key");
        }
    }
    public function show_license_info(){
        $license_key=get_option('wp_metrics_master_license_key', "");
        if(empty($license_key)){
            echo '<div class="updated">
        		<p><strong>WP Metrics Master:</strong> How do I get License Key?<br />Please visit this URL <a href="'.$this->plugin_links->license_link.'" target="_blank">'.$this->plugin_links->license_link.'</a> to get a License Key .</p>
	    	</div>';
        }
        $message=get_option("wp_metrics_master_message", "");
        if($message!=""){
            echo '<div class="error">
        		<p><strong>WP Metrics Master:</strong> '.$message.'</p>
	    	</div>';
        }
    }
    function plugin_action_link( $links ) {
        return array_merge(
            array(
                'settings' => '<a href="edit.php?post_type=wpmm_widgets&page=wp_metrics_master_settings">Settings</a>',
                'support_link' => '<a href="'.$this->plugin_links->support_link.'" target="_blank">Support</a>'
            ),
            $links
        );
    }
    function plugin_meta_link( $links, $file ) {
        $plugin = plugin_basename(__FILE__);
        if ( $file == $plugin ) {
            return array_merge(
                $links,
                array(
                    'settings' => '<a href="edit.php?post_type=wpmm_widgets&page=wp_metrics_master_settings">Settings</a>',
                    'support_link' => '<a href="'.$this->plugin_links->support_link.'" target="_blank">Support</a>'
                )
            );
        }
        return $links;
    }
    public function getSettings($data){
        if(is_user_logged_in()){
            $response = [
                    "goalsRequired" => get_option('wp_metrics_master_goals_required')=="1",
                "actualsRequired" => get_option('wp_metrics_master_actuals_required')=="1"
            ];
            $terms = get_post_meta($data["post"], '_terms', true);
            if($terms){
                $response["terms"] = json_decode($terms);
                for($i = 0; $i < count($response["terms"]); $i++){
                    $startDate = strtotime(explode("T", $response["terms"][$i]->startDateUTC)[0]);
                    $response["terms"][$i]->startDate = [
                        "y" => (int)date("Y", $startDate),
                        "m" => date("n", $startDate)-1,
                        "d" => (int)date("j", $startDate),
                    ];
                    $endDate = strtotime(explode("T", $response["terms"][$i]->endDateUTC)[0]);
                    $response["terms"][$i]->endDate = [
                        "y" => (int)date("Y", $endDate),
                        "m" => date("n", $endDate)-1,
                        "d" => (int)date("j", $endDate),
                    ];
                }
            }
            else{
                $response["terms"] = [];
            }
            if(isset($data["userId"]) && !empty($data["userId"]) && current_user_can('manage_options')){
                $userId = $data["userId"];
            }
            else{
                $userId = get_current_user_id();
            }
            $user = get_user_by('ID', $userId);
            $response["usermeta"] = [];
            $posts = get_posts([
                'post_type' => 'wpmm_usermeta',
                'posts_per_page' => -1,
                'orderby' => 'menu_order',
                'order' => 'asc'
            ]);
            foreach($posts as $post){
                $type = (int)get_post_meta($post->ID, 'type', true);
                $options = get_post_meta($post->ID, 'options', true);
                $linkedField = get_post_meta($post->ID, 'linkedField', true);
                if($type == 0){
                    $defaultValue = $options;
                    $options = [];
                }
                else{
                    $defaultValue = "";
                    $options = explode(PHP_EOL, $options);
                }
                $response["usermeta"][] = [
                    'id' => $post->ID,
                    'title' => $post->post_title,
                    'type' => $type,
                    'defaultValue' => $defaultValue,
                    'options' => $options,
                    'viewOnly' => get_post_meta($post->ID, 'viewOnly', true)==1,
                    'linkedField' => $linkedField,
                    'value' => $linkedField=="" ?
                        get_user_meta($userId, 'usermeta_'.$post->ID, true):
                        $user->$linkedField
                ];
            }
            $response["usermeta"][] = [
                'id' => 0,
                'title' => 'Date Last Edited',
                'type' => 0,
                'defaultValue' => '',
                'options' => [],
                'viewOnly' => true,
                'linkedField' => '',
                'value' => date("d M, Y h:i A", strtotime(get_user_meta($userId, 'usermeta_date_last_edited', true)))
            ];
            return $response;
        }
        return [];
    }
    public function getMetrics($data){
        $metrics = [];
        $posts = get_posts([
                'post_type' => 'wpmm_metrics',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'asc'
        ]);
        foreach($posts as $post){
            $metrics[] = $this->getMetric($post, $data["post"]);
        }
        return $metrics;
    }
    public function getMetric($post, $widget_id = null){
        if($widget_id){
            $metric = get_post_meta($widget_id, "_metric_".$post->ID, true);
            if(!empty($metric)){
                $metric = json_decode($metric);
                $metric->options = str_replace('<br />', PHP_EOL, $metric->options);
                return $metric;
            }
        }
        return [
            'id' => $post->ID,
            'title' => $post->post_title,
            'override' => false,
            'type' => (int)get_post_meta($post->ID, 'type', true),
            'decimalDigits' => get_post_meta($post->ID, 'decimalDigits', true) === "" ? 2 : (int)get_post_meta($post->ID, 'decimalDigits', true),
            'options' => get_post_meta($post->ID, 'dropdownOptions', true),
            'hasGoal' => get_post_meta($post->ID, 'hasGoal', true)==1,
            'goalAlert' => get_post_meta($post->ID, 'goalAlert', true)==1,
            'editable' => get_post_meta($post->ID, 'editable', true)==1,
            'description' => get_post_meta($post->ID, 'description', true),
            'hasCustomGoal' => get_post_meta($post->ID, 'hasCustomGoal', true)==1,
            'goalTitle' => get_post_meta($post->ID, 'goalTitle', true),
            'goalType' => get_post_meta($post->ID, 'goalType', true),
            'goalDescription' => get_post_meta($post->ID, 'goalDescription', true),
        ];

    }
    public function newMetric($data){
        if(current_user_can('manage_options')) {
            $title = 'Metric title';
            $post_id = wp_insert_post([
                 'post_type' => 'wpmm_metrics',
                'post_title' => $title,
                'post_status' => 'publish'
            ]);
            update_post_meta($post_id, 'type', 0);
            update_post_meta($post_id, 'hasGoal', 0);
            update_post_meta($post_id, 'editable', 0);
            return [
                    "status" => true,
                "metric" => [
                    'id' => $post_id,
                    'title' => $title,
                    'type' => (int)get_post_meta($post_id, 'type', true),
                    'hasGoal' => (boolean)get_post_meta($post_id, 'hasGoal', true),
                    'editable' => (boolean)get_post_meta($post_id, 'editable', true),
                ]
            ];
        }
    }
    public function saveSettings($data){
        if($this->is_authenticated() && current_user_can('manage_options')) {
            if (!$data["post"]) {
                $post = wp_insert_post([
                    'post_type' => 'wpmm_widgets',
                    'post_status' => 'publish'
                ]);
            }
            else{
                $post = $data["post"];
            }
            $terms = update_post_meta($post, '_terms', json_encode($data["terms"]));
            foreach($data["metrics"] as $metric){
                if($metric["override"]){
                    $metric["options"] = str_replace(PHP_EOL, '', nl2br($metric["options"]));
                    update_post_meta($post, '_metric_'.$metric["id"], json_encode($metric));
                }
                else{
                    delete_post_meta($post, '_metric_'.$metric["id"]);
                    wp_update_post([
                        'ID' => $metric["id"],
                        'post_title' => $metric["title"]
                    ]);
                    update_post_meta($metric["id"], 'type', $metric["type"]);
                    update_post_meta($metric["id"], 'decimalDigits', $metric["decimalDigits"]);
                    if(isset($metric["options"])){
                        update_post_meta($metric["id"], 'dropdownOptions', $metric["options"]);
                    }
                    update_post_meta($metric["id"], 'hasGoal', $metric["hasGoal"]==1?1:0);
                    update_post_meta($metric["id"], 'goalAlert', $metric["goalAlert"]==1?1:0);
                    update_post_meta($metric["id"], 'editable', $metric["editable"]==1?1:0);
                    update_post_meta($metric["id"], 'hasCustomGoal', $metric["hasCustomGoal"]==1?1:0);
                    update_post_meta($metric["id"], 'goalTitle', $metric["goalTitle"]);
                    update_post_meta($metric["id"], 'goalType', $metric["goalType"]);
                    update_post_meta($metric["id"], 'goalDescription', $metric["goalDescription"]);
                }
            }
            return [
                "status" => true,
                "post" => $post
            ];
        }
        return [
            "status" => true
        ];
    }
    public function getValues($data){
        global $wpdb;
        if(isset($data["userId"]) && !empty($data["userId"]) && current_user_can('manage_options')){
            $userId = $data["userId"];
        }
        else{
            $userId = get_current_user_id();
        }
        $terms = get_post_meta($data["post"], '_terms', true);
        $values = [];
        if($terms){
            $terms = json_decode($terms);
            $metrics = [];
            foreach($terms as $term){
                foreach($term->metrics as $metric){
                    if(!in_array($metric, $metrics)){
                        $metrics[] = $metric;
                    }
                }
            }
            foreach($metrics as $metric){
                $metaValues = $wpdb->get_results("select * from $wpdb->usermeta where meta_key like '_metric_m".$metric."_%' and user_id = '".$userId."'");
                foreach($metaValues as $metaValue){
                    $values[str_replace('_metric_', '', $metaValue->meta_key)] = $metaValue->meta_value;
                }
            }
        }
        if(count($values)>0){
            return $values;
        }
        return new stdClass();
    }
    public function saveValues($data){
        if(!$this->is_authenticated()){
            return;
        }
        if(isset($data["userId"]) && !empty($data["userId"]) && current_user_can('manage_options')){
            $userId = $data["userId"];
        }
        else{
            $userId = get_current_user_id();
        }
        foreach($data["metricValues"] as $metric => $value){
            update_user_meta($userId, '_metric_'.$metric, $value);
        }
        update_user_meta($userId, 'usermeta_date_last_edited', date("Y-m-d H:i:s"));
        $webhooks = get_posts([
                'post_type' => 'wpmm_webhook',
            'posts_per_page' => -1,
        ]);
        foreach($webhooks as $webhook){
            $send_on_widget = get_post_meta($webhook->ID, "send_on_widget", true);
            $widgets = get_post_meta($webhook->ID, "widgets", true);
            if(!$send_on_widget || !is_array($widgets) || !in_array($data["post"], $widgets)){
                continue;
            }
            wp_remote_post($webhook->post_title, [ 'body' => [
                    'metricValues' => $data["metricValues"],
                'user_id' => $userId,
                'widget_id' => $data["post"],
                'timestamp' => time()
            ]]);
        }
        return [
            "status" => true
        ];
    }
    public function saveUserSettings($data){
        if(!$this->is_authenticated()){
            return;
        }
        if(isset($data["userId"]) && !empty($data["userId"]) && current_user_can('manage_options')){
            $userId = $data["userId"];
        }
        else{
            $userId = get_current_user_id();
        }
        foreach($data["usermeta"] as $meta){
            if(!$meta["viewOnly"]){
                update_user_meta($userId, 'usermeta_'.$meta["id"], $meta["value"]);
            }
        }
        $webhooks = get_posts([
            'post_type' => 'wpmm_webhook',
            'posts_per_page' => -1,
        ]);
        foreach($webhooks as $webhook){
            $send_on_usermeta = get_post_meta($webhook->ID, "send_on_usermeta", true);
            if(!$send_on_usermeta){
                continue;
            }
            wp_remote_post($webhook->post_title, [ 'body' => [
                'usermeta' => $data["usermeta"],
                'widget_id' => $data["post"],
                'user_id' => $userId,
                'timestamp' => time()
            ]]);
        }
        return [
            "status" => true
        ];
    }
    public function init(){
        if(current_user_can('manage_options')){
            if(isset($_POST["wp_metrics_master_actuals_required"])){
                update_option("wp_metrics_master_actuals_required", $_POST["wp_metrics_master_actuals_required"]);
            }
            if(isset($_POST["wp_metrics_master_goals_required"])){
                update_option("wp_metrics_master_goals_required", $_POST["wp_metrics_master_goals_required"]);
            }
            if(isset($_POST["wp_metrics_master_primary_color"])){
                update_option("wp_metrics_master_primary_color", $_POST["wp_metrics_master_primary_color"]);
            }
            if(isset($_POST["wp_metrics_master_vitalstats_company_id"])){
                update_option("wp_metrics_master_vitalstats_company_id", $_POST["wp_metrics_master_vitalstats_company_id"]);
            }
            if(isset($_POST["wp_metrics_master_vitalstats_sso_secret"])){
                update_option("wp_metrics_master_vitalstats_sso_secret", $_POST["wp_metrics_master_vitalstats_sso_secret"]);
            }
            if(isset($_POST["wp_metrics_master_vitalstats_url"])){
                update_option("wp_metrics_master_vitalstats_url", $_POST["wp_metrics_master_vitalstats_url"]);
            }
            if(isset($_POST["wp_metrics_master_license_key"])){
                update_option("wp_metrics_master_license_key", $_POST["wp_metrics_master_license_key"]);
                $this->plugin_authentication();
                header('Location: admin.php?page=wp_metrics_master_settings');
                die;
            }
            if(isset($_POST["wp_metrics_master_sso_op"])){
                update_option("wp_metrics_master_sso_op", $_POST["wp_metrics_master_sso_op"]);
                if(isset($_POST["wp_metrics_master_sso_op_app_id"])){
                    update_option("wp_metrics_master_sso_op_app_id", $_POST["wp_metrics_master_sso_op_app_id"]);
                }
                if(isset($_POST["wp_metrics_master_sso_op_api_key"])){
                    update_option("wp_metrics_master_sso_op_api_key", $_POST["wp_metrics_master_sso_op_api_key"]);
                }
                if(isset($_POST["wp_metrics_master_sso_op_object"])){
                    update_option("wp_metrics_master_sso_op_object", $_POST["wp_metrics_master_sso_op_object"]);
                }
                if(isset($_POST["wp_metrics_master_sso_op_field"])){
                    update_option("wp_metrics_master_sso_op_field", $_POST["wp_metrics_master_sso_op_field"]);
                }
                $this->testOpConnection();
            }
            if(isset($_POST["wp_metrics_master_sso_ac"])){
                update_option("wp_metrics_master_sso_ac", $_POST["wp_metrics_master_sso_ac"]);
                if(isset($_POST["wp_metrics_master_sso_ac_api_url"])){
                    update_option("wp_metrics_master_sso_ac_api_url", $_POST["wp_metrics_master_sso_ac_api_url"]);
                }
                if(isset($_POST["wp_metrics_master_sso_ac_api_key"])){
                    update_option("wp_metrics_master_sso_ac_api_key", $_POST["wp_metrics_master_sso_ac_api_key"]);
                }
                if(isset($_POST["wp_metrics_master_sso_ac_field"])){
                    update_option("wp_metrics_master_sso_ac_field", $_POST["wp_metrics_master_sso_ac_field"]);
                }
                $this->testAcConnection();
            }
        }
        register_post_type('wpmm_widgets', [
            'label' => 'Widgets',
            'public' => false,
            'show_ui' => true,
            'show_in_menu' => 'wp_metrics_master_settings',
            'supports' => ['title']
        ]);
        register_post_type('wpmm_metrics', [
            'label' => 'Metrics',
            'public' => false,
            'show_ui' => true,
            'show_in_menu' => 'wp_metrics_master_settings',
            'supports' => ['title']
        ]);
        register_post_type('wpmm_usermeta', [
            'label' => 'User Meta',
            'public' => false,
            'show_ui' => true,
            'show_in_menu' => 'wp_metrics_master_settings',
            'supports' => ['title', 'page-attributes']
        ]);
        register_post_type('wpmm_webhook', [
            'label' => 'Webhooks',
            'public' => false,
            'show_ui' => true,
            'show_in_menu' => 'wp_metrics_master_settings',
            'supports' => ['title']
        ]);
        $vitalstats_company_id=get_option('wp_metrics_master_vitalstats_company_id', "");
        if(!empty($vitalstats_company_id)){
            register_post_type('wpmm_kreport', [
                'label' => 'Embedded Reports',
                'public' => false,
                'show_ui' => true,
                'show_in_menu' => 'wp_metrics_master_settings',
                'supports' => ['title']
            ]);
        }
        add_filter( 'post_row_actions', [$this, 'action_row'], 10, 2 );
        add_filter('gettext', [$this, 'custom_enter_title']);
        add_filter( 'manage_wpmm_widgets_posts_columns', [$this, 'columns']);
        add_filter( 'manage_wpmm_webhook_posts_columns', [$this, 'webhook_columns']);
        add_filter( 'manage_wpmm_metrics_posts_columns', [$this, 'metrics_columns']);
        add_filter( 'manage_wpmm_kreport_posts_columns', [$this, 'kreport_columns']);
        add_action( 'manage_posts_custom_column' , [$this, 'columns_content'], 10, 2 );
        add_action( 'add_meta_boxes', [$this, 'meta_box'] );
        add_action( 'save_post', [$this, 'save_post'] );
        add_action('admin_menu', [$this, 'menu']);
        add_action( 'admin_footer', [$this, 'js']);
        add_action( 'admin_footer', [$this, 'admin_footer']);
        add_action( 'wp_enqueue_scripts', [$this, 'wp_enqueue_scripts']);
        add_action( 'wp_footer', [$this, 'js'], 1000);
        add_action( 'wp_ajax_wp_metrics_master_get_users', [$this, 'get_users']);
        add_action( 'wp_ajax_wp_metrics_master_get_widgets', [$this, 'get_widgets']);
        add_shortcode( 'wpmm_widgets', [$this, 'shortcode'] );
        add_shortcode( 'wpmm_kreport', [$this, 'shortcode_kreport'] );
        add_shortcode( 'wpmm_sso', [$this, 'shortcode_sso'] );
        add_action( 'rest_api_init', function () {
            register_rest_route( 'wp-metrics-master/v1', '/metrics/(?P<id>\d+)', array(
                'methods' => 'GET',
                'callback' => [$this, 'metrics_feed'],
            ));
            register_rest_route( 'wp-metrics-master/v1', '/single-metric/(?P<id>\d+)', array(
                'methods' => 'GET',
                'callback' => [$this, 'single_metric_feed'],
            ));
            register_rest_route( 'wp-metrics-master/v1', '/users', array(
                'methods' => 'GET',
                'callback' => [$this, 'users_feed'],
            ));
            register_rest_route( 'wp-metrics-master/v1', '/users/(?P<id>\d+)', array(
                'methods' => 'GET',
                'callback' => [$this, 'users_feed'],
            ));
            register_rest_route( 'wp-metrics-master/v1', '/user/update', array(
                'methods' => 'POST',
                'callback' => [$this, 'user_update'],
            ));
        });
        if(isset($_GET["wpmm_ajax"])){
            if($_GET["action"] == 'checkLogin'){
                if(is_user_logged_in()){
                    $response = [
                        'status' => true,
                    ];
                    $sso = $this->get_sso_details();
                    $response["kfSso"] = $sso["encryptedData"];
                    $response["kfCompany"] = $sso["vitalstats_company_id"];
                    $response["ssoRedirect"] = $sso["vitalstats_url"];
                }
                else{
                    $response = [
                        'status' => false,
                        'message' => 'You must be logged in to view this content. <a href="'.wp_login_url().'" target="_top">Login here</a>'
                    ];
                }
            }
            else{
                if(is_user_logged_in() && $_GET["action"] != 'checkLogin'){
                    $data = json_decode(file_get_contents('php://input'), true);
                    $response = $this->{$_GET["action"]}($data);
                }
            }
            header("Access-Control-Allow-Origin: *");
            echo json_encode($response);
            die;
        }
        if(isset($_GET["export_specimen"]) && current_user_can('manage_options')){
            header( 'Content-Type: text/csv' );
            header( 'Content-Disposition: attachment;filename=wp-metrics-master-'.($period["type"]==0?'monthly':'weekly').'.csv');
            $fp = fopen('php://output', 'w');
            $usermeta = get_posts([
                'post_type' => 'wpmm_usermeta',
                'posts_per_page' => -1,
                'orderby' => 'title',
                'order' => 'asc'
            ]);
            list($periods, $metrics) = $this->getPeriodMetrics($_GET["type"]);
            $row = [
                    'User ID',
                'User Email',
                'User Display Name'
            ];
            foreach($usermeta as $post){
                $linkedField = get_post_meta($post->ID, 'linkedField', true);
                if($linkedField == ""){
                    $row[] = $post->post_title;
                }
            }
            foreach(['Label', 'Actual', 'Goal'] as $key){
                foreach ($metrics as $metric) {
                    if($key == 'Label'){
                        if($metric["editable"]){
                            $row[] = $metric["title"]." - Label";
                        }
                    }
                    else{
                        foreach ($periods as $period) {
                            $dateStr = " (".date("Y/m/d", strtotime($period["date"])).") ".($period["type"]==0?'Monthly':'Weekly');
                            if($key == 'Actual') {
                                $row[] = $metric["title"] . " - Actual" . $dateStr;
                            }
                            else{
                                if($metric["hasGoal"]){
                                    $row[] = $metric["title"]." - Goal".$dateStr;
                                }
                            }
                        }
                    }
                }
            }
            fputcsv($fp, $row);
            foreach (get_users() as $user) {
                $row = [
                        $user->ID,
                        $user->user_email,
                    $user->display_name
                ];
                foreach($usermeta as $post){
                    $linkedField = get_post_meta($post->ID, 'linkedField', true);
                    if($linkedField == ""){
                        $row[] = get_user_meta($user->ID, 'usermeta_'.$post->ID, true);
                    }
                }
                foreach(['Label', 'Actual', 'Goal'] as $key){
                    foreach ($metrics as $metric) {
                        if($key == 'Label'){
                            if($metric["editable"]){
                                $row[] = get_user_meta($user->ID, '_metric_m'.$metric["id"]."_title", true);
                            }
                        }
                        else{
                            foreach ($periods as $period) {
                                $dateStr = " (".date("Y/m/d", strtotime($period["date"])).")";
                                if($key == 'Actual') {
                                    $row[] = get_user_meta($user->ID, '_metric_m'.$metric["id"]."_t".$period["type"].'_p'.$period["date"]."_actual", true);
                                }
                                else{
                                    if($metric["hasGoal"]){
                                        $row[] = get_user_meta($user->ID, '_metric_m'.$metric["id"]."_t".$period["type"].'_p'.$period["date"]."_goal", true);
                                    }
                                }
                            }
                        }
                    }
                }
                fputcsv($fp, $row);
            }
            fclose($fp);
            die;
        }
        if(current_user_can('manage_options') && isset($_GET["action"]) && $_GET["action"] == 'duplicate'){
            $post_id = $this->duplicate_post($_GET["post_id"]);
            wp_redirect(admin_url('post.php')."?post=".$post_id."&action=edit");
            die;
        }
        if(!empty($vitalstats_company_id)) {
            add_action('show_user_profile', [$this, 'user_profile_fields']);
            add_action('edit_user_profile', [$this, 'user_profile_fields']);
            add_action('personal_options_update', [$this, 'save_user_profile_fields']);
            add_action('edit_user_profile_update', [$this, 'save_user_profile_fields']);
        }

        $this->checkSSO();
    }

    public function checkSSO(){
        if(isset($_GET["wpmm_sso"]) && isset($_GET["id"])){
            $ac_enabled = get_option('wp_metrics_master_sso_ac_enabled');
            if($_GET["wpmm_sso"] == 'ac' && $ac_enabled == '1'){
                $acDetails = $this->getAcDetails();
                $ac_args = [
                    'headers' => [
                        'Api-Token' => $acDetails["api_key"]
                    ]
                ];
                $response = wp_remote_retrieve_body(wp_remote_get( $acDetails["api_url"] . '/contacts/'.absint($_GET["id"]), $ac_args));
                if(!empty($response)){
                    $response = json_decode($response);
                    if(isset($response->contact)){
                        $contact = $response->contact;
                        if(isset($response->fieldValues)){
                            foreach($response->fieldValues as $field){
                                if($field->field == $acDetails["field_key"] && $field->value == $_GET["key"]){
                                    $this->doSSOLogin($contact->email, absint($_GET["id"]), $contact->firstName, $contact->lastName);
                                }
                            }
                        }
                    }
                }
                die;
            }
            $op_enabled = get_option('wp_metrics_master_sso_op_enabled');
            if($_GET["wpmm_sso"] == 'op' && $op_enabled == '1'){
                $opDetails = $this->getOpDetails();
                $client = new Ontraport($opDetails["app_id"],$opDetails["api_key"]);
                $payload = [
                    "objectID" => constant('OntraportAPI\ObjectType::'.strtoupper($opDetails["object"])),
                    "id" => absint($_GET["id"])
                ];
                $body = $client->object()->retrieveSingle($payload);
                $response = json_decode($body);
                if(json_last_error() === JSON_ERROR_NONE){
                    if(isset($response->data->{$opDetails["field_key"]}) && $response->data->{$opDetails["field_key"]} == $_GET["key"]){
                        $this->doSSOLogin($response->data->email, $response->data->id, $response->data->firstname, $response->data->lastname);
                    }
                }
            }
        }
    }

    public function doSSOLogin($email, $id, $firstname, $lastname){
        $user = get_user_by("email", $email);
        if(!$user){
            global $wpdb;
            $wpdb->insert( $wpdb->users, [ 'ID' => $id ] );
            $user = wp_insert_user([
                "ID" => $id,
                "user_login" => $email,
                "user_pass" => rand(),
                "user_email" => $email,
                "first_name" => $firstname,
                "last_name" => $lastname,
                "role" => "subscriber"
            ]);
            $wpdb->update(
                $wpdb->users, 
                ['user_login' => $email],
                ['ID' => $user]
            );
            $user = get_user_by("ID", $user);
            update_user_meta($user->ID, 'vitalstats_email', $email);
        }
        if($user){
            global $wp;
            wp_clear_auth_cookie();
            clean_user_cache($user->ID);
            wp_set_current_user ( $user->ID );
            wp_set_auth_cookie  ( $user->ID );
            update_user_caches($user);
            wp_safe_redirect( explode("?", home_url($_SERVER['REQUEST_URI']))[0] );
            exit();
        }
    }

    public function testOpConnection(){
        $opDetails = $this->getOpDetails();
        update_option('wp_metrics_master_sso_op_enabled', "0");
        if(!empty($opDetails["app_id"]) && !empty($opDetails["api_key"]) && !empty($opDetails["object"])){
            $client = new Ontraport($opDetails["app_id"],$opDetails["api_key"]);
            
            $payload = [
                "objectID" => constant('OntraportAPI\ObjectType::'.strtoupper($opDetails["object"]))
            ];
            if(!empty($opDetails["field_key"])){
                $payload["field_key"] = $opDetails["field_key"];
            }
            $body = $client->object()->retrieveFields($payload);
            $response = json_decode($body);
            if(json_last_error() === JSON_ERROR_NONE){
                if(isset($response->data)){
                    if(empty($opDetails["field"])){
                        //check if already exists
                        $exists = false;
                        $fieldKey = '';
                        foreach($response->data as $id => $object){
                            foreach($object->fields as $rows){
                                foreach($rows as $field){
                                    if($field->alias == 'WPMM SSO Key'){
                                        $exists = true;
                                        $fieldKey = $field->field;
                                    }
                                }
                            }
                        }
                        if(!$exists){
                            $opField = new ObjectField("WPMM SSO Key", ObjectField::TYPE_TEXT);
                            $opSection = new ObjectSection("WPMM SSO", [$opField]);

                            $requestParams = $opSection->toRequestParams();
                            $requestParams["objectID"] = ObjectType::CONTACT;
                            $body = $client->object()->createFields($requestParams);
                            $response = json_decode($body);
                            if(json_last_error() === JSON_ERROR_NONE){
                                if(isset($response->data->success)){
                                    foreach($response->data->success as $key => $label){
                                        if($label == 'WPMM SSO Key'){
                                            $fieldKey = $key;
                                            break;
                                        }
                                    }
                                }
                            }
                            if(empty($fieldKey)){
                                update_option('wp_metrics_master_sso_op_message', 'Error: '.(isset($body->data->error) ? $body->data->error : $body));
                            }
                        }
                        if(!empty($fieldKey)){
                            update_option('wp_metrics_master_sso_op_field_key', $fieldKey);
                            update_option('wp_metrics_master_sso_op_field', "WPMM SSO Key");
                            update_option('wp_metrics_master_sso_op_enabled', "1");
                            update_option('wp_metrics_master_sso_op_message', 'Success: Successfully configured the ontraport SSO');
                        }
                    }
                    else{
                        $exists = false;
                        foreach($response->data as $id => $object){
                            foreach($object->fields as $fields){
                                foreach($fields as $field){
                                    if($field->alias == $opDetails["field"]){
                                        update_option('wp_metrics_master_sso_op_field_key', $field->field);
                                        $exists = true;
                                        break;
                                    }
                                }
                                if($exists){
                                    break;
                                }
                            }
                        }
                        if(!$exists){
                            update_option('wp_metrics_master_sso_op_message', 'Error: Field "'.$opDetails["field"].'" does not exists.');
                        }
                        else{
                            update_option('wp_metrics_master_sso_op_enabled', "1");
                            update_option('wp_metrics_master_sso_op_message', 'Success: Successfully configured the ontraport SSO');
                        }
                    }
                }
            }
            else{
                update_option('wp_metrics_master_sso_op_message', 'Error: '.$body);
            }
        }
    }

    public function getOpDetails(){
        return [
            'app_id' => get_option('wp_metrics_master_sso_op_app_id'),
            'api_key' => get_option('wp_metrics_master_sso_op_api_key'),
            'object' => get_option('wp_metrics_master_sso_op_object'),
            'field' => get_option('wp_metrics_master_sso_op_field'),
            'field_key' => get_option('wp_metrics_master_sso_op_field_key'),
        ];
    }

    public function testAcConnection(){
        $acDetails = $this->getAcDetails();
        update_option('wp_metrics_master_sso_ac_enabled', "0");
        if(!empty($acDetails["api_key"]) && !empty($acDetails["api_url"])){
            $ac_args = [
                'headers' => [
                    'Api-Token' => $acDetails["api_key"]
                ]
            ];
            $offset = 0;
            $total = 0;
            $exists = false;
            $apiError = false;
            $fieldObject = null;
            do{
                $body = wp_remote_retrieve_body(wp_remote_get( $acDetails["api_url"] . '/fields?limit=100&offset='.$offset, $ac_args));
                $response = json_decode($body);
                // print_r($response);
                // die;
                if(json_last_error() === JSON_ERROR_NONE){
                    foreach($response->fields as $field){
                        if(!empty($acDetails["field"]) && $field->title == $acDetails["field"] || $field->title == 'WPMM SSO Key'){
                            $exists = true;
                            $fieldObject = $field;
                            break;
                        }
                    }
                    $offset += 100;
                    $total = $response->meta->total;
                }
                else{
                    $apiError = true;
                }
                
            } while(!$exists && $total > $offset);
            if(!$apiError){
                if(empty($acDetails["field"]) && !$exists){
                    $args = [
                        'headers' => [
                            'Api-Token' => $acDetails["api_key"],
                            'accept' => 'application/json',
                            'content-type' => 'application/json',
                        ],
                        'body' => json_encode([
                            "field" => [
                                "type" => "text",
                                "title" => "WPMM SSO Key",
                                "show_in_list" => 0
                            ]
                        ])
                    ];
                    $body = wp_remote_retrieve_body(wp_remote_post( $acDetails["api_url"] . '/fields', $args));
                    $response = json_decode($body);
                    if(json_last_error() === JSON_ERROR_NONE){
                        if(isset($response->field->id)){
                            update_option('wp_metrics_master_sso_ac_field_key', $response->field->id);   
                            update_option('wp_metrics_master_sso_ac_field', $response->field->title);   
                            update_option('wp_metrics_master_sso_ac_enabled', "1");   
                        }
                        else{
                            update_option('wp_metrics_master_sso_ac_message', 'Error: '.$body);
                        }
                    }
                    else{
                        update_option('wp_metrics_master_sso_ac_message', 'Error: '.$body);
                    }
                }
                else if($fieldObject){
                    update_option('wp_metrics_master_sso_ac_field_key', $fieldObject->id);
                    update_option('wp_metrics_master_sso_ac_field', $fieldObject->title);      
                    update_option('wp_metrics_master_sso_ac_enabled', "1");
                    update_option('wp_metrics_master_sso_ac_message', 'Success: Successfully configured the Active Campaign SSO');
                }
                else{
                    update_option('wp_metrics_master_sso_ac_message', 'Error: Field "'.$acDetails["field"].'" does not exists.');
                }
            }
            else{
                update_option('wp_metrics_master_sso_ac_message', 'Error: '.$body);
            }
        }
    }
    public function getAcDetails(){
        return [
            'api_key' => get_option('wp_metrics_master_sso_ac_api_key'),
            'api_url' => get_option('wp_metrics_master_sso_ac_api_url'),
            'field' => get_option('wp_metrics_master_sso_ac_field'),
            'field_key' => get_option('wp_metrics_master_sso_ac_field_key'),
        ];
    }
    public function custom_enter_title($input){
        global $post_type;
        if( is_admin() && 'Add title' == $input && 'wpmm_webhook' == $post_type )
            return 'Add Webhook URL';

        return $input;
    }
    public function user_profile_fields($user){
        ?>
        <h3><?php _e("WP Metrics Master", 'wp-metrics-master'); ?></h3>
        <table class="form-table wpmm">
            <tr>
                <th><label for="vitalstats_email"><?php _e("VitalStats Email"); ?></label></th>
                <td>
                    <input type="text" name="vitalstats_email" id="vitalstats_email" value="<?php echo esc_attr( get_user_meta( $user->ID, 'vitalstats_email', true ) ); ?>" class="regular-text" />
                </td>
            </tr>
            <tr>
                <th><label for="klipfolio_profile"><?php _e("Embedded Report Profile"); ?></label></th>
                <td>
                    <input type="text" name="klipfolio_profile" id="klipfolio_profile" value="<?php echo esc_attr( get_user_meta( $user->ID, 'klipfolio_profile', true ) ); ?>" class="regular-text" />
                </td>
            </tr>
        </table>
        <?php
    }
    public function save_user_profile_fields($user_id){
        if ( empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'update-user_' . $user_id ) ) {
            return;
        }

        if ( !current_user_can( 'edit_user', $user_id ) ) {
            return false;
        }
        if(isset($_POST["vitalstats_email"])){
            update_user_meta( $user_id, 'vitalstats_email', $_POST['vitalstats_email'] );
            update_user_meta( $user_id, 'klipfolio_profile', $_POST['klipfolio_profile'] );
        }
    }
    public function getPeriodMetrics($type=''){
        $periods = [];
        $metrics = [];
        $period_dates = [];
        $metricPosts = [];
        $widgets = get_posts([
            'post_type' => 'wpmm_widgets',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'asc'
        ]);
        foreach($widgets as $widget){
            $terms = get_post_meta($widget->ID, '_terms', true);
            if($terms){
                $terms = json_decode($terms);
                foreach($terms as $index => $term){
                    if($type=='' || $term->period==0 && $type=='monthly' || $term->period==1 && $type=='weekly'){
                        $start = new DateTime(date("Y-m-d", strtotime($term->startDateUTC)));
                        $end = new DateTime(date("Y-m-d", strtotime($term->endDateUTC)));
                        $interval = DateInterval::createFromDateString('1 '.($term->period==0?'month':'week'));
                        $period   = new DatePeriod($start, $interval, $end);
                        foreach ($period as $dt) {
                            $period = $dt->format("Ymd");
                            if(!in_array($period, $period_dates)){
                                $period_dates[] = $period;
                                $periods[] = [
                                    "term" => $term->title,
                                    "type" => $term->period,
                                    "date" => $period,
                                ];
                            }
                        }
                        foreach ($term->metrics as $metric) {
                            if(!in_array($metric, $metrics)){
                                $metrics[] = $metric;
                            }
                        }
                    }
                }
            }
        }
        foreach ($metrics as $metric) {
            $metricPosts[] = $this->getMetric(get_post($metric));
        }
        usort($periods, function($a, $b){
            if ($a["date"] == $b["date"]) {
                return 0;
            }
            return ($a["date"] < $b["date"]) ? -1 : 1;
        });
        return [$periods, $metricPosts];
    }
    public function meta_box(){
        add_meta_box( 'user-stats-widget-settings', __( 'WP Metrics Master Widget', 'wp-metrics-master' ), [$this, 'wp_metrics_master_admin_settings'], 'wpmm_widgets' );
        add_meta_box( 'user-stats-widget-entries', __( 'WP Metrics Master Widget (Entries)', 'wp-metrics-master' ), [$this, 'wp_metrics_master_admin_entries'], 'wpmm_widgets' );
        add_meta_box( 'user-stats-metric-settings', __( 'Metric Settings', 'wp-metrics-master' ), [$this, 'wp_metrics_master_metric_settings'], 'wpmm_metrics' );
        add_meta_box( 'user-stats-usermeta', __( 'User Meta', 'wp-metrics-master' ), [$this, 'wp_metrics_master_usermeta'], 'wpmm_usermeta' );
        add_meta_box( 'user-stats-kreport-settings', __( 'Embedded Reports Settings', 'wp-metrics-master' ), [$this, 'wp_metrics_master_kreport_settings'], 'wpmm_kreport' );
        add_meta_box( 'user-stats-webhook', __( 'Webhook Settings', 'wp-metrics-master' ), [$this, 'wp_metrics_master_webhook_settings'], 'wpmm_webhook' );
    }
    public function get_primary_color(){
        $primary_color = get_option('wp_metrics_master_primary_color', '#FF5455');
        $primary_color = str_replace('#', '', $primary_color);
        if(strlen($primary_color)!=6){
            $primary_color = '#FF5455';
        }
        return $primary_color;
    }
    public function wp_metrics_master_admin_settings(){
        if(!isset($_GET["post"])){
            echo 'Publish this post to view the widget editor.';
            return;
        }
        $id = rand();
        $primary_color = $this->get_primary_color();
        echo '<iframe src="'.home_url("/").'wp-content/plugins/wp-metrics-master/views/?is_authenticated='.$this->is_authenticated().'&primary_color='.$primary_color.'&url='.urlencode(home_url("/")).'&v='.$id.(isset($_GET["post"])?"&post=".$_GET["post"]:"").'" style="
    width: 100%;
    height: 500px;
    border: 0;
    overflow: auto;
" id="v'.$id.'" class="wp-metrics-master-iframe"></iframe>';
    }
    public function wp_metrics_master_admin_entries($post_id){
        if(!isset($_GET["post"])){
            echo 'Publish this post to view the user entries.';
            return;
        }
        $id = rand();
        $primary_color = $this->get_primary_color();
        echo '<div class="user-select" style="margin-bottom: 20px;">
            <select>
                <option value="">Select User</option>
            </select>
        </div><iframe src="'.home_url("/").'wp-content/plugins/wp-metrics-master/views/?is_authenticated='.$this->is_authenticated().'&primary_color='.$primary_color.'&url='.urlencode(home_url("/")).'&v='.$id.'&post='.$_GET["post"].'&public=true&user_id=" style="
    width: 100%;
    height: 500px;
    border: 0;
    overflow: auto;
" class="wp-metrics-masterEntry" id="v'.$id.'" class="wp-metrics-master-iframe"></iframe>';
    }
    public function wp_metrics_master_metric_settings(){
        $type = 0;
        $decimalDigits = 2;
        $dropdownOptions = "";
        $hasGoal = 0;
        $goalAlert = 0;
        $editable = 0;
        $description = "";
        $hasCustomGoal = 0;
        $goalTitle = "";
        $goalDescription = "";
        $goalType = 0;
        if(isset($_GET["post"])){
            $post = $_GET["post"];
            $type = get_post_meta($post, "type", true);
            $decimalDigits = get_post_meta($post, "decimalDigits", true);
            if($decimalDigits === ''){
                $decimalDigits = 2;
            }
            $dropdownOptions = get_post_meta($post, "dropdownOptions", true);
            $hasGoal = get_post_meta($post, "hasGoal", true);
            $goalAlert = get_post_meta($post, "goalAlert", true);
            $editable = get_post_meta($post, "editable", true);
            $description = get_post_meta($post, "description", true);
            $hasCustomGoal = get_post_meta($post, "hasCustomGoal", true);
            $goalTitle = get_post_meta($post, "goalTitle", true);
            $goalDescription = get_post_meta($post, "goalDescription", true);
            $goalType = get_post_meta($post, "goalType", true);
        }
        ?>
        <div class="form-field">
            <label>Metric Type</label>
            <div class="field">
                <select name="metric_type">
                    <?php
                    foreach($this->field_types as $key => $value){
                        ?>
                        <option value="<?php echo $key?>"<?php echo $type==$key?' selected':''?>><?php echo $value;?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-field decimal-digits-hidden">
            <label>Decimal Digits</label>
            <div class="field">
                <select name="metric_decimalDigits">
                    <?php
                    for($i = 0; $i <= 2; $i++){
                        ?>
                        <option value="<?php echo $i?>"<?php echo $decimalDigits==$i?' selected':''?>>###<?php echo $i == 0 ? '' : ($i == 1 ? '.0' : '.00');?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-field list-option-hidden">
            <label>Dropdown Options</label>
            <div class="field">
                <textarea name="metric_dropdownOptions" style="height: 120px;"><?php echo $dropdownOptions?></textarea>
                <br><small>Each in new Line</small>
            </div>
        </div>
        <div class="form-field">
            <label>Goal</label>
            <div class="field">
                <select name="metric_hasGoal">
                    <?php
                    foreach(['No', 'Yes'] as $key => $value){
                        ?>
                        <option value="<?php echo $key?>"<?php echo $hasGoal==$key?' selected':''?>><?php echo $value;?></option>
                        <?php
                    }
                    ?>
                </select>
                <br><small>Select yes if this metric has a goal as well as an actual that will be reported.</small>
            </div>
        </div>
        <div class="form-field">
            <label>Goal Alert</label>
            <div class="field">
                <select name="metric_goalAlert">
                    <?php
                    foreach(['On Increase', 'On Decrease'] as $key => $value){
                        ?>
                        <option value="<?php echo $key?>"<?php echo $goalAlert==$key?' selected':''?>><?php echo $value;?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-field">
            <label>Custom Goal</label>
            <div class="field">
                <select name="metric_hasCustomGoal">
                    <?php
                    foreach(['No', 'Yes'] as $key => $value){
                        ?>
                        <option value="<?php echo $key?>"<?php echo $hasCustomGoal==$key?' selected':''?>><?php echo $value;?></option>
                        <?php
                    }
                    ?>
                </select>
                <br><small>Select yes if this metric has a custom goal.</small>
            </div>
        </div>
        <div class="form-field goal-hidden">
            <label>Goal Title</label>
            <div class="field">
                <textarea name="metric_goalTitle"><?php echo $goalTitle?></textarea>
            </div>
        </div>
        <div class="form-field goal-hidden">
            <label>Goal Type</label>
            <div class="field">
                <select name="metric_goalType">
                    <?php
                    foreach($this->field_types as $key => $value){
                        ?>
                        <option value="<?php echo $key?>"<?php echo $goalType==$key?' selected':''?>><?php echo $value;?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-field goal-hidden">
            <label>Goal Description</label>
            <div class="field">
                <textarea name="metric_goalDescription"><?php echo $goalDescription?></textarea>
            </div>
        </div>
        <div class="form-field">
            <label>User Defined Title</label>
            <div class="field">
                <select name="metric_editable">
                    <?php
                    foreach(['No', 'Yes'] as $key => $value){
                        ?>
                        <option value="<?php echo $key?>"<?php echo $editable==$key?' selected':''?>><?php echo $value;?></option>
                        <?php
                    }
                    ?>
                </select>
                <br><small>Check this box if this metric will have a name that the user submitting the data will define. Eg. Leads may be called 'Phone Calls' in their scenario.</small>
            </div>
        </div>
        <div class="form-field">
            <label>Description</label>
            <div class="field">
                <textarea name="metric_description"><?php echo $description?></textarea>
            </div>
        </div>
        <?php
    }
    public function wp_metrics_master_usermeta(){
        $type = 0;
        $options = "";
        $viewOnly = 0;
        $linkedField = "";
        if(isset($_GET["post"])){
            $post = $_GET["post"];
            $type = get_post_meta($post, "type", true);
            $options = get_post_meta($post, "options", true);
            $viewOnly = get_post_meta($post, "viewOnly", true);
            $linkedField = get_post_meta($post, "linkedField", true);
        }
        ?>
        <div class="form-field">
            <label>Type</label>
            <div class="field">
                <select name="usermeta_type">
                    <?php
                    foreach(['text', 'select', 'currency'] as $key => $value){
                        ?>
                        <option value="<?php echo $key?>"<?php echo $type==$key?' selected':''?>><?php echo $value;?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-field">
            <label>Default Value / Options</label>
            <div class="field">
                <textarea name="usermeta_options" rows="5"><?php echo $options?></textarea>
            </div>
        </div>
        <div class="form-field">
            <label>View Only?</label>
            <div class="field">
                <select name="usermeta_viewOnly">
                    <?php
                    foreach(['No', 'Yes'] as $key => $value){
                        ?>
                        <option value="<?php echo $key?>"<?php echo $viewOnly==$key?' selected':''?>><?php echo $value;?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-field">
            <label>Linked Field (Optional)</label>
            <?php
            $user = wp_get_current_user();
            $fields = array_keys((array)$user->data);
            ?>
            <div class="field">
                <select name="usermeta_linkedField">
                    <option value=""<?php echo $linkedField==''?' selected':''?>>Select Field</option>
                    <?php
                    foreach($fields as $key){
                        ?>
                        <option value="<?php echo $key?>"<?php echo $linkedField==$key?' selected':''?>><?php echo ucwords(str_replace("_", " ", $key));?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <?php
    }
    public function wp_metrics_master_kreport_settings(){
        $width = 500;
        $theme = "light";
        $borderStyle = "round";
        $borderColor = '#cccccc';
        $productName = 'Vital Stats';
        $actionText = 'Join the Data Driven!';
        $actionLink = 'https://vitalstats.live';
        if(isset($_GET["post"])){
            $post = $_GET["post"];
            $width = get_post_meta($post, "width", true);
            $theme = get_post_meta($post, "theme", true);
            $borderStyle = get_post_meta($post, "borderStyle", true);
            $borderColor = get_post_meta($post, "borderColor", true);
            $productName = get_post_meta($post, "productName", true);
            $actionText = get_post_meta($post, "actionText", true);
            $actionLink = get_post_meta($post, "actionLink", true);
        }
        ?>
        <div class="form-field goal-hidden">
            <label>Width</label>
            <div class="field">
                <input type="text" name="kreport_width" value="<?php echo $width?>" />
            </div>
        </div>
        <div class="form-field">
            <label>Theme</label>
            <div class="field">
                <select name="kreport_theme">
                    <?php
                    foreach(['light', 'dark'] as $key => $value){
                        ?>
                        <option value="<?php echo $key?>"<?php echo $theme==$key?' selected':''?>><?php echo $value;?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-field goal-hidden">
            <label>Border Style</label>
            <div class="field">
                <input type="text" name="kreport_borderStyle" value="<?php echo $borderStyle?>" />
            </div>
        </div>
        <div class="form-field goal-hidden">
            <label>Border Color</label>
            <div class="field">
                <input type="text" name="kreport_borderColor" value="<?php echo $borderColor?>" />
            </div>
        </div>
        <div class="form-field goal-hidden">
            <label>Product Name</label>
            <div class="field">
                <input type="text" name="kreport_productName" value="<?php echo $productName?>" />
            </div>
        </div>
        <div class="form-field goal-hidden">
            <label>Action Text</label>
            <div class="field">
                <input type="text" name="kreport_actionText" value="<?php echo $actionText?>" />
            </div>
        </div>
        <div class="form-field goal-hidden">
            <label>Action Link</label>
            <div class="field">
                <input type="text" name="kreport_actionLink" value="<?php echo $actionLink?>" />
            </div>
        </div>
        <?php
    }
    public function wp_metrics_master_webhook_settings(){
        $widgets = [];
        $send_on_usermeta = '';
        $send_on_widget = '1';
        if(isset($_GET["post"])){
            $post = $_GET["post"];
            $widgets = get_post_meta($post, "widgets", true);
            $send_on_usermeta = get_post_meta($post, "send_on_usermeta", true);
            $send_on_widget = get_post_meta($post, "send_on_widget", true);
            //var_dump($widgets); die;
        }
        ?>
        <div class="form-field goal-hidden">
            <label>Call this webhook on user meta save</label>
            <div class="field">
                <select name="wpmm_webhook_send_on_usermeta">
                    <option value="0" <?php echo !$send_on_usermeta ? 'selected':''?>>No</option>
                    <option value="1" <?php echo $send_on_usermeta ? 'selected':''?>>Yes</option>
                </select>
            </div>
        </div>
        <div class="form-field goal-hidden">
            <label>Call this webhook on widget save</label>
            <div class="field">
                <select name="wpmm_webhook_send_on_widget">
                    <option value="1" <?php echo $send_on_widget ? 'selected':''?>>Yes</option>
                    <option value="0" <?php echo !$send_on_widget ? 'selected':''?>>No</option>
                </select>
            </div>
        </div>
        <div class="form-field goal-hidden">
            <label>Widgets</label>
            <small>Leave empty to run this webhook on all the widgets.</small>
            <div class="field wpmm-metrics-select">
                <select style="width: 100%" name="wpmm_webhook_widgets[]" multiple data-placeholder="Select widgets">
                    <?php
                    foreach($widgets as $widget){
                        $post = get_post($widget);
                        if($post){
                            ?>
                            <option value="<?php echo $widget?>" selected><?php echo $post->post_title?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <?php
    }
    public function save_post($post_id){
        if(!current_user_can('manage_options')){
            return;
        }
        $post = get_post($post_id);
        if($post->post_type == 'wpmm_metrics'){
            foreach(['type', 'decimalDigits', 'dropdownOptions', 'hasGoal', 'goalAlert', 'editable', 'description', 'hasCustomGoal', 'goalTitle', 'goalDescription', 'goalType'] as $field){
                if(isset($_POST['metric_'.$field])){
                    update_post_meta($post_id, $field, $_POST['metric_'.$field]);
                }
            }
        }
        if($post->post_type == 'wpmm_usermeta'){
            foreach(['type', 'options', 'viewOnly', 'linkedField'] as $field){
                if(isset($_POST['usermeta_'.$field])){
                    update_post_meta($post_id, $field, $_POST['usermeta_'.$field]);
                }
            }
        }
        if($post->post_type == 'wpmm_kreport'){
            foreach(['width', 'theme', 'borderStyle', 'borderColor', 'productName', 'actionText', 'actionLink'] as $field){
                if(isset($_POST['kreport_'.$field])){
                    update_post_meta($post_id, $field, $_POST['kreport_'.$field]);
                }
            }
        }
        if($post->post_type == 'wpmm_webhook'){
            if(isset($_POST['wpmm_webhook_widgets'])){
                update_post_meta($post_id, 'widgets', $_POST['wpmm_webhook_widgets']);
            }
            else{
                update_post_meta($post_id, 'widgets', []);
            }
            if(isset($_POST['wpmm_webhook_send_on_widget'])){
                update_post_meta($post_id, 'send_on_widget', $_POST['wpmm_webhook_send_on_widget']);
            }
            if(isset($_POST['wpmm_webhook_send_on_usermeta'])){
                update_post_meta($post_id, 'send_on_usermeta', $_POST['wpmm_webhook_send_on_usermeta']);
            }
        }
    }
    public function menu(){
        add_menu_page('WP Metrics Master', 'WP Metrics Master', 'manage_options', 'wp_metrics_master_settings', [$this, 'settings']);
        add_submenu_page('wp_metrics_master_settings', 'Import/Export', 'Tools', 'manage_options', 'wp_metrics_master_tools', [$this, 'import_export']);
        add_submenu_page('wp_metrics_master_settings', 'Single Sign-On', 'Single Sign-On', 'manage_options', 'wp_metrics_master_sso', [$this, 'sso']);
        add_submenu_page('wp_metrics_master_settings', 'Settings', 'Settings', 'manage_options', 'wp_metrics_master_settings', [$this, 'settings']);
    }
    public function settings(){
        ?>
        <div>
            <h2>WP Metrics Master Settings</h2>
            <form method="post" action="options.php">
                <h3>Plugin Credentials</h3>
                Provide Plugin Credentials below:
                <?php
                $license_key=get_option('wp_metrics_master_license_key', "");
                $vitalstats_company_id=get_option('wp_metrics_master_vitalstats_company_id', "");
                $vitalstats_sso_secret=get_option('wp_metrics_master_vitalstats_sso_secret', "");
                $vitalstats_url=get_option('wp_metrics_master_vitalstats_url', "https://my.vitalstats.live/dashboard");
                $primary_color=get_option('wp_metrics_master_primary_color', "#FF5455");
                $goals_required=get_option('wp_metrics_master_goals_required', "0");
                $actuals_required=get_option('wp_metrics_master_actuals_required', "0");
                ?>
                <table class="form-table wpmm">
                    <tr>
                        <th scope="row">License Key</th>
                        <td><input type="text" name="wp_metrics_master_license_key" id="wp_metrics_master_license_key" value="<?php echo $license_key?>" /></td>
                    </tr>
                    <tr>
                        <th scope="row">Primary Color</th>
                        <td><input type="text" name="wp_metrics_master_primary_color" id="wp_metrics_master_primary_color" value="<?php echo $primary_color?>" /></td>
                    </tr>
                    <tr>
                        <th scope="row">VitalStats Company ID</th>
                        <td><input type="text" name="wp_metrics_master_vitalstats_company_id" id="wp_metrics_master_vitalstats_company_id" value="<?php echo $vitalstats_company_id?>" /></td>
                    </tr>
                    <tr>
                        <th scope="row">VitalStats SSO Secret</th>
                        <td><input type="text" name="wp_metrics_master_vitalstats_sso_secret" id="wp_metrics_master_vitalstats_sso_secret" value="<?php echo $vitalstats_sso_secret?>" /></td>
                    </tr>
                    <tr>
                        <th scope="row">VitalStats URL</th>
                        <td><input type="text" name="wp_metrics_master_vitalstats_url" id="wp_metrics_master_vitalstats_url" value="<?php echo $vitalstats_url?>" /></td>
                    </tr>
                    <tr>
                        <th scope="row">Goals required before actuals can be entered</th>
                        <td><select name="wp_metrics_master_goals_required" id="wp_metrics_master_goals_required">
                                <option value="0"<?php echo !$goals_required ? ' selected':''?>>No</option>
                                <option value="1"<?php echo $goals_required ? ' selected':''?>>Yes</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Actuals required before saving</th>
                        <td><select name="wp_metrics_master_actuals_required" id="wp_metrics_master_actuals_required">
                                <option value="0"<?php echo !$actuals_required ? ' selected':''?>>No</option>
                                <option value="1"<?php echo $actuals_required ? ' selected':''?>>Yes</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <?php
                $message=get_option("wp_metrics_master_message", "");
                if($message!=""){
                    echo $message;
                }
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }
    public function import_export(){
        if(isset($_POST["csvImport"])){
            if(isset($_FILES["csvFile"]) && !empty($_FILES["csvFile"]["tmp_name"]) && $_FILES["csvFile"]["type"]=='text/csv'){
                if (($handle = fopen($_FILES["csvFile"]["tmp_name"], "r")) !== FALSE) {
                    if(($headers = fgetcsv($handle)) !== FALSE){
                        list($periods, $metrics) = $this->getPeriodMetrics();
                        $usermeta = get_posts([
                            'post_type' => 'wpmm_usermeta',
                            'posts_per_page' => -1,
                            'orderby' => 'title',
                            'order' => 'asc'
                        ]);
                        $i = 0;
                        while (($row = fgetcsv($handle)) !== FALSE) {
                            $i++;
                            $user = [];
                            $j = 0;
                            foreach($row as $col){
                                $user[$headers[$j++]] = $col;
                            }
                            if(isset($user["User ID"]) && !empty($user["User ID"])) {
                                $user["user"] = get_user_by('ID', $user["User ID"]);
                            }
                            if(!isset($user["user"])){
                                if(isset($user["User Email"]) && !empty($user["User Email"])) {
                                    $user["user"] = get_user_by('email', $user["User Email"]);
                                }
                            }
                            if($user["user"]){
                                foreach($usermeta as $post){
                                    $linkedField = get_post_meta($post->ID, 'linkedField', true);
                                    if($linkedField == "" && isset($user[$post->post_title])){
                                        update_user_meta($user["user"]->ID, 'usermeta_'.$post->ID, $user[$post->post_title]);
                                    }
                                }
                                foreach ($periods as $period) {
                                    $dateStr = " (".date("Y/m/d", strtotime($period["date"])).") ".($period["type"]==0?'Monthly':'Weekly');
                                    foreach ($metrics as $metric) {
                                        if($metric["editable"] && isset($user[$metric["title"]." - Label"])){
                                            update_user_meta($user["user"]->ID, '_metric_m'.$metric["id"]."_title", $user[$metric["title"]." - Label"]);
                                        }
                                        if(isset($user[$metric["title"]." - Actual".$dateStr])) {
                                            update_user_meta($user["user"]->ID, '_metric_m' . $metric["id"] . "_t" . $period["type"] . '_p' . $period["date"] . "_actual", $user[$metric["title"]." - Actual".$dateStr]);
                                        }
                                        if($metric["hasGoal"] && isset($user[$metric["title"]." - Goal".$dateStr])){
                                           update_user_meta($user["user"]->ID, '_metric_m'.$metric["id"]."_t".$period["type"].'_p'.$period["date"]."_goal", $user[$metric["title"]." - Goal".$dateStr]);
                                        }
                                    }
                                }
                                $this->printMessage("User #".$user["user"]->ID.' ('.$user["User Display Name"].') has been updated.', 'updated');
                            }
                            else{
                                $this->printMessage("User ID/Email is missing/not found in Row #".$i);
                            }
                        }
                    }
                    fclose($handle);
                }
            }
            else{
                $this->printMessage('Upload valid CSV File.');
            }
        }
        else{
            ?>
            <form method="post" enctype="multipart/form-data">
                <div class="wrap">
                    <h3>Import/Export Tool</h3>
                    <div class="form-container">
                        <div class="form-field">
                            <label>Users Feed</label>
                            <div class="field">
                                <a href="<?php echo home_url('/')?>wp-json/wp-metrics-master/v1/users" name="save" id="publish" class="" target="_blank"><?php echo home_url('/')?>wp-json/wp-metrics-master/v1/users</a>
                            </div>
                        </div>
                        <div class="form-field">
                            <label>Export CSV (Specimen)</label>
                            <div class="field">
                                <a href="<?php echo admin_url('edit.php?post_type=wpmm_widgets&page=wp_metrics_master_tools&export_specimen=true&type=monthly')?>" name="save" id="publish" class="button button-primary button-large">Export (Monthly)</a>
                                <a href="<?php echo admin_url('edit.php?post_type=wpmm_widgets&page=wp_metrics_master_tools&export_specimen=true&type=weekly')?>" name="save" id="publish" class="button button-primary button-large">Export (Weekly)</a>
                            </div>
                        </div>
                        <div class="form-field">
                            <label>Import CSV</label>
                            <div class="field">
                                <input type="file" name="csvFile" />
                            </div>
                        </div>
                        <div class="form-field">
                            <div class="field">
                                <button name="csvImport" id="publish" class="button button-primary button-large">Import</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php
        }
    }

    public function sso(){
        // check user capabilities
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }

        $sso_ac = get_option('wp_metrics_master_sso_ac', 0);
        $sso_ac_api_url = get_option('wp_metrics_master_sso_ac_api_url', '');
        $sso_ac_api_key = get_option('wp_metrics_master_sso_ac_api_key', '');
        $sso_ac_field = get_option('wp_metrics_master_sso_ac_field', '');

        $sso_op = get_option('wp_metrics_master_sso_op', 0);
        $sso_op_app_id = get_option('wp_metrics_master_sso_op_app_id', '');
        $sso_op_api_key = get_option('wp_metrics_master_sso_op_api_key', '');
        $sso_op_object = get_option('wp_metrics_master_sso_op_object', 'Contact');
        $sso_op_field = get_option('wp_metrics_master_sso_op_field', '');

        //Get the active tab from the $_GET param
        $default_tab = null;
        $tab = isset($_GET['tab']) ? $_GET['tab'] : $default_tab;

        ?>
        <!-- Our admin page content should all be inside .wrap -->
        <div class="wrap">
            <!-- Print the page title -->
            <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
            <!-- Here are our tabs -->
            <nav class="nav-tab-wrapper">
                <a href="?page=wp_metrics_master_sso" class="nav-tab <?php if($tab===null):?>nav-tab-active<?php endif; ?>">Ontraport</a>
                <a href="?page=wp_metrics_master_sso&tab=ac" class="nav-tab <?php if($tab==='ac'):?>nav-tab-active<?php endif; ?>">Active Campaign</a>
            </nav>

            <div class="tab-content">
                <?php switch($tab) :
                case 'ac':
                    ?>
                    <form method="post" enctype="multipart/form-data">
                        <div class="wrap">
                            <h3>Active Campaign</h3>
                            <table class="form-table wpmm">
                                <tr>
                                    <th scope="row">Enable SSO for Active Campaign</th>
                                    <td><select name="wp_metrics_master_sso_ac" id="wp_metrics_master_sso_ac">
                                            <option value="0"<?php echo !$sso_ac ? ' selected':''?>>No</option>
                                            <option value="1"<?php echo $sso_ac ? ' selected':''?>>Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="wp_metrics_master_sso_ac_enabled">
                                    <th scope="row">API URL</th>
                                    <td><input type="text" name="wp_metrics_master_sso_ac_api_url" id="wp_metrics_master_sso_ac_api_url" value="<?php echo $sso_ac_api_url?>" /></td>
                                </tr>
                                <tr class="wp_metrics_master_sso_ac_enabled">
                                    <th scope="row">API Key</th>
                                    <td><input type="text" name="wp_metrics_master_sso_ac_api_key" id="wp_metrics_master_sso_ac_api_key" value="<?php echo $sso_ac_api_key?>" /></td>
                                </tr>
                                <tr class="wp_metrics_master_sso_ac_enabled">
                                    <th scope="row">Field Key</th>
                                    <td><input type="text" name="wp_metrics_master_sso_ac_field" id="wp_metrics_master_sso_ac_field" value="<?php echo $sso_ac_field?>" /></td>
                                </tr>
                            </table>
                        </div>
                        <?php
                        $message=get_option("wp_metrics_master_sso_ac_message", "");
                        if($message!=""){
                            echo $message;
                        }
                        submit_button();
                        ?>
                    </form>
                    <?php
                    break;
                default:
                    ?>
                    <form method="post" enctype="multipart/form-data">
                        <div class="wrap">
                            <h3>Ontraport</h3>
                            <table class="form-table wpmm">
                                <tr>
                                    <th scope="row">Enable SSO for Ontraport</th>
                                    <td><select name="wp_metrics_master_sso_op" id="wp_metrics_master_sso_op">
                                            <option value="0"<?php echo !$sso_op ? ' selected':''?>>No</option>
                                            <option value="1"<?php echo $sso_op ? ' selected':''?>>Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="wp_metrics_master_sso_op_enabled">
                                    <th scope="row">APP ID</th>
                                    <td><input type="text" name="wp_metrics_master_sso_op_app_id" id="wp_metrics_master_sso_op_app_id" value="<?php echo $sso_op_app_id?>" /></td>
                                </tr>
                                <tr class="wp_metrics_master_sso_op_enabled">
                                    <th scope="row">API Key</th>
                                    <td><input type="text" name="wp_metrics_master_sso_op_api_key" id="wp_metrics_master_sso_op_api_key" value="<?php echo $sso_op_api_key?>" /></td>
                                </tr>
                                <tr class="wp_metrics_master_sso_op_enabled">
                                    <th scope="row">Object Name</th>
                                    <td><input type="text" name="wp_metrics_master_sso_op_object" id="wp_metrics_master_sso_op_object" value="<?php echo $sso_op_object?>" /></td>
                                </tr>
                                <tr class="wp_metrics_master_sso_op_enabled">
                                    <th scope="row">Field Key</th>
                                    <td><input type="text" name="wp_metrics_master_sso_op_field" id="wp_metrics_master_sso_op_field" value="<?php echo $sso_op_field?>" /></td>
                                </tr>
                            </table>
                        </div>
                        <?php
                        $message=get_option("wp_metrics_master_sso_op_message", "");
                        if($message!=""){
                            echo $message;
                        }
                        submit_button();
                        ?>
                    </form>
                    <?php
                    break;
                endswitch; ?>
            </div>
        </div>
        <?php
    }
    public function printMessage($text, $type='error'){
        ?>
        <div class="<?php echo $type?>">
            <p><?php echo $text?></p>
        </div>
        <?php
    }
    public function wp_enqueue_scripts(){
        wp_enqueue_script('jquery');
    }
    public function js(){
        ?>
        <script>
            (function($) {
                window.addEventListener("message", function (data) {
                    if (data.data.iframeHeight) {
                        $('#' + data.data.iframeId).height(data.data.iframeHeight);
                    }
                }, false);
                $(window).on('scroll', function(){
                    $(".wp-metrics-master-iframe").each(function(){
                        document.getElementById($(this).attr("id")).contentWindow.postMessage({
                            windowHeight: $(window).height(),
                            iframePosition: $(this).offset().top,
                            scrollTop: $(window).scrollTop()
                        });
                    });
                });
            })(jQuery);
            function wpmmSsoLogin(){
                jQuery.post('<?php echo home_url('/')."?wpmm_ajax=1&action=checkLogin"?>', {}, function(response){
                    response = JSON.parse(response);
                    if(response.status && response.kfSso){
                        jQuery.ajax({
                            url: 'https://my.vitalstats.live/users/sso_auth',
                            method: 'post',
                            headers: {
                                'Content-Type': 'application/json',
                                'KF-SSO': response.kfSso,
                                'KF-Company': response.kfCompany,
                            },
                            dataType: 'json',
                            xhrFields: {
                                withCredentials: true
                            },
                            success: function(){
                                window.open(response.ssoRedirect, '_blank');
                            },
                            error: function(response){
                                alert('Try again!');
                            }
                        });
                    }
                    else{
                        if(response.message){
                            let message = response.message
                        }
                        else{
                            let message = 'Error login to sso. Refresh the page and then try again.';
                        }
                        $("body").append('<div id="wpmm-sso-error" style="' +
                            'position: fixed;' +
                            'bottom: 0;' +
                            'width: 100%;' +
                            'background: red;' +
                            'color: #fff;' +
                            'padding: 10px;' +
                            'font-size: 12px;' +
                            'z-index: 99999999;' +
                            '">'+message+'</div>'
                        );
                        setTimeout(function(){
                            $("#wpmm-sso-error").fadeOut();
                        }, 5000);
                    }
                });
            }
        </script>
        <?php
    }
    public function admin_footer(){
        ?>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script>
            (function($){
                $('.user-select select').select2({
                    ajax: {
                        url: ajaxurl,
                        type: 'post',
                        data: function (params) {
                            return {
                                search: params.term,
                                action: 'wp_metrics_master_get_users'
                            }
                        },
                        dataType: 'json'
                    }
                });
                $('.user-select select').on('change', function(){
                    let src = $(".wp-metrics-masterEntry").attr("src").split("user_id=");
                    src = src[0]+"user_id="+$(this).val();
                    $(".wp-metrics-masterEntry").attr("src", src);
                });
                $('.wpmm-metrics-select select').select2({
                    ajax: {
                        url: ajaxurl,
                        type: 'post',
                        data: function (params) {
                            return {
                                search: params.term,
                                action: 'wp_metrics_master_get_widgets'
                            }
                        },
                        dataType: 'json'
                    }
                });
                $("select[name='metric_hasCustomGoal']").change(function(){
                    if($(this).val() == "1"){
                        $(".goal-hidden").show();
                    }
                    else{
                        $(".goal-hidden").hide();
                    }
                }).change();
                $("select[name='metric_type']").change(function(){
                    if($(this).val() == "5"){
                        $(".list-option-hidden").show();
                    }
                    else{
                        $(".list-option-hidden").hide();
                    }
                    if($(this).val() <= 2){
                        $(".decimal-digits-hidden").show();
                    }
                    else{
                        $(".decimal-digits-hidden").hide();
                    }
                }).change();
                $("#wp_metrics_master_sso_op").change(function(){
                    $val = $(this).val();
                    console.log($val);
                    if($val=="1"){
                        $(".wp_metrics_master_sso_op_enabled").show();
                    }
                    else{
                        $(".wp_metrics_master_sso_op_enabled").hide();
                    }
                }).change();
                $("#wp_metrics_master_sso_ac").change(function(){
                    $val = $(this).val();
                    if($val=="1"){
                        $(".wp_metrics_master_sso_ac_enabled").show();
                    }
                    else{
                        $(".wp_metrics_master_sso_ac_enabled").hide();
                    }
                }).change();
            })(jQuery);
        </script>
        <style>
            .form-field {
                margin-bottom: 10px;
            }

            .form-field label {
                display: block;
                margin-bottom: 5px;
                font-weight: bold;
            }
            .form-table.wpmm input{
                width: 320px;
                max-width: 100%;
            }
        </style>
        <?php
    }
    public function get_users(){
        $users = [];
        foreach(get_users([
            "search" => $_POST["search"],
            "number" => 20
        ]) as $user){
            $users[] = [
                "id" => $user->ID,
                "text" => $user->display_name
            ];
        }
        echo json_encode([
            "results" => $users
        ]);
        die;
    }
    public function get_widgets(){
        $posts = [];
        foreach(get_posts([
            "s" => $_POST["search"],
            "post_type" => 'wpmm_widgets'
        ]) as $post){
            $posts[] = [
                "id" => $post->ID,
                "text" => $post->post_title
            ];
        }
        echo json_encode([
            "results" => $posts
        ]);
        die;
    }
    public function columns($columns){
        unset($columns['date']);
        $columns['shortcode'] = 'Shortcode';
        $columns['feed_url'] = 'Feed URL';
        return $columns;
    }
    public function webhook_columns($columns){
        $columns["title"] = "Webhook URL";
        return $columns;
    }
    public function metrics_columns($columns){
        unset($columns['date']);
        $columns["feed_url_metrics"] = "Feed URL";
        return $columns;
    }
    public function kreport_columns($columns){
        unset($columns['date']);
        $columns['shortcode_kreport'] = 'Shortcode';
        return $columns;
    }
    public function columns_content( $column, $post_id ) {
        switch ( $column ) {
            case 'shortcode':
                echo '[wpmm_widgets id="'.$post_id.'"]';
                break;
            case 'feed_url':
                echo '<a href="'.home_url("/").'wp-json/wp-metrics-master/v1/metrics/'.$post_id.'" target="_blank">'.home_url("/").'wp-json/wp-metrics-master/v1/metrics/'.$post_id.'</a>';
                break;
            case 'shortcode_kreport':
                echo '[wpmm_kreport id="'.$post_id.'"]';
                break;
            case 'feed_url_metrics':
                echo '<a href="'.home_url("/").'wp-json/wp-metrics-master/v1/single-metric/'.$post_id.'" target="_blank">'.home_url("/").'wp-json/wp-metrics-master/v1/single-metric/'.$post_id.'</a>';
                break;
        }
    }
    public function shortcode($args){
        if (isset($args["id"])) {
            $extra = '';
            if(isset($args["compact"])){
                $extra .= '&compact=1';
            }
            if(isset($args["hide_goal"])){
                $extra .= '&hideGoal=1';
            }
            if(isset($args["hide_settings"])){
                $extra .= '&hideSettings=1';
            }
            if(isset($args["hide_vitalstats"])){
                $extra .= '&hideVitalStats=1';
            }
            if(isset($args["transparent"])){
                $extra .= '&transparent=1';
            }
            if(isset($args["goals"]) && $args["goals"] == 'periods'){
                $extra .= '&hasPeriodGoal=1';
            }
            if(isset($args["compact_metrics"])){
                $metrics = [];
                foreach(explode(",", $args["compact_metrics"]) as $metric){
                    $metrics[] = (int)trim($metric);
                }
                $extra .= '&compactMetrics='.urlencode(json_encode($metrics));
            }
            if(isset($args["theme"])){
                $extra .= '&theme='.$args["theme"];
            }
            if(isset($args["allow_current_period"])){
                $extra .= '&allowCurrentPeriod=1';
            }
            if(isset($args["user_setting_on_main_page"])){
                $extra .= '&userSettingOnMainPage='.$args["user_setting_on_main_page"];
            }
            $primary_color = $this->get_primary_color();
            $id = rand();
            return '<iframe src="'.home_url("/").'wp-content/plugins/wp-metrics-master/views/?is_authenticated='.$this->is_authenticated().'&primary_color='.$primary_color.'&v='.$id.'&url='.urlencode(home_url("/")).'&post='.$args["id"].'&public=true'.$extra.'" style="
    width: 100%;
    height: 500px;
    border: 0;
    overflow: auto;
" id="v'.$id.'" class="wp-metrics-master-iframe"></iframe>';
        }
    }
    public function shortcode_kreport($args){
        if (isset($args["id"])) {
            if(is_user_logged_in()){
                $profile = get_user_meta(get_current_user_id(), 'klipfolio_profile', true);
                if(!empty($profile)){
                    $post = get_post($args["id"]);
                    $width = get_post_meta($post->ID, "width", true);
                    $theme = get_post_meta($post->ID, "theme", true);
                    $borderStyle = get_post_meta($post->ID, "borderStyle", true);
                    $borderColor = get_post_meta($post->ID, "borderColor", true);
                    $productName = get_post_meta($post->ID, "productName", true);
                    $actionText = get_post_meta($post->ID, "actionText", true);
                    $actionLink = get_post_meta($post->ID, "actionLink", true);
                    return '<div style="display:inline-block" id="kf-embed-container-'.$profile.'"></div>
                    <script type="text/javascript" src="https://embed.klipfolio.com/a/js/embed.api"></script>
                    <script type="text/javascript">
                        KF.embed.embedKlip({
                            profile : "'.$profile.'",
                            settings : {"width":'.(strpos($width, '%')!==-1?'"'.$width.'"':$width).',"theme":"'.$theme.'","borderStyle":"'.$borderStyle.'","borderColor":"'.$borderColor.'"},
                            title : "'.$post->post_title.'",
                            productName : "'.$productName.'",
                            actionText : "'.$actionText.'",
                            actionLink : "'.$actionLink.'"
                        });
                    </script>';
                }
            }
        }
    }
    public function get_sso_details(){
        $sso = [
            "encryptedData" => "",
            "vitalstats_company_id" => "",
            "vitalstats_url" => ""
        ];
        $vitalstats_company_id=get_option('wp_metrics_master_vitalstats_company_id', "");
        $vitalstats_sso_secret=get_option('wp_metrics_master_vitalstats_sso_secret', "");
        $vitalstats_url=get_option('wp_metrics_master_vitalstats_url', "https://my.vitalstats.com/dashboard");
        if (!empty($vitalstats_company_id) && !empty($vitalstats_sso_secret)){
            $user_id = get_current_user_id();
            $email = get_user_meta($user_id, 'vitalstats_email', true );
            if (!empty($email)){
                $encryptedData = wp_remote_post('https://kfsso.app.itmooti.com/sso.php', [
                    'body' => [
                        "secret" => $vitalstats_sso_secret,
                        "company_id" => $vitalstats_company_id,
                        "email" => $email,
                    ]
                ]);
                if(!is_wp_error($encryptedData)){
                    $encryptedData = $encryptedData["body"];
                    $sso = [
                        "encryptedData" => $encryptedData,
                        "vitalstats_company_id" => $vitalstats_company_id,
                        "vitalstats_url" => $vitalstats_url
                    ];
                }
                else{

                }
            }
        }
        return $sso;
    }
    public function metrics_feed($data){
        global $wpdb;
        $headers = array_change_key_case(getallheaders());
        $license_key = get_option('wp_metrics_master_license_key', "");
        if(!isset($headers["api-key"]) || $headers["api-key"] != $license_key){
            return ['error' => 'Api-Key is required.'];
        }
        if(isset($data["id"])){
            $records = [];
            $post = get_post($data["id"]);
            if($post->post_type == 'wpmm_widgets') {
                $terms = json_decode(get_post_meta($post->ID, '_terms', true));
                $periods = [];
                $periods_unique = [];
                $metrics = [];
                if ($terms && count($terms) > 0){
                    foreach ($terms as $index => $term) {
                        //print_r($term); die;
                        $start = new DateTime(date("Y-m-d", strtotime($term->startDateUTC)));
                        $end = new DateTime(date("Y-m-d", strtotime($term->endDateUTC)));
                        $interval = DateInterval::createFromDateString('1 ' . ($term->period == 0 ? 'month' : 'week'));
                        $period = new DatePeriod($start, $interval, $end);
                        foreach ($period as $dt) {
                            $period = $dt->format("Ymd");
                            if (!in_array($period.$term->period, $periods_unique)) {
                                $periods_unique[] = $period.$term->period;
                                $periods[] = [
                                    "term" => $term->title,
                                    "type" => $term->period,
                                    "date" => $period,
                                ];
                            }
                        }
                        foreach ($term->metrics as $metric) {
                            if (!in_array($metric, $metrics)) {
                                $metrics[] = $metric;
                            }
                        }
                    }
                    $i = 0;
                    $where = [];
                    foreach ($metrics as $metric) {
                        $metrics[$i++] = $this->getMetric(get_post($metric));
                        $where[] = "meta_key like '_metric_m".$metric."_%'";
                    }
                    if(isset($_GET["email"])){
                        $user = get_user_by("email", $_GET["email"]);
                    }
                    $users = $wpdb->get_results("select distinct(user_id) from $wpdb->usermeta where (".implode(' OR ', $where).")".(isset($_GET["email"])?" and user_id = '".$user->ID."'":""));
                    foreach($users as $user){
                        foreach ($periods as $period) {
                            $record = [
                                    "user_id" => $user->user_id,
                                "term" => $period["term"],
                                "date" => $period["date"],
                            ];
                            foreach ($metrics as $metric) {
                                if($metric["editable"]){
                                    $record[$metric["title"]." - Label"] = get_user_meta($user->user_id, '_metric_m'.$metric["id"]."_title", true);
                                }
                                $record[$metric["title"]." - Actual"] = get_user_meta($user->user_id, '_metric_m'.$metric["id"]."_t".$period["type"].'_p'.$period["date"]."_actual", true);
                                if($metric["hasGoal"]){
                                    $record[$metric["title"]." - Goal"] = get_user_meta($user->user_id, '_metric_m'.$metric["id"]."_t".$period["type"].'_p'.$period["date"]."_goal", true);
                                }
                            }
                            $records[] = $record;
                        }
                    }
                }
                return $records;
            }
        }
    }
    public function single_metric_feed($data){
        global $wpdb;
        $headers = array_change_key_case(getallheaders());
        $license_key = get_option('wp_metrics_master_license_key', "");
        if(!isset($headers["api-key"]) || $headers["api-key"] != $license_key){
            return ['error' => 'Api-Key is required.'];
        }
        if(isset($data["id"])){
            $records = [];
            $widgets = get_posts(['post_type' => 'wpmm_widgets', 'posts_per_page' => -1]);
            $periods = [];
            $periods_unique = [];
            foreach($widgets as $post) {
                $terms = json_decode(get_post_meta($post->ID, '_terms', true));
                if ($terms && count($terms) > 0){
                    foreach ($terms as $index => $term) {
                        foreach ($term->metrics as $metric) {
                            if ($metric == $data["id"]) {
                                $start = new DateTime(date("Y-m-d", strtotime($term->startDateUTC)));
                                $end = new DateTime(date("Y-m-d", strtotime($term->endDateUTC)));
                                $interval = DateInterval::createFromDateString('1 ' . ($term->period == 0 ? 'month' : 'week'));
                                $period = new DatePeriod($start, $interval, $end);
                                foreach ($period as $dt) {
                                    $period = $dt->format("Ymd");
                                    if (!in_array($period.$term->period, $periods_unique)) {
                                        $periods_unique[] = $period.$term->period;
                                        $periods[] = [
                                            "term" => $term->title,
                                            "type" => $term->period,
                                            "date" => $period,
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            usort($periods, function($a, $b){
                if ($a["data"] == $b["date"]) {
                    return 0;
                }
                return ($a["date"] < $b["date"]) ? -1 : 1;
            });
            $post = get_post($data["id"]);
            $metrics = [];
            $metrics[] = $this->getMetric(get_post($post->ID));
            $where[] = "meta_key like '_metric_m".$post->ID."_%'";
            if(isset($_GET["email"])){
                $user = get_user_by("email", $_GET["email"]);
            }
            //print_r($periods); die;
            $users = $wpdb->get_results("select distinct(user_id) from $wpdb->usermeta where (".implode(' OR ', $where).")".(isset($_GET["email"])?" and user_id = '".$user->ID."'":""));
            foreach($users as $user){
                foreach ($periods as $period) {
                    $record = [
                        "user_id" => $user->user_id,
                        "term" => $period["term"],
                        "date" => $period["date"],
                    ];
                    foreach ($metrics as $metric) {
                        if($metric["editable"]){
                            $record[$metric["title"]." - Label"] = get_user_meta($user->user_id, '_metric_m'.$metric["id"]."_title", true);
                        }
                        $record[$metric["title"]." - Actual"] = get_user_meta($user->user_id, '_metric_m'.$metric["id"]."_t".$period["type"].'_p'.$period["date"]."_actual", true);
                        if($metric["hasGoal"]){
                            $record[$metric["title"]." - Goal"] = get_user_meta($user->user_id, '_metric_m'.$metric["id"]."_t".$period["type"].'_p'.$period["date"]."_goal", true);
                        }
                    }
                    $records[] = $record;
                }
            }
            return $records;
        }
    }
    public function users_feed($data){
        $headers = array_change_key_case(getallheaders());
        $license_key = get_option('wp_metrics_master_license_key', "");
        if(!isset($headers["api-key"]) || $headers["api-key"] != $license_key){
            return ['error' => 'Api-Key is required.'];
        }
        $records = [];
        $response["usermeta"] = [];
        $args = [
            'post_type' => 'wpmm_usermeta',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'asc'
        ];
        if(isset($data["id"])){
            $users = [get_user_by("ID", $data["id"])];
        }
        else{
            $users = get_users(["fields" => ["ID","user_email","display_name"]]);
        }
        $posts = get_posts($args);
        foreach($users as $user){
            $record = [
                "id" => $user->ID,
                "email" => $user->user_email,
                "name" => $user->display_name
            ];
            foreach($posts as $post){
                $linkedField = get_post_meta($post->ID, 'linkedField', true);
                if($linkedField == ""){
                    $record[$post->post_title] = get_user_meta($user->ID, 'usermeta_'.$post->ID, true);
                }
            }
            $record['date_last_edited'] = date("Y-m-d H:i:s", strtotime(get_user_meta($user->ID, 'usermeta_date_last_edited', true)));
            $records[] = $record;
        }
        return $records;
    }
    public function user_update($data){
        $headers = array_change_key_case(getallheaders());
        $license_key = get_option('wp_metrics_master_license_key', "");
        if(!isset($headers["api-key"]) || $headers["api-key"] != $license_key){
            return ['error' => 'Api-Key is required.'];
        }
        $body = $data->get_body_params();
        $err = '';
        if(isset($body["email"]) && isset($body["vitalstats_email"])){
            $user = get_user_by('email', $body["email"]);
            if($user){
                update_user_meta( $user->ID, 'vitalstats_email', $body["vitalstats_email"] );
                if(isset($body["klipfolio_profile"])){
                    update_user_meta( $user->ID, 'klipfolio_profile', $body["klipfolio_profile"] );
                }
            }
            else{
                $err = 'No user exists with this email: '.$body["email"];
            }
        }
        else{
            $err = 'email and vitalstats_email are required parameters.';
        }
        if($err == ''){
            $response = ['status' => true];
        }
        else{
            $response = ['status' => false, 'message' => $err];
        }
        return $response;
    }
    public function action_row($actions, $post){
        if($post->post_type == 'wpmm_widgets'){
            $actions['duplicate_post'] = '<a href="'.admin_url('edit.php').'?post_type=wpmm_widgets&action=duplicate&post_id='.$post->ID.'&nonce='.wp_create_nonce( 'wpmm_duplicate_nonce' ).'">Duplicate</a>';
        }
        return $actions;
    }
    public function duplicate_post( $original_id, $args=array(), $do_action=true ) {
        global $wpdb;
        $duplicate = get_post( $original_id, 'ARRAY_A' );
        $duplicate['post_title'] = $duplicate['post_title']." - Copy";
        $duplicate['post_name'] = sanitize_title($duplicate['post_name'].'-'.$settings['slug']);
        $duplicate['post_status'] = 'draft';
        $timestamp = current_time('timestamp',0);
        $timestamp_gmt = current_time('timestamp',1);
        $duplicate['post_date'] = date('Y-m-d H:i:s', $timestamp);
        $duplicate['post_date_gmt'] = date('Y-m-d H:i:s', $timestamp_gmt);
        $duplicate['post_modified'] = date('Y-m-d H:i:s', current_time('timestamp',0));
        $duplicate['post_modified_gmt'] = date('Y-m-d H:i:s', current_time('timestamp',1));
        unset( $duplicate['ID'] );
        unset( $duplicate['guid'] );
        unset( $duplicate['comment_count'] );
        $duplicate['post_content'] = str_replace( array( '\r\n', '\r', '\n' ), '<br />', $duplicate['post_content'] ); //Handles guttenburg escaping in returns for blocks
        $duplicate_id = wp_insert_post( $duplicate );
        $custom_fields = get_post_custom( $original_id );
        foreach ( $custom_fields as $key => $value ) {
            if( is_array($value) && count($value) > 0 ) {
                foreach( $value as $i=>$v ) {
                    $result = $wpdb->insert( $wpdb->prefix.'postmeta', array(
                        'post_id' => $duplicate_id,
                        'meta_key' => $key,
                        'meta_value' => $v
                    ));
                }
            }
        }
        return $duplicate_id;
    }
}
$userStats = new WPMetricsMaster();
