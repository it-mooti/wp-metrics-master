﻿=== User Statistics ===
Contributors: ITMOOTI
Donate link: http://www.itmooti.com/
Tags: ontraport, klipfolio, stats, metrics
Requires at least: 4.0
Tested up to: 5.8.1
Stable tag: 1.4.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Send Tailored WordPress Content To Your ONTRAPORT Contacts.

== Description ==

--pending--

== Installation ==

1. Upload `userstats` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Learn More In Our Knowledge Base =

<a href="https://itmooti.helpdocs.com/userstats">User Statistics</a>

== Screenshots ==

== Upgrade Notice ==

Nothing yet.

= 1.0 =

First Release
